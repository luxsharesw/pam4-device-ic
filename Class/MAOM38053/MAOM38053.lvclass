﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="17008000">
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">16769464</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorProtected" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">16448250</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="NI.Lib.ContainingLib" Type="Str">PAM4 Device IC.lvlib</Property>
	<Property Name="NI.Lib.ContainingLibPath" Type="Str">../../../PAM4 Device IC.lvlib</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!,G!!!*Q(C=T:3`&lt;B."%)@H%K21)*"2?C)`!&lt;JJ%:64)&amp;J=7K+;6T!6O&gt;)]QLS#=R7N'Z").WV+#_5"=)MIY0BOP@ZTC2/$"&amp;*W&lt;XRXP^`=\(?\[R.*\;()!WHWN;-\WT+B3=@GF'`;KT;B069^(6OXT@LC;,PH9MWK`EVH]`TK&gt;P@Y?`HX.,GT`;5`+E?DW`V2W3*V-L&lt;]J8MN9_VPX%\'WC]\@HX4(WUKV(6&gt;\P"T2FX@[J-RKO`UMXX``&amp;((4_@O`*@FWO=VFWH&gt;^3O8`J:\@@V4RF'^M7`MH\,D\NR@7_[@\,`^W\P;`S@YPUWE3+2%%%Y9I?HNGE!0^%!0^%!0&gt;%&gt;X&gt;%&gt;X&gt;%&gt;X&gt;%-X&gt;%-X&gt;%-X&gt;%68&gt;%68&gt;%68^.42"6X1":V:#190"AK+"A7#:&amp;!E_!FY!J[!*_$B6A+?A#@A#8A#(F)EY!FY!J[!*_$B-1FY!J[!*_!*?#C63#*V&gt;(A#(MK,Q_0Q/$Q/D]0$E/,Q/!$/9%ZBJQA9YJD/B=0D]$A]8)L$Y`!Y0![0QY-N$I`$Y`!Y0!Y0D[2:]544&gt;H2Y+#-'D]&amp;D]"A]"A_FR?!R?!Q?A]@A94AR?!Q?!])9U#A/ABA0'1H'D=&amp;D](!3A]@A-8A-(I-(+[W1J:FJ;&gt;K/$I`#I`!I0!K0QE-*58A5(I6(Y6&amp;Y+#M+D]+D]#A]#A^$C=+D]#A]#IAS+-/,5ERZ5%F3")7()_U74;PEC524TX`.Z5;6P!%F&lt;ST*'U&lt;S2J#]Q*)84P+#3*ZIS2-I?7)EP\$E&amp;Z%-+(FAS15F*]K#XTER)[&lt;%B"A41W*!^)F?__A`4FQM&amp;D+@TW5WG]FU/J8*:#,D]6C'Q[%-"A0J^`P3[`878[M4_L)6K_^3R@78$]?HXY9@TS]PX\S_'$Y^PXBV@.LK+T`&amp;Q;?C?PC]K%Y?&amp;66VC%;]@6R5\VY7V@N@287W)ZLGKHLW]U@VZ0N6&gt;@DV=S6H00?C7.7_(^^'/:"G[T00(0U'QI,A-1!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.DataValRefToSelfLimitedLibFlag" Type="Bool">true</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!#"W5F.31QU+!!.-6E.$4%*76Q!!'_1!!!33!!!!)!!!']1!!!!L!!!!!B2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=Q!!!!#A&amp;Q#!!!!Q!!!I!!!!!!!!"!!$!$Q!P!!@1)!#!!!!!!%!!1!'`````Q!!!!!!!!!!!!!!!.,^W2DP#P^)MC:R72J@[51!!!!-!!!!%!!!!!$_(_)^8..H4IH8I]N2GXS'V"W-W9]!MA4JA!G9\0B#@A!!%!!!!!!!$5`&gt;,E?,,E#Q)K2OW"7\Q1%!!!$`````V"W-W9]!MA4JA!G9\0B#@A!!!"#PS[1]+VF-A?2M.SSM&amp;B&gt;!!!!!"!!!!!!!!!$U!!&amp;-6E.$!!!!!A!#6EF-1A!!!!"16%AQ!!!!"1!"!!%!!!!!!A!#6EF131!!!!!$&amp;E^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9H!84X"U;7.B&lt;#"1=G^E&gt;7.U,GRW9WRB=X-#"Q"16%AQ!!!!&lt;1!"!!Y!!!!!!!!!!U6Y:1R-&gt;8BT;'&amp;S:3V0261*4'FC=G&amp;S;76T"V"M&gt;7&gt;J&lt;H-74X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC=!^0=(2J9W&amp;M)&amp;"S&lt;W2V9X184X"U;7.B&lt;#"1=G^E&gt;7.U,GRW9WRB=X-!!!!!!!!"!!!!!!!!!!%!!!!!!A!!!!!!!!!!!Q!!!!)!!1!!!!!!)Q!!!"RYH'.A:W"K9,D!!-3-1"98E-7E!/2^9""A!!"6\A9J!!!!!"1!!!!/?*RD9'8A:/#!1A9!!8)!,Q!!!']!!!&amp;??*RD9-!%`Y%!3$%S-$$&gt;!^+M;/*A'M;G*M"F,C[\I/,-1-Q#&gt;3?DA:B`15FG=G+/1E"2@EJJ=IF?4FF/:F+"/+:Q=EZC=4''I3%QQY'_XQ/EG5#G1WXCAEAR81(C%_A;_;(U!S1R!/(I/V1!!!!!$!!"6EF%5Q!!!!!!!Q!!!==!!!0M?*T&lt;Q-D!E'FM9=&lt;!R-$!$'3,-T1Q*/?HJ0)S!0E-%+!$9V!!!K$G;;'*'RYYH!9%?PTS,7"_]RO?&lt;B=6A?9;&amp;1GG5J&amp;O(R724B]6FEY7F2&gt;``P``XXS%ZX#X2]ZR2RO1WGY/I0BR&amp;R5/%!&gt;)MY$I`Y%:)&amp;7IZMFU!G7"N!33"LC"+09(!&amp;6R.&amp;1I-Z3Q'"[)/HS]Q912YF#9%[+QO:&gt;Y]ZP@=!!^*8$Q)5NXIQ;1XTM22!+&amp;?$J$/#3/OX$IC!(ZD#&gt;!"H&lt;SQ(T.!@&gt;0'-C!%B7"4B/123S--)O[W9Y\;)$$Q5%%1G6!K!I)61#C&gt;I"&gt;=)1D\D!]`.?_PL?,&amp;5CT)=7*!R!XA"B-K&amp;C0A:'"%=RE:&amp;A,67M$:$."R7"R#W)L1)/.E=%?LO=W6&amp;Y$S2Q82JA?B,JK*(=QA=VA:0D$!$-0;"^54Q05X3!R8[$9!3A\"-C?!'6(!^E@I/QE)&amp;M!SMY%MAU9)?Q]+"NM'1.OWNH@R25JG-$Z!J9V@)%Y/&lt;@!Q%#PWFEHUU@(3;@5K&gt;;[WNH'P[!E-TER2S'A+$_F.,F%,[=M*T/JQ!J4/$EHM&lt;D9$KY8;*Y0S-S#Z$*K'1E!/:+`[1!!!!%&lt;!!!"E(C==W"A9-AUND$\!+3:'2E9R"E;'*,T5V):E)!+)Q./%"Q?VPR'I,N%2;'T2)7HOU:&amp;J&lt;.'B1/)74J.6&amp;\]_@``@_M"`CE(3FDZNRVYH&gt;-&lt;RN):IM,4']`3';0#U?X'9OX/!J1NV?FV9OFU!5JYMX4[K(!U6,-Q]%]^X&amp;!&amp;IIZV@$+Y:8#PB*&gt;`W[(G$YSPV5%G+@5'-)*.#A22(&amp;U/D&amp;;/D%#4_,?&gt;W!&amp;S,&lt;`LQ?9D!H(RU9@2X,PW^&lt;V&gt;)!8)8H)!YM^!%:!9%R$,)9G$A,/`CSN[?)$5_A*R=G["A9&amp;?N&lt;./JI_/EU[J5[VVN&lt;/.@U&amp;*:H*CDE*!58Z+;8+*8EZ:4G:3A27G=(*/9H'R(6QPU$Q!&amp;46BXQ!!!!$F!!!"8(C==W"A9-AUND!\!+3:'2E9R"E;'*,T5V):E-!8"NQA0+TZD5"XC9J-:YE+4X?.CE*HD1I(%,.UGKC]_00````7![6_P7YMH3YK0,XO))KD']BT:_E'#8CQ&gt;0I!N@GI=01'MH3'K("U/&lt;"U/%,F'"&amp;SD"!ZRAZ(2M-$T5=%YO)01_V@_`L?,K#T'2C2X/1!R'_")C!R*C#722)(!7&gt;`&amp;V&gt;U`Y(5_A"R=E&amp;SG6[VMU[GDY[44KF4L87VMYV`15FG=G+/1E"2@EJJ=IF?4FF/:F+"&amp;;:Q=EZC=&lt;%&gt;8#]!UIR04!!!!!!!!!Y8!9!1!!!'-4=O-#YR!!!!!!!!$"=!A!!!!!1R.SYQ!!!!!!Y8!9!1!!!'-4=O-#YR!!!!!!!!$"=!A!!!!!1R.SYQ!!!!!!Y8!9!1!!!'-4=O-#YR!!!!!!!!&amp;!%!!!$V6T7#?3;CD#ZT5EY'34G&gt;!!!!$1!!!!!!!!!!!!!!!!!!!!!!!!#!`````Y!!!!(%C22&amp;\67SK^866%P&amp;62+LR5E529!!!!(`````A!!!!9!!!!'!!M!"A!AQ!9!A$!'!A!-"A!!$!9"A$A'!?$Y"A(\_!9"``A'!@`Y"A(`_!9"``A'!@`Y"A0``Y9!@`@G!$`@BA!0@Q9!!@Q'!!$Q"A!!!!@````]!!!)!``````````````````````!!!!!!!!!!!!!!!!!!!!``!!]!]!$Q$Q!0$Q!0!!]0``$`$Q]0$Q`Q`Q$Q]0$Q``]0$Q``$Q]0$Q]0!!]!]0``!!]0$Q]0$Q!0!0$Q]0$``Q!0$Q]!]!]!$Q]!$Q!0$`!!!!!!!!!!!!!!!!!!!!``````````````````````]!!!!!!!!!!!!!!!!!!!$`!!!!!!!!!!!!!!!!!!!!`Q!!!!!!!!X.U!!!!!!!!0]!!!!!!!X!!!X1!!!!!!$`!!!!!!X!!!!!$&gt;!!!!!!`Q!!!!X!!!!!!!!.U!!!!0]!!!!-Q!!!!!!!$^!!!!$`!!!!$.X!!!!!$``!!!!!`Q!!!!T&gt;X=!!$```Q!!!!0]!!!!-X&gt;X&gt;T@```]!!!!$`!!!!$.X&gt;X&gt;`````!!!!!`Q!!!!T&gt;X&gt;X@````Q!!!!0]!!!!-X&gt;X&gt;X````]!!!!$`!!!!$.X&gt;X&gt;`````!!!!!`Q!!!!T&gt;X&gt;X@````Q!!!!0]!!!!.X&gt;X&gt;X````&gt;``!!$`!!!!!-T&gt;X&gt;```&gt;T```]!`Q!!!!!!T&gt;X@`&gt;T```]!!0]!!!!!!!$.X&gt;$````Q!!$`!!!!!!!!!-$````Q!!!!`Q!!!!!!!!!!$``Q!!!!!0]!!!!!!!!!!!!!!!!!!!$`````````````````````]!!!1!````````````````````````````````````````````"Q=("Q=("Q=("Q=("Q=("Q=("Q=("Q=("Q=("Q=(````"Q=(`Q=(`Q=("`]("`]("Q@`"`]("Q@`"Q=(`Q@`````"```"`](`Q@`"`](``](``]("`](`Q@`"`](`````Q@`"`](````"`](`Q@`"`](`Q@`"Q=(`Q=(`Q@`````"Q=(`Q@`"`](`Q@`"`]("Q@`"Q@`"`](`Q@`"`````]("Q@`"`](`Q=(`Q=(`Q=("`](`Q=("`]("Q@`"```"Q=("Q=("Q=("Q=("Q=("Q=("Q=("Q=("Q=("Q=(`````````````````````````````````````````````Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!84*&gt;81!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!84)("Q=(86U!!!!!!!!!!!!!!0``!!!!!!!!!!!!84)("Q=("Q=("VV&gt;!!!!!!!!!!!!``]!!!!!!!!!84)("Q=("Q=("Q=("Q&gt;&gt;81!!!!!!!!$``Q!!!!!!!!!S-A=("Q=("Q=("Q=("[R&gt;!!!!!!!!!0``!!!!!!!!!$*&gt;84)("Q=("Q=("[SML$)!!!!!!!!!``]!!!!!!!!!-FV&gt;86US"Q=("[SML+SM-A!!!!!!!!$``Q!!!!!!!!!S86V&gt;86V&gt;-FWML+SML+QS!!!!!!!!!0``!!!!!!!!!$*&gt;86V&gt;86V&gt;L+SML+SML$)!!!!!!!!!``]!!!!!!!!!-FV&gt;86V&gt;86WML+SML+SM-A!!!!!!!!$``Q!!!!!!!!!S86V&gt;86V&gt;8;SML+SML+QS!!!!!!!!!0``!!!!!!!!!$*&gt;86V&gt;86V&gt;L+SML+SML$)!!!!!!!!!``]!!!!!!!!!-FV&gt;86V&gt;86WML+SML+SM-A!!!!!!!!$``Q!!!!!!!!"&gt;86V&gt;86V&gt;8;SML+SML&amp;V&gt;L+SM!!!!!0``!!!!!!!!!!!S-FV&gt;86V&gt;L+SML&amp;V&gt;-KSML+SML!!!``]!!!!!!!!!!!!!-FV&gt;86WML&amp;V&gt;-KSML+SML!!!!!$``Q!!!!!!!!!!!!!!!$*&gt;86V&gt;"[SML+SML+Q!!!!!!0``!!!!!!!!!!!!!!!!!!!S"[SML+SML+Q!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!+SML+Q!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0```````````````````````````````````````````Q!!!!)!!1!!!!!"PA!"2F")5!!!!!)!!E:15%E!!!!$&amp;E^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9H!84X"U;7.B&lt;#"1=G^E&gt;7.U,GRW9WRB=X-#"Q"16%AQ!!!!&lt;1!"!!Y!!!!!!!!!!U6Y:1R-&gt;8BT;'&amp;S:3V0261*4'FC=G&amp;S;76T"V"M&gt;7&gt;J&lt;H-74X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC=!^0=(2J9W&amp;M)&amp;"S&lt;W2V9X184X"U;7.B&lt;#"1=G^E&gt;7.U,GRW9WRB=X-!!!!!!!!"!!!!!!!!!!%!!!!!!A!!!!!!!!!!!!!!!!%!!!!P!!*%2&amp;"*!!!!!!!$&amp;E^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9H!84X"U;7.B&lt;#"1=G^E&gt;7.U,GRW9WRB=X-#"Q"16%AQ!!!!&lt;1!"!!Y!!!!!!!!!!U6Y:1R-&gt;8BT;'&amp;S:3V0261*4'FC=G&amp;S;76T"V"M&gt;7&gt;J&lt;H-74X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC=!^0=(2J9W&amp;M)&amp;"S&lt;W2V9X184X"U;7.B&lt;#"1=G^E&gt;7.U,GRW9WRB=X-!!!!!!!!"!!!!!!!!!!%!!!!!!A!!!!!!!(1!!!!!!!)!!!!P!!!!+A!$!!!!!!7D!!!0W8C=R6&gt;P3"NH'(`O0/X&amp;0]OFL@_'.J@MEEF:&lt;#:-WV)\L;?&lt;86&amp;&lt;N7N&gt;W2*./D.%L=:3SGA\/!3BB='AA\&amp;#0_8L0AD&lt;&amp;T_-%4L'@6D(W.D;1GQ`$\90ER&lt;8]`;]\_6S&gt;UF-8&amp;O9(R[/_0S?@\`@_TZX!/Z@B#:W#[ZIQ!A&lt;_("#A_K9SA#E1TRE`Y,,))QT`Q#TNZH2I)=@&amp;_[T7US,"L5R.=C(Z28Y#\XV&gt;@V,:I,^18C)LF6#-Q;LVM!&gt;5RM^R[7-)(X6)KV5GF%^Y".O-&amp;PM'=H\G,_BT'&amp;#5.K)^939,7"E(]=J`J(I&lt;&amp;S2S+_O%.^-1\IU%'3V&lt;E(+P)I2-@6X.'2&amp;#Z/I#*EB!5/WQ?LKKA8S'+!A,?-A9JA%!(/ZIK5%:L?MNCZ+G&gt;=IJJJC--^F-Y`MX@K-V%Z!_&gt;!^MCIA&amp;('8MCU4L)H4?CDOU;.(C%/&lt;R5VLM&amp;@+(//&lt;_9?R?Z6V*Z6PA!%G`1'PJ`2$"/]:)CR1X^V)2/!Q)YTC][A'_R76D1"HDH=&gt;5J1'TK4B#.,!$&amp;!;80++!"90K4)]S&amp;T;X^IXM\39D#_)=_@&amp;K:HIYK)YPZ#Y'%X'R6AU'3VEK&amp;N786WE?Z+-CA0KA9-(%,.0?QZ3K21/!+U&amp;09L12CG4QXFI.^/ZC=?MC:/MVO4?R-H*4]9_*.-,(':.T8*5MQ?I(;,W',8(,26XI)I^1H[B$B7`]?*6X)HCOJ+H9ID!^X#BB#+\$*#F9P1(_!UC*4!(%806LG,%2""TI&lt;S+$R7IG'$T6(TTZEU($LEYH&amp;-RRT#'CJ7\_F0^+&gt;(ST`I@]"1_JFIW^$G#&gt;#BK4R7Q*-NVT0)V&gt;&amp;)#&gt;JE%2,&lt;6&lt;XNJ"E175-.3WL_L&lt;TIR,Q\+?5SM;R#.K:SL1]J5/_2;$5(I-&amp;P60&lt;46T=V.&lt;"7N&amp;6TRQ48/LUDE6S&amp;A(%`](UBN$%YAY!IJS5"\_&amp;)W7[U'E\,;@&amp;4+M$2:G$9C["P1AC/;A\7V.5S!VO0&amp;5#Q*63'GP5;PN6#LC.4&amp;2[TAT^)XR@N*_%*8QIS07+?LSWN6-Y86)&amp;M6NGLW948\H\W;S:V8-_GIJE;$'"Z_*-*NE0SN?2S$5)X6`(\P!6;TN(R&lt;_J3B"673,#41E\#2E4Y@M$WXWZZ$VP0DHWRJ5?&lt;R)H@/@F2AW4PH0/I]="9#%S!`/66DX$Q6Q75W+%,1JQ@^JOMQXDIE]!3CL_-"=NQNQU4;&lt;V&amp;JVVP3LM1K)G7E818'^@R3XX2U&gt;D9_)Q\/8IQP*0-5DCS0'(-^44/Y;9:7?!8;I!0\WP=/BC77$J6T5F=$&lt;K3/O0C)T6(X53(,BGN-*[ZIS\M_@V3\EE^CDQV3JNYEM1F(')+O!BHH.6C0-SAGYU2B+9:LI9SX&gt;8X_K09'4RF,^;S.2.*D"RSR8^\&amp;?HQ:RS"3&amp;R_RO22S946&amp;8=F;W:HH@Q^K\X$5[0#-L=-^##`7)?0-Y%789M6-/V,AC8X0@G*&gt;RIHF]&amp;VB_8V#2.90^^&amp;14+USTGQ9QU^!LX-&gt;D26&gt;2T6Y:I`M:"VR&gt;"X2-WM=VK+&lt;Q^J1YU5XF"MPRO\3'YJO*\&lt;=&gt;DJP58!;+?D-WUZ98QP?Y0`$&gt;HL8O,==WYH?T=^=T@.MJT0'LH2O*U*#P8U\P&gt;D6&gt;,:A.&gt;6A_\XF6^.%E&gt;65C;NJ7&lt;S'@MO_K\JO,3A':XV!6E5S9@V0`(]9TDG`(`KW@@][N]-FZ2Z:G)MN438&lt;:S\34YD#.W*:6PH8J5Q55W4@M6NR_8?$6/,FNN_Y-FTGWO[&amp;M$G&lt;P^]O_FK,MRGQT;&lt;O%W-W68A*Y/C(&lt;,HY_X?7=N`!`+AE[X=G"MC88,BB?$[:G)L/C&amp;:0-YH*_=&lt;#HWGLE0]X6CK4U)^68N7A8O\&lt;`;-QC#T5]I.]PX!,?=)0FQ&lt;_&amp;L_2`D8XO:U_E5-K&gt;_&amp;T&gt;I`UB:&gt;P_B=N#%^&gt;!!!!!!1!!!"%!!!!"!!!!!!!!!!-!!&amp;#2%B1!!!!!!!$!!!!9A!!!(*YH'.A9-A4E'$[RV$XFY&amp;*Y#O1)@W8A6H1D`%X!Q/HH]"B)-UI)!E5FPX,Q#[I$2&lt;70K,,Q1!&amp;KGS-(*)=BQ5ZQ$)=,2I-````Z`B[Z"J=R2%@/&amp;.FFDS("!!59BE!!!!!!!!%!!!!"Q!!!`!!!!!(!!!!)6^O;6^-98.U3WZP&gt;WZ0&gt;WZJ&lt;G&gt;-6E.M98.T1WRV=X2F=A!!!,Q8!)!!!!!!!1!)!$$`````!!%!!!!!!+!!!!!&amp;!%J!=!!?!!!Q&amp;E^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9H!84X"U;7.B&lt;#"1=G^E&gt;7.U,GRW9WRB=X-!!!^1=G^E&gt;7.U,GRW9WRB=X-!$5!$!!&gt;$;'&amp;O&lt;G6M!"2!)1Z$;'&amp;O&lt;G6M)%FO&gt;G6S&gt;!!!$5!&amp;!!&gt;$;'FQ)%F%!#"!5!!%!!!!!1!#!!-245&amp;044-Y-$5T,GRW9WRB=X-!!1!%!!!!!!!!!"J-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;597*0=G2F=A!!!$58!)!!!!!!!A!&amp;!!=!!!Q!1!!"`````Q!!!!%!!1!!!!1!!!!!!!!!!1!!!!)!!!!$!!!!!!!!!"N-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;5;7VF=X2B&lt;8!!!!!:&amp;Q#!!!!!!!%!"1!(!!!"!!$:6RAZ!!!!!!!!!#:-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;-98.U18"Q&lt;'FF:&amp;2J&lt;76T&gt;'&amp;N=!!!!"E8!)!!!!!!!1!&amp;!!=!!!%!!.F8'$E!!!!!!!!!'ER71WRB=X.1=GFW982F2'&amp;U962Z='6%:8.D!!!!P"=!A!!!!!!"!!A!-0````]!!1!!!!!!I!!!!!5!3E"Q!"Y!!$!74X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC="&gt;0=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:D&lt;'&amp;T=Q!!$V"S&lt;W2V9X1O&lt;(:D&lt;'&amp;T=Q!.1!-!"U.I97ZO:7Q!&amp;%!B$E.I97ZO:7QA37ZW:8*U!!!.1!5!"U.I;8!A351!)%"1!!1!!!!"!!)!!R&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=Q!"!!1!!!!!!!!!(ER71WRB=X.1=GFW982F2'&amp;U952G&lt;(2%982B5WF[:1!!!"E8!)!!!!!!!1!&amp;!!-!!!%!!!!!!!I!!!!!!!!!'ER71WRB=X.1=GFW982F2'&amp;U952G&lt;(2%982B!!!!\B=!A!!!!!!&amp;!%J!=!!?!!!Q&amp;E^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9H!84X"U;7.B&lt;#"1=G^E&gt;7.U,GRW9WRB=X-!!!^1=G^E&gt;7.U,GRW9WRB=X-!$5!$!!&gt;$;'&amp;O&lt;G6M!"2!)1Z$;'&amp;O&lt;G6M)%FO&gt;G6S&gt;!!!$5!&amp;!!&gt;$;'FQ)%F%!#"!5!!%!!!!!1!#!!-245&amp;044-Y-$5T,GRW9WRB=X-!!1!%!!!!!4!74X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC="&gt;0=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:D&lt;'&amp;T=Q!!!!!!!!!!!!!!!!!!!!!!6!!!!!!!!!!%!!9!$A!!!!1!!!"G!!!!+!!!!!)!!!1!!!!!%Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!&amp;(!!!#6(C=H6".3].!&amp;*RW4&gt;/K`9ZN&amp;:4N8;21"@75EC*5+!V?P"K4D1&lt;7*+4&lt;Y.'`[&lt;`1@_$LNK6A4]L!Y\X:W:VZ#_!%^X;+-W$1G;5K]DX*X3Q*&amp;L[[E,G-HN0O,OV,&lt;TY([L_:KMVA/K^?(!M*S_\8VDW@R,H)&amp;%BA,!62SC&gt;DP/(\]\&amp;X!Y":\GB[S==CDXT"*]\+O4E&gt;T;&lt;$[](6='.1X4+_EBD9,P&lt;I?A&amp;&amp;M&amp;.(,O:+:$Q*O2&lt;T.)NS4QE??-K$1=!_!F*`[9'N2QRBIG+&lt;9+&amp;]1&gt;H_9-G$7B%C#&gt;%A$Y9+;1`9)ABRCX^_&amp;C8NI[546]GR"I/J^Y$?&gt;3H^)?L5_;1KUD&lt;H@^KL##KF:3E!:4RJKTN[FK-.3_^KY)AM.T$7W'7W*RW.V1=6U.6RWF2,Z."!%SXU=,S*3&amp;W,Q(2Y`!"2UH&amp;"!!!!!*Y!!1!#!!-!"1!!!&amp;A!$Q!!!!!!$Q$N!/-!!!"O!!]!!!!!!!]!\1$D!!!!B!!0!!!!!!!0!/U!YQ!!!*K!!)!!!_A!$Q$X!/E!!!#=A!#!!)!!!!]!\1$D&amp;5VJ9X*P=W^G&gt;#"+;'6O:UBF;3"6326.;7.S&lt;X.P:H1A3GBF&lt;G&gt;):7EA65E647FD=G^T&lt;W:U)%JI:7ZH3'6J)&amp;6*!4!"-!!!5F.31QU+!!.-6E.$4%*76Q!!'_1!!!33!!!!)!!!']1!!!!!!!!!!!!!!#!!!!!U!!!%B!!!!"V-35*/!!!!!!!!!7R-6F.3!!!!!!!!!9"36&amp;.(!!!!!!!!!:2$1V.5!!!!!!!!!;B-38:J!!!!!!!!!&lt;R$4UZ1!!!!!!!!!&gt;"544AQ!!!!!1!!!?2%2E24!!!!!!!!!AR-372T!!!!!!!!!C"735.%!!!!!A!!!D2W:8*T!!!!"!!!!H"41V.3!!!!!!!!!N2(1V"3!!!!!!!!!OB*1U^/!!!!!!!!!PRJ9WQU!!!!!!!!!R"J9WQY!!!!!!!!!S2$5%-S!!!!!!!!!TB-37:Q!!!!!!!!!UR'5%BC!!!!!!!!!W"'5&amp;.&amp;!!!!!!!!!X275%21!!!!!!!!!YB-37*E!!!!!!!!!ZR#2%BC!!!!!!!!!\"#2&amp;.&amp;!!!!!!!!!]273624!!!!!!!!!^B%6%B1!!!!!!!!!_R.65F%!!!!!!!!"!")36.5!!!!!!!!""271V21!!!!!!!!"#B'6%&amp;#!!!!!!!!"$Q!!!!!`````Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$!!!!!!!!!!!0````]!!!!!!!!!V!!!!!!!!!!!`````Q!!!!!!!!$I!!!!!!!!!!$`````!!!!!!!!!0!!!!!!!!!!!0````]!!!!!!!!"[!!!!!!!!!!!`````Q!!!!!!!!(Q!!!!!!!!!!,`````!!!!!!!!!BA!!!!!!!!!!0````]!!!!!!!!#-!!!!!!!!!!!`````Q!!!!!!!!+E!!!!!!!!!!$`````!!!!!!!!!L1!!!!!!!!!!@````]!!!!!!!!%A!!!!!!!!!!#`````Q!!!!!!!!7A!!!!!!!!!!4`````!!!!!!!!"IQ!!!!!!!!!"`````]!!!!!!!!'I!!!!!!!!!!)`````Q!!!!!!!!;Q!!!!!!!!!!H`````!!!!!!!!"M1!!!!!!!!!#P````]!!!!!!!!'V!!!!!!!!!!!`````Q!!!!!!!!&lt;I!!!!!!!!!!$`````!!!!!!!!"Q!!!!!!!!!!!0````]!!!!!!!!(&amp;!!!!!!!!!!!`````Q!!!!!!!!?9!!!!!!!!!!$`````!!!!!!!!#:Q!!!!!!!!!!0````]!!!!!!!!.I!!!!!!!!!!!`````Q!!!!!!!!WI!!!!!!!!!!$`````!!!!!!!!$WQ!!!!!!!!!!0````]!!!!!!!!6&amp;!!!!!!!!!!!`````Q!!!!!!!"5=!!!!!!!!!!$`````!!!!!!!!&amp;31!!!!!!!!!!0````]!!!!!!!!6.!!!!!!!!!!!`````Q!!!!!!!"7=!!!!!!!!!!$`````!!!!!!!!&amp;;1!!!!!!!!!!0````]!!!!!!!!:G!!!!!!!!!!!`````Q!!!!!!!"GA!!!!!!!!!!$`````!!!!!!!!';A!!!!!!!!!!0````]!!!!!!!!:V!!!!!!!!!#!`````Q!!!!!!!"MA!!!!!!V.15^.-TAQ.4-O9X2M!!!!!!</Property>
	<Property Name="NI.LVClass.Geneology" Type="Xml"><String>
<Name></Name>
<Val>!!!!!B2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=Q"16%AQ!!!!!!!!!!!!!!!.!!%!!!!!!!!"!!!!!1!91&amp;!!!"&amp;.16.$-TAQ.4-O&lt;(:D&lt;'&amp;T=Q!"!!!!!!!!!!!!!!!!!1Z-97*73568)%^C;G6D&gt;!"16%AQ!!!!!!!!!!!!&amp;Q#!!!!!!!!!!!!!!!!!!!%!!!!!!!%"!!!!!Q"+1(!!(A!!-":0=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7*Q&amp;U^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;G.M98.T!!!05(*P:(6D&gt;#ZM&gt;G.M98.T!!V!!Q!(1WBB&lt;GZF&lt;!"K!0(9QC%C!!!!!R6115UU)%.%5C"%:8:J9W5O&lt;(:M;7)245&amp;41T-Y-$5T,GRW9WRB=X-.45&amp;41T-Y-$5T,G.U&lt;!!M1&amp;!!!A!!!!%&gt;1WRV=X2F=C"P:C"D&lt;'&amp;T=S"Q=GFW982F)'2B&gt;'%!!1!#!!!!!P``````````!!!!!4!74X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC="&gt;0=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:D&lt;'&amp;T=Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!%/4'&amp;C6EF&amp;6S"09GJF9X1!5&amp;2)-!!!!!!!!!!!!"=!A!!!!!!!!!!!!!!!!!!"!!!!!!!#!1!!!!]!3E"Q!"Y!!$!74X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC="&gt;0=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:D&lt;'&amp;T=Q!!$V"S&lt;W2V9X1O&lt;(:D&lt;'&amp;T=Q!.1!-!"U.I97ZO:7Q!#5!(!!.5&lt;X!!#5!(!!..;71!#5!(!!.#&lt;X1!91$R!!!!!!!!!!-65%&amp;..#"$2&amp;)A2'6W;7.F,GRW&lt;'FC%5V"5U-T/$!V-SZM&gt;G.M98.T&amp;EVB;7YA6G&amp;M&gt;75N1WRV=X2F=CZD&gt;'Q!'E"1!!-!!A!$!!1*47&amp;J&lt;C"$3#!Q!'%!]1!!!!!!!!!$&amp;6""441A1U23)%2F&gt;GFD:3ZM&gt;GRJ9B&amp;.16.$-TAQ.4-O&lt;(:D&lt;'&amp;T=R:.97FO)&amp;:B&lt;(6F,5.M&gt;8.U:8)O9X2M!"J!5!!$!!)!!Q!%#5VB;7YA1UAA-1"B!0%!!!!!!!!!!R6115UU)%.%5C"%:8:J9W5O&lt;(:M;7)245&amp;41T-Y-$5T,GRW9WRB=X-747&amp;J&lt;C"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!;1&amp;!!!Q!#!!-!"!F.97FO)%.))$)!91$R!!!!!!!!!!-65%&amp;..#"$2&amp;)A2'6W;7.F,GRW&lt;'FC%5V"5U-T/$!V-SZM&gt;G.M98.T&amp;EVB;7YA6G&amp;M&gt;75N1WRV=X2F=CZD&gt;'Q!'E"1!!-!!A!$!!1*47&amp;J&lt;C"$3#!T!'%!]1!!!!!!!!!$&amp;6""441A1U23)%2F&gt;GFD:3ZM&gt;GRJ9B&amp;.16.$-TAQ.4-O&lt;(:D&lt;'&amp;T=R:.97FO)&amp;:B&lt;(6F,5.M&gt;8.U:8)O9X2M!"J!5!!$!!)!!Q!%#5VB;7YA1UAA.!"B!0%!!!!!!!!!!R6115UU)%.%5C"%:8:J9W5O&lt;(:M;7)245&amp;41T-Y-$5T,GRW9WRB=X-747&amp;J&lt;C"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!;1&amp;!!!Q!#!!-!"!F.97FO)%.))$5!91$R!!!!!!!!!!-65%&amp;..#"$2&amp;)A2'6W;7.F,GRW&lt;'FC%5V"5U-T/$!V-SZM&gt;G.M98.T&amp;EVB;7YA6G&amp;M&gt;75N1WRV=X2F=CZD&gt;'Q!'E"1!!-!!A!$!!1*47&amp;J&lt;C"$3#!W!'%!]1!!!!!!!!!$&amp;6""441A1U23)%2F&gt;GFD:3ZM&gt;GRJ9B&amp;.16.$-TAQ.4-O&lt;(:D&lt;'&amp;T=R:.97FO)&amp;:B&lt;(6F,5.M&gt;8.U:8)O9X2M!"J!5!!$!!)!!Q!%#5VB;7YA1UAA.Q!C1&amp;!!#!!&amp;!!9!"Q!)!!E!#A!,!!Q+47&amp;J&lt;C"797RV:1!!&lt;!$RW-)C&amp;!!!!!-65%&amp;..#"$2&amp;)A2'6W;7.F,GRW&lt;'FC%5V"5U-T/$!V-SZM&gt;G.M98.T$5V"5U-T/$!V-SZD&gt;'Q!,E"1!!-!!!!"!!U&gt;1WRV=X2F=C"P:C"D&lt;'&amp;T=S"Q=GFW982F)'2B&gt;'%!!1!/!!!!!Q!!!!!!!!!"`````Q!!!!%Q&amp;E^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9H!84X"U;7.B&lt;#"1=G^E&gt;7.U,GRW9WRB=X-!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1Z-97*73568)%^C;G6D&gt;!"16%AQ!!!!!!!!!!!!&amp;Q#!!!!!!!!!!!!!!!!!!!%!!!!!!!-"!!!!%1"+1(!!(A!!-":0=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7*Q&amp;U^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;G.M98.T!!!05(*P:(6D&gt;#ZM&gt;G.M98.T!!V!!Q!(1WBB&lt;GZF&lt;!!*1!=!!V2P=!!*1!=!!UVJ:!!*1!=!!U*P&gt;!"B!0%!!!!!!!!!!R6115UU)%.%5C"%:8:J9W5O&lt;(:M;7)245&amp;41T-Y-$5T,GRW9WRB=X-747&amp;J&lt;C"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!;1&amp;!!!Q!#!!-!"!F.97FO)%.))$!!91$R!!!!!!!!!!-65%&amp;..#"$2&amp;)A2'6W;7.F,GRW&lt;'FC%5V"5U-T/$!V-SZM&gt;G.M98.T&amp;EVB;7YA6G&amp;M&gt;75N1WRV=X2F=CZD&gt;'Q!'E"1!!-!!A!$!!1*47&amp;J&lt;C"$3#!R!'%!]1!!!!!!!!!$&amp;6""441A1U23)%2F&gt;GFD:3ZM&gt;GRJ9B&amp;.16.$-TAQ.4-O&lt;(:D&lt;'&amp;T=R:.97FO)&amp;:B&lt;(6F,5.M&gt;8.U:8)O9X2M!"J!5!!$!!)!!Q!%#5VB;7YA1UAA-A"B!0%!!!!!!!!!!R6115UU)%.%5C"%:8:J9W5O&lt;(:M;7)245&amp;41T-Y-$5T,GRW9WRB=X-747&amp;J&lt;C"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!;1&amp;!!!Q!#!!-!"!F.97FO)%.))$-!91$R!!!!!!!!!!-65%&amp;..#"$2&amp;)A2'6W;7.F,GRW&lt;'FC%5V"5U-T/$!V-SZM&gt;G.M98.T&amp;EVB;7YA6G&amp;M&gt;75N1WRV=X2F=CZD&gt;'Q!'E"1!!-!!A!$!!1*47&amp;J&lt;C"$3#!U!'%!]1!!!!!!!!!$&amp;6""441A1U23)%2F&gt;GFD:3ZM&gt;GRJ9B&amp;.16.$-TAQ.4-O&lt;(:D&lt;'&amp;T=R:.97FO)&amp;:B&lt;(6F,5.M&gt;8.U:8)O9X2M!"J!5!!$!!)!!Q!%#5VB;7YA1UAA.1"B!0%!!!!!!!!!!R6115UU)%.%5C"%:8:J9W5O&lt;(:M;7)245&amp;41T-Y-$5T,GRW9WRB=X-747&amp;J&lt;C"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!;1&amp;!!!Q!#!!-!"!F.97FO)%.))$9!91$R!!!!!!!!!!-65%&amp;..#"$2&amp;)A2'6W;7.F,GRW&lt;'FC%5V"5U-T/$!V-SZM&gt;G.M98.T&amp;EVB;7YA6G&amp;M&gt;75N1WRV=X2F=CZD&gt;'Q!'E"1!!-!!A!$!!1*47&amp;J&lt;C"$3#!X!#*!5!!)!!5!"A!(!!A!#1!+!!M!$!J.97FO)&amp;:B&lt;(6F!!!01!5!#5VB;7YA1HFU:1!71%!!!@````]!$AF.97FO)%*Z&gt;'5!&lt;A$RW-)DTQ!!!!-65%&amp;..#"$2&amp;)A2'6W;7.F,GRW&lt;'FC%5V"5U-T/$!V-SZM&gt;G.M98.T$5V"5U-T/$!V-SZD&gt;'Q!-%"1!!1!!!!"!!U!$RV$&lt;(6T&gt;'6S)'^G)'.M98.T)("S;8:B&gt;'5A:'&amp;U91!"!"!!!!!E!!!!!!!!!!%!!!!#!!!!!Q!!!!1!!!!&amp;!!!!"A!!!!=!!!!)!!!!#1!!!!I!!!!,!!!!$!!!!!U!!!!/!!!!$Q!!!"!!!!!2!!!!%A!!!"-!!!!5!!!!&amp;1!!!"9!!!!8!!!!'!!!!"E!!!!;!!!!'Q!!!"Q!!!!&gt;!!!!(A!!!"]!!!!A!!!!)1!!!#,`````!!!!!4!74X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC="&gt;0=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:D&lt;'&amp;T=Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1Z-97*73568)%^C;G6D&gt;!"16%AQ!!!!!!!!!!!!&amp;Q#!!!!!!!!!!!!!!!!!!!%!!!!!!!1"!!!!$Q"+1(!!(A!!-":0=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7*Q&amp;U^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;G.M98.T!!!05(*P:(6D&gt;#ZM&gt;G.M98.T!!V!!Q!(1WBB&lt;GZF&lt;!!*1!=!!V2P=!!*1!=!!UVJ:!!*1!=!!U*P&gt;!"B!0%!!!!!!!!!!R6115UU)%.%5C"%:8:J9W5O&lt;(:M;7)245&amp;41T-Y-$5T,GRW9WRB=X-747&amp;J&lt;C"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!;1&amp;!!!Q!#!!-!"!F.97FO)%.))$!!91$R!!!!!!!!!!-65%&amp;..#"$2&amp;)A2'6W;7.F,GRW&lt;'FC%5V"5U-T/$!V-SZM&gt;G.M98.T&amp;EVB;7YA6G&amp;M&gt;75N1WRV=X2F=CZD&gt;'Q!'E"1!!-!!A!$!!1*47&amp;J&lt;C"$3#!R!'%!]1!!!!!!!!!$&amp;6""441A1U23)%2F&gt;GFD:3ZM&gt;GRJ9B&amp;.16.$-TAQ.4-O&lt;(:D&lt;'&amp;T=R:.97FO)&amp;:B&lt;(6F,5.M&gt;8.U:8)O9X2M!"J!5!!$!!)!!Q!%#5VB;7YA1UAA-A"B!0%!!!!!!!!!!R6115UU)%.%5C"%:8:J9W5O&lt;(:M;7)245&amp;41T-Y-$5T,GRW9WRB=X-747&amp;J&lt;C"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!;1&amp;!!!Q!#!!-!"!F.97FO)%.))$-!91$R!!!!!!!!!!-65%&amp;..#"$2&amp;)A2'6W;7.F,GRW&lt;'FC%5V"5U-T/$!V-SZM&gt;G.M98.T&amp;EVB;7YA6G&amp;M&gt;75N1WRV=X2F=CZD&gt;'Q!'E"1!!-!!A!$!!1*47&amp;J&lt;C"$3#!U!'%!]1!!!!!!!!!$&amp;6""441A1U23)%2F&gt;GFD:3ZM&gt;GRJ9B&amp;.16.$-TAQ.4-O&lt;(:D&lt;'&amp;T=R:.97FO)&amp;:B&lt;(6F,5.M&gt;8.U:8)O9X2M!"J!5!!$!!)!!Q!%#5VB;7YA1UAA.1"B!0%!!!!!!!!!!R6115UU)%.%5C"%:8:J9W5O&lt;(:M;7)245&amp;41T-Y-$5T,GRW9WRB=X-747&amp;J&lt;C"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!;1&amp;!!!Q!#!!-!"!F.97FO)%.))$9!91$R!!!!!!!!!!-65%&amp;..#"$2&amp;)A2'6W;7.F,GRW&lt;'FC%5V"5U-T/$!V-SZM&gt;G.M98.T&amp;EVB;7YA6G&amp;M&gt;75N1WRV=X2F=CZD&gt;'Q!'E"1!!-!!A!$!!1*47&amp;J&lt;C"$3#!X!#*!5!!)!!5!"A!(!!A!#1!+!!M!$!J.97FO)&amp;:B&lt;(6F!!"M!0(9QC:L!!!!!R6115UU)%.%5C"%:8:J9W5O&lt;(:M;7)245&amp;41T-Y-$5T,GRW9WRB=X-.45&amp;41T-Y-$5T,G.U&lt;!!O1&amp;!!!Q!!!!%!$2V$&lt;(6T&gt;'6S)'^G)'.M98.T)("S;8:B&gt;'5A:'&amp;U91!"!!Y!!!!D!!!!!!!!!!%!!!!#!!!!!Q!!!!1!!!!&amp;!!!!"A!!!!=!!!!)!!!!#1!!!!I!!!!,!!!!$!!!!!U!!!!/!!!!$Q!!!"!!!!!2!!!!%A!!!"-!!!!5!!!!&amp;1!!!"9!!!!8!!!!'!!!!"E!!!!;!!!!'Q!!!"Q!!!!&gt;!!!!(A!!!"]!!!!A!!!!)1!!!#)!!!!"-":0=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7*Q&amp;U^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;G.M98.T!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!%/4'&amp;C6EF&amp;6S"09GJF9X1!5&amp;2)-!!!!!!!!!!!!"=!A!!!!!!!!!!!!!!!!!!"!!!!!!!&amp;!1!!!!-!3E"Q!"Y!!$!74X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC="&gt;0=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:D&lt;'&amp;T=Q!!$V"S&lt;W2V9X1O&lt;(:D&lt;'&amp;T=Q!.1!-!"U.I97ZO:7Q!;A$RW-)O?Q!!!!-65%&amp;..#"$2&amp;)A2'6W;7.F,GRW&lt;'FC%5V"5U-T/$!V-SZM&gt;G.M98.T$5V"5U-T/$!V-SZD&gt;'Q!,%"1!!)!!!!"(5.M&gt;8.U:8)A&lt;W9A9WRB=X-A=(*J&gt;G&amp;U:3"E982B!!%!!A!!!!)!!!!!!!!!!1!!!!%Q&amp;E^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9H!84X"U;7.B&lt;#"1=G^E&gt;7.U,GRW9WRB=X-!!!!!!!!!!!!!!!!!!!!!!!!!!!!"$ERB9F:*26=A4W*K:7.U!&amp;"53$!!!!!!!!!!!!!8!)!!!!!!!!!!!!!!!!!!!1!!!!!!!!)!!!!$!%J!=!!?!!!Q&amp;E^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9H!84X"U;7.B&lt;#"1=G^E&gt;7.U,GRW9WRB=X-!!!^1=G^E&gt;7.U,GRW9WRB=X-!$5!$!!&gt;$;'&amp;O&lt;G6M!'I!]&gt;D#,HM!!!!$&amp;6""441A1U23)%2F&gt;GFD:3ZM&gt;GRJ9B&amp;.16.$-TAQ.4-O&lt;(:D&lt;'&amp;T=QV.16.$-TAQ.4-O9X2M!#R!5!!#!!!!!2V$&lt;(6T&gt;'6S)'^G)'.M98.T)("S;8:B&gt;'5A:'&amp;U91!"!!)!!!!"`````A!!!!%Q&amp;E^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9H!84X"U;7.B&lt;#"1=G^E&gt;7.U,GRW9WRB=X-!!!!!!!!!!!!!!!!!!!!!!!!!!!!"$ERB9F:*26=A4W*K:7.U!&amp;"53$!!!!!!!!!!!!!8!)!!!!!!!!!!!!!!!!!!!1!!!!!!!1)!!!!%!%J!=!!?!!!Q&amp;E^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9H!84X"U;7.B&lt;#"1=G^E&gt;7.U,GRW9WRB=X-!!!^1=G^E&gt;7.U,GRW9WRB=X-!$5!$!!&gt;$;'&amp;O&lt;G6M!"2!)1Z$;'&amp;O&lt;G6M)%FO&gt;G6S&gt;!!!&lt;!$RW-*7TA!!!!-65%&amp;..#"$2&amp;)A2'6W;7.F,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T$5V"4UUT/$!V-SZD&gt;'Q!,E"1!!-!!!!"!!)&gt;1WRV=X2F=C"P:C"D&lt;'&amp;T=S"Q=GFW982F)'2B&gt;'%!!1!$!!!!!Q!!!!!!!!!"`````Q!!!!%Q&amp;E^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9H!84X"U;7.B&lt;#"1=G^E&gt;7.U,GRW9WRB=X-!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!%/4'&amp;C6EF&amp;6S"09GJF9X1!5&amp;2)-!!!!!!!!!!!!"=!A!!!!!!!!!!!!!!!!!!"!!!!!!!!!Q!!!!1!3E"Q!"Y!!$!74X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC="&gt;0=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:D&lt;'&amp;T=Q!!$V"S&lt;W2V9X1O&lt;(:D&lt;'&amp;T=Q!.1!-!"U.I97ZO:7Q!&amp;%!B$E.I97ZO:7QA37ZW:8*U!!"M!0(9QF&lt;/!!!!!R6115UU)%.%5C"%:8:J9W5O&lt;(:M;7)245&amp;044-Y-$5T,GRW9WRB=X-.45&amp;044-Y-$5T,G.U&lt;!!O1&amp;!!!Q!!!!%!!BV$&lt;(6T&gt;'6S)'^G)'.M98.T)("S;8:B&gt;'5A:'&amp;U91!"!!-!!!!"`````A!!!!%Q&amp;E^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9H!84X"U;7.B&lt;#"1=G^E&gt;7.U,GRW9WRB=X-!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!%/4'&amp;C6EF&amp;6S"09GJF9X1!5&amp;2)-!!!!!!!!!!!!"=!A!!!!!!!!!!!!!!!!!!"!!!!!!!"!Q!!!!5!3E"Q!"Y!!$!74X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC="&gt;0=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:D&lt;'&amp;T=Q!!$V"S&lt;W2V9X1O&lt;(:D&lt;'&amp;T=Q!.1!-!"U.I97ZO:7Q!&amp;%!B$E.I97ZO:7QA37ZW:8*U!!!31#%-2DJ*1T%M)&amp;1[35-S!!"W!0(:5TVR!!!!!RV25U:1,52%)&amp;""441A1U23)%2F&gt;GFD:3ZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=QV.15^.-TAQ.4-O9X2M!$"!5!!%!!!!!1!#!!-&gt;1WRV=X2F=C"P:C"D&lt;'&amp;T=S"Q=GFW982F)'2B&gt;'%!!1!%!!!!"!!!!!!!!!!"!!!!!P````]!!!!"-":0=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7*Q&amp;U^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;G.M98.T!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"$ERB9F:*26=A4W*K:7.U!&amp;"53$!!!!!!!!!!!!!8!)!!!!!!!!!!!!!!!!!!!1!!!!!!!A-!!!!%!%J!=!!?!!!Q&amp;E^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9H!84X"U;7.B&lt;#"1=G^E&gt;7.U,GRW9WRB=X-!!!^1=G^E&gt;7.U,GRW9WRB=X-!$5!$!!&gt;$;'&amp;O&lt;G6M!"2!)1Z$;'&amp;O&lt;G6M)%FO&gt;G6S&gt;!!!&gt;!$RW61L`A!!!!-&gt;56.'5#V%2#"115UU)%.%5C"%:8:J9W5O&lt;(:M;7)245&amp;044-Y-$5T,GRW9WRB=X-.45&amp;044-Y-$5T,G.U&lt;!!O1&amp;!!!Q!!!!%!!BV$&lt;(6T&gt;'6S)'^G)'.M98.T)("S;8:B&gt;'5A:'&amp;U91!"!!-!!!!$!!!!!!!!!!%!!!!#!!!!!4!74X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC="&gt;0=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:D&lt;'&amp;T=Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1Z-97*73568)%^C;G6D&gt;!"16%AQ!!!!!!!!!!!!&amp;Q#!!!!!!!!!!!!!!!!!!!%!!!!!!!-$!!!!"1"+1(!!(A!!-":0=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7*Q&amp;U^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;G.M98.T!!!05(*P:(6D&gt;#ZM&gt;G.M98.T!!V!!Q!(1WBB&lt;GZF&lt;!!51#%/1WBB&lt;GZF&lt;#"*&lt;H:F=H1!!!V!"1!(1WBJ=#"*2!"W!0(:6RAZ!!!!!RV25U:1,52%)&amp;""441A1U23)%2F&gt;GFD:3ZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=QV.15^.-TAQ.4-O9X2M!$"!5!!%!!!!!1!#!!-&gt;1WRV=X2F=C"P:C"D&lt;'&amp;T=S"Q=GFW982F)'2B&gt;'%!!1!%!!!!"!!!!!!!!!!"!!!!!P````]!!!!"-":0=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7*Q&amp;U^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;G.M98.T!!!!!!!!!!!!!!!!!!!!!!"5!!!!!!!!!1Z-97*73568)%^C;G6D&gt;!"16%AQ!!!!!!!!!!!!&amp;Q#!!!!!!!!!!!!!!!!!!!%!!!!!!!!!!!!!"1"+1(!!(A!!-":0=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7*Q&amp;U^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;G.M98.T!!!05(*P:(6D&gt;#ZM&gt;G.M98.T!!V!!Q!(1WBB&lt;GZF&lt;!!51#%/1WBB&lt;GZF&lt;#"*&lt;H:F=H1!!!V!"1!(1WBJ=#"*2!"W!0(:6RAZ!!!!!RV25U:1,52%)&amp;""441A1U23)%2F&gt;GFD:3ZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=QV.15^.-TAQ.4-O9X2M!$"!5!!%!!!!!1!#!!-&gt;1WRV=X2F=C"P:C"D&lt;'&amp;T=S"Q=GFW982F)'2B&gt;'%!!1!%!!!!!@````Y!!!!"-":0=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7*Q&amp;U^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;G.M98.T!!!!!!!!!!!!!!!!!!!!!!"5!!!!!!!!!1Z-97*73568)%^C;G6D&gt;!"16%AQ!!!!!!!!!!!!&amp;Q#!!!!!!!!!!!!!!!-!!!!H5%&amp;..#"$2&amp;)A2'6W;7.F,GRW&lt;'FC/EV"5U-T/$!V-SZM&gt;G.M98.T!!!!*V""441A1U23)%2F&gt;GFD:3ZM&gt;GRJ9DJ.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=Q!!!#^25U:1,52%)&amp;""441A1U23)%2F&gt;GFD:3ZM&gt;GRJ9DJ.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=Q</Val>
</String>
</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">false</Property>
	<Property Name="NI.LVClass.LowestCompatibleVersion" Type="Str">1.0.0.0</Property>
	<Item Name="MAOM38053.ctl" Type="Class Private Data" URL="MAOM38053.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="Accessor" Type="Folder">
		<Item Name="Configure" Type="Folder">
			<Item Name="Channel" Type="Property Definition">
				<Property Name="NI.ClassItem.Property.LongName" Type="Str">Configure:Channel</Property>
				<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Channel</Property>
				<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
				<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
				<Item Name="Read Channel.vi" Type="VI" URL="../Accessor/Channel Property/Read Channel.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;8!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!V!!Q!(1WBB&lt;GZF&lt;!"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=Q!!$5V"4UUT/$!V-S"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=Q!!$%V"4UUT/$!V-S"J&lt;A!!91$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A#!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
				</Item>
				<Item Name="Write Channel.vi" Type="VI" URL="../Accessor/Channel Property/Write Channel.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;8!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T!!!.45&amp;044-Y-$5T)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!V!!Q!(1WBB&lt;GZF&lt;!"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=Q!!$%V"4UUT/$!V-S"J&lt;A!!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A#!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
				</Item>
			</Item>
			<Item Name="Channel Invert" Type="Property Definition">
				<Property Name="NI.ClassItem.Property.LongName" Type="Str">Configure:Channel Invert</Property>
				<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Channel Invert</Property>
				<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
				<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
				<Item Name="Read Channel Invert.vi" Type="VI" URL="../Accessor/Channel Invert Property/Read Channel Invert.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;?!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"2!)1Z$;'&amp;O&lt;G6M)%FO&gt;G6S&gt;!!!1%"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)245&amp;044-Y-$5T,GRW9WRB=X-!!!V.15^.-TAQ.4-A&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!1%"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)245&amp;044-Y-$5T,GRW9WRB=X-!!!R.15^.-TAQ.4-A;7Y!!'%!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!A!!?!!!$1A!!!!!!!!*!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
				</Item>
				<Item Name="Write Channel Invert.vi" Type="VI" URL="../Accessor/Channel Invert Property/Write Channel Invert.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;?!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T!!!.45&amp;044-Y-$5T)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"2!)1Z$;'&amp;O&lt;G6M)%FO&gt;G6S&gt;!!!1%"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)245&amp;044-Y-$5T,GRW9WRB=X-!!!R.15^.-TAQ.4-A;7Y!!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!A!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!"!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
				</Item>
			</Item>
		</Item>
		<Item Name="Chip ID" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">Chip ID</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Chip ID</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="Read Chip ID.vi" Type="VI" URL="../Accessor/Chip ID Property/Read Chip ID.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;8!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!V!"1!(1WBJ=#"*2!"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=Q!!$5V"4UUT/$!V-S"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=Q!!$%V"4UUT/$!V-S"J&lt;A!!91$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A#!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
			</Item>
			<Item Name="Write Chip ID.vi" Type="VI" URL="../Accessor/Chip ID Property/Write Chip ID.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;8!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T!!!.45&amp;044-Y-$5T)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!V!"1!(1WBJ=#"*2!"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=Q!!$%V"4UUT/$!V-S"J&lt;A!!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A#!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
			</Item>
		</Item>
		<Item Name="Product.lvclass" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">Product.lvclass</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Product.lvclass</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="Read Product.lvclass.vi" Type="VI" URL="../Accessor/Product.lvclass Property/Read Product.lvclass.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'U!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!'J!=!!?!!!Q&amp;E^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9H!84X"U;7.B&lt;#"1=G^E&gt;7.U,GRW9WRB=X-!!#Z0=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7*Q/E^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;G.M98.T!!"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=Q!!$5V"4UUT/$!V-S"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=Q!!$%V"4UUT/$!V-S"J&lt;A!!91$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A#!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
			</Item>
			<Item Name="Write Product.lvclass.vi" Type="VI" URL="../Accessor/Product.lvclass Property/Write Product.lvclass.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'U!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T!!!.45&amp;044-Y-$5T)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!'J!=!!?!!!Q&amp;E^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9H!84X"U;7.B&lt;#"1=G^E&gt;7.U,GRW9WRB=X-!!#Z0=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7*Q/E^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;G.M98.T!!"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=Q!!$%V"4UUT/$!V-S"J&lt;A!!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A#!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
			</Item>
		</Item>
	</Item>
	<Item Name="Control" Type="Folder">
		<Item Name="Output Boolean-Cluster.ctl" Type="VI" URL="../Control/Output Boolean-Cluster.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#&lt;!!!!"!!)1#%$6'^Q!!B!)1..;71!#%!B!U*P&gt;!"\!0%!!!!!!!!!!RV25U:1,52%)&amp;""441A1U23)%2F&gt;GFD:3ZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=RJ0&gt;82Q&gt;81A1G^P&lt;'6B&lt;CV$&lt;(6T&gt;'6S,G.U&lt;!!I1&amp;!!!Q!!!!%!!B:0&gt;82Q&gt;81A1G^P&lt;'6B&lt;C"$&lt;(6T&gt;'6S!!!"!!-!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">1048576</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082143232</Property>
		</Item>
		<Item Name="Output Value-Cluster.ctl" Type="VI" URL="../Control/Output Value-Cluster.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#'!!!!"!!*1!=!!V2P=!!*1!=!!UVJ:!!*1!=!!U*P&gt;!"D!0%!!!!!!!!!!R6115UU)%.%5C"%:8:J9W5O&lt;(:M;7)245&amp;41T-Y-$5T,GRW9WRB=X-747&amp;J&lt;C"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!=1&amp;!!!Q!!!!%!!AJ.97FO)&amp;:B&lt;(6F!!!"!!-!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">1048576</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082143232</Property>
		</Item>
	</Item>
	<Item Name="Override" Type="Folder">
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Item Name="Get Chip Status.vi" Type="VI" URL="../Override/Get Chip Status.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;.!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!""!)1N$;'FQ)&amp;.U982V=Q"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=Q!!$5V"4UUT/$!V-S"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=Q!!$%V"4UUT/$!V-S"J&lt;A!!6!$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A$!!"Y!!!.#!!!!!!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!*!!!!!!!1!*!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082139152</Property>
		</Item>
		<Item Name="Get Bandwidth.vi" Type="VI" URL="../Override/Get Bandwidth.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;-!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!^!"1!*1G&amp;O:(&gt;J:(2I!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T!!!.45&amp;044-Y-$5T)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T!!!-45&amp;044-Y-$5T)'FO!!"5!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!-!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!E!!!!!!"!!E!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082139152</Property>
		</Item>
		<Item Name="Get Main Value.vi" Type="VI" URL="../Override/Get Main Value.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!']!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!F!"Q!$6'^Q!!F!"Q!$47FE!!F!"Q!$1G^U!'1!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!=1&amp;!!!Q!&amp;!!9!"QJ.97FO)&amp;:B&lt;(6F!!"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=Q!!$5V"4UUT/$!V-S"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=Q!!$%V"4UUT/$!V-S"J&lt;A!!6!$Q!!Q!!Q!%!!A!#1!%!!1!"!!%!!I!"!!%!!M$!!"Y!!!.#!!!!!!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!*!!!!!!!1!-!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
		</Item>
		<Item Name="Get Main Polarity.vi" Type="VI" URL="../Override/Get Main Polarity.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'^!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!B!)1.5&lt;X!!#%!B!UVJ:!!)1#%$1G^U!'A!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'E^V&gt;("V&gt;#"#&lt;W^M:7&amp;O,5.M&gt;8.U:8)O9X2M!"Z!5!!$!!5!"A!($5VB;7YA5'^M98*J&gt;(E!1%"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)245&amp;044-Y-$5T,GRW9WRB=X-!!!V.15^.-TAQ.4-A&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!1%"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)245&amp;044-Y-$5T,GRW9WRB=X-!!!R.15^.-TAQ.4-A;7Y!!&amp;1!]!!-!!-!"!!)!!E!"!!%!!1!"!!+!!1!"!!,!Q!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!#1!!!!!!%!$!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
		</Item>
		<Item Name="Get Pre Value.vi" Type="VI" URL="../Override/Get Pre Value.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'[!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!F!"Q!$6'^Q!!F!"Q!$47FE!!F!"Q!$1G^U!')!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!;1&amp;!!!Q!&amp;!!9!"QF1=G5A6G&amp;M&gt;75!1%"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)245&amp;044-Y-$5T,GRW9WRB=X-!!!V.15^.-TAQ.4-A&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!1%"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)245&amp;044-Y-$5T,GRW9WRB=X-!!!R.15^.-TAQ.4-A;7Y!!&amp;1!]!!-!!-!"!!)!!E!"!!%!!1!"!!+!!1!"!!,!Q!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!#1!!!!!!%!$!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
		</Item>
		<Item Name="Get Delay Value.vi" Type="VI" URL="../Override/Get Delay Value.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!']!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!F!"Q!$6'^Q!!F!"Q!$47FE!!F!"Q!$1G^U!'1!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!=1&amp;!!!Q!&amp;!!9!"QN%:7RB?3"797RV:1"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=Q!!$5V"4UUT/$!V-S"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=Q!!$%V"4UUT/$!V-S"J&lt;A!!6!$Q!!Q!!Q!%!!A!#1!%!!1!"!!%!!I!"!!%!!M$!!"Y!!!.#!!!!!!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!*!!!!!!!1!-!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
		</Item>
		<Item Name="Get Tx Algrethom.vi" Type="VI" URL="../Override/Get Tx Algrethom.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;0!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"*!)1R5?#""&lt;'&gt;S:82I&lt;WU!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T!!!.45&amp;044-Y-$5T)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T!!!-45&amp;044-Y-$5T)'FO!!"5!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!-!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!E!!!!!!"!!E!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082139152</Property>
		</Item>
		<Item Name="Set AFE1.vi" Type="VI" URL="../Override/Set AFE1.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;)!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T!!!.45&amp;044-Y-$5T)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!N!"1!%15:&amp;-1!!1%"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)245&amp;044-Y-$5T,GRW9WRB=X-!!!R.15^.-TAQ.4-A;7Y!!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!Q!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!A!!!#1!!!!!!%!#1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
		</Item>
		<Item Name="Set Bandwidth.vi" Type="VI" URL="../Override/Set Bandwidth.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;-!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T!!!.45&amp;044-Y-$5T)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!^!"1!*1G&amp;O:(&gt;J:(2I!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T!!!-45&amp;044-Y-$5T)'FO!!"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!-!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!)!!!!E!!!!!!"!!E!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082139152</Property>
		</Item>
		<Item Name="Set CTLE BOOST.vi" Type="VI" URL="../Override/Set CTLE BOOST.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;/!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T!!!.45&amp;044-Y-$5T)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"&amp;!"1!+1V2-23"#4U^46!!!1%"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)245&amp;044-Y-$5T,GRW9WRB=X-!!!R.15^.-TAQ.4-A;7Y!!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!Q!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!A!!!#1!!!!!!%!#1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
		</Item>
		<Item Name="Set Main Value.vi" Type="VI" URL="../Override/Set Main Value.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!']!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T!!!.45&amp;044-Y-$5T)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!F!"Q!$6'^Q!!F!"Q!$47FE!!F!"Q!$1G^U!'1!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!=1&amp;!!!Q!(!!A!#1J.97FO)&amp;:B&lt;(6F!!"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=Q!!$%V"4UUT/$!V-S"J&lt;A!!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!+!!M$!!"Y!!!.#!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!#!!!!*!!!!!!!1!-!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
		</Item>
		<Item Name="Set Main Polarity.vi" Type="VI" URL="../Override/Set Main Polarity.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'\!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T!!!.45&amp;044-Y-$5T)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!B!)1.5&lt;X!!#%!B!UVJ:!!)1#%$1G^U!'9!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'E^V&gt;("V&gt;#"#&lt;W^M:7&amp;O,5.M&gt;8.U:8)O9X2M!"R!5!!$!!=!#!!*#EVB;7YA6G&amp;M&gt;75!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T!!!-45&amp;044-Y-$5T)'FO!!"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!I!#Q-!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!)!!!!E!!!!!!"!!Q!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
		</Item>
		<Item Name="Set Pre Value.vi" Type="VI" URL="../Override/Set Pre Value.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'[!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T!!!.45&amp;044-Y-$5T)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!F!"Q!$6'^Q!!F!"Q!$47FE!!F!"Q!$1G^U!')!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!;1&amp;!!!Q!(!!A!#1F1=G5A6G&amp;M&gt;75!1%"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)245&amp;044-Y-$5T,GRW9WRB=X-!!!R.15^.-TAQ.4-A;7Y!!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!#A!,!Q!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!A!!!#1!!!!!!%!$!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
		</Item>
		<Item Name="Set Delay Value.vi" Type="VI" URL="../Override/Set Delay Value.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!']!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T!!!.45&amp;044-Y-$5T)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!F!"Q!$6'^Q!!F!"Q!$47FE!!F!"Q!$1G^U!'1!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!=1&amp;!!!Q!(!!A!#1N%:7RB?3"797RV:1"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=Q!!$%V"4UUT/$!V-S"J&lt;A!!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!+!!M$!!"Y!!!.#!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!#!!!!*!!!!!!!1!-!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
		</Item>
		<Item Name="Set Tx Algrethom.vi" Type="VI" URL="../Override/Set Tx Algrethom.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;0!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T!!!.45&amp;044-Y-$5T)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"*!)1R5?#""&lt;'&gt;S:82I&lt;WU!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T!!!-45&amp;044-Y-$5T)'FO!!"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!-!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!+!!!!E!!!!!!"!!E!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082139152</Property>
		</Item>
	</Item>
	<Item Name="Private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="Get AFE1 Address.vi" Type="VI" URL="../Private/Get AFE1 Address.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;8!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!V!"1!(172E=G6T=Q"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=Q!!$5V"4UUT/$!V-S"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=Q!!$%V"4UUT/$!V-S"J&lt;A!!91$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A$!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082139152</Property>
		</Item>
		<Item Name="Get CTLE BOOST Address.vi" Type="VI" URL="../Private/Get CTLE BOOST Address.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;8!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!V!"1!(172E=G6T=Q"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=Q!!$5V"4UUT/$!V-S"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=Q!!$%V"4UUT/$!V-S"J&lt;A!!91$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A$!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082139152</Property>
		</Item>
		<Item Name="Get Delay Address.vi" Type="VI" URL="../Private/Get Delay Address.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;8!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!V!"1!(172E=G6T=Q"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=Q!!$5V"4UUT/$!V-S"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=Q!!$%V"4UUT/$!V-S"J&lt;A!!91$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A$!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082139152</Property>
		</Item>
		<Item Name="Get Main Address.vi" Type="VI" URL="../Private/Get Main Address.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;8!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!V!"1!(172E=G6T=Q"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=Q!!$5V"4UUT/$!V-S"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=Q!!$%V"4UUT/$!V-S"J&lt;A!!91$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A$!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082139152</Property>
		</Item>
		<Item Name="Get Pre Address.vi" Type="VI" URL="../Private/Get Pre Address.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;8!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!V!"1!(172E=G6T=Q"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=Q!!$5V"4UUT/$!V-S"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=Q!!$%V"4UUT/$!V-S"J&lt;A!!91$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A$!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082139152</Property>
		</Item>
	</Item>
</LVClass>
