﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="17008000">
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">16769464</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorProtected" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">16448250</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="NI.Lib.ContainingLib" Type="Str">PAM4 Device IC.lvlib</Property>
	<Property Name="NI.Lib.ContainingLibPath" Type="Str">../../../PAM4 Device IC.lvlib</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!,N!!!*Q(C=T:3R&lt;B."%)&lt;H%K2,A5"_!C)X.%CAGR:22+[A&gt;GG*;F\"6/2+P]+]!)6F]1*O1#(&gt;N*17CE3,7U1"RX&gt;\;]&gt;/H"AEE,,LM?`_@X&lt;WO^XVC;2W*(*0GHWNP,6V#5V\U57:"D4&gt;&gt;&gt;F6;$_&gt;U:2.O:'9\:4?L&amp;0+6@J+,K_-8YP.NL^T`LX]?ZL=WP\3(V7DU=X_K'K2ND)W`-[^EL(W,^WND,6@&lt;@GT[`\IMM*M.KNW_$FD.LP2*W-UO^80^NXT2VN__NV?`[J;_TRGF\;^@V8H&lt;\B8^T^FF,.,_^LZK&lt;&lt;=H?&gt;LQ`W4]\@`?.@\`Q4`NYE5C:1)QAED.$V&gt;%_C"(OC"(OC"\OC/\OC/\OC/&lt;OC'&lt;OC'&lt;OC'LOC+LOC+LOC+HDK[I!O[I,-KQ?4"2%(2I%#1$)I%8Q&amp;0Q"0Q"$T=3M!4]!1]!5`!1YI%0!&amp;0Q"0Q"$Q-EY!HY!FY!J[!BV+**&amp;*(BS@AI&lt;QY0![0Q_0Q/$R-+1[0!_"-ZB2WCI!BDOF=/$Q/D]0$J4A]$I`$Y`!Y0.DC]$A]$I`$Y`!Q*+W+*ZKWI].$'4&amp;Y$"[$R_!R?#AN"I`"9`!90!90UYH"9`!9%-;%2H%1R"BE*"AX"I`"QY]90!;0Q70Q'$R9;9=ML5R,UX:U?"1?B5@B58A5(EK)QK0Q+$Q+D]*$76&amp;Y&amp;"[&amp;2_&amp;2?*B+&amp;"[&amp;2_&amp;21*2*G6[59MJ!*5E2&amp;"Y_[&lt;2IWC60**J[`GNW"V8S!:2]M#1@'-E(1@)'3^YYS2MC?;%F,[$EB:(]Q*)@2$+AZ)EF&amp;Z3=+%O_&amp;]3=G")49EQ-C1(2*XLNU(_=O&amp;QO:&lt;&amp;9S(Q_F_FU+J0*2-&lt;DM1S(1RE-"N,P^[88[[X@6M@ULB7L^V,.^;=Y/@P7@`@Y]^?8L]\\\Z_?0TEZ;`76H_,A1V%@03PKY`N&amp;82_C%;]@&amp;07&lt;&amp;U8^^F&gt;2H_[)JLGI(`X]54`]@F%@@PF9SSHDHB?LWH@DX3A(UGS]ZFGDXR;SY.Y!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.DataValRefToSelfLimitedLibFlag" Type="Bool">true</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!"]G5F.31QU+!!.-6E.$4%*76Q!!'L1!!!2S!!!!)!!!'J1!!!!L!!!!!B2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.162"-TAU-T1O&lt;(:D&lt;'&amp;T=Q!!!!#A&amp;Q#!!!!Q!!!I!!!!!!!!"!!$!$Q!P!!@1)!#!!!!!!%!!1!'`````Q!!!!!!!!!!!!!!!*+&lt;;15H.12#J'$^*/;B8FE!!!!-!!!!%!!!!!!7DY&amp;89(8[4)V*C(LH4N:BV"W-W9]!MA4JA!G9\0B#@A!!%!!!!!!!D_PE-8]\&gt;%CA8E/M;US7SQ%!!!$`````V"W-W9]!MA4JA!G9\0B#@A!!!"$$V^,8"6,SD7A2/QL#^;$?!!!!"!!!!!!!!!$U!!&amp;-6E.$!!!!!A!#6EF-1A!!!!"16%AQ!!!!"1!"!!%!!!!!!A!#6EF131!!!!!$&amp;E^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9H!84X"U;7.B&lt;#"1=G^E&gt;7.U,GRW9WRB=X-#"Q"16%AQ!!!!&lt;1!"!!Y!!!!!!!!!!U6Y:1R-&gt;8BT;'&amp;S:3V0261*4'FC=G&amp;S;76T"V"M&gt;7&gt;J&lt;H-74X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC=!^0=(2J9W&amp;M)&amp;"S&lt;W2V9X184X"U;7.B&lt;#"1=G^E&gt;7.U,GRW9WRB=X-!!!!!!!!"!!!!!!!!!!%!!!!!!A!!!!!!!!!!!Q!!!!)!!1!!!!!!*1!!!#"YH'0A9'"K9,D!!-3-1"98E-7EQ1#G'4YQ#$!!!(4L"YQ!!!!!!!!5!!!!$(C=9W"BY',A!%-'!!%+!#=!!!",!!!"'(C=9W$!"0_"!%AR-D!QX103&lt;'DC9"L'JC&lt;!:3YOO[$CT%$-!M3M-''AO`=!;3;1/&amp;1.*U3+[1I1HU!XBR^+0U!3!Q#&gt;%3EL!!!!!!Q!!6:*2&amp;-!!!!!!!-!!!(%!!!$Y(C=W]$)Q*"J&lt;'('Q-4!Q!RECT-U-#4HJ[4S-A$Z$"#A!W.1!!+AZGGBC2M?/*Q'"(L]]CVA@P-&lt;HGY8&amp;9(G'B5*JF+2&lt;B]6E5Y@&amp;::/&amp;J58@`\``^^]B/&gt;QNU@/=5=&lt;E.JO$K$Y=2=6$B!(3,/![0_"'3"6K/&lt;*&gt;!*FA&lt;1%EA;YA3DW"Q"6=425+$/5M"A?C$J]P-'%%?*1G"/CM,G8?0/&lt;XX!!036Q]#&amp;,&gt;[-'E.]\%51#B8A[1TAEDLNQ[)A"_9QH1!:W]M"]T1(X4RD)A")6A5Y4E%5MD$#,ONG//WC!Q]&amp;""%*F1+A+#&amp;5!IH;!88#%)_YQ00T8PL[XCR6)MS(&amp;C1-1.Y!94+B9DY'2A2(-:'29#V6L!W1T1=6A=1NC+U#$D:("(K\H.F2?!]E=&amp;U;9(I3[;C2X-)(.9'4YQQ!T$WA@6%]$V.UA-6_AW!%I/Q4)HA"F2Q0:([$M*#"&lt;!-L/",).'#(M0#A&lt;&lt;"E$&lt;NL:X]56+:D!_1+7.&gt;S"/$GXQ-"!L^J:*^.(R[H7ONL:RL_A*$-Z-5=BI#A`J43Z2#_H,#=TK=!+5TAZ*\'YW![CE=%.:&amp;:"=BE6D')!!+@NP&amp;%!!!$_!!!";(C==W"A9-AUND#\!+3:'2E9R"E;'*,T5V):E!!,)Q./%"\7`%;AOU2&amp;JL.%B;?\2E7BMU;&amp;!YB:/EV58PTZ``^`[Q(_+1&gt;+70GX(8DNV_P'UOGCQN0LT^,JI],2(=:C(=\#0_61K6&amp;P!%NH#&amp;!C%%2R.&amp;3S-0"00&gt;B1!;)/&gt;"QS/-7`\6,T*U;$%TN!,O&amp;X0@3;G8`&lt;I?9D!H(RB^(=M`&lt;VP6UA2=B/&gt;A$CTU!2E"A4%-MCC9/!M\_,+\J`17L&gt;A4AZN]$!1+`;73@42]?JVLL;W=;`I#1T/4&amp;();!I0[5UO51PJSQH-[H!#F-Y/3?RO.A/IJ%"!#\D6/E!!!!!!.E!!!%Y?*RT9'"AS$3W-&amp;M!J*E:'2D%'2I9EP.45BG1Q"5'X#!]L0G.1(?*CERHC1J0&gt;YW+1G?.#A=1MX3;K,TY]````^9$J5;^&lt;CS&gt;,CI]P?YACK-&lt;S(.H[19*?,"U_A#V_;BQ^!;S&gt;);I=(1ZM(1YMBA?;$YC%"&gt;`''D_WN@X&gt;A'&gt;R=#):+=$%,]&amp;CI$%G)"9&amp;EE="*T^86T2X1^3[Q&lt;%S18*:8L6TDK:0DJ/N&gt;&lt;6TD&lt;_"37:S9EZ#A&amp;&amp;_3GFS36[/75ZG5E&amp;6JD#S4G*R=6W%)U-$!#O=E7K!!!!!!!!$B="A"!!!!9R.SYQ,D%!!!!!!!!-&amp;Q#!!!!!"$%X,D!!!!!!$B="A"!!!!9R.SYQ,D%!!!!!!!!-&amp;Q#!!!!!"$%X,D!!!!!!$B="A"!!!!9R.SYQ,D%!!!!!!!!5!1!!!068.9*Z*K+-,H.34A:*/:U!!!!.!!!!!!!!!!!!!!!!!!!!!!!!!)$`````A!!!!=4-1M0N)K9HV?2+3]5CLC`&amp;,%,$A!!!!@````_!!!!"A!!!!9!#Q!'!#$!"A#!-!9#!!Q'!!!-"A'!/!9"Y0A'!@PY"A(`_!9"``A'!@`Y"A(`_!9"``A'!```BA"`^_9!0^_'!!^`"A!"`!9!!0!'!!!!"`````Q!!!A$`````````````````````]!!!!!!!!!!!!!!!!!!!$`]!$Q$`!0]!$Q!!]0]!!0``]0]0!0!!]0$Q$`!!]!```Q]0$``Q$Q!0!0$Q$Q$Q``]!$Q]!]!$Q]0$`]!$Q````!!]0!0$`!!]!!0$`!!$`]!!!!!!!!!!!!!!!!!!!$``````````````````````Q!!!!!!!!!!!!!!!!!!!0]!!!!!!!!!!!!!!!!!!!$`!!!!!!!!$=X1!!!!!!!!`Q!!!!!!$=!!$&gt;!!!!!!!0]!!!!!$=!!!!!.U!!!!!$`!!!!$=!!!!!!!!X1!!!!`Q!!!!T!!!!!!!!0U!!!!0]!!!!-X=!!!!!0`]!!!!$`!!!!$.X&gt;Q!!0```!!!!!`Q!!!!T&gt;X&gt;X.````Q!!!!0]!!!!-X&gt;X&gt;X````]!!!!$`!!!!$.X&gt;X&gt;`````!!!!!`Q!!!!T&gt;X&gt;X@````Q!!!!0]!!!!-X&gt;X&gt;X````]!!!!$`!!!!$.X&gt;X&gt;`````!!!!!`Q!!!!X&gt;X&gt;X@```^X`]!!0]!!!!!T.X&gt;X``^X0```Q$`!!!!!!$.X&gt;`^X0```Q!!`Q!!!!!!!-X&gt;U0````!!!0]!!!!!!!!!Q0````!!!!$`!!!!!!!!!!!0``!!!!!!`Q!!!!!!!!!!!!!!!!!!!0`````````````````````Q!!"!$```````````````````````````````````````````]("Q=("Q=("Q=("Q=("Q=("Q=("Q=("Q=("Q=("Q@```]("Q@`"Q@``Q=(``]("Q@`"Q=("`](``]("Q=(``````](``](`Q=(`Q=("`](`Q@`"Q@``Q=("`]("```````"`](`Q@`````"Q@`"Q=(`Q=(`Q@`"Q@`"Q@`"`````]("Q@`"`]("`]("Q@`"`](`Q@```]("Q@`"````````Q=("`](`Q=(`Q@``Q=("`]("Q=(`Q@``Q=("Q@```]("Q=("Q=("Q=("Q=("Q=("Q=("Q=("Q=("Q=("Q@`````````````````````````````````````````````!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!"&gt;-FV&gt;!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!"&gt;-A=("Q&gt;&gt;81!!!!!!!!!!!!!!``]!!!!!!!!!!!"&gt;-A=("Q=("Q=(86U!!!!!!!!!!!$``Q!!!!!!!!"&gt;-A=("Q=("Q=("Q=("VV&gt;!!!!!!!!!0``!!!!!!!!!$)S"Q=("Q=("Q=("Q=(L&amp;U!!!!!!!!!``]!!!!!!!!!-FV&gt;-A=("Q=("Q=(L+SM-A!!!!!!!!$``Q!!!!!!!!!S86V&gt;84)("Q=(L+SML+QS!!!!!!!!!0``!!!!!!!!!$*&gt;86V&gt;86US8;SML+SML$)!!!!!!!!!``]!!!!!!!!!-FV&gt;86V&gt;86WML+SML+SM-A!!!!!!!!$``Q!!!!!!!!!S86V&gt;86V&gt;8;SML+SML+QS!!!!!!!!!0``!!!!!!!!!$*&gt;86V&gt;86V&gt;L+SML+SML$)!!!!!!!!!``]!!!!!!!!!-FV&gt;86V&gt;86WML+SML+SM-A!!!!!!!!$``Q!!!!!!!!!S86V&gt;86V&gt;8;SML+SML+QS!!!!!!!!!0``!!!!!!!!!&amp;V&gt;86V&gt;86V&gt;L+SML+SM86WML+Q!!!!!``]!!!!!!!!!!$)S86V&gt;86WML+SM86USL+SML+SM!!$``Q!!!!!!!!!!!!!S86V&gt;8;SM86USL+SML+SM!!!!!0``!!!!!!!!!!!!!!!!-FV&gt;86U(L+SML+SML!!!!!!!``]!!!!!!!!!!!!!!!!!!$)(L+SML+SML!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!L+SML!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!````````````````````````````````````````````!!!"PA!"2F")5!!!!!)!!E:15%E!!!!$&amp;E^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9H!84X"U;7.B&lt;#"1=G^E&gt;7.U,GRW9WRB=X-#"Q"16%AQ!!!!&lt;1!"!!Y!!!!!!!!!!U6Y:1R-&gt;8BT;'&amp;S:3V0261*4'FC=G&amp;S;76T"V"M&gt;7&gt;J&lt;H-74X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC=!^0=(2J9W&amp;M)&amp;"S&lt;W2V9X184X"U;7.B&lt;#"1=G^E&gt;7.U,GRW9WRB=X-!!!!!!!!"!!!!!!!!!!%!!!!!!A!!!!!!!!!!!!!!!!%!!!"&amp;!!*%2&amp;"*!!!!!!!$&amp;E^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9H!84X"U;7.B&lt;#"1=G^E&gt;7.U,GRW9WRB=X-#"Q"16%AQ!!!!&lt;1!"!!Y!!!!!!!!!!U6Y:1R-&gt;8BT;'&amp;S:3V0261*4'FC=G&amp;S;76T"V"M&gt;7&gt;J&lt;H-74X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC=!^0=(2J9W&amp;M)&amp;"S&lt;W2V9X184X"U;7.B&lt;#"1=G^E&gt;7.U,GRW9WRB=X-!!!!!!!!"!!!!!!!!!!%!!!!!!A!!!!!!!!!!!!!!!!)!!!"&amp;!!!!+A!$!!!!!!4Y!!!-J(C=L6&gt;04"RF&amp;(`@-N":&gt;H&amp;H;&lt;?&amp;"LKT/,O32H!FM61#7MKAUC+FR4;!C'Q:+D5%&amp;"&lt;CQ:3;4$9BM3=4$M:.?NKEFZJ)IB=/RGS-S2SM&amp;R.&lt;T&lt;&lt;%IYEGW*2A:]@X@&lt;/T-`P(";V\?*HMPN`\]XO`^]WX!.[&lt;1I-L#^&gt;U)-*$@"D5I6&lt;2#%#[D9@=*Z)!Y3,Z#]CB2K,$+@[C=-_6*5U[?"5NQE@F.@A$P9X\RG@Q+`%)$^#V2GD%9,5[_"4NC0_-F"'ETZOEN7ILKB^#QAW3&gt;9V+Q5@]$85"%Y,;3KW`D73"S#'/5VO'9`-TKE3`&gt;&lt;@RD3SE7Q&gt;"VOI7J=QT'"&amp;4@].#EEH9*AN73-#1L&lt;#RM7'$`#9IQMIY34(&lt;C/,):!6-07+7J-ST$&amp;0,-'13-&lt;E]=D$\#;W&gt;AIKB"S5.E1D\Q/JYWUIF"`64$,;VN95QN$H9L![(J-RJPJ&amp;`I.SNLDOP@A5%30JNXEA&lt;NSD?0U3(Q(TL=1\B,C+-Y0/)$M&gt;6T45&amp;H*8L3UCR+8$7&amp;,JR#O16.A7XP#;!09&lt;5(G/1O82,=^`=]F*]:F&amp;=O#*/T]77FM4X&amp;K_OR/)TIB+,RUI(V#.L\E\;05X'N!%"Y/!,5*RE,U!KF5)#U.L1FR"[2-LE=8\7T7S?=-5GH';VG8M:G:.XXHC(MB@O=FG3L7+3\7@W%L/^NHQ\5,Z_I&lt;D%!PG_]0`,^Y3M.6]LEC^-12,?LS$&amp;4B0EE']35&lt;&gt;BKA,G*'*7#_3&lt;R$SXL4S6Z0OCL!F&amp;_EV;O@,[86^@,]$B&amp;,LS_O5)-@7LXD%?'Y_JCH]R@I0PY%/GYCK'/)@DI0((-@\(',_!_D.5M[]S[A/W:KO2_[F^;0;JPNH9`0T-H$AQPT+T'#];AV?(=[CU$CFTC58XM?D.]$3U1A?W&gt;OQMBK47(T48C'3LR(41:-)$0F6E,C&amp;KB:9=G_`S,42]K;NC5&amp;?U?\M_?62XU/ZR'(M],'5#FM1&lt;E,YW[-1'.T=XM5'UZ2I-)!=C=QF2GY^`N&lt;15UZ5K)54NXKZ0(N8:Y(HTJ"FT$*(WW!(&gt;4FW8[`%IUC!SFR#V_22S;46F8?H'\=`TXQ&gt;V&gt;HD"\($5U?&amp;"B*@LE"2G#+*,O7*G#V,AUI\AUI&lt;()$Q/]MY&amp;NXG!6O%"GJCEA]DZ$?0ZK7AV=*J7%-8QY^$,^P7!N;_D:&gt;]R(NT8\ML\+LL!X.E$O:UVFV5)7TU"3+U%[QLH^`?_$G/+RN%&amp;LCVYS`AA!DX7/78YW4GVO\O,,+%V]S%&gt;VTG8+N&amp;PSO:1Y_(W["6\"/-YAB.3RM5326F@!N&lt;8".[328)6DM#,,O5E0VWK"^/V60,4R9*YUTSXKBT6(%0]]@^?T?8^6X/ZI"K0$B/SVIC(PM_=_&gt;@W%!*9T5^X@]:KFB-X7486.!7.MB-VU\(HZRT0\9\H.PPZU1_/H0BS?KPEDO$"^HPXPC.-/I8O-98/22+OB(A&gt;`2+B6=-Q)CUZ!%'O_W6.J!Q&lt;P_0P5:AIP&amp;3&gt;`=&gt;,V=1?AK`*#&gt;YXP,CA,%`(W_&gt;7W,WK^,)Q+'P]]V)GBCFSVY^GP+&lt;UA&amp;4BP@_[?73YL@N4,U1N&lt;PZ]L?Q&lt;(\E:=H"4^Z(*445?!ED^E#-8@_`&lt;Z@T`!HZ%EIV+$E)`"F`6)3$XV8]P$#"Z8H[!\R?33#^?R1\T3@ZB_M@]0Y@U9"[JXI%2=F4[.-AX`!U!8('P!!!!"!!!!$I!!!!%!!!!!!!!!!Q!!5*%3&amp;!!!!!!!!-!!!"C!!!!=HC=9W"AS"/190L(50?8A5HA+Z!B`:?"7&gt;#0]4=$![?@Q'%AT3AA#237`=P!,KA.&amp;N9_IMP"!!7K&lt;)Q=EBS("4H!-BQN'AT````H_(LE'FT&amp;%2]Y5W770)=%!"2C'1!!!!!!!!1!!!!(!!!$PA!!!!=!!!!B8WZJ8URB=X2,&lt;G^X&lt;E^X&lt;GFO:UR71WRB=X.$&lt;(6T&gt;'6S!!!!L2=!A!!!!!!"!!A!-0````]!!1!!!!!!E1!!!!1!3E"Q!"Y!!$!74X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC="&gt;0=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:D&lt;'&amp;T=Q!!$V"S&lt;W2V9X1O&lt;(:D&lt;'&amp;T=Q!.1!-!"U.I97ZO:7Q!&amp;%!B$E.I97ZO:7QA37ZW:8*U!!!?1&amp;!!!Q!!!!%!!B&amp;.162"-TAU-T1O&lt;(:D&lt;'&amp;T=Q!"!!-!!!!!!!!!'ER71WRB=X.1=GFW982F2'&amp;U962B9E^S:'6S!!!!-2=!A!!!!!!#!!5!"Q!!$!"!!!(`````!!!!!1!"!!!!!Q!!!!!!!!!"!!!!!A!!!!!!!!!&lt;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6'FN:8.U97VQ!!!!'2=!A!!!!!!"!!5!"Q!!!1!!W-*&gt;IA!!!!!!!!!G4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B4'&amp;T&gt;%&amp;Q='RJ:725;7VF=X2B&lt;8!!!!!:&amp;Q#!!!!!!!%!"1!(!!!"!!$9QFWC!!!!!!!!!"J-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;5?8"F2'6T9Q!!!+U8!)!!!!!!!1!)!$$`````!!%!!!!!!*%!!!!%!%J!=!!?!!!Q&amp;E^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9H!84X"U;7.B&lt;#"1=G^E&gt;7.U,GRW9WRB=X-!!!^1=G^E&gt;7.U,GRW9WRB=X-!$5!$!!&gt;$;'&amp;O&lt;G6M!"2!)1Z$;'&amp;O&lt;G6M)%FO&gt;G6S&gt;!!!(E"1!!-!!!!"!!)245&amp;514-Y.$-U,GRW9WRB=X-!!1!$!!!!!!!!!"Z-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;%:GRU2'&amp;U96.J?G5!!!!:&amp;Q#!!!!!!!%!"1!$!!!"!!!!!!!*!!!!!!!!!"J-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;%:GRU2'&amp;U91!!!.Y8!)!!!!!!"!"+1(!!(A!!-":0=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7*Q&amp;U^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;G.M98.T!!!05(*P:(6D&gt;#ZM&gt;G.M98.T!!V!!Q!(1WBB&lt;GZF&lt;!!51#%/1WBB&lt;GZF&lt;#"*&lt;H:F=H1!!"Z!5!!$!!!!!1!#%5V"6%%T/$1T.#ZM&gt;G.M98.T!!%!!Q!!!!%Q&amp;E^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9H!84X"U;7.B&lt;#"1=G^E&gt;7.U,GRW9WRB=X-!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1!"1!/!!!!"!!!!&amp;Y!!!!I!!!!!A!!"!!!!!!(!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!59!!!**?*S&gt;5-N+QU!50?HUL8V%;[O#-NVW592G);Z3+E+&amp;9B#XAD':;(")1DI.,PU70]7P]$0U$\RZF),&gt;C"S9O@@-P@?=O1#/=7V'/!8/_D?2]BV&lt;=CM/X:7DRD+2`G-UW+9&gt;;3_81/=XUT):;L.H/QC%2-]=NIO9TY.%R!JYQ@@HR`U\!.;TJAO$8YL%&gt;Q3@TX)R@4'^GU\/D9GRHNH;-)[3'*M7',6L+*X-Z'KJ2-R$DW?F0)L^R&amp;;#O\;S53'A#:&gt;KPV#GB"5J*KCC9&gt;&lt;!00G%OPH'QFO6%S,UU-U5[GCAS6;OBQP]=TME/CSG\:*CC[F8FRR9+'%(&lt;9K=&gt;!XUE^'@`Z4X;)1+4;'DGBY;S/Z$*HB&amp;YTHWU;-YR1&amp;6L&amp;%OM-VM8PI:]D6J'"!(\*'Z#GLII!O&gt;ME-=Z7&lt;JVAGFV-!05BBQ!!!!!!!!HA!"!!)!!Q!&amp;!!!!7!!0!!!!!!!0!/U!YQ!!!'Y!$Q!!!!!!$Q$N!/-!!!#%!!]!!!!!!!]!\1$D!!!!GI!!A!!$[!!0!0=![1!!!*S!!)!!A!!!$Q$N!/-647FD=G^T&lt;W:U)%JI:7ZH3'6J)&amp;6*&amp;5VJ9X*P=W^G&gt;#"+;'6O:UBF;3"6326.;7.S&lt;X.P:H1A3GBF&lt;G&gt;):7EA65E"-!%Q!!"35V*$$1I!!UR71U.-1F:8!!!;N!!!"()!!!!A!!!;F!!!!!!!!!!!!!!!)!!!!$1!!!2E!!!!(%R*1EY!!!!!!!!"9%R75V)!!!!!!!!"&gt;&amp;*55U=!!!!!!!!"C%.$5V1!!!!!!!!"H%R*&gt;GE!!!!!!!!"M%.04F!!!!!!!!!"R&amp;2./$!!!!!"!!!"W%2'2&amp;-!!!!!!!!#!%R*:(-!!!!!!!!#&amp;&amp;:*1U1!!!!#!!!#+(:F=H-!!!!%!!!#:&amp;.$5V)!!!!!!!!#S%&gt;$5&amp;)!!!!!!!!#X%F$4UY!!!!!!!!#]'FD&lt;$1!!!!!!!!$"'FD&lt;$A!!!!!!!!$'%R*:H!!!!!!!!!$,%:13')!!!!!!!!$1%:15U5!!!!!!!!$6&amp;:12&amp;!!!!!!!!!$;%R*9G1!!!!!!!!$@%*%3')!!!!!!!!$E%*%5U5!!!!!!!!$J&amp;:*6&amp;-!!!!!!!!$O%253&amp;!!!!!!!!!$T%V6351!!!!!!!!$Y%B*5V1!!!!!!!!$^&amp;:$6&amp;!!!!!!!!!%#%:515)!!!!!!!!%(!!!!!$`````!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!-!!!!!!!!!!!`````Q!!!!!!!!$5!!!!!!!!!!$`````!!!!!!!!!/A!!!!!!!!!!0````]!!!!!!!!!]!!!!!!!!!!!`````Q!!!!!!!!(I!!!!!!!!!!$`````!!!!!!!!!@!!!!!!!!!!!P````]!!!!!!!!#(!!!!!!!!!!!`````Q!!!!!!!!)U!!!!!!!!!!$`````!!!!!!!!!I1!!!!!!!!!!0````]!!!!!!!!#F!!!!!!!!!!"`````Q!!!!!!!!2=!!!!!!!!!!,`````!!!!!!!!"7!!!!!!!!!!"0````]!!!!!!!!'1!!!!!!!!!!(`````Q!!!!!!!!:5!!!!!!!!!!D`````!!!!!!!!"G1!!!!!!!!!#@````]!!!!!!!!'?!!!!!!!!!!+`````Q!!!!!!!!;)!!!!!!!!!!$`````!!!!!!!!"JQ!!!!!!!!!!0````]!!!!!!!!'N!!!!!!!!!!!`````Q!!!!!!!!&lt;)!!!!!!!!!!$`````!!!!!!!!"UQ!!!!!!!!!!0````]!!!!!!!!*5!!!!!!!!!!!`````Q!!!!!!!!V5!!!!!!!!!!$`````!!!!!!!!$RA!!!!!!!!!!0````]!!!!!!!!5&amp;!!!!!!!!!!!`````Q!!!!!!!"1=!!!!!!!!!!$`````!!!!!!!!&amp;#1!!!!!!!!!!0````]!!!!!!!!5.!!!!!!!!!!!`````Q!!!!!!!"3=!!!!!!!!!!$`````!!!!!!!!&amp;+1!!!!!!!!!!0````]!!!!!!!!9;!!!!!!!!!!!`````Q!!!!!!!"BQ!!!!!!!!!!$`````!!!!!!!!'(A!!!!!!!!!!0````]!!!!!!!!9J!!!!!!!!!#!`````Q!!!!!!!"HQ!!!!!!V.162"-TAU-T1O9X2M!!!!!!</Property>
	<Property Name="NI.LVClass.Geneology" Type="Xml"><String>

<Name></Name>

<Val>!!!!!B2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.162"-TAU-T1O&lt;(:D&lt;'&amp;T=Q"16%AQ!!!!!!!!!!!!!!!&amp;!!%!!!!!!!!"!!!!!1!91&amp;!!!"&amp;.162"-TAU-T1O&lt;(:D&lt;'&amp;T=Q!"!!!!!!!!!!!!!!!!!1Z-97*73568)%^C;G6D&gt;!"16%AQ!!!!!!!!!!!!&amp;Q#!!!!!!!!!!!!!!!!!!!%!!!!!!!%"!!!!!Q"+1(!!(A!!-":0=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7*Q&amp;U^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;G.M98.T!!!05(*P:(6D&gt;#ZM&gt;G.M98.T!!V!!Q!(1WBB&lt;GZF&lt;!"K!0(9QE?X!!!!!R6115UU)%.%5C"%:8:J9W5O&lt;(:M;7)245&amp;514-Y.$-U,GRW9WRB=X-.45&amp;514-Y.$-U,G.U&lt;!!M1&amp;!!!A!!!!%&gt;1WRV=X2F=C"P:C"D&lt;'&amp;T=S"Q=GFW982F)'2B&gt;'%!!1!#!!!!!P``````````!!!!!4!74X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC="&gt;0=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:D&lt;'&amp;T=Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!%/4'&amp;C6EF&amp;6S"09GJF9X1!5&amp;2)-!!!!!!!!!!!!"=!A!!!!!!!!!!!!!!!!!!"!!!!!!!#!1!!!!1!3E"Q!"Y!!$!74X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC="&gt;0=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:D&lt;'&amp;T=Q!!$V"S&lt;W2V9X1O&lt;(:D&lt;'&amp;T=Q!.1!-!"U.I97ZO:7Q!&amp;%!B$E.I97ZO:7QA37ZW:8*U!!"M!0(9QFWC!!!!!R6115UU)%.%5C"%:8:J9W5O&lt;(:M;7)245&amp;514-Y.$-U,GRW9WRB=X-.45&amp;514-Y.$-U,G.U&lt;!!O1&amp;!!!Q!!!!%!!BV$&lt;(6T&gt;'6S)'^G)'.M98.T)("S;8:B&gt;'5A:'&amp;U91!"!!-!!!!$!!!!!!!!!!(`````!!!!!4!74X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC="&gt;0=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:D&lt;'&amp;T=Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1Z-97*73568)%^C;G6D&gt;!"16%AQ!!!!!!!!!!!!&amp;Q#!!!!!!!!!!!!!!!!!!!%!!!!!!!!#!!!!"!"+1(!!(A!!-":0=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7*Q&amp;U^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;G.M98.T!!!05(*P:(6D&gt;#ZM&gt;G.M98.T!!V!!Q!(1WBB&lt;GZF&lt;!!51#%/1WBB&lt;GZF&lt;#"*&lt;H:F=H1!!'Q!]&gt;D#8;)!!!!$&amp;6""441A1U23)%2F&gt;GFD:3ZM&gt;GRJ9B&amp;.162"-TAU-T1O&lt;(:D&lt;'&amp;T=QV.162"-TAU-T1O9X2M!#Z!5!!$!!!!!1!#(5.M&gt;8.U:8)A&lt;W9A9WRB=X-A=(*J&gt;G&amp;U:3"E982B!!%!!Q!!!!(````_!!!!!4!74X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC="&gt;0=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:D&lt;'&amp;T=Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1Z-97*73568)%^C;G6D&gt;!"16%AQ!!!!!!!!!!!!&amp;Q#!!!!!!!!!!!!!!!!!!!%!!!!!!!!!!!!!"!"+1(!!(A!!-":0=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7*Q&amp;U^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;G.M98.T!!!05(*P:(6D&gt;#ZM&gt;G.M98.T!!V!!Q!(1WBB&lt;GZF&lt;!!51#%/1WBB&lt;GZF&lt;#"*&lt;H:F=H1!!'Q!]&gt;D#8;)!!!!$&amp;6""441A1U23)%2F&gt;GFD:3ZM&gt;GRJ9B&amp;.162"-TAU-T1O&lt;(:D&lt;'&amp;T=QV.162"-TAU-T1O9X2M!#Z!5!!$!!!!!1!#(5.M&gt;8.U:8)A&lt;W9A9WRB=X-A=(*J&gt;G&amp;U:3"E982B!!%!!Q!!!!(````_!!!!!4!74X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC="&gt;0=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:D&lt;'&amp;T=Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1Z-97*73568)%^C;G6D&gt;!"16%AQ!!!!!!!!!!!!&amp;Q#!!!!!!!!!!!!!!!)!!!!H5%&amp;..#"$2&amp;)A2'6W;7.F,GRW&lt;'FC/EV"6%%T/$1T.#ZM&gt;G.M98.T!!!!,V&amp;42F!N2%1A5%&amp;..#"$2&amp;)A2'6W;7.F,GRW&lt;'FC/EV"6%%T/$1T.#ZM&gt;G.M98.T</Val>

</String>

</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">false</Property>
	<Property Name="NI.LVClass.LowestCompatibleVersion" Type="Str">1.0.0.0</Property>
	<Item Name="MATA38434.ctl" Type="Class Private Data" URL="MATA38434.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="Accessor" Type="Folder">
		<Item Name="Configure" Type="Folder">
			<Item Name="Channel" Type="Property Definition">
				<Property Name="NI.ClassItem.Property.LongName" Type="Str">Conigure:Channel</Property>
				<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Channel</Property>
				<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
				<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
				<Item Name="Read Channel.vi" Type="VI" URL="../Accessor/Channel Property/Read Channel.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;8!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!V!!Q!(1WBB&lt;GZF&lt;!"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.162"-TAU-T1O&lt;(:D&lt;'&amp;T=Q!!$5V"6%%T/$1T.#"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.162"-TAU-T1O&lt;(:D&lt;'&amp;T=Q!!$%V"6%%T/$1T.#"J&lt;A!!91$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A#!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
				</Item>
				<Item Name="Write Channel.vi" Type="VI" URL="../Accessor/Channel Property/Write Channel.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;8!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"6%%T/$1T.#ZM&gt;G.M98.T!!!.45&amp;514-Y.$-U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!V!!Q!(1WBB&lt;GZF&lt;!"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.162"-TAU-T1O&lt;(:D&lt;'&amp;T=Q!!$%V"6%%T/$1T.#"J&lt;A!!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A#!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
				</Item>
			</Item>
			<Item Name="Channel Invert" Type="Property Definition">
				<Property Name="NI.ClassItem.Property.LongName" Type="Str">Conigure:Channel Invert</Property>
				<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Channel Invert</Property>
				<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
				<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
				<Item Name="Read Channel Invert.vi" Type="VI" URL="../Accessor/Channel Invert Property/Read Channel Invert.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;?!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"2!)1Z$;'&amp;O&lt;G6M)%FO&gt;G6S&gt;!!!1%"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)245&amp;514-Y.$-U,GRW9WRB=X-!!!V.162"-TAU-T1A&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!1%"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)245&amp;514-Y.$-U,GRW9WRB=X-!!!R.162"-TAU-T1A;7Y!!'%!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!A!!?!!!$1A!!!!!!!!*!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
				</Item>
				<Item Name="Write Channel Invert.vi" Type="VI" URL="../Accessor/Channel Invert Property/Write Channel Invert.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;?!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"6%%T/$1T.#ZM&gt;G.M98.T!!!.45&amp;514-Y.$-U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"2!)1Z$;'&amp;O&lt;G6M)%FO&gt;G6S&gt;!!!1%"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)245&amp;514-Y.$-U,GRW9WRB=X-!!!R.162"-TAU-T1A;7Y!!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!A!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!"!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
				</Item>
			</Item>
		</Item>
		<Item Name="Product.lvclass" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">Product.lvclass</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Product.lvclass</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="Read Product.lvclass.vi" Type="VI" URL="../Accessor/Product.lvclass Property/Read Product.lvclass.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'U!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!'J!=!!?!!!Q&amp;E^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9H!84X"U;7.B&lt;#"1=G^E&gt;7.U,GRW9WRB=X-!!#Z0=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7*Q/E^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;G.M98.T!!"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.162"-TAU-T1O&lt;(:D&lt;'&amp;T=Q!!$5V"6%%T/$1T.#"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.162"-TAU-T1O&lt;(:D&lt;'&amp;T=Q!!$%V"6%%T/$1T.#"J&lt;A!!91$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A#!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
			</Item>
			<Item Name="Write Product.lvclass.vi" Type="VI" URL="../Accessor/Product.lvclass Property/Write Product.lvclass.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'U!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"6%%T/$1T.#ZM&gt;G.M98.T!!!.45&amp;514-Y.$-U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!'J!=!!?!!!Q&amp;E^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9H!84X"U;7.B&lt;#"1=G^E&gt;7.U,GRW9WRB=X-!!#Z0=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7*Q/E^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;G.M98.T!!"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.162"-TAU-T1O&lt;(:D&lt;'&amp;T=Q!!$%V"6%%T/$1T.#"J&lt;A!!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A#!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
			</Item>
		</Item>
	</Item>
	<Item Name="Override" Type="Folder">
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Item Name="Set Low Power Mode.vi" Type="VI" URL="../Override/Set Low Power Mode.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;2!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"6%%T/$1T.#ZM&gt;G.M98.T!!!.45&amp;514-Y.$-U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"2!)1Z&amp;&lt;G&amp;C&lt;'5P2'FT97*M:1!!1%"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)245&amp;514-Y.$-U,GRW9WRB=X-!!!R.162"-TAU-T1A;7Y!!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!Q!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!A!!!#1!!!!!!%!#1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342710290</Property>
		</Item>
		<Item Name="Get Chip Status.vi" Type="VI" URL="../Override/Get Chip Status.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;.!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!""!)1N$;'FQ)&amp;.U982V=Q"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.162"-TAU-T1O&lt;(:D&lt;'&amp;T=Q!!$5V"6%%T/$1T.#"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.162"-TAU-T1O&lt;(:D&lt;'&amp;T=Q!!$%V"6%%T/$1T.#"J&lt;A!!6!$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A$!!"Y!!!.#!!!!!!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!*!!!!!!!1!*!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082139152</Property>
		</Item>
		<Item Name="Get Bias Mode.vi" Type="VI" URL="../Override/Get Bias Mode.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;,!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!Z!)1B":'&amp;Q&gt;'FW:1!!1%"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)245&amp;514-Y.$-U,GRW9WRB=X-!!!V.162"-TAU-T1A&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!1%"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)245&amp;514-Y.$-U,GRW9WRB=X-!!!R.162"-TAU-T1A;7Y!!&amp;1!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!Q!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!#1!!!!!!%!#1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
		</Item>
		<Item Name="Get DCD Mode.vi" Type="VI" URL="../Override/Get DCD Mode.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;,!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!Z!)1B":'&amp;Q&gt;'FW:1!!1%"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)245&amp;514-Y.$-U,GRW9WRB=X-!!!V.162"-TAU-T1A&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!1%"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)245&amp;514-Y.$-U,GRW9WRB=X-!!!R.162"-TAU-T1A;7Y!!&amp;1!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!Q!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!#1!!!!!!%!#1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
		</Item>
		<Item Name="Get Adaptive AGC Value.vi" Type="VI" URL="../Override/Get Adaptive AGC Value.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;'!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!F!"A!$15&gt;$!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"6%%T/$1T.#ZM&gt;G.M98.T!!!.45&amp;514-Y.$-U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"6%%T/$1T.#ZM&gt;G.M98.T!!!-45&amp;514-Y.$-U)'FO!!"5!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!-!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!E!!!!!!"!!E!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
		</Item>
		<Item Name="Get Adaptive Bias Value.vi" Type="VI" URL="../Override/Get Adaptive Bias Value.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;)!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!N!"A!%1GFB=Q!!1%"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)245&amp;514-Y.$-U,GRW9WRB=X-!!!V.162"-TAU-T1A&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!1%"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)245&amp;514-Y.$-U,GRW9WRB=X-!!!R.162"-TAU-T1A;7Y!!&amp;1!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!Q!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!#1!!!!!!%!#1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
		</Item>
		<Item Name="Get Adaptive DCD Value.vi" Type="VI" URL="../Override/Get Adaptive DCD Value.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;'!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!F!"A!$2%.%!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"6%%T/$1T.#ZM&gt;G.M98.T!!!.45&amp;514-Y.$-U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"6%%T/$1T.#ZM&gt;G.M98.T!!!-45&amp;514-Y.$-U)'FO!!"5!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!-!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!E!!!!!!"!!E!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
		</Item>
		<Item Name="Get Pre-Emphasis.vi" Type="VI" URL="../Override/Get Pre-Emphasis.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;1!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!".!#A!-5(*F,56N='BB=WFT!!"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.162"-TAU-T1O&lt;(:D&lt;'&amp;T=Q!!$5V"6%%T/$1T.#"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.162"-TAU-T1O&lt;(:D&lt;'&amp;T=Q!!$%V"6%%T/$1T.#"J&lt;A!!6!$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A$!!"Y!!!.#!!!!!!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!*!!!!!!!1!*!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
		</Item>
		<Item Name="Get Output Swing.vi" Type="VI" URL="../Override/Get Output Swing.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;1!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!".!"1!-4X6U=(6U)&amp;.X;7ZH!!"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.162"-TAU-T1O&lt;(:D&lt;'&amp;T=Q!!$5V"6%%T/$1T.#"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.162"-TAU-T1O&lt;(:D&lt;'&amp;T=Q!!$%V"6%%T/$1T.#"J&lt;A!!6!$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A$!!"Y!!!.#!!!!!!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!*!!!!!!!1!*!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342710290</Property>
		</Item>
		<Item Name="Get Tune Ref.vi" Type="VI" URL="../Override/Get Tune Ref.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;-!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!^!"1!)6(6O:3"3:79!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"6%%T/$1T.#ZM&gt;G.M98.T!!!.45&amp;514-Y.$-U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"6%%T/$1T.#ZM&gt;G.M98.T!!!-45&amp;514-Y.$-U)'FO!!"5!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!-!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!E!!!!!!"!!E!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342972432</Property>
		</Item>
		<Item Name="Get Sel Ref Low.vi" Type="VI" URL="../Override/Get Sel Ref Low.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;.!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!""!)1N4:7QA5G6G)%RP&gt;Q"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.162"-TAU-T1O&lt;(:D&lt;'&amp;T=Q!!$5V"6%%T/$1T.#"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.162"-TAU-T1O&lt;(:D&lt;'&amp;T=Q!!$%V"6%%T/$1T.#"J&lt;A!!6!$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A$!!"Y!!!.#!!!!!!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!*!!!!!!!1!*!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
		</Item>
		<Item Name="Set Bias Mode.vi" Type="VI" URL="../Override/Set Bias Mode.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;,!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"6%%T/$1T.#ZM&gt;G.M98.T!!!.45&amp;514-Y.$-U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!Z!)1B":'&amp;Q&gt;'FW:1!!1%"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)245&amp;514-Y.$-U,GRW9WRB=X-!!!R.162"-TAU-T1A;7Y!!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!Q!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!A!!!#1!!!!!!%!#1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
		</Item>
		<Item Name="Set DCD Mode.vi" Type="VI" URL="../Override/Set DCD Mode.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;,!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"6%%T/$1T.#ZM&gt;G.M98.T!!!.45&amp;514-Y.$-U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!Z!)1B":'&amp;Q&gt;'FW:1!!1%"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)245&amp;514-Y.$-U,GRW9WRB=X-!!!R.162"-TAU-T1A;7Y!!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!Q!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!A!!!#1!!!!!!%!#1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
		</Item>
		<Item Name="Set Manual Bias Value.vi" Type="VI" URL="../Override/Set Manual Bias Value.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;)!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"6%%T/$1T.#ZM&gt;G.M98.T!!!.45&amp;514-Y.$-U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!N!"A!%1GFB=Q!!1%"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)245&amp;514-Y.$-U,GRW9WRB=X-!!!R.162"-TAU-T1A;7Y!!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!Q!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!A!!!#1!!!!!!%!#1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
		</Item>
		<Item Name="Set Manual DCD Value.vi" Type="VI" URL="../Override/Set Manual DCD Value.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;'!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"6%%T/$1T.#ZM&gt;G.M98.T!!!.45&amp;514-Y.$-U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!F!"A!$2%.%!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"6%%T/$1T.#ZM&gt;G.M98.T!!!-45&amp;514-Y.$-U)'FO!!"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!-!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!)!!!!E!!!!!!"!!E!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
		</Item>
		<Item Name="Set Pre-Emphasis.vi" Type="VI" URL="../Override/Set Pre-Emphasis.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;1!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"6%%T/$1T.#ZM&gt;G.M98.T!!!.45&amp;514-Y.$-U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!".!#A!-5(*F,56N='BB=WFT!!"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.162"-TAU-T1O&lt;(:D&lt;'&amp;T=Q!!$%V"6%%T/$1T.#"J&lt;A!!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A$!!"Y!!!.#!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!#A!!!*!!!!!!!1!*!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
		</Item>
	</Item>
	<Item Name="Private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Item Name="Get Biad Mode Address.vi" Type="VI" URL="../Private/Get Biad Mode Address.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;8!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!V!"1!(172E=G6T=Q"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.162"-TAU-T1O&lt;(:D&lt;'&amp;T=Q!!$5V"6%%T/$1T.#"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.162"-TAU-T1O&lt;(:D&lt;'&amp;T=Q!!$%V"6%%T/$1T.#"J&lt;A!!91$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A$!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!!A!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082139152</Property>
		</Item>
		<Item Name="Get DCD Mode Address.vi" Type="VI" URL="../Private/Get DCD Mode Address.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;8!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!V!"1!(172E=G6T=Q"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.162"-TAU-T1O&lt;(:D&lt;'&amp;T=Q!!$5V"6%%T/$1T.#"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.162"-TAU-T1O&lt;(:D&lt;'&amp;T=Q!!$%V"6%%T/$1T.#"J&lt;A!!91$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A$!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!!A!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082139152</Property>
		</Item>
		<Item Name="Get AGC Value Address.vi" Type="VI" URL="../Private/Get AGC Value Address.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;8!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!V!"1!(172E=G6T=Q"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.162"-TAU-T1O&lt;(:D&lt;'&amp;T=Q!!$5V"6%%T/$1T.#"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.162"-TAU-T1O&lt;(:D&lt;'&amp;T=Q!!$%V"6%%T/$1T.#"J&lt;A!!91$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A$!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!!A!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082139152</Property>
		</Item>
		<Item Name="Get Bias Value Address.vi" Type="VI" URL="../Private/Get Bias Value Address.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;8!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!V!"1!(172E=G6T=Q"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.162"-TAU-T1O&lt;(:D&lt;'&amp;T=Q!!$5V"6%%T/$1T.#"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.162"-TAU-T1O&lt;(:D&lt;'&amp;T=Q!!$%V"6%%T/$1T.#"J&lt;A!!91$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A$!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!!A!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082139152</Property>
		</Item>
		<Item Name="Get DCD Value Address.vi" Type="VI" URL="../Private/Get DCD Value Address.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;8!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!V!"1!(172E=G6T=Q"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.162"-TAU-T1O&lt;(:D&lt;'&amp;T=Q!!$5V"6%%T/$1T.#"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.162"-TAU-T1O&lt;(:D&lt;'&amp;T=Q!!$%V"6%%T/$1T.#"J&lt;A!!91$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A$!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!!A!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082139152</Property>
		</Item>
		<Item Name="Get Pre-Emphasis Address.vi" Type="VI" URL="../Private/Get Pre-Emphasis Address.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;8!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!V!"1!(172E=G6T=Q"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.162"-TAU-T1O&lt;(:D&lt;'&amp;T=Q!!$5V"6%%T/$1T.#"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.162"-TAU-T1O&lt;(:D&lt;'&amp;T=Q!!$%V"6%%T/$1T.#"J&lt;A!!91$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A$!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!!A!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082139152</Property>
		</Item>
		<Item Name="Get Output Swing Address.vi" Type="VI" URL="../Private/Get Output Swing Address.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;8!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!V!"1!(172E=G6T=Q"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.162"-TAU-T1O&lt;(:D&lt;'&amp;T=Q!!$5V"6%%T/$1T.#"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.162"-TAU-T1O&lt;(:D&lt;'&amp;T=Q!!$%V"6%%T/$1T.#"J&lt;A!!91$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A$!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!!A!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074278928</Property>
		</Item>
		<Item Name="Get Tune Ref Address.vi" Type="VI" URL="../Private/Get Tune Ref Address.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;8!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!V!"1!(172E=G6T=Q"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.162"-TAU-T1O&lt;(:D&lt;'&amp;T=Q!!$5V"6%%T/$1T.#"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.162"-TAU-T1O&lt;(:D&lt;'&amp;T=Q!!$%V"6%%T/$1T.#"J&lt;A!!91$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A$!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!!A!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074278928</Property>
		</Item>
		<Item Name="Get Sel Ref Low Address.vi" Type="VI" URL="../Private/Get Sel Ref Low Address.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;8!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!V!"1!(172E=G6T=Q"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.162"-TAU-T1O&lt;(:D&lt;'&amp;T=Q!!$5V"6%%T/$1T.#"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.162"-TAU-T1O&lt;(:D&lt;'&amp;T=Q!!$%V"6%%T/$1T.#"J&lt;A!!91$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A$!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!!A!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">2</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074278928</Property>
		</Item>
	</Item>
</LVClass>
