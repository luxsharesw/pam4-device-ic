﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="17008000">
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">16769464</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorProtected" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">16448250</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="NI.Lib.ContainingLib" Type="Str">PAM4 Device IC.lvlib</Property>
	<Property Name="NI.Lib.ContainingLibPath" Type="Str">../../PAM4 Device IC.lvlib</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!,:!!!*Q(C=T:2.&lt;B."%%:L%K2EA5!_!:&amp;0A+;W##G3,Z")8I\%KKZA6G35$6[Q9F&gt;8M/9'XM#_LG#B8-"&lt;R!,-GZ\WTS3/$2*)[8:Z0.^88@WGOTUCK:W,0*06M8:WM(6_G^&lt;W\DZ^6GOH3TPL`.[V\[`D-@`9_/X]_7P80]"`L-H"^J&gt;_66&lt;6YXZ6NEC^D"W`=_^F&lt;0SNW]P9_'80&lt;R\[V&lt;:#UT4F(D^H.-WD0BF6=^$0^N0TKZ[@LPXV,]O.TW.W;@X^+TN`R\W``SHDL.H;$]Z0W80XHK]&gt;^U`/X`(D82``%`T@*F)E5C))*YT1^(3L1!`U1!`U1!^U2X&gt;U2X&gt;U2X&gt;U1T&gt;U1T&gt;U1T&gt;U26&gt;U26&gt;U26@UV.%&amp;8&gt;!&amp;H65**A]G#II'"9*E5#4Y#HA#HI!HY/&amp;7!J[!*_!*?!)?5C4A#8A#HI!HY''9"$Q"4]!4]!1]F%IEE4I[0!%0Z=8B=8A=(I@(Y7&amp;+=8A=!'=SJ\"4"!RR4/?(Q_0Q/$T]&amp;)@(Y8&amp;Y("[("VM=(I@(Y8&amp;Y("['J&amp;8R2..W&gt;(AI)Q;0Q70Q'$Q'$[8&amp;Y$&amp;Y$"[$R_"B/D&amp;Y$"Y$QJD1+![#')/-"/0'Y$&amp;YO)D"9`!90!;0Q9/6&gt;MD3SL1U&lt;5?(2_&amp;2?"1?B5@BI91I0!K0QK0Q+$S5&amp;96(Y6&amp;Y&amp;"[&amp;B[F%Y6&amp;Y&amp;"Y&amp;2*G5[55JJAR5EB2"Y?'44IOG8@*%IKHHPW:X5#5@1-E(3`+"E8Q1*'_QZ)W4P#'3&amp;VLS!EJ?'-E0,0F"*!.+HFBS1=G*MO2\1=S*'4%F*M39'"&amp;$9N!/`=?*S_63&amp;IO&amp;T/&gt;TG=VG-JV/:4+:S(A]FN&amp;I*-0B5!;$Q?:N&gt;5(P7L&amp;_,^8]`HDV_?\W_PLS&gt;ECUV[N0F[W_^F/=@#HK]^&gt;&amp;@@']K/N4./,&gt;C[*_`\;I0`QK[JM^M6L&gt;V;^_`KB@@L_L4\^^L?7'=7_+&gt;?WH]7[5%VHNP/::I^^@3-H^!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.1</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.DataValRefToSelfLimitedLibFlag" Type="Bool">true</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!)FW5F.31QU+!!.-6E.$4%*76Q!!B11!!!2S!!!!)!!!B/1!!!!L!!!!!B2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;5?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q!!!!#A&amp;Q#!!!!Q!!!I!!!!!!!!"!!$!$Q!P!!@1)!#!!!!!!%!!1!'`````Q!!!!!!!!!!!!!!!"0WJ,CK(-:(E&lt;NR/Z'*X41!!!!-!!!!%!!!!!$)C\'G.-Y23,1%2:O+.&gt;9!V"W-W9]!MA4JA!G9\0B#@A!!%!!!!!!!S428;_2+:5'7\;`"&lt;Y_-ZA%!!!$`````V"W-W9]!MA4JA!G9\0B#@A!!!"$3Z]2/`4.GFR=7=Q/TZ3.N!!!!"!!!!!!!!!2[!!&amp;-6E.$!!!!#A!#6EF-1A!!!!"16%AQ!!!!"1!"!!%!!!!!!A!#6EF131!!!!!#&amp;%&amp;'5UEN4D=U1T24?#ZM&gt;G.M98.T!A=!5&amp;2)-!!!!#]!!1!'!!!!"5.M98.T$%&amp;'5UEN4D=U1T24?"2"2F.*,5YX.%-U5XAO&lt;(:D&lt;'&amp;T=Q!!!!-!!!%!!!!!!!!!!1!!!!!#!!!!!!!!!!!#6EF131!!!!)245&amp;044-Y-$5T,GRW9WRB=X-#"Q!!5&amp;2)-!!!!#E!!1!'!!!!"5.M98.T#5V"4UUT/$!V-R&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=Q!!!!-!!!%!!!!!!!!!!1!!!!!#!!!!!!!!!!!#6EF$1Q!!!!)245&amp;044-Y-$5T,GRW9WRB=X-94X6U=(6U)&amp;:B&lt;(6F,5.M&gt;8.U:8)O9X2M!&amp;"53$!!!!!Y!!%!"Q!!!!6$&lt;'&amp;T=QF.15^.-TAQ.4-(1W^O&gt;(*P&lt;"B0&gt;82Q&gt;81A6G&amp;M&gt;75N1WRV=X2F=CZD&gt;'Q!!!!$!!&amp;#!!!!!!!#6EF$1Q!!!!!!!!%11GFB=SV$&lt;(6T&gt;'6S,G.U&lt;!"16%AQ!!!!(Q!"!!1!!!&gt;$&lt;WZU=G^M%%*J98-N1WRV=X2F=CZD&gt;'Q!!!!#!!,`!!!!!1!"!!!!!!!"!!!!!!!!!!!!!!!!!!!!!!!#6EF$1Q!!!!!!!1^.&lt;W1N1WRV=X2F=CZD&gt;'R16%AQ!!!!(A!"!!1!!!&gt;$&lt;WZU=G^M$UVP:#V$&lt;(6T&gt;'6S,G.U&lt;!!!!!)!!`]!!!!"!!%!!!!!!!%!!!!!!!!!!!!!!!!!!!!!!!*735.$!!!!!!%61G&amp;O:(&gt;J:(2I,5.M&gt;8.U:8)O9X2M5&amp;2)-!!!!#1!!1!%!!!(1W^O&gt;(*P&lt;"6#97ZE&gt;WFE&gt;'AN1WRV=X2F=CZD&gt;'Q!!!!#!!4`!!!!!1!"!!!!!!!"!!!!!!!!!!!!!!!!!!!!!!!#6EF131!!!!!$&amp;E^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9H!84X"U;7.B&lt;#"1=G^E&gt;7.U,GRW9WRB=X-#"Q"16%AQ!!!!&lt;!!"!!U!!!!!!!!$28BF$%RV?(.I98*F,5^&amp;6!F-;7*S98*J:8-(5'RV:WFO=R:0=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7*Q$U^Q&gt;'FD97QA5(*P:(6D&gt;"&gt;0=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:D&lt;'&amp;T=Q!!!!!!!!!"!!!!!!!!!!%!!!!!!A!!!!!!!!!!!F:*1U-!!!!"&amp;U.I;8!A5X2B&gt;(6T,5.M&gt;8.U:8)O9X2M5&amp;2)-!!!!#9!!1!%!!!(1W^O&gt;(*P&lt;"&gt;$;'FQ)&amp;.U982V=SV$&lt;(6T&gt;'6S,G.U&lt;!!!!!)!"@]!!!!"!!%!!!!!!!%!!!!!!!!!!!!!!!!!!!!!!!*735.$!!!!!!%76(6O:3"1&lt;WRB=GFU?3V&amp;&lt;H6N,G.U&lt;!"16%AQ!!!!*1!"!!1!!!&gt;$&lt;WZU=G^M&amp;F2V&lt;G5A5'^M98*J&gt;(EN27ZV&lt;3ZD&gt;'Q!!!!#!!&lt;`!!!!!1!"!!!!!!!"!!!!!!!!!!!!!!!!!!!!!!!$!!!!!!!#!!=!!!!!!#9!!!!??*RDY'$A;'#YQ!$%D!R-$6R!&amp;J-'E0?"A9&amp;$A!%!9U='1!!!!!!!&amp;!!!!"ZYH'0A:2"AY-!"'1!)BQ"_!!!!3Q!!!2BYH'.AQ!4`A1")-4)QM)5";69U=4!.9V-4Y$)8FVV1=79A:E&amp;S*V#-;1_1:A+*1^1Q(I.)M5Q!#J^!.Y=@3D^!%A-!.9MJ)A!!!!!-!!&amp;73524!!!!!!!$!!!#,Q!!#FRYH.P!S-#1;7RBRM$%Q-!-:)MT.$!EZ[?E]D)!_1Q1I!.D5!!#I/:JI9E&lt;(DC="A2[`0)N9(\T'ZZO&amp;R7"ZBI6#;:3E7Y@&amp;:&amp;/(R774B;6&amp;X`_```@@)4H=,&gt;(TH&amp;('Z$;&lt;A[A_(%8&amp;1Y1"UCTA/D`A2EA6;DGS81#:9'U"*)'O)%I^A=!68%U6#ATF,!9(IA[@,T"B"(C5*A4IL#ZFXDTG^^Q!$UF=0!B3X?D"J$@/R&amp;%!I6Y/E-Y*)[\=/C)!@G-*U!'&gt;P,!@-U"^U]9S)!3&amp;9&amp;/%Z"&amp;,)QQC\L:DDNIA-0"112#:5#I#AB6!+*WA&amp;VQB#0O-$T]V\[_NYM63,-BR9E$%$?!'%SI7)_"E9%2T'2E7!N6;Q.E-U(&amp;9(%,9CN!AUU$39],6)S2Q2YO6AX8AR#\D?1/*L"[2I9`$$#^10OAZD2!X1U3]Q7+(9#S1Y$M#6"W.*$^!=J/!L)&amp;I/R-).O!%=,/A\,"FD(AJJX^86S2AAG=,W":IRH)3#Z),N/LTP42=1&lt;#[OJ3(RUQKN7B*B0)4H02Q9&lt;QSJ1[[7"$N5#8"N03L&lt;1TW5H(#5;GBNO9W/E5J&gt;6;AU0@*M$2VU4"*&lt;5M-TF6Q&gt;.:,[=M*T0*SN%NW&amp;08T^T%W33Y!CC5H*.98'S(3\'PI\_PM97"K4&amp;#Z:#*5"P`AJ,-Z-1=B9#C`*43Z"+)FQKM-)7B0BN'#9#B$:10=QM-$04]2L0C;&amp;9=T9I$FR5:!'GMXRU!!!!#)Q!!"I2YH(.A9'$).,9Q=W"C9'"G:'!1:WBA3-Z0376!!E_9'(##U-[HQ?&amp;BD7^%A(K\84E[84^U_`*U_L\J:OBE\#\^U(S-J&lt;0U1X@NG_&lt;D,*WV&lt;TJL6&amp;A[462?`0H``X`L!@\7V5"&gt;%#89Z&lt;P2Z5O!]D9KRVV5/"+!FI06&gt;&lt;N_;$[1!,&lt;Y4@0""+$&gt;H3Z!64RA61YIKBRAKBRQK_LV!%K#605'/H3'PPH%,_$A])F@U.'B/`:$]W'/TNA0X;6PGI^R&gt;*:#8#/$=/V&gt;I$'^V5"X1LT^JNO:";S%";&amp;E&amp;V!*W#N1*6!0%__B""3H*M#=GA"U;FKX1U*;JW-#7)9**M-%E7%#SD"V_X\I&gt;71#G?T[JD=!)O0*F,;,G3'N_3&gt;,'L`4R&lt;4/!#:-`5$N1.W'"RK0A+)Z,DY[^D!M`N?_PA@5TM$!C*1G1-':"21"C9'3DDJ5EIV"E)%6+F9(61NC,U&amp;CHU*C_T-CW+61^FOAK=R1-4-EN7&amp;)&lt;":'B$N!Q.H@R25^09/5.!-.3CZ),N/LTP42=1&lt;#[OJ3(RUQKN7B*B0)4H02Q9&lt;QSJ1[[7"$N5#8"N03L&lt;1TW5H(#5;GBNO9W/E5J&gt;6;AU0@*M$2VU4"*&lt;5M-TF6Q&gt;.:,[=M*T0*SN%NW&amp;08T^T%W33Y!CC5H*.98'S(3\'PI\_PM97"K4&amp;#Z:#*5"P`AJ,-Z-1=B9#C`*43Z"+)FQKM-)7B0BN'#9!"!%QR*#)!!!!)]Q!!&amp;\2YH.V9@X"56R6_&lt;`-3.O76&amp;QID[$"$:$*#H5CX!SVJFN,&gt;T?_1T19B#;W%,!V%YE3".)PZU=6A7-VFO2;54/EI)RU:'W&gt;QJ$-*R5L'45D(&gt;+3;;G:+(7J4*QI:-BJLN&gt;"BC/?=&gt;V\S8C#75PO(,MP^XHXXH/^_X^\\^J[M4V'5OF6:$Z^?I#B*KK)M5NK5GFX&lt;&gt;SCW6^J#:&gt;:8O@DTRMK+`?/,)4??ZR:Z%`'A,I,D]44P@/.I@W2Z?U+$G_V^'NQ5*2G;U$+OX*C=H)QFD+/*RD4D4'*M0MRNH/E@3VW!(".G2DQY0FO3'Z-OX1@"Z2/S2202C8BE8*:3=03+#)[+[)A)8II(.'__"C+-W-UZCC)L.6%_)P&gt;I)H)J8K"Z`4"UXIB&gt;R;%!T$AC]['V:VWE,&amp;W58R(82/T+T=F*W;S,[+AIPSCCQX'@\P8LR0%3"D9!]56:L9GK9?1I!)[%%4O*1UB]5?:!/RT0V\QZZN!2SN*&amp;Z)KMVE86K)A-C;I,]9$O,&gt;!JI!E$+E$VE'Q&amp;FR&gt;MO4MMV5/S!.I,NBG`3&amp;GI7L;;;A&gt;&amp;&gt;##?LXNT4.J(,,7$J(&lt;!FPMZ3_UAK2WQT&lt;DA&amp;L5*588/JP;$&amp;&amp;;&lt;),8H&lt;,G85VBNAN3?M]UYH$*4\6E2\&lt;;J`55+KTV,;LNNO6UJL09MK?WWT&gt;C:-F0N;6&amp;VSK&lt;W;5PN;6*\SJ:&lt;:[E^47J0W7;MO%6NFYC?N+F^V&amp;,&lt;27J0WH)`&lt;[HN)L5HJW;-&gt;_,7-G,T-;!F$.N:\AG,#/TF%S,SAX?.N%!9GK*QL&lt;(SV&gt;95;"-N[&lt;8'CK&amp;XD@\8F6JDXV$\C!JX_YT9I!%0UM&amp;_;%8P+'\8E&amp;M%4]CN&lt;F'&amp;0!6O;%L&gt;(]\42DQNU-JG.]C1'^WC("H]S,$_$BD7%5-7-JA;HL"L+,Y$BBPTE/&amp;@]_Z?Q]P%U$XP\D5U%50$R^$Q-$&amp;YE+%!PBR0S!#U(UH$?`=CQV_BD=&gt;I41N^)DAB^`F%&gt;&amp;Q%DYHI53$S_8"F@2`/^G.C/Q'NE03&amp;NA%=(:06]-E=N8S&amp;\E$66O,:@#`O&gt;L?)(*0LY2.'BM#&gt;@D*,C/&amp;4S&amp;""'BZH$@H)5(1($(`1E7&amp;9PXM.RYDBO`L&gt;;[AABL+0I7%B-=R$BHT9']?E(^K0J/&amp;X=Z(BQFT=)&lt;3GOXUC-C%L@;*]8%1/C`*$"QN^W3'@=23#O_'=FK7K#"[7,;K)(DKY6=V_1K64\(E9%DX%M%?&amp;2&amp;GNCKJ$"YP5\*"+O2X4O;W5G[^GZZCZ%2SS:`H6\!)T[]H:MU+T:[X&amp;I8R6Z"W7/&gt;"/:=(1`;LFN$W""5&gt;\PRO&gt;&lt;JJ2*BA(X#L7&amp;9GR&amp;_]R[YO@%*Y@?Z\\TX(`S$VGH'$=T^D-O)?RDH%&lt;Y_/-'RC,'0W-DT!_S(A`YW=:0]UYHT'6577]HGLCXQH\RM:34&lt;XP=0^.\L`/`6?ZX]@^HT/_G%JVF3S%#AHL+LE"TLXRO%_,&amp;WHW[KL^GEIHV4)Y[O0Q(&gt;.MF6:Y4M*RV#(+$YBIG_B]'I,D@L./[O/40Q4E&lt;8DU^&gt;!I&amp;&amp;A"M]!;R6%`H(BN?0L&amp;:O4_BH+B9OS9KL&amp;;Y'1^)'14")L?2AS(3CNA6FIP9$C5=$UYCE@M]5&lt;GYV0W/XT+RCA!$NI9"5S@\B&amp;CA-KAASK$![*H.UZUP*ZYJOI$/OGBJ*1YCC6#&lt;\W4ZV'W&amp;;-!+"2C^5YF'=3!TKB7!%=\S&gt;&amp;WYJGK'*)N2TMN2^O&gt;0/0*\'CHZ7C\5]HPEW=[#J/D,5Z(X=HM+'QZWO,E_8YS/QJ&lt;DL9YF8QT?;;DT?2IE^.244)\WGQZWO4E+&lt;9=&lt;&lt;9=&lt;8)K?@!72W8EK-4J[$\,5:HFK-4*=VVD2W77IR+HEB&amp;NJK.#=J4L&gt;.3HM;.#SV'OE[&gt;,9U?&amp;FK.=OZ+Y.#M]A7(.6/%V5)88[]0J?N&lt;3J&amp;FUP:K9Q^Z!G*CX9AIKRV&amp;]%$MJ)+2Z3]X(+Q]$Y-G7&amp;&amp;!-&amp;R4A-Z]`9-AE"FX%;+\L1F)6M5]8P42NT%/4:W*3E?YNV?F0H]EE3#K$W4S79QIIV,QB]\H^#Q&lt;A&lt;"3Q(CYII&amp;DTLD=$8C-'884[C%%8RWG[TB7UEBH]70.+&gt;C7:*8&gt;MB&lt;73'5[W:Z,9*18!&gt;YX-9*?]!HO*A6XC9L+\&gt;(+XB.BU\XK&gt;W$:9\N)N&gt;UO=\L)M&gt;_G7OS6/05NH=&lt;?9X#VUOPP!R?Y77_Y7/NH?=&lt;'\R:;\B5ZXA[\&lt;OUMD&gt;\L4X9^=\#\.=K=\X2VUM&lt;MUSZXOV,0(&gt;8NX&lt;H+H/&gt;W67/\=FDP.S&lt;&lt;+=O?WX'F/&gt;Z_:R:V#\G\=&gt;,D\J]LO&amp;-M&gt;"5ST86*:DW)^CR11G(I72_F:\-5Q@RDK"VE2RO-N&gt;OUG?HQ0WL:^9=8I0.`7D0"+RZDH4=^&lt;RI(P54H2VT[BDF7\T*/ZUG7?M#'8?@)7=0]R\K`BO!=9FT-O:6T%;$$/9:TESO6^RL]R8G:]G`%.RN]S`ILRFYQP-@[-]18'(T)_S`C-;OL^.P?`I:I6UN&gt;.SW/\',`-Y^8=L_2_C0M&amp;D)]RLO(R"\C`H0N,O&lt;_)U7#=Q_/4CNF`8W(`X,`-`&lt;=:XV"Q;[F#YNL*9LCAZ7N29&gt;X[WJI1TH?]Z8H.]_P'O=;:6X$J4F++#`_';H$B&gt;\#=Q(8P';&gt;%&amp;W4UNT5B$(3-?)9]Q`2L%S2?`:LUKS+'Q6C8RCB_(][1;'N7+@Z0HH\0!-&lt;X9`Q[[8@2\H,"\KK.&amp;\FKP;5OW-+2:4R^M1N`*`/ZP!%8&lt;NQTAW&gt;6N*88&gt;T5*P/Y@Q*`5NF:`K?K]`@?WHV\^Y]OQ3246&gt;M](`Z=JC_E?DAWGGP@`!8?3_.Z$()P88\&amp;&gt;(X&amp;0=_!L*Z3&lt;.`/X0_4^&amp;B$6@(7XR\/S&gt;'6L85FG$PRL&lt;9W5:.)\GPH@P)4LWNT-W\X`YUAEE(G\&gt;R35&lt;PQEN8ZST)(-A.8OK&amp;S\?FVG1WX53Z`_WD*`=(6[\I[^&gt;45\UINS6N&lt;PL;^\-NO@P\(I#[6L6O?MXNA%NWLKNTXVV,L:AI0_5("6FO?B6&gt;/2`T-,OD;UO\'O:FN^?FH$LOW2GE&lt;4UO\M7W_TM`_D$;!I`Q9!)C"D!!!!!!Y8!9!1!!!'-4=O-#YR!!!!!!!!$"=!A!!!!!1R.SYQ!!!!!!Y8!9!1!!!'-4=O-#YR!!!!!!!!$"=!A!!!!!1R.SYQ!!!!!!Y8!9!1!!!'-4=O-#YR!!!!!!!!&amp;!%!!!$V6T7#?3;CD#ZT5EY'34G&gt;!!!!$1!!!!!!!!!!!!!!!!!!!!!!!!#!`````Y!!!!'(5:,BAF*319)C5E'#5F*"AF'-19!!!!(`````A!!!!9!!!!'!!M!"A!AQ!9!A$!'!A!-"A!!$!9"A$A'!?$Y"A(\_!9"``A'!@`Y"A(`_!9"``A'!@`Y"A0``Y9!@`@G!$`@BA!0@Q9!!@Q'!!$Q"A!!!!@````]!!!)!``````````````````````!!!!!!!!!!!!!!!!!!!!`Q!!``$Q]!$`!0!0$`]!!0]!!!]!]0!0!0$Q$Q$Q!!$`!!!0!!]!$Q$Q]!]!]!!!`Q!!$Q$Q]!]!]0!0!0!!!0]!!!]!]0!!`Q!0]!$Q!!$`!!!!!!!!!!!!!!!!!!!!``````````````````````]!!!!!!!!!!!!!!!!!!!$`!!!!!!!!!!!!!!!!!!!!`Q!!!!!!!!X.U!!!!!!!!0]!!!!!!!X!!!X1!!!!!!$`!!!!!!X!!!!!$&gt;!!!!!!`Q!!!!X!!!!!!!!.U!!!!0]!!!!-Q!!!!!!!$^!!!!$`!!!!$.X!!!!!$``!!!!!`Q!!!!T&gt;X=!!$```Q!!!!0]!!!!-X&gt;X&gt;T@```]!!!!$`!!!!$.X&gt;X&gt;`````!!!!!`Q!!!!T&gt;X&gt;X@````Q!!!!0]!!!!-X&gt;X&gt;X````]!!!!$`!!!!$.X&gt;X&gt;`````!!!!!`Q!!!!T&gt;X&gt;X@````Q!!!!0]!!!!.X&gt;X&gt;X````&gt;``!!$`!!!!!-T&gt;X&gt;```&gt;T```]!`Q!!!!!!T&gt;X@`&gt;T```]!!0]!!!!!!!$.X&gt;$````Q!!$`!!!!!!!!!-$````Q!!!!`Q!!!!!!!!!!$``Q!!!!!0]!!!!!!!!!!!!!!!!!!!$`````````````````````]!!!1!````````````````````````````````````````````"Q=("Q=("Q=("Q=("Q=("Q=("Q=("Q=("Q=("Q=(``]("Q=(````"`](`Q=("```"Q@`"Q@`"````Q=("Q@``Q=("Q=(`Q=(`Q@`"Q@`"Q@`"`]("`]("`]("Q=("```"Q=("Q@`"Q=(`Q=("`]("`](`Q=(`Q=(`Q=("Q=(``]("Q=("`]("`](`Q=(`Q=(`Q@`"Q@`"Q@`"Q=("Q@``Q=("Q=(`Q=(`Q@`"Q=(``]("Q@``Q=("`]("Q=("```"Q=("Q=("Q=("Q=("Q=("Q=("Q=("Q=("Q=("Q=(`````````````````````````````````````````````Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!84*&gt;81!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!84)("Q=(86U!!!!!!!!!!!!!!0``!!!!!!!!!!!!84)("Q=("Q=("VV&gt;!!!!!!!!!!!!``]!!!!!!!!!84)("Q=("Q=("Q=("Q&gt;&gt;81!!!!!!!!$``Q!!!!!!!!!S-A=("Q=("Q=("Q=("[R&gt;!!!!!!!!!0``!!!!!!!!!$*&gt;84)("Q=("Q=("[SML$)!!!!!!!!!``]!!!!!!!!!-FV&gt;86US"Q=("[SML+SM-A!!!!!!!!$``Q!!!!!!!!!S86V&gt;86V&gt;-FWML+SML+QS!!!!!!!!!0``!!!!!!!!!$*&gt;86V&gt;86V&gt;L+SML+SML$)!!!!!!!!!``]!!!!!!!!!-FV&gt;86V&gt;86WML+SML+SM-A!!!!!!!!$``Q!!!!!!!!!S86V&gt;86V&gt;8;SML+SML+QS!!!!!!!!!0``!!!!!!!!!$*&gt;86V&gt;86V&gt;L+SML+SML$)!!!!!!!!!``]!!!!!!!!!-FV&gt;86V&gt;86WML+SML+SM-A!!!!!!!!$``Q!!!!!!!!"&gt;86V&gt;86V&gt;8;SML+SML&amp;V&gt;L+SM!!!!!0``!!!!!!!!!!!S-FV&gt;86V&gt;L+SML&amp;V&gt;-KSML+SML!!!``]!!!!!!!!!!!!!-FV&gt;86WML&amp;V&gt;-KSML+SML!!!!!$``Q!!!!!!!!!!!!!!!$*&gt;86V&gt;"[SML+SML+Q!!!!!!0``!!!!!!!!!!!!!!!!!!!S"[SML+SML+Q!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!+SML+Q!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0```````````````````````````````````````````Q!!"V9!!5:13&amp;!!!!!-!!*'5&amp;"*!!!!!B2"2F.*,5YX.%-U5XAO&lt;(:D&lt;'&amp;T=Q)(!&amp;"53$!!!!!P!!%!"A!!!!6$&lt;'&amp;T=QR"2F.*,5YX.%-U5XA515:433V/.T2$.&amp;.Y,GRW9WRB=X-!!!!$!!!"!!!!!!!!!!%!!!!!!A!!!!!!!!!!!!!!!!%!!!"?!!*%2&amp;"*!!!!!!!#&amp;%&amp;'5UEN4D=U1T24?#ZM&gt;G.M98.T!A=!5&amp;2)-!!!!#]!!1!'!!!!"5.M98.T$%&amp;'5UEN4D=U1T24?"2"2F.*,5YX.%-U5XAO&lt;(:D&lt;'&amp;T=Q!!!!-!!!%!!!!!!!!!!1!!!!!#!!!!!!!!!!!!!!!!!1!!!#I!!E:15%E!!!!!!!)245&amp;044-Y-$5T,GRW9WRB=X-#"Q!!5&amp;2)-!!!!#E!!1!'!!!!"5.M98.T#5V"4UUT/$!V-R&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=Q!!!!-!!!%!!!!!!!!!!1!!!!!#!!!!!!!!&lt;!!!!!!!!1!!!!M!!E2%5%E!!!!!!!)245&amp;044-Y-$5T,GRW9WRB=X-#"Q!!5&amp;2)-!!!!#E!!1!'!!!!"5.M98.T#5V"4UUT/$!V-R&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=Q!!!!-!!!%!!!!!!!!!!1!!!!!#!!!!!!!!&lt;!!!!!!!!A!!!!M!!!!K!!*52%.$!!!!!!!#%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!"16%AQ!!!!/!!"!!=!!!!&amp;1WRB=X-*45&amp;044-Y-$5T"U.P&lt;H2S&lt;WQ94X6U=(6U)&amp;:B&lt;(6F,5.M&gt;8.U:8)O9X2M!!!!!Q!!1A!!!!!!!!!9!!!!=Q!!!+Y!!!$J!!!"*!!!!6]!!!';!!!"V1!!!B!!!!,;!!!$&amp;Q!!!V-!!!/0!!!%.Q!!"+)!!!4&gt;!!!&amp;'!!!"&lt;-!!!8R!!!',1!!"GA!!!;D!!!'XA!!"RE!!!&gt;75&amp;2)-!!!!#E!!1!'!!!!"5.M98.T#5V"4UUT/$!V-R&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=Q!#6%2$1Q!!!!!!!2"#;7&amp;T,5.M&gt;8.U:8)O9X2M!&amp;"53$!!!!!@!!%!"!!!"U.P&lt;H2S&lt;WQ11GFB=SV$&lt;(6T&gt;'6S,G.U&lt;!!!!!)!!0]!!!!"!!%!!!!!!!%!!!!!!!!!!!!!!!!!!!!!!!!!!1!!!WV16%AQ!!!!!!!!!!!!!F2%1U-!!!!!!!%047^E,5.M&gt;8.U:8)O9X2M5&amp;2)-!!!!"Y!!1!%!!!(1W^O&gt;(*P&lt;!^.&lt;W1N1WRV=X2F=CZD&gt;'Q!!!!#!!$`!!!!!1!"!!!!!!!"!!!!!!!!!!!!!!!!!!!!!!!!!!%!!!1'5&amp;2)-!!!!!!!!!!!!!*52%.$!!!!!!%61G&amp;O:(&gt;J:(2I,5.M&gt;8.U:8)O9X2M5&amp;2)-!!!!#1!!1!%!!!(1W^O&gt;(*P&lt;"6#97ZE&gt;WFE&gt;'AN1WRV=X2F=CZD&gt;'Q!!!!#!!$`!!!!!1!"!!!!!!!"!!!!!!!!!!!!!!!!!!!!!!!!!!%!!!=C5&amp;2)-!!!!!!!!!!!!!*'5&amp;"*!!!!!!-74X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC="&gt;0=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:D&lt;'&amp;T=Q)(!&amp;"53$!!!!"M!!%!$1!!!!!!!!.&amp;?'5-4(6Y=WBB=G5N4U65#5RJ9H*B=GFF=Q&gt;1&lt;(6H;7ZT&amp;E^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9H!04X"U;7.B&lt;#"1=G^E&gt;7.U&amp;U^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;G.M98.T!!!!!!!!!!%!!!!!!!!!!1!!!!!#!!!!!!!!,A!!!!!!!1!!!KI!!E2%5%E!!!!!!!-74X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC="&gt;0=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:D&lt;'&amp;T=Q)(!&amp;"53$!!!!"M!!%!$1!!!!!!!!.&amp;?'5-4(6Y=WBB=G5N4U65#5RJ9H*B=GFF=Q&gt;1&lt;(6H;7ZT&amp;E^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9H!04X"U;7.B&lt;#"1=G^E&gt;7.U&amp;U^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;G.M98.T!!!!!!!!!!%!!!!!!!!!!1!!!!!#!!!!!!!!!!!!!!!!!A!!!KI!!!!K!!*52%.$!!!!!!!"&amp;U.I;8!A5X2B&gt;(6T,5.M&gt;8.U:8)O9X2M5&amp;2)-!!!!#9!!1!%!!!(1W^O&gt;(*P&lt;"&gt;$;'FQ)&amp;.U982V=SV$&lt;(6T&gt;'6S,G.U&lt;!!!!!)!!0]!!!!"!!%!!!!!!!%!!!!!!!!!!!!!!!!!!!!!!!!!!A!!"^E!!!RT5&amp;2)-!!!!!!!!!!!!!*52%.$!!!!!!%76(6O:3"1&lt;WRB=GFU?3V&amp;&lt;H6N,G.U&lt;!"16%AQ!!!!*1!"!!1!!!&gt;$&lt;WZU=G^M&amp;F2V&lt;G5A5'^M98*J&gt;(EN27ZV&lt;3ZD&gt;'Q!!!!#!!$`!!!!!1!"!!!!!!!"!!!!!!!!!!!!!!!!!!!!!!!!!!%!!!?M5&amp;2)-!!!!!!!!!!!!!-!!!!!+!Q!!:UQ?*TFH1&gt;]&amp;-8_Q'@&lt;X17#*!3%5#`B1%4!U'NI/8IH6/7"A1"*"))E),V)[!A72'G_^U=M$\%]9XN0&amp;!O#''Q]J0EUW,"A1V'2Z/\_MXNN:W`O&gt;G`=S=\H0@T=@0,"W&gt;P&gt;G?`M\TP-:H[!W^-KK2\P"=M^A%P[&amp;@YQV!/KZ::R!"RO\1#"0]X8AK3R8!8A;K&gt;S(N$&lt;-4&lt;J)^\,.@3!R.SSZIY-^Q&lt;Q%[TN?]_XB0^+T%D[&amp;&amp;;V*;8#,[PG!46TS_IG$X;6*\G?;OD;)!7`.2EU3&lt;K4]`)48%V_&gt;^R:5AB0#%J;S'6S;]Y,/(?;+*;ED]S:-\X%*@^N1GN(KP+6#2[1Z#[L-=^6@BX]2HDKVZ7P&amp;0=*&gt;P'ZY&amp;=#_*5N1'FJ;@CA:0^"T:8,[!+0%?Q!#08%@4'/K?5O;V4E+G_F(&amp;../1;?JV\Q0/YGXHPF;Z=0UB[;YCZ,AI@#YR9'&lt;FE_.HC=J\&gt;SX'?@@1;0AW8AO$Q0K/UK\_N)&gt;8S;?Y[L-;LE"=!"LO2^TUL9(6-=PF\?UT;HO#^ZO.Q8SB'V9(=U[]9F:=/@MTWA:5E:@QM1!W?5JAA,F=Y1AZX2!X9'VV`JD!4XBC11\IX&gt;/LXB&amp;A_H.]K;.&lt;_I?0I]:_%-Z\2:/56&amp;TLHT]B@E&amp;%^XZO95ZU4W5[;\,+'TX!&lt;SS22%1"UA3D=,R?IW,Q3000))&lt;!:9BA`N#1_N[SI0(:==O*NAO_?'WVU_;\D^?M(W=V]:-V.OQW&lt;&gt;_##Z.26S&lt;V4+S5J:83E&lt;S;5Q7SZ&amp;GVT;U_73X[_54SHF'X)J\6&lt;_\VGZ4#R3@D[AF%_&amp;2U-\/"K3E\3XCIS'DO;0BEY1UO7;U3$-F@I,KW+1X&gt;F`5(AU30VB#Y]8ZM9YJAM]:I6[.-"TT)8(L.)@$6UD2I.]L'9U&lt;.__(4E/^G;XU'A1/3YY'HS6PEIY'LSFPB;W8?*):4456)Y9#\P$8?;5M29PQX.U"%/6ZL=(GN^_#-M`"^O`DQ(_%\+,#_=[R]Q0^E*3-`]YB$7!KQ5(,\3:KG0ML\L,('V&gt;Z2Q]6[#L;Q-"&gt;!".A`@^2=?)2L;`ZD]I*XR19^!5^!":U4KGOM@_OH_=K1=,[!/'KLMCO1G]6&amp;[_6+7DPJ-0^P`M3X+E*D1*8]!&lt;\L,5\OCAP2:)]!I'"#`&lt;FRSFG_W(X78VOLL+5]-8H_S\!$,AC1-8`Q0WS$S0`5VFW.;9ZB_WADRMZ5@;#)`^#%3HB`J+/M+_5I]J_VPSG"KC&gt;'K&gt;1+@[,M/+%OCH/[DC[^2%D`U9&lt;*ZWLP*RSLFK+A"6ARV5"\3$.^:Y#$S"8#L.,=J@)DA0.`'0]?KA:IF4K:)GFUHJA&lt;YI=+4\?U.&lt;.&gt;=H6Y7F@N5``[UB"/!^PAXPM:;LP%[QV?7GL!][QRM]?0!AP%&amp;9YGZ1&lt;A/H5C6.,E0@HR^Z+@[K]I"0EUP^KH`_7^5X7!:PM,WL@++K%_6\&lt;!*[2)Q:T4X+T?"5KK4*:?A5\MCLQ6;6R[_RGP&amp;`K@I/D`PP=),K$O5`O$PEU$0)68!8EY?=!A\:&gt;Z1B/TL"0W2&amp;/'484J;\Q*%&gt;K*-,1[3LT#E`D)^"1&lt;M-.CPD6AC/WW?#$W0X5UU'IT9S7O^J\$K=8E-?M][2B&lt;.SZO58,^)@P0#33_%F.ZM)GNU%Y,.G3/$PRXPMTZ?5^29"=*=X'3R@1SXY44=%LD95/F[5HT,$E&gt;$B_Q%!HQ^5UXX+O.+"`P7&gt;^^D`G6MGJE)\K+9+4T;Q"&gt;1&amp;$WI?PC&gt;/H)"&gt;?08K6@^*9&amp;_N&amp;..,80,@*,1O+7\7*G.'Y'NL?/T`AP%"0L-369^U%@Z8"[QX]+WOY,=?&lt;N-\;8DBP.EZMZQDZE[@FV/=8TCHTMA_QTIY"]V:-(V?M8.E4PY]:`&lt;N/8/P#@^N%;S5%0I@94Z@AHT#Y-YLFZ3B.+&lt;0^SN]2(%24RE?Z4-2*'+@"^-C"YO`;O4T9*JWN"TU8YU1PBJ1!VZ.+PH64$6_.6/2KY&amp;2^'7._]J`5C'/K0MK@97YL`U6&amp;&gt;ZDJPP(J&gt;2],&gt;`=#:KH_:KHA_3,]!Z!QE8``\,")8NB;WCQ*B9&amp;"ONE/&amp;C0!4=S7"-822_M\ND]/WX!LU`6BLL&gt;TOTCH/,Z25:';_*CX'D.^C1O]U^SAK-V"8\4L3!$G?AELM#-VO`B;07;.6I40)EL)[=X]L!;!VLJ47]36W'[?!+]"\XJ4?)&gt;[CY/4(,MQ5E/LUR/\F,+"U,4EM1VS,1E?)FKB5J=&amp;WV;EB&lt;8N%3HQ&gt;:D:CKA"2A*OE;@&gt;32OU-Z59(U!=E#,'-&gt;MV-Z5Y$%NY$&amp;&gt;&gt;7=KC:MC:CLSM&lt;ITF=4.I9[*.F_J+0$^6P%:;+@-6XDFO"'QH_":+P&lt;[+CP?"CY5Y:VSHQR!N$&lt;1+3VU"BQ@'(#WL,T]O=ZW2LQW=2=EML&lt;+_?I!(D1(+8J3GQS3M@IZ+`,ZZ[];K:^2K`\Z&lt;V5^YR.X_U@L?*52J3DYV1G"!':A\\%/L")U)FAF&gt;)LJ/!.66@5:L\K$P+J+S2,XK*Y/IWP\HQY/PZD"&gt;AL5A^$&gt;*5/X'%+X6QP&gt;03:"V^91&gt;0@_NU/X\8]!OPMQU#7II%M9LHJ3/TY[-N^2=3&gt;%$X\]&gt;;OB'G)`[^=1Y2?I):0B&amp;"\2E04I'K)TW1]"7CX,04I?$7E;25/O.[!B.V3"BL1CVZ$7J"L3"K-BV6%.[;'5\L#'N.88E0:6IC%&gt;3$3E)Y''&gt;#,8E-[%'N*&amp;8U-_^0V7/5SL)7ZYFMI;PML+DNK)U,^+.74!@XN%'0A`%"%'93*#)E:$?MA;]C7%LI97OJZ6KC'^`NOB[`U`!&amp;U@$(1V9GP)&amp;;AB6Y);=IV;1\)^UO[3-M[`F#LU!E$YB2_*)#LND&lt;K5[D+)K/3?0CP(Q$^=*HCEBS*D@&amp;U91*`GB_L&amp;?'G@*M&lt;,(@]MP"G^'#]^D)HR.9-RXK'MC:9KZ37ZN,67SDSF@%AJ0V(72V/6=FT)![4(%!])XI&lt;;![4^6?%"UO-9$_!(ABX]R/AR84I1Y1%\Y*HW]Q.D(0.%B!@MA/@:(TR0$!_1HITUA"X"=]8Q!/EJ81_IX/C&lt;YSHETSE?Y!D+^TC`@&amp;@OY#I]\8DUXQ$N%[,,&gt;V_D]KUQ\]Q;[/SM[C.?V5?"([_4N0^Y0R(PY@:*E2[_!QR#"KN^-HU0NU`"?`A&gt;Y9F*N$&amp;KPQ8DY7PB0?C.58M/:IQG"=?II)S]1K6=L*2L1K01HBNBYX=%ZFKB&gt;&gt;5:JL_L9*_*5_]_9(FYQ3&gt;S_.DT)I&lt;==HD5VP#30?;9`)ABNRS?:WPQ0$''H,UA=MAN$ZYLRJ#TX[I\Z(TP?C["D&gt;\2SJ$TD[K2M&amp;.SSWSALXSK4)](T04W2.?&gt;F(_;D9ATV@5@BI&amp;`&gt;]^.\VN9L/G*]R\\_NQS56Y5KI;]H&amp;-4?E_G:F(I[N7L%9N#0(;J+&gt;&amp;DX_!O3_W%L/O!*(DGBC$2CH7&gt;D@\&amp;?P7[4G.Y@%PSKS&amp;?V[HOM7`SLT,6^(@EK_%7FSXM\,H`Q+O:P`:PSN6)Q&lt;=TLG3%X^3Y=K0KZT;KHVO(@`\^!X1N;4-]*`I?686Y_XXUHSRXKH7KOP`*EKR?3QJ5B1!P$A'=!A(/V!+].#L!T1Q#0#Q`.R,A:81!8MY5Q#O9!HCF"1#P)A8Y$AT!N@!!&amp;Q9"^HU$!5\2!HT&lt;HQ:Y4/(=3)$HU1'YC#G!CZE#?,Y&amp;!#]A"@BW$-!J+-#2=VJ@J==D@`T6;WP_;4UV:0@0?.JR&amp;;D&gt;.T$4\DP&amp;;`=.I^B^%Q.WHV9&amp;&gt;N_5X/[&lt;E&gt;J^=YT&gt;VU(NPI.3^F,+Q7'\PV\@\G]QX_Z&lt;E&gt;B^;Q+\&lt;U.O^T=3WHW'!&lt;PX&gt;I.WPQOV_]&amp;BO`@WBX;`$IUN1_H9`4![M75Y5\&amp;F"&amp;/R:;1&amp;M755;7Q:D9ENV_,FK&amp;@9\C(!G6K!_^#R_\ZU!-ZC#G!X5Q$XMQ$A`K1!$]!!8"=0=)?QX5/!5\1!&gt;[*D^ZXJ!.S&amp;+9#\-A6Q.QM!\EY+=!]-Q08U\&gt;\&lt;8`\YK[?C&gt;G`\*'4X_&gt;$OHU(MXH&lt;?4,PP'+@&gt;WT\&amp;W\XN#XW\NVWA&lt;`?WLYDNXP9VI&gt;X&lt;PM(9@8X%\GW`+V[PP%^DLR'S?^NXOH:P_]&amp;UO\@^3'$XNJ`CNXP&lt;*7+\N`V-:P?W8YT9`7JI^V_A&gt;F^$:@&gt;&lt;I&gt;W`D];7GH4M0IF/&lt;%FG+L&lt;59CKWJ&amp;A17WK4RJ9[G.D3!#^(P-LONU+).1#,&gt;/R?IA/QD3G!\5Q"\,!!Y!23A+NB!'[)"&gt;DWO]LONU+)59"N@V#R?^N6+A$&lt;+FA#W&amp;&lt;*%M!W4^5$&lt;0-3!GTT91"O:-$ON]I@@`8''LN`+'4X4GDX_;D&gt;0WSGX8?)V_Y@C7,X@T&gt;A^Y^8A&gt;U@),@\*UDN`EG-X4&gt;"\@[A5BZ6SB.BO`_(PNW8GG`XTZ,9`8-%&gt;P]]O&gt;W`1'DX,RKR__.AI[]V9P?W%SK\0Q.G_F,2W(+3CNX&lt;0K147UYR&amp;6N/-R6&lt;TFA17][3RJ:TG.DCR-P25:8&gt;HQ':7I#05&lt;&amp;\W^NU!#ZD#O$D4!(]DA5!PUM+](M9A.0Q!"^5W@U:E+)&amp;_"5[&gt;H_)$M#P-A8Q;UQ"`,I&amp;!,^"#P"B$-$J"OT_D0TR6W_KM@O]I.W$U^$OH;D&gt;&amp;ZBJ^_XDN@N&lt;I^D^(!.W0\=+\0YW=LO@2WLX22C\&gt;[&amp;WPUIJ.SPF`7'\8["P^QP.N`N&amp;*(;`G-$OFZ$&lt;`6*#OV^GQ/Z^&gt;;$&gt;,U$N`P[QX@P3I.V0270,$DJWPZ./&lt;.H&amp;6'T:T62MW7."&lt;(G1.,&lt;]&amp;2.&lt;GO(F;(09\C(!G6K!N^#R_[VU!,[,+9$P:ALA?SQ!_&amp;Z3A,&gt;B!'[/"XB6W/YBQ#F;A&amp;@4M@M3/A#P91LAN5Q"P-Y#A.?4!LQ"!`"V_H&lt;P3Z-``OIN.(&lt;@/G4X7TTNQ'H5\G]UU_\&lt;R7PX'6(MPLU"O_^9"8&lt;@C&gt;TO/Z0;@2?-X6_0WHV`J2SFF*0#&gt;N^&gt;X_YTT&lt;@\HC2WXYP!\HO4WXU@1LPP;]4O*U/\0Y4;`336X2&gt;!OX]=D3W4[&gt;D^&amp;$KRZ2;G9EM/5\&amp;FKA7R:2JJ&lt;-H&amp;R*;7?$E;J&lt;,\!ACR"O"M/H9`BA\!9ZE#?"R4!)_X!/!*J!"0R!"]!R\A`CK\,Y!1;Q!?3-@O"^%"?$"4!!^B#O#B&amp;A!]D"4AY2C!7RGQ_Q,ZY[`?'L6\[6,)\PN"O^_#W,XUCZFWXT:/OZ&gt;_R&gt;O^&gt;%8@\K7L^/V?KC#W?[G3U/YF$]&lt;OW["WL_1'MFWLF'FBOQ@[&gt;M_&lt;&lt;`=#C&gt;W,"(9PE&gt;O^D&gt;$O\5&lt;M`F'Q%4B1OU^4W8UJN0P,;'RJ3M@O882C3T/G9ENTJG,,&gt;2&lt;%FB;EM?6[4'SZ%3^(V[LMPB2#L!'Y(BW\4[5$=(WG!'\!&amp;-!.,1#Y%3H!D4%!:_!"LK[S_V))M1&lt;A'H4M`BI[!.&gt;E#O!EJA"/NA$A7K1!JW!!&lt;GP!\EPFD\^[/YX&gt;FQ&lt;NXF=*\2\&gt;DFZ[TES\TYD8\J_09P@`-G$X"[P!\F]GN`N83/X_%-&lt;OWS.W,\WLF%IG4_H,],[UL_P;P849&gt;,O8XC3Q?_F)`(9P(37W?_EN-LO8DBGR_Z_AX9^#\&amp;\[5G8XF7!G@,;L9YPU&amp;27\F\[G%FOE&lt;VC+,&gt;+X,-57[7,6RR&lt;J/],9)HW0C3U&gt;M()EH68:@38)V!,]%27\F`Z$"_#0G1,Y%[9!,L=!Y%^*!@Y-!X"(0-$PKOS_%AK%"O$XK&gt;C^^!%&gt;A%]Q"@#`G1,YJ!5!@UA+]#E-Q*U-W,XSH\^[ZY$&gt;9_J&gt;B,5OSP5#'MW`%=I!Q7]#1/CFS1$"(YO;!;+O)@@042]ZTV#G!PZNYP10@"FB_A@_/%;EOS$J(`BT=CH56=JMJ&gt;QKFW*HJ&gt;SLF"]LGFUPJ.H]?\LJ(`A0KC,^!X_#)0U$`_`YUT`Q*YH40`!@EK6`Y%`JGL?HJW_/N[EG`9.5TT`:^1TD+DQ8./E@J0L2*\M&gt;$5ZW(:"Y)]E@"/V5NU'5K7ZD!V.&gt;:R6-&gt;&gt;0)J\LJJ&amp;0&gt;JJA2WB7&gt;[L:6SB&amp;+72C?[D&lt;8H_KW-(_K?TX*6,=FQ64X"P+J&lt;CP#K7ZLMO105G(-Z!`30$J4X56U2'MR5[+VB#H27G;";*71CN9;D'BVQ]]52M2-`C".I$06H5A(Y*O9!HA35Q"0MQ$A8&amp;+!JW-!\IY(O'X-Z!^3&amp;TJ4X;ZU!/\/&amp;-"^G1)YSQ+!X;1!^]=!X#/_Z!_:[%+7_(()\:`R8.!E@R$,T8.\P&gt;107L=8T_0&gt;8PR=X_X&amp;,_G\P8C"W/X&amp;LQD&gt;8PQ;Y`9^%&lt;=8@V/MHF0+R*$&lt;CR&gt;VX6\]XH3X&amp;X]A=(PRR`D&gt;8PS*W/X&amp;3W2O,`Z-FPJ"3IS:_E'[BI\&lt;V[147:+9CCT*4%777B:%FB43S&amp;)&lt;%VF[Y&gt;7)CZH[12,IO,V)"W#*+9"N4!&amp;MNQ"A"SH!#2C!?W-"&amp;H_,G@J"P%,&amp;\=5`K!!M8G5*9,'#*9$&amp;SKI(7011!CR[-1$XC3`V1V_.W_].O8U_&gt;(MU^9/YTTSXVUP]%/(W$U&gt;R_]=-O0X_+H$\R]H&gt;`A#JWT_"=@MMV/V@5MID3PF"W/W@VH@\:]RX_V)3NX_7Q/W@)X@\ZQH&gt;`A7SR!`C"T%40YD`JO,WYEE[E?6$JC,,+;9CSWE,)MM:UMBS&amp;B.:X(AV/B)T]90Y&amp;B7X&amp;Y`2!@BNJA!O9QLAYR9!`!YJQ/^C!/[("`CFG)E@R*@JO0UL&gt;!!_R"4!LT)&amp;]'M7!0Q[+="P9!$O(V`CBQ%;N_]==HMH&gt;(MU]909T4SXVUP\%/(W@;/Y`4!$&lt;D_S#NR_!LH&lt;4S2V_ZMR&lt;D]1&gt;@O63LF*+&lt;?(X8['PNM8GO`WNZ'Y`3)#NV^-\P:,#.V_+6H;"X&amp;\T,10YA.UX(Y(H=CSE[H)MIOJS,,&lt;AMCSBT3S0)C*,)0Q;L1J:NI(]5Y[&lt;L_&amp;$M"&lt;G1,Y,K9!PNM#A/]B"@B?$-#$]1#PD*HW1&lt;S$DNOPJA.Q#6-!LW%+Y,57!,S/&amp;/$V')#(R*@W93DK^M,7I.O$U^$NU&lt;10QLXGO&lt;V?UA?NWQP&lt;]'YP\.:X?_&amp;B_GYP0%,M^M+DB'YP`"XD^M-1NR=_5;R??3&gt;(L"FS?_&amp;:8&lt;=88D&lt;&gt;\96$"'YP()X@\97XC.V??*P-\954:%E@R*IRET[)V^*R_\JU)EMK5Z(&amp;S62E3&lt;-AMK342B98*L)-R[M2&amp;T0JAUDHH2S2TDMZ)F0PZ)B-*7Q7,5D9,*)G&lt;":R#:N(9!%70IG:^%(YGIL&lt;#^^1!6CYS",!QC]M!3R=LHK!B&gt;])!2:Q\_3-D#`JQSC.WW?(X([,ZY)G[9-QVDSXVUPZ%/(WY[+Y`51$&lt;H^T&amp;&lt;D^*(+X`QOJWU`'O0VIV/VH+?6#J6Q&gt;&gt;PM=@&lt;?@:L\&lt;ZZ+Y`81#NZ^"\P9T#&gt;U_DSTFA\![:MI(91U6NR@7UIEM[ZC+,/O:CCQ&lt;,)AM'UEDSS:-:-H'K^(#G#E@B-65X&amp;Z91A@AJ5Q"P)QJA*&gt;&lt;!0!+5I"89A!?AQ&gt;Y6MS5$U)B(&lt;?@3Q@AWZA#?"Z4!"&gt;:!(!R+=$T-1#0D3`FQTC.W^=.O8U`[0:ISA=BV4SXVUPY%/(W^;/YP9&amp;^=I1KW#&gt;().]H2S$&gt;*U@!\:-T(H6\::]=I9&gt;3$AC\P@Y_/9,Z__1)*0PE#!4\Z!DE__1)B0PE#%&lt;WS=%E@"!'R%TY)!SCY`;$[537)5R&amp;FK&amp;-2::B&amp;E37Y;32:11GMET!KV'0G!E@B*ZUX,Y8(9"\-Q6Q([9!\GM"Q&amp;GE!,MR!%`%!^QW:M)(I4U&gt;N_^!"_#/4!(=C3G!/VM!="&gt;3A,NC!,YJPI10.[.OTZ],OLWP%LI^GP#"`Y^Z&lt;K_8\E(L^PT(?,@HT_O\0@]:@&lt;@H0S&gt;W?`Y,1L@HP]3Y`34%\@H,3OF4X,Z[?"`;LX8&gt;HP`7&gt;,@H,R+Y0@^&gt;`'\0@U`M^PQ0:'\0`UC7\E'I(D0&gt;AR!^F&gt;#@=HM[K91%JF)*#5SF%B)M3#5EE+93%H#JB0[#630?&amp;T0&gt;A]$2=8O?$M!#5Q#,4!%M71#QD22A"Q&lt;AS8C!,]&gt;-^]$`2M8N_&gt;_J!-R@91FA`A_7!/;P6DX!@!5BQ(QF"O!J];6\O-6IOI&gt;2(PYJ?((]&amp;!$Y43!8^=#8IC:[['+)@_D]1QO,H6G&amp;O19S%]"?/IDUEE-_84XIJSZ1,;+8R&amp;#LP[8KA;/KHY_I@HY4J?&amp;F`XE7)$3)I)6S(HA'5]Y$WU^/ZA&lt;(1(86/+_LH/=7!_.=$)\T-&amp;'(-$EN7M&amp;/US8K6@6MY=VG3XA`*4H.VQL_3BQ=M@P&gt;:5[ZCXEY4C%0E^#*6'F5&amp;C9::+(GS(G&amp;O@/H&amp;&lt;?:N7$;L*SC)E.TKW@&gt;:9[WLP)=?.&lt;!4+12&lt;-&amp;-Y)IR"8D/0R^,]!][?:K2%7SCSQ/R]A_&lt;#-EJO-L@0F0B+!L.D_XJ`PER&gt;R9W4W.B,D)`NLOCTY`&gt;/J-GO7U3_O&lt;-S&lt;U^0\=Y4\^6MDV3/X_'&amp;P8-NV`ACE,:(TI1TXT&gt;BL-`&gt;-40@.M)M`2GPF)HT-SX(&lt;Q(0:;FTJC:\T1E0YP54SF(+O8.3DF$+7^4SK6+O5YJ\QHHBOA7-3_7&lt;Q0*$&gt;'D+P+T3*G9K&lt;+1"VI+C[)T,`7-G#KXB'@K,O4&amp;/+:8R&amp;3Z*4R0^_"Z9ES6J&gt;[25_77Q80&amp;3B@2*]:5_6.@":QK?WP\LP//&amp;L.5_6F'QHY+CJKX0H`&amp;[R;GITNS&lt;IMK;L/.`//5[X$[.;%"K%X4%K7DTHOE_\!?6]?AR[5D(L=Q:%\3&gt;J9]4LK@*9_4(P"@D9\(O?\CT.WT=Q@JHJW8-3K8C^^U&gt;JU+]5V?.X]&amp;28S$S9BX-I4Y2DK)&lt;W)+]=V-)8[H.9BP)58]0!&lt;R[8D%F[I1&lt;QQ2XY1CPNRER$M;1HQ&amp;(=28-I8Y+K91P]-;R&amp;?4)HY=A`A-0/+XB2(H`A%2&lt;YQC8G1SYBU-)6Z-"`(Z4#'_A#H%&lt;\=']97EC,_)18QG(P%:+M4&gt;8D@X$R4R0*-2&lt;W])]8Q[C"=QB@CN4#%_SRL%:Z/O#T4!)*[(2`TG-/,A.%4=D3,_&amp;Z-2&lt;W=)]=FU%*`#&amp;/+X-)6YDD7)4S6&amp;X)Z"0"_0_%A6YA6?&gt;_$X&gt;%/)DT9:]&lt;;'%-_GA`A9JB!@SR4CY[R"@$TJYNAP'-1,])DX#S0O]U$%#V$%"ZC-?)9BR!@318Q15YA0:ALR)&gt;9A0J15]?U1]4$=N_KN`(JL_3LF4`A,HN#]-#F`T3T.\MKWQ'L7.A#YM`R!&gt;(@FR/CL74KP!):@X3H-V2]0?2[R2J4.F*-Q3VS"SQRNJFSL#D:44IGSR-7\V3MLW-W5;_/7O-*:\+0P+F-(M]1V'VHC%JMK:2OF\+K5&lt;K655J[,.SHF&gt;+7='^ZKO6\E%F=AVXVIK_8[6&lt;(%*4&lt;!,((RP5&amp;,@F4UZ3KR)8[*C_]&gt;YZB'E5N=]$T&gt;A_?*M=1F.M9M=180&amp;7PXZ3;[&lt;Y.[?`LG?J@S\S.,8/,=5,2S=Z?^=`A?[!:8U4/C$T4[`D5=EKL&amp;,@5W6U7GP:(5&gt;-L-5"A1CVE+3O*]FI+3O-##&lt;;ZO*^XG;C&amp;'OO&lt;!O(4%[@$^J.QX/*,G7"(]-&gt;X@&amp;O'PA(B06_&amp;^Q$O(1X_.2JRJ'N[&gt;)P(/IY.X0F.Y&amp;T#&amp;^[U7Y$W,&amp;/`:',Q,Y]0\*B8?O2$P!SD?EUT$OW-EXH_BA`&gt;EJP#?QB4?NVC!&gt;QYJXF-R?-_.$_]2+LQ&lt;1LRT5&lt;R(G9:XBUC]2^0"/ZMJP-=QB@&gt;9#`!?2YLX?!T?N]7(NTO-.TA*]7[)YNX@.,T&lt;2_)^A!\?!ZH#?R"4?!_W!/]BJ(A0R?!^,T[]O[LQXOS&gt;!U[C?(=X$?^WE8DXI).X*F.Y^W1+\VY7Y.W&lt;&amp;/]_',S,YM/\D1JP.]2\-YJXBGFYNYX%OSU&gt;P.MRB8&gt;\JP$O9!(?(5HR\I4"OTA_P*O']@:61LT2FR(%:K&lt;BH2'*&gt;X-[?&amp;`(&amp;.YNG-,\?APQ&lt;EG+^QU9P/=&lt;QROTE$5?]DU__#O-#T2&lt;$]Y/,&amp;C.!)$&lt;RN&gt;$NR[]/`K#61.$#V9F[8XT=QT]3FK?2\AHSF;$^_&amp;7L/KB7QX?8Q6&lt;$4Y1&lt;=7KNOZ7ATOQ+V&lt;V^&amp;]]W9F:M&lt;I&gt;7&lt;%3(F0+5K6]23G0+?6*J3R8SG_6]N@Q2I2\-#N7N&gt;'.#0^;&amp;3N7QN^Q+V:*I#7@JGZ5T7;"`R&gt;FR3IJRD&amp;\-3N73@#9.0W^#2`#L6AF[?^.O#`'CN7X0J_]9H7@&lt;Z(XX`T&gt;S)K6]'MI-/XCDHL@Z"01\2^_DRK9_BI-4!HSG-1P71F8K%1GY1_7)J.QF;8)*&amp;29M!N%*?GL="Z-:&amp;I9FXA*X[LY8O.^ETO+]PW&gt;?8R(LFE*X^0B_Q?G_0[2+&lt;Z`MI$P3[2]`YTB?V&amp;]@*?L_-[#@+^"_@\50,YD&amp;[W%T_DQ`4F4@(`"&amp;.^@7M$X"6+_P],QP4A_PE_K_/9AXVEIX[@-YTNSV5IY49@P-UTR@:9JPM^:Q0&gt;(J(T`"]0XEPDY0B&lt;G'RS#@(-IXW8G]2WZ&lt;#5=J]0X/UTR`3Z4@,^H!&gt;`PE`,^!9&lt;PJ@(R`9K+\W8?.]%BF/^8T?-\=NV+?)U/X[]TR@=&lt;40&amp;^W!+_XS4F_QC'\W8R]6WKYLM\Z(M:SP&gt;TZP%&gt;O8!F0%_(\R?9YPN&amp;JPD_JQ6]`YO5\Z=Q@#_0D_`(QHT\`I"]&gt;U@ZXG]?XZ%L6],D&gt;0A_Q"4@4T$&amp;^Z-7]0U5+&gt;^09`B?1&lt;RSN&gt;N8+8`EL_H8$;R5,VVF?U!D`]K1@(W\!/"']#/263(1./L?CM:W_CZ*(Z;40]@)/A:Q23Y/S6N30MU0V6M=!MUQOU]_#W^&amp;L[F"]YD&amp;)&gt;B'KZ$6)6#EF%]KZ4&gt;SS&lt;G5=IJ3\F4+$Z5.\:.#KU/A.&lt;)[&amp;,Q0^?I1'&amp;56KU.A#GZV;#$9Q5_-PN)$&lt;IF9(&gt;I"T\3@(RDDG*S)V;%&gt;]$T\A_?*M4I%JE;O$OU)`T*;V.5B-%V`&gt;`MWPDEAB4_H7BX+B@XE8[`V:8)6PF/"8T-,J:/I&amp;8W^NK@"\514:/:RCU.Z(DY&amp;PU4,8WMA9U3^+MA9E5K?-;)_;=;)"LB2?!?;-O*[J?SAF,X#+3-;[[?-=*K@-C+.*'6%/E(+C+&lt;E+3.=B#EDGBF)'?'Z"$:[2S-J)`B?5+G3`#ED0"YQU^M4X&lt;#]$Z75%8R@[&amp;+JZG^9HM733`&amp;OFFS+\W@"BO8^38^B@5#%3]&amp;(SWL]FPM&gt;1A3H1))TN12XIJ)TAO^-B_!O4"(=F3G#OVF!=(&gt;3AHPA##\"%XR^E'$@.Z$A&amp;#X".^"*'N'+$M'NG3+Y$6-%XWA"Q2GE",@&amp;%&lt;R'0WO%RS.`!P88IO^==B_'(0Y:XSGO!H&amp;Y\L3*$K^^!3&lt;0QZX"/TTXE&lt;\$=R`4&gt;XDO%W+(Z]I*(:Y\DX0Y&gt;9D$=T]IZ2_+QYMBB_=_VX6Y\EP4(:[\1/$QX&amp;@R/TTX.&lt;($=^_1/4TXL1'(^X;$$L],&gt;8AR\0$?`N$BV[(RQU&lt;(Y?VUYI?$K@C2Q&amp;4]K':"`+B/'D]3=@&amp;D0&gt;;!O$`#$A]*TN11T&amp;61=8COEAL"H)=FADEP3Q2T0AM)"K1%=TC#._!*`C(M]*$A&amp;#X"0V&amp;R?/Y3(9*`:ILA8ZAC_(,6%]T^3EAQ^RO/Y)X[$O`N,X]#^4&gt;J((ZHS/(TI=-`ATL]&lt;B-&gt;8PO3,\SB06%=`G]'((ZP&amp;4D]1_1/PY`5Y2`'/@RGV/'6XZ,CF._3YI[&amp;(@YR@9@@&lt;\\$0U\C]!=)(0Y*=I&gt;`EN$BHT,C]+OBQX_"/$RX4/8Q7[($PY`'D_CP8PY:B_?/UYE@4,VTS4(VTC6HQ4O8(/E\FVTE/Z@QU8)HXI"?54H]6ECRBO$I,V@_+9&gt;`D1\"4,V6S4(V6C6HQ6O6(/F&lt;F6TE7Z71Y#VYAEN6$L]65KQB/0LLEX`+Y:_H1T"4\UVS4,UXS6HQXC2(_NYE&amp;`H?*#2YKQ'(XSJ`!P8PUDD]F*$$/[($Z[-/HW/CQWN`E1H?U.1I$D`&gt;A-00L!+(TS.X_(R3BS`!/@T&gt;K--P6-L63LEF\0#T^2W_U(S(HUPC],=2/0Q]=I=P)H4Y9C-/@RRM^,6'(8[,SO(0A*G_6$2_X%8(Y?_G%T`O93J_X-N5`.BG1@SYDT2_&lt;-@&amp;DXPQ"L2;Z@"H1+;7Y$6U((YN(9,8-58Q?K9)XG!"Q2N*#&gt;[%)`B?0-%,61Z`"K2I#6Z-R_'8U#&amp;Y+6-%,W/+Y/57%,S#F/#6/)+X'8$Y-`)H50]_D=/\AAY04E/(&gt;[)/X^R%B^@_MD;]I?OC/(R,!Q\@KAI=PD7ZQ\=B&gt;@A&lt;=1[`(88Y4+5=K*4:99&gt;PJ_`Q(=RX_)YE$N_*Q/%\ETN]&amp;U+(\WL!Y8VVI--P1"U_/_TQPD4I]&amp;02_$'7DM/0IR-`RD-60S9Q&amp;4]G7B!`&lt;C+.(T@DYM@^?!-;'(:Y3(#GFO$"&gt;"R_#"W#BT*&amp;]$#G#"ZO!=%D3!E?C30Y!4T"G7'(BQ3H;!HO2=@B?^-BO!^4"0&gt;FCO!M#QBWER,=$U@Q$HW(^[8*HU$^H;D$AW^#$L`&amp;&gt;SK1-DHI]/#CC1[PX:!GTQ/_QTM]_&amp;(@Y=%F_AY0@C:W?0!,I=/$STC(XY5[0+_5.:3S8PBXRX`8&gt;8DQB_E/$[Y3/$SIC.`B137RQQ-0G=-$LR'(HQQ&gt;`B$K]065$F]!(@ZR.(\5J_0Q$?D%DY:-R9^'4-70RB&lt;%DS;E]=/*CR_\]1:51_8Q":"C$=%V[4B]%BW#EZECO":4"+&gt;91("N5I,LY!D?AS?96TF]!;291\")R_%F/A4&lt;G#,9TB4"$AM)4C!FO"K/Y!=./(S"`!H5`[P'Y:]-/8Q`[0"&lt;5)&gt;`WE3(VW[["R8J(V%=`FE$$P^]&amp;4D]#_1/`S+JQ`]4Z`"`1RQ?(&amp;8+%ULZ=&gt;DB$_I\`#PG/`QB%I&gt;`F=$B8S.X_.=*(@Y.)Q\`+.A)()D$AY^6$F]+(2\.\AX+K4A]/%]F@I"079I@Y$/7YA@YP/LD"`C#-([!,X(RY`_Q"A2/K"S_&amp;&amp;+M)@AE&amp;9=((^)B_"24"*^GCO!T&amp;B"]FJ4A=TC#^_)*0KJS_&amp;*)M9&lt;A9V1=(LR.B_!SJAA_TB4"\VB!],OE",_()`AB!QZ@+H]#^@&gt;J(,YI[0#QTKG!]I5=@L[*$K`&gt;7"D?U))I$L`)A--PK1+(8ULO]-N)(8YZTO%@2BV_MV,?LZ2\QQ[`3N`B6ZPP]#5E$L_'Q/(8EDP]/E+(8W`%Y8_#$D]+&gt;@C^+I?P"$0B]RO*(`PI/0T$&gt;/,()UT&amp;DU?:CB_072!``EY;0`&lt;DYM=D?!/[8_8QF3"43`!//A[`EQ\"OZAC?$&gt;4"/_RA/!(31H_+Y\A2`%%&lt;V9Z@#6U#!X"7_AY`&amp;9[".`&amp;&amp;-&amp;X-U8Q0291@#]JQ&gt;NQ"$^GQ/'6`Q,V`RZQ?%T&amp;C\$;2&lt;GC@%)/^F6V&gt;ZF4[;'&lt;Y(8M!IN2CS[)GCZBM5'`4R\7:]3Q^FUS/L:P-WP"N&amp;EZ219S@]-'P.6&gt;ZGDL+M_"ZQVY;#0IKZH!&amp;5-!:`FV0-(@Y,*E:A1&lt;`0*!L0L""J_NHG/M#D4A@NC!Y3;;,$&gt;2M#&gt;4Y2@@"&amp;;AT41T;D/N-.B-N@PUTR\5?HDH$FE&gt;MB@'V6*Z"#W64^*3]X!N^8CIJ?2H[IX".$3_(_'XJE,AE7&gt;K&amp;L;:Z'&gt;K$ZVG#K;BM7@FZ=S:-XV7Z)06&lt;6I3'O4"WA_W&amp;$M0VP\Q;NBZM![QY-%[5$.P.PRA(92\M"[!$^;VTJ8SX$NNB=]8\@%+!H]=W3[X,V;&amp;J(\Q:#M]I)Y\K^:\390AD3=["DH[*?W"$R)Y&amp;\`7M=@R[_&amp;4$N^\PC8]6W,'Y;'B)UP?"[0";[[&gt;42TV`B^8J&amp;?V!!!!"!!!"NA!!!!%!!!!!!!!!!Q!!5*%3&amp;!!!!!!!!-!!!"C!!!!=HC=9W"AS"/190L(50?8A5HA+Z!B`:?"7&gt;#0]4=$![?@Q'%AT3AA#237`=P!,KA.&amp;N9_IMP"!!7K&lt;)Q=EBS("4H!-BQN'AT````H_(LE'FT&amp;%2]Y5W770)=%!"2C'1!!!!!!!!1!!!!(!!!U!1!!!!=!!!!B8WZJ8URB=X2,&lt;G^X&lt;E^X&lt;GFO:UR71WRB=X.$&lt;(6T&gt;'6S!!!06R=!A!!!!!!"!!A!-0````]!!1!!!!!0/Q!!!%9!$5!$!!&gt;$;'&amp;O&lt;G6M!%J!=!!?!!!L&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC&amp;%&amp;'5UEN4D=U1T24?#ZM&gt;G.M98.T!"2"2F.*,5YX.%-U5XAO&lt;(:D&lt;'&amp;T=Q!!2%"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)245&amp;044-Y-$5T,GRW9WRB=X-!!"&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=Q!*1!=!!V2P=!!*1!=!!UVJ:!!*1!=!!U*P&gt;!"C!0%!!!!!!!!!!R2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=RB0&gt;82Q&gt;81A6G&amp;M&gt;75N1WRV=X2F=CZD&gt;'Q!'E"1!!-!!Q!%!!5*47&amp;J&lt;C"$3#!Q!')!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!;1&amp;!!!Q!$!!1!"1F.97FO)%.))$%!9A$R!!!!!!!!!!-55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)245&amp;044-Y-$5T,GRW9WRB=X-94X6U=(6U)&amp;:B&lt;(6F,5.M&gt;8.U:8)O9X2M!"J!5!!$!!-!"!!&amp;#5VB;7YA1UAA-A"C!0%!!!!!!!!!!R2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=RB0&gt;82Q&gt;81A6G&amp;M&gt;75N1WRV=X2F=CZD&gt;'Q!'E"1!!-!!Q!%!!5*47&amp;J&lt;C"$3#!T!')!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!;1&amp;!!!Q!$!!1!"1F.97FO)%.))$1!9A$R!!!!!!!!!!-55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)245&amp;044-Y-$5T,GRW9WRB=X-94X6U=(6U)&amp;:B&lt;(6F,5.M&gt;8.U:8)O9X2M!"J!5!!$!!-!"!!&amp;#5VB;7YA1UAA.1"C!0%!!!!!!!!!!R2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=RB0&gt;82Q&gt;81A6G&amp;M&gt;75N1WRV=X2F=CZD&gt;'Q!'E"1!!-!!Q!%!!5*47&amp;J&lt;C"$3#!W!')!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!;1&amp;!!!Q!$!!1!"1F.97FO)%.))$=!(%"1!!A!"A!(!!A!#1!+!!M!$!!."%VB;7Y!!!^!#A!*1GFB=S"$3#!Q!!^!#A!*1GFB=S"$3#!R!!^!#A!*1GFB=S"$3#!S!!^!#A!*1GFB=S"$3#!T!!^!#A!*1GFB=S"$3#!U!!^!#A!*1GFB=S"$3#!V!!^!#A!*1GFB=S"$3#!W!!^!#A!*1GFB=S"$3#!X!'!!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T%%*J98-N1WRV=X2F=CZD&gt;'Q!)%"1!!A!$Q!1!"%!%A!4!"1!&amp;1!7"%*J98-!!!^!#A!)47^E)%.))$!!!!^!#A!)47^E)%.))$%!!!^!#A!)47^E)%.))$)!!!^!#A!)47^E)%.))$-!!!^!#A!)47^E)%.))$1!!!^!#A!)47^E)%.))$5!!!^!#A!)47^E)%.))$9!!!^!#A!)47^E)%.))$=!!&amp;U!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T$UVP:#V$&lt;(6T&gt;'6S,G.U&lt;!!?1&amp;!!#!!9!"E!'A!&lt;!"Q!(1!?!"]$47^E!"6!"1!/1G&amp;O:(&gt;J:(2I)%.))$!!!"6!"1!/1G&amp;O:(&gt;J:(2I)%.))$%!!"6!"1!/1G&amp;O:(&gt;J:(2I)%.))$)!!"6!"1!/1G&amp;O:(&gt;J:(2I)%.))$-!!"6!"1!/1G&amp;O:(&gt;J:(2I)%.))$1!!"6!"1!/1G&amp;O:(&gt;J:(2I)%.))$5!!"6!"1!/1G&amp;O:(&gt;J:(2I)%.))$9!!"6!"1!/1G&amp;O:(&gt;J:(2I)%.))$=!!'E!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T&amp;5*B&lt;G2X;72U;#V$&lt;(6T&gt;'6S,G.U&lt;!!E1&amp;!!#!!B!#)!)Q!E!#5!*A!H!#A*1G&amp;O:(&gt;J:(2I!%J!=!!?!!!Q&amp;E^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9H!84X"U;7.B&lt;#"1=G^E&gt;7.U,GRW9WRB=X-!!!^1=G^E&gt;7.U,GRW9WRB=X-!%E!Q`````QB-&lt;X1A1W^E:1!!9A$R!!!!!!!!!!-55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)245&amp;044-Y-$5T,GRW9WRB=X-94X6U=(6U)&amp;:B&lt;(6F,5.M&gt;8.U:8)O9X2M!"J!5!!$!!-!"!!&amp;#&amp;"S:3"$3#!Q!!"C!0%!!!!!!!!!!R2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=RB0&gt;82Q&gt;81A6G&amp;M&gt;75N1WRV=X2F=CZD&gt;'Q!'E"1!!-!!Q!%!!5)5(*F)%.))$%!!')!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!;1&amp;!!!Q!$!!1!"1B1=G5A1UAA-A!!9A$R!!!!!!!!!!-55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)245&amp;044-Y-$5T,GRW9WRB=X-94X6U=(6U)&amp;:B&lt;(6F,5.M&gt;8.U:8)O9X2M!"J!5!!$!!-!"!!&amp;#&amp;"S:3"$3#!T!!"C!0%!!!!!!!!!!R2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=RB0&gt;82Q&gt;81A6G&amp;M&gt;75N1WRV=X2F=CZD&gt;'Q!'E"1!!-!!Q!%!!5)5(*F)%.))$1!!')!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!;1&amp;!!!Q!$!!1!"1B1=G5A1UAA.1!!9A$R!!!!!!!!!!-55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)245&amp;044-Y-$5T,GRW9WRB=X-94X6U=(6U)&amp;:B&lt;(6F,5.M&gt;8.U:8)O9X2M!"J!5!!$!!-!"!!&amp;#&amp;"S:3"$3#!W!!"C!0%!!!!!!!!!!R2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=RB0&gt;82Q&gt;81A6G&amp;M&gt;75N1WRV=X2F=CZD&gt;'Q!'E"1!!-!!Q!%!!5)5(*F)%.))$=!!"J!5!!)!#Q!,1!O!#]!-!!R!$)!-Q.1=G5!:!$R!!!!!!!!!!-55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)245&amp;044-Y-$5T,GRW9WRB=X-94X6U=(6U)&amp;:B&lt;(6F,5.M&gt;8.U:8)O9X2M!"R!5!!$!!-!"!!&amp;#E2F&lt;'&amp;Z)%.))$!!!'1!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!=1&amp;!!!Q!$!!1!"1J%:7RB?3"$3#!R!!"E!0%!!!!!!!!!!R2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=RB0&gt;82Q&gt;81A6G&amp;M&gt;75N1WRV=X2F=CZD&gt;'Q!(%"1!!-!!Q!%!!5+2'6M98EA1UAA-A!!:!$R!!!!!!!!!!-55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)245&amp;044-Y-$5T,GRW9WRB=X-94X6U=(6U)&amp;:B&lt;(6F,5.M&gt;8.U:8)O9X2M!"R!5!!$!!-!"!!&amp;#E2F&lt;'&amp;Z)%.))$-!!'1!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!=1&amp;!!!Q!$!!1!"1J%:7RB?3"$3#!U!!"E!0%!!!!!!!!!!R2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=RB0&gt;82Q&gt;81A6G&amp;M&gt;75N1WRV=X2F=CZD&gt;'Q!(%"1!!-!!Q!%!!5+2'6M98EA1UAA.1!!:!$R!!!!!!!!!!-55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)245&amp;044-Y-$5T,GRW9WRB=X-94X6U=(6U)&amp;:B&lt;(6F,5.M&gt;8.U:8)O9X2M!"R!5!!$!!-!"!!&amp;#E2F&lt;'&amp;Z)%.))$9!!'1!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!=1&amp;!!!Q!$!!1!"1J%:7RB?3"$3#!X!!!=1&amp;!!#!!V!$9!.Q!Y!$E!/A!\!$Q&amp;2'6M98E!$%!B"E.I;8!A-1!!$%!B"E.I;8!A-A!!91$R!!!!!!!!!!-55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)26(AA4X6U=(6U,GRW9WRB=X-81WBJ=#"4&gt;'&amp;U&gt;8-N1WRV=X2F=CZD&gt;'Q!'E"1!!)!0A!`#E.%5C"4&gt;'&amp;U&gt;8-!!'%!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T&amp;U.I;8!A5X2B&gt;(6T,5.M&gt;8.U:8)O9X2M!"J!5!!#!$Y!0QJ-2%1A5X2B&gt;(6T!!#@!0%!!!!!!!!!!R2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;5?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=R:5&gt;7ZF)&amp;"P&lt;'&amp;S;82Z,56O&gt;7UO9X2M!&amp;F!&amp;A!%%%ZP=GVB&lt;#"0='6S982J&lt;WY65%&amp;..#"*&lt;H:F=H1A5'&amp;J=C"4&gt;W&amp;Q$F""441A37ZW:8*T;7^O#6"B;8)A5X&gt;B=!!.6(6O:3"1&lt;WRB=GFU?1!/1#%*5X2P=#"5&gt;7ZF!"2!=!!:!!%!1QF4&gt;'^Q)&amp;2V&lt;G5!.E"1!!]!!!!"!!)!$A!8!#!!+1!K!#M!.!!^!%!!11"#!%126(AA4X6U=(6U,GRW9WRB=X-!!1"&amp;!!!!!!!!!"J-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;597*0=G2F=A!!!F%8!)!!!!!!!A!&amp;!!=!!!Q!1!!"`````Q!!!!%!!1!!!)M!!!!!!!!!!1!!!!)!!!!$!!!!"!!!!!5!!!!'!!!!"Q!!!!A!!!!*!!!!#A!!!!M!!!!-!!!!$1!!!!Y!!!!0!!!!%!!!!"%!!!!3!!!!%Q!!!"1!!!!6!!!!&amp;A!!!"=!!!!9!!!!'1!!!"I!!!!&lt;!!!!(!!!!"U!!!!?!!!!(Q!!!#!!!!!B!!!!)A!!!#-!!!!E!!!!*1!!!#9!!!!H!!!!+!!!!#E!!!!K!!!!+Q!!!#Q!!!!N!!!!,A!!!#]!!!!Q!!!!-1!!!$)!!!!T!!!!.!!!!$5!!!!W!!!!.Q!!!$A!!!!Z!!!!/A!!!$M!!!!]!!!!01!!!$Y!!!!`!!!!1!!!!%%!!!"#!!!!1Q!!!%1!!!"&amp;!!!!2A!!!%=!!!")!!!!31!!!%I!!!",!!!!4!!!!%U!!!"/!!!!4Q!!!&amp;!!!!"2!!!!5A!!!&amp;-!!!"5!!!!61!!!&amp;9!!!"8!!!!7!!!!&amp;E!!!";!!!!7Q!!!&amp;Q!!!"&gt;!!!!8A!!!&amp;]!!!"A!!!!91!!!')!!!"D!!!!:!!!!'5!!!"G!!!!:Q!!!'A!!!"J!!!!;A!!!'M!!!"M!!!!&lt;1!!!'Y!!!"P!!!!=!!!!(%!!!"S!!!!=Q!!!(1!!!"V!!!!&gt;A!!!(=!!!"Y!!!!?1!!!(I!!!"\!!!!@!!!!(U!!!"_!!!!@Q!!!)!!!!#"!!!!AA!!!)-!!!#%!!!!B1!!!)9!!!#(!!!!C!!!!)E!!!#+!!!!!!!!!"N-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;5;7VF=X2B&lt;8!!!!!:&amp;Q#!!!!!!!%!"1!(!!!"!!$;HT$8!!!!!!!!!#:-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;-98.U18"Q&lt;'FF:&amp;2J&lt;76T&gt;'&amp;N=!!!!"E8!)!!!!!!!1!&amp;!!=!!!%!!.K@-.=!!!!!!!!!'ER71WRB=X.1=GFW982F2'&amp;U962Z='6%:8.D!!!06R=!A!!!!!!"!!A!-0````]!!1!!!!!0/Q!!!%9!$5!$!!&gt;$;'&amp;O&lt;G6M!%J!=!!?!!!L&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC&amp;%&amp;'5UEN4D=U1T24?#ZM&gt;G.M98.T!"2"2F.*,5YX.%-U5XAO&lt;(:D&lt;'&amp;T=Q!!2%"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)245&amp;044-Y-$5T,GRW9WRB=X-!!"&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=Q!*1!=!!V2P=!!*1!=!!UVJ:!!*1!=!!U*P&gt;!"C!0%!!!!!!!!!!R2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=RB0&gt;82Q&gt;81A6G&amp;M&gt;75N1WRV=X2F=CZD&gt;'Q!'E"1!!-!!Q!%!!5*47&amp;J&lt;C"$3#!Q!')!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!;1&amp;!!!Q!$!!1!"1F.97FO)%.))$%!9A$R!!!!!!!!!!-55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)245&amp;044-Y-$5T,GRW9WRB=X-94X6U=(6U)&amp;:B&lt;(6F,5.M&gt;8.U:8)O9X2M!"J!5!!$!!-!"!!&amp;#5VB;7YA1UAA-A"C!0%!!!!!!!!!!R2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=RB0&gt;82Q&gt;81A6G&amp;M&gt;75N1WRV=X2F=CZD&gt;'Q!'E"1!!-!!Q!%!!5*47&amp;J&lt;C"$3#!T!')!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!;1&amp;!!!Q!$!!1!"1F.97FO)%.))$1!9A$R!!!!!!!!!!-55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)245&amp;044-Y-$5T,GRW9WRB=X-94X6U=(6U)&amp;:B&lt;(6F,5.M&gt;8.U:8)O9X2M!"J!5!!$!!-!"!!&amp;#5VB;7YA1UAA.1"C!0%!!!!!!!!!!R2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=RB0&gt;82Q&gt;81A6G&amp;M&gt;75N1WRV=X2F=CZD&gt;'Q!'E"1!!-!!Q!%!!5*47&amp;J&lt;C"$3#!W!')!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!;1&amp;!!!Q!$!!1!"1F.97FO)%.))$=!(%"1!!A!"A!(!!A!#1!+!!M!$!!."%VB;7Y!!!^!#A!*1GFB=S"$3#!Q!!^!#A!*1GFB=S"$3#!R!!^!#A!*1GFB=S"$3#!S!!^!#A!*1GFB=S"$3#!T!!^!#A!*1GFB=S"$3#!U!!^!#A!*1GFB=S"$3#!V!!^!#A!*1GFB=S"$3#!W!!^!#A!*1GFB=S"$3#!X!'!!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T%%*J98-N1WRV=X2F=CZD&gt;'Q!)%"1!!A!$Q!1!"%!%A!4!"1!&amp;1!7"%*J98-!!!^!#A!)47^E)%.))$!!!!^!#A!)47^E)%.))$%!!!^!#A!)47^E)%.))$)!!!^!#A!)47^E)%.))$-!!!^!#A!)47^E)%.))$1!!!^!#A!)47^E)%.))$5!!!^!#A!)47^E)%.))$9!!!^!#A!)47^E)%.))$=!!&amp;U!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T$UVP:#V$&lt;(6T&gt;'6S,G.U&lt;!!?1&amp;!!#!!9!"E!'A!&lt;!"Q!(1!?!"]$47^E!"6!"1!/1G&amp;O:(&gt;J:(2I)%.))$!!!"6!"1!/1G&amp;O:(&gt;J:(2I)%.))$%!!"6!"1!/1G&amp;O:(&gt;J:(2I)%.))$)!!"6!"1!/1G&amp;O:(&gt;J:(2I)%.))$-!!"6!"1!/1G&amp;O:(&gt;J:(2I)%.))$1!!"6!"1!/1G&amp;O:(&gt;J:(2I)%.))$5!!"6!"1!/1G&amp;O:(&gt;J:(2I)%.))$9!!"6!"1!/1G&amp;O:(&gt;J:(2I)%.))$=!!'E!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T&amp;5*B&lt;G2X;72U;#V$&lt;(6T&gt;'6S,G.U&lt;!!E1&amp;!!#!!B!#)!)Q!E!#5!*A!H!#A*1G&amp;O:(&gt;J:(2I!%J!=!!?!!!Q&amp;E^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9H!84X"U;7.B&lt;#"1=G^E&gt;7.U,GRW9WRB=X-!!!^1=G^E&gt;7.U,GRW9WRB=X-!%E!Q`````QB-&lt;X1A1W^E:1!!9A$R!!!!!!!!!!-55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)245&amp;044-Y-$5T,GRW9WRB=X-94X6U=(6U)&amp;:B&lt;(6F,5.M&gt;8.U:8)O9X2M!"J!5!!$!!-!"!!&amp;#&amp;"S:3"$3#!Q!!"C!0%!!!!!!!!!!R2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=RB0&gt;82Q&gt;81A6G&amp;M&gt;75N1WRV=X2F=CZD&gt;'Q!'E"1!!-!!Q!%!!5)5(*F)%.))$%!!')!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!;1&amp;!!!Q!$!!1!"1B1=G5A1UAA-A!!9A$R!!!!!!!!!!-55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)245&amp;044-Y-$5T,GRW9WRB=X-94X6U=(6U)&amp;:B&lt;(6F,5.M&gt;8.U:8)O9X2M!"J!5!!$!!-!"!!&amp;#&amp;"S:3"$3#!T!!"C!0%!!!!!!!!!!R2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=RB0&gt;82Q&gt;81A6G&amp;M&gt;75N1WRV=X2F=CZD&gt;'Q!'E"1!!-!!Q!%!!5)5(*F)%.))$1!!')!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!;1&amp;!!!Q!$!!1!"1B1=G5A1UAA.1!!9A$R!!!!!!!!!!-55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)245&amp;044-Y-$5T,GRW9WRB=X-94X6U=(6U)&amp;:B&lt;(6F,5.M&gt;8.U:8)O9X2M!"J!5!!$!!-!"!!&amp;#&amp;"S:3"$3#!W!!"C!0%!!!!!!!!!!R2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=RB0&gt;82Q&gt;81A6G&amp;M&gt;75N1WRV=X2F=CZD&gt;'Q!'E"1!!-!!Q!%!!5)5(*F)%.))$=!!"J!5!!)!#Q!,1!O!#]!-!!R!$)!-Q.1=G5!:!$R!!!!!!!!!!-55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)245&amp;044-Y-$5T,GRW9WRB=X-94X6U=(6U)&amp;:B&lt;(6F,5.M&gt;8.U:8)O9X2M!"R!5!!$!!-!"!!&amp;#E2F&lt;'&amp;Z)%.))$!!!'1!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!=1&amp;!!!Q!$!!1!"1J%:7RB?3"$3#!R!!"E!0%!!!!!!!!!!R2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=RB0&gt;82Q&gt;81A6G&amp;M&gt;75N1WRV=X2F=CZD&gt;'Q!(%"1!!-!!Q!%!!5+2'6M98EA1UAA-A!!:!$R!!!!!!!!!!-55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)245&amp;044-Y-$5T,GRW9WRB=X-94X6U=(6U)&amp;:B&lt;(6F,5.M&gt;8.U:8)O9X2M!"R!5!!$!!-!"!!&amp;#E2F&lt;'&amp;Z)%.))$-!!'1!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!=1&amp;!!!Q!$!!1!"1J%:7RB?3"$3#!U!!"E!0%!!!!!!!!!!R2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=RB0&gt;82Q&gt;81A6G&amp;M&gt;75N1WRV=X2F=CZD&gt;'Q!(%"1!!-!!Q!%!!5+2'6M98EA1UAA.1!!:!$R!!!!!!!!!!-55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)245&amp;044-Y-$5T,GRW9WRB=X-94X6U=(6U)&amp;:B&lt;(6F,5.M&gt;8.U:8)O9X2M!"R!5!!$!!-!"!!&amp;#E2F&lt;'&amp;Z)%.))$9!!'1!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!=1&amp;!!!Q!$!!1!"1J%:7RB?3"$3#!X!!!=1&amp;!!#!!V!$9!.Q!Y!$E!/A!\!$Q&amp;2'6M98E!$%!B"E.I;8!A-1!!$%!B"E.I;8!A-A!!91$R!!!!!!!!!!-55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)26(AA4X6U=(6U,GRW9WRB=X-81WBJ=#"4&gt;'&amp;U&gt;8-N1WRV=X2F=CZD&gt;'Q!'E"1!!)!0A!`#E.%5C"4&gt;'&amp;U&gt;8-!!'%!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T&amp;U.I;8!A5X2B&gt;(6T,5.M&gt;8.U:8)O9X2M!"J!5!!#!$Y!0QJ-2%1A5X2B&gt;(6T!!#@!0%!!!!!!!!!!R2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;5?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=R:5&gt;7ZF)&amp;"P&lt;'&amp;S;82Z,56O&gt;7UO9X2M!&amp;F!&amp;A!%%%ZP=GVB&lt;#"0='6S982J&lt;WY65%&amp;..#"*&lt;H:F=H1A5'&amp;J=C"4&gt;W&amp;Q$F""441A37ZW:8*T;7^O#6"B;8)A5X&gt;B=!!.6(6O:3"1&lt;WRB=GFU?1!/1#%*5X2P=#"5&gt;7ZF!"2!=!!:!!%!1QF4&gt;'^Q)&amp;2V&lt;G5!.E"1!!]!!!!"!!)!$A!8!#!!+1!K!#M!.!!^!%!!11"#!%126(AA4X6U=(6U,GRW9WRB=X-!!1"&amp;!!!!!!!!!"Z-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;%:GRU2'&amp;U96.J?G5!!!!:&amp;Q#!!!!!!!%!"1!$!!!"!!!!!!('!!!!!!!!!"J-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;%:GRU2'&amp;U91!!%;U8!)!!!!!!2A!.1!-!"U.I97ZO:7Q!3E"Q!"Y!!#M55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)515:433V/.T2$.&amp;.Y,GRW9WRB=X-!&amp;%&amp;'5UEN4D=U1T24?#ZM&gt;G.M98.T!!"%1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=Q!!%5V"4UUT/$!V-SZM&gt;G.M98.T!!F!"Q!$6'^Q!!F!"Q!$47FE!!F!"Q!$1G^U!')!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!;1&amp;!!!Q!$!!1!"1F.97FO)%.))$!!9A$R!!!!!!!!!!-55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)245&amp;044-Y-$5T,GRW9WRB=X-94X6U=(6U)&amp;:B&lt;(6F,5.M&gt;8.U:8)O9X2M!"J!5!!$!!-!"!!&amp;#5VB;7YA1UAA-1"C!0%!!!!!!!!!!R2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=RB0&gt;82Q&gt;81A6G&amp;M&gt;75N1WRV=X2F=CZD&gt;'Q!'E"1!!-!!Q!%!!5*47&amp;J&lt;C"$3#!S!')!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!;1&amp;!!!Q!$!!1!"1F.97FO)%.))$-!9A$R!!!!!!!!!!-55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)245&amp;044-Y-$5T,GRW9WRB=X-94X6U=(6U)&amp;:B&lt;(6F,5.M&gt;8.U:8)O9X2M!"J!5!!$!!-!"!!&amp;#5VB;7YA1UAA.!"C!0%!!!!!!!!!!R2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=RB0&gt;82Q&gt;81A6G&amp;M&gt;75N1WRV=X2F=CZD&gt;'Q!'E"1!!-!!Q!%!!5*47&amp;J&lt;C"$3#!V!')!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!;1&amp;!!!Q!$!!1!"1F.97FO)%.))$9!9A$R!!!!!!!!!!-55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)245&amp;044-Y-$5T,GRW9WRB=X-94X6U=(6U)&amp;:B&lt;(6F,5.M&gt;8.U:8)O9X2M!"J!5!!$!!-!"!!&amp;#5VB;7YA1UAA.Q!=1&amp;!!#!!'!!=!#!!*!!I!#Q!-!!U%47&amp;J&lt;A!!$U!+!!F#;7&amp;T)%.))$!!$U!+!!F#;7&amp;T)%.))$%!$U!+!!F#;7&amp;T)%.))$)!$U!+!!F#;7&amp;T)%.))$-!$U!+!!F#;7&amp;T)%.))$1!$U!+!!F#;7&amp;T)%.))$5!$U!+!!F#;7&amp;T)%.))$9!$U!+!!F#;7&amp;T)%.))$=!9!$R!!!!!!!!!!-55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)26(AA4X6U=(6U,GRW9WRB=X-11GFB=SV$&lt;(6T&gt;'6S,G.U&lt;!!A1&amp;!!#!!0!"!!%1!3!"-!&amp;!!6!"9%1GFB=Q!!$U!+!!B.&lt;W1A1UAA-!!!$U!+!!B.&lt;W1A1UAA-1!!$U!+!!B.&lt;W1A1UAA-A!!$U!+!!B.&lt;W1A1UAA-Q!!$U!+!!B.&lt;W1A1UAA.!!!$U!+!!B.&lt;W1A1UAA.1!!$U!+!!B.&lt;W1A1UAA.A!!$U!+!!B.&lt;W1A1UAA.Q!!81$R!!!!!!!!!!-55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)26(AA4X6U=(6U,GRW9WRB=X-047^E,5.M&gt;8.U:8)O9X2M!"Z!5!!)!"A!'1!;!"M!(!!&gt;!"Y!(Q..&lt;W1!&amp;5!&amp;!!Z#97ZE&gt;WFE&gt;'AA1UAA-!!!&amp;5!&amp;!!Z#97ZE&gt;WFE&gt;'AA1UAA-1!!&amp;5!&amp;!!Z#97ZE&gt;WFE&gt;'AA1UAA-A!!&amp;5!&amp;!!Z#97ZE&gt;WFE&gt;'AA1UAA-Q!!&amp;5!&amp;!!Z#97ZE&gt;WFE&gt;'AA1UAA.!!!&amp;5!&amp;!!Z#97ZE&gt;WFE&gt;'AA1UAA.1!!&amp;5!&amp;!!Z#97ZE&gt;WFE&gt;'AA1UAA.A!!&amp;5!&amp;!!Z#97ZE&gt;WFE&gt;'AA1UAA.Q!!;1$R!!!!!!!!!!-55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)26(AA4X6U=(6U,GRW9WRB=X-61G&amp;O:(&gt;J:(2I,5.M&gt;8.U:8)O9X2M!#2!5!!)!#%!)A!D!#1!*1!G!#=!+!F#97ZE&gt;WFE&gt;'A!3E"Q!"Y!!$!74X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC="&gt;0=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:D&lt;'&amp;T=Q!!$V"S&lt;W2V9X1O&lt;(:D&lt;'&amp;T=Q!31$$`````#%RP&gt;#"$&lt;W2F!!"C!0%!!!!!!!!!!R2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=RB0&gt;82Q&gt;81A6G&amp;M&gt;75N1WRV=X2F=CZD&gt;'Q!'E"1!!-!!Q!%!!5)5(*F)%.))$!!!')!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!;1&amp;!!!Q!$!!1!"1B1=G5A1UAA-1!!9A$R!!!!!!!!!!-55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)245&amp;044-Y-$5T,GRW9WRB=X-94X6U=(6U)&amp;:B&lt;(6F,5.M&gt;8.U:8)O9X2M!"J!5!!$!!-!"!!&amp;#&amp;"S:3"$3#!S!!"C!0%!!!!!!!!!!R2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=RB0&gt;82Q&gt;81A6G&amp;M&gt;75N1WRV=X2F=CZD&gt;'Q!'E"1!!-!!Q!%!!5)5(*F)%.))$-!!')!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!;1&amp;!!!Q!$!!1!"1B1=G5A1UAA.!!!9A$R!!!!!!!!!!-55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)245&amp;044-Y-$5T,GRW9WRB=X-94X6U=(6U)&amp;:B&lt;(6F,5.M&gt;8.U:8)O9X2M!"J!5!!$!!-!"!!&amp;#&amp;"S:3"$3#!V!!"C!0%!!!!!!!!!!R2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=RB0&gt;82Q&gt;81A6G&amp;M&gt;75N1WRV=X2F=CZD&gt;'Q!'E"1!!-!!Q!%!!5)5(*F)%.))$9!!')!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!;1&amp;!!!Q!$!!1!"1B1=G5A1UAA.Q!!'E"1!!A!,!!N!#Y!,Q!Q!$%!-A!T!V"S:1"E!0%!!!!!!!!!!R2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=RB0&gt;82Q&gt;81A6G&amp;M&gt;75N1WRV=X2F=CZD&gt;'Q!(%"1!!-!!Q!%!!5+2'6M98EA1UAA-!!!:!$R!!!!!!!!!!-55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)245&amp;044-Y-$5T,GRW9WRB=X-94X6U=(6U)&amp;:B&lt;(6F,5.M&gt;8.U:8)O9X2M!"R!5!!$!!-!"!!&amp;#E2F&lt;'&amp;Z)%.))$%!!'1!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!=1&amp;!!!Q!$!!1!"1J%:7RB?3"$3#!S!!"E!0%!!!!!!!!!!R2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=RB0&gt;82Q&gt;81A6G&amp;M&gt;75N1WRV=X2F=CZD&gt;'Q!(%"1!!-!!Q!%!!5+2'6M98EA1UAA-Q!!:!$R!!!!!!!!!!-55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)245&amp;044-Y-$5T,GRW9WRB=X-94X6U=(6U)&amp;:B&lt;(6F,5.M&gt;8.U:8)O9X2M!"R!5!!$!!-!"!!&amp;#E2F&lt;'&amp;Z)%.))$1!!'1!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!=1&amp;!!!Q!$!!1!"1J%:7RB?3"$3#!V!!"E!0%!!!!!!!!!!R2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=RB0&gt;82Q&gt;81A6G&amp;M&gt;75N1WRV=X2F=CZD&gt;'Q!(%"1!!-!!Q!%!!5+2'6M98EA1UAA.A!!:!$R!!!!!!!!!!-55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)245&amp;044-Y-$5T,GRW9WRB=X-94X6U=(6U)&amp;:B&lt;(6F,5.M&gt;8.U:8)O9X2M!"R!5!!$!!-!"!!&amp;#E2F&lt;'&amp;Z)%.))$=!!"R!5!!)!$5!.A!X!$A!/1![!$M!0!6%:7RB?1!-1#%'1WBJ=#!R!!!-1#%'1WBJ=#!S!!"B!0%!!!!!!!!!!R2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;5?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=R&gt;$;'FQ)&amp;.U982V=SV$&lt;(6T&gt;'6S,G.U&lt;!!;1&amp;!!!A!_!$]+1U23)&amp;.U982V=Q!!91$R!!!!!!!!!!-55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)26(AA4X6U=(6U,GRW9WRB=X-81WBJ=#"4&gt;'&amp;U&gt;8-N1WRV=X2F=CZD&gt;'Q!'E"1!!)!0A!`#ER%2#"4&gt;'&amp;U&gt;8-!!*]!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T&amp;F2V&lt;G5A5'^M98*J&gt;(EN27ZV&lt;3ZD&gt;'Q!75!7!!114G^S&lt;7&amp;M)%^Q:8*B&gt;'FP&lt;B6115UU)%FO&gt;G6S&gt;#"197FS)&amp;.X98!/5%&amp;..#"*&lt;H:F=H.J&lt;WY*5'&amp;J=C"4&gt;W&amp;Q!!V5&gt;7ZF)&amp;"P&lt;'&amp;S;82Z!!Z!)1F4&gt;'^Q)&amp;2V&lt;G5!&amp;%"Q!"E!!1"$#6.U&lt;X!A6(6O:1!W1&amp;!!$Q!!!!%!!A!/!"=!)!!J!#I!+Q!U!$U!1!""!%)!2"&amp;5?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q!"!%5!!!!!!!!!!3M55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)515:433V/.T2$.&amp;.Y,GRW9WRB=X-!!!!!!!!!!!!!!!!"+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"-":0=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7*Q&amp;U^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;G.M98.T!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"!#M!"U!!!!%!!!-H1!!!#A!!!!#!!!%!!!!!$-!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!&amp;&gt;A!!'O.YH,V9;VM46R"_*?%3Z+:#R"2RK6:2B*,M*C$VMJ#A1!GEA,;UN&lt;)GCWQ&lt;MGH9)0:K\T&gt;\\X@`4P^"H[?`IP[#&gt;P;31T;=&amp;+.:=JY(:NY^/_`-\*[:=R&lt;!(QD)T@#N[(F&lt;3'I:7ZD3$;TD#?S@LTMVG:3%B,KNJ66B.D[3X=ZK&gt;Y]E*R?4YPBI6#1^H67WNHI8CU;_;!CXF'R2(9ZHCVO'7BB*'VHUSSHYS.)B.,364Q)[Z69%JD2F3YD0#+.O.?R7)WZ6&gt;+O37YW[V:B&lt;(5.GP_B7&gt;A4&lt;UV*U8?&lt;.LKD/5&amp;1N]+-247AG+9"7(,9Z%IKB7*1N34VDB_:3QWYVYF:&amp;NSKZV;B&lt;D&lt;H6-3"&gt;=WS&gt;&gt;$-HN(:UI".&gt;/)+D/):OC]++$$VS)TKGF&amp;TGPJ9R.JQ!^Y*B(BDBA3)0F(BAF!@'?##F1KMZ&amp;4X-"#=B12R(,UYAB"@1BZ-".B&gt;N]E"4@%0,GS%TG3*6;H&lt;AO(8LMK%92@@L&amp;C)8'H!+QO'S'8B=-U&amp;QJ:B4B:3?61K;]7"Y/F@=N/SPSE(YOR&lt;UQK;3&amp;2&lt;T;E%R.$X89RG&gt;T7WL"5.)+6J"7,[PZ$NWU3W;&amp;'!8U/[SDH&lt;:B_&lt;YBJ,,K6H-S8HU!U.=2\MHLS`0$C_-38&amp;J?;@E+`AI%L;FQ;?M4!!(OVP(!B@;,8#"J+,F\"8B.509=Y;)ZQSCZQS3ZQR2TRFCHD/-I=]K=7&gt;R$I-YDQM9QE5-_]XLQ&amp;I&gt;WK8!;Z&gt;_=RJQ_`F&lt;6D_P:@FI6KHMD!98]Y;7JO+7+OC:9NKQ4??0\Y7&gt;IN&amp;:C2S62`_F8]O]&lt;ABR050\&amp;Y]?3UOKI$J&gt;V7/'M/=-%=]:2-]:*-]:IJYTR$RHI!V8S&amp;K&amp;)C2%%=-9RH%*%T[[PP_/OQ&lt;OPFXOVI3;62YY;]6TDP!"=%1/A%-]!!\J!$CC"]!2/Q!/7DFW_\W-+\C+;Z!RC3H%'[U:HJQC7O/*J&gt;)BQBO#_53#%84)!Y&amp;F1]],ZP%!X&gt;324V!'&lt;J2B8_$*XY^(`[L"B@:&gt;R#3_4M3&gt;/)UT?!ED?"GDV04$C/!6*$#.[XA2-S=&gt;2Q6^8&lt;"-#0G#NKU9KJ!RD\7..'B4EK'(]Q`G30%Z+B7U?@P,S(LW(FLEBTZ^S&lt;!"66^(F`6)&amp;\#)F+_97;@C6`-ZB`SM]52$\D5$&lt;^4RT?S#'=;+.9)QX];&lt;&amp;;/2UI%X[\!8$&amp;HG8[]9K]_`#_1&lt;PF/PTQ#W_&lt;G+-9&amp;HX'#3,7N$3&lt;PC!5JPX&gt;:A"]QV?*P'9NU_$Q2IWU[0XVGY',27WTS7M)R&lt;^"KOUHPR&amp;NYG_2&lt;?I8%(;TZD*U.XG+[]2B7G&amp;7FSIY(O$H*0STQUT%5D8&amp;4EIB)8D8,2'"=NH9QS5,'/?^C!BH@RHH-S[PO@5UWI[J&amp;%W/&gt;T5JE@:5="$BLGIB%O+H*2C9N'O7C-C\*N8R\PIY!N'#BC'`?N&lt;6^PF;U;&amp;Q^8Q3.6=,%+,F8"IV8Q7"7=N?5(_!!@YC.]D%`Q+2Y[&lt;4H)\;7\;(E$P/H&amp;V\7JMJ;8R3:SU+W7NY00]$FVV#_@IO5VQ(JX[=]BMR[N51\]V#V4%0!VPH%KX,?UUEOD608W)LN8PL/'X4M0Y8O8I]`7G`VE[1&lt;KE5/H*.%$1BP91X=[8M73&gt;.#+*?SA&amp;9M?@Z+N.P21;!-5U#R?2:*/8'F]B2`Q#$^SQJ]W![=LDWD-5(A`E@YT_87+5G,`H[9%`7)643$/J#EG44**:N)V*FVFUB5G8;;EF_1**FVCUDC4RJA59V+532+42)IED&amp;]J7XW5N2"FLZ?S'+19@[.=(;/=(;(=&gt;6)/WSHSXSF4L:3R&amp;MJ=%W814`E99&lt;;'G83235.-OM#E]UQ;:.)Z*JWF0*`"[@]!SB)[JA!!!!!!HA!"!!)!!Q!&amp;!!!!7!!0!!!!!!!0!/U!YQ!!!'Y!$Q!!!!!!$Q$N!/-!!!#%!!]!!!!!!!]!\1$D!!!!GI!!A!#!!!!0!/U!YQ!!!*S!!)!!!_A!$Q$X!/E647FD=G^T&lt;W:U)%JI:7ZH3'6J)&amp;6*&amp;5VJ9X*P=W^G&gt;#"+;'6O:UBF;3"6326.;7.S&lt;X.P:H1A3GBF&lt;G&gt;):7EA65E"-!%Q!!"35V*$$1I!!UR71U.-1F:8!!#&amp;"!!!"()!!!!A!!#%Z!!!!!!!!!!!!!!!)!!!!$1!!!2E!!!!(%R*1EY!!!!!!!!"9%R75V)!!!!!!!!"&gt;&amp;*55U=!!!!!!!!"C%.$5V1!!!!!!!!"H%R*&gt;GE!!!!!!!!"M%.04F!!!!!!!!!"R&amp;2./$!!!!!"!!!"W%2'2&amp;-!!!!!!!!#!%R*:(-!!!!!!!!#&amp;&amp;:*1U1!!!!#!!!#+(:F=H-!!!!%!!!#:&amp;.$5V)!!!!!!!!#S%&gt;$5&amp;)!!!!!!!!#X%F$4UY!!!!!!!!#]'FD&lt;$1!!!!!!!!$"'FD&lt;$A!!!!!!!!$'%R*:H!!!!!!!!!$,%:13')!!!!!!!!$1%:15U5!!!!!!!!$6&amp;:12&amp;!!!!!!!!!$;%R*9G1!!!!!!!!$@%*%3')!!!!!!!!$E%*%5U5!!!!!!!!$J&amp;:*6&amp;-!!!!!!!!$O%253&amp;!!!!!!!!!$T%V6351!!!!!!!!$Y%B*5V1!!!!!!!!$^&amp;:$6&amp;!!!!!!!!!%#%:515)!!!!!!!!%(!!!!!$`````!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!-!!!!!!!!!!!`````Q!!!!!!!!$5!!!!!!!!!!$`````!!!!!!!!!/A!!!!!!!!!!0````]!!!!!!!!!]!!!!!!!!!!!`````Q!!!!!!!!6Q!!!!!!!!!!$`````!!!!!!!!"8A!!!!!!!!!!@````]!!!!!!!!&amp;J!!!!!!!!!!!`````Q!!!!!!!!7]!!!!!!!!!!$`````!!!!!!!!"AQ!!!!!!!!!!0````]!!!!!!!!'(!!!!!!!!!!"`````Q!!!!!!!!B1!!!!!!!!!!,`````!!!!!!!!#HA!!!!!!!!!"0````]!!!!!!!!4=!!!!!!!!!!(`````Q!!!!!!!"/%!!!!!!!!!!D`````!!!!!!!!%Z1!!!!!!!!!#@````]!!!!!!!!4K!!!!!!!!!!+`````Q!!!!!!!"/Y!!!!!!!!!!$`````!!!!!!!!%]Q!!!!!!!!!!0````]!!!!!!!!4Z!!!!!!!!!!!`````Q!!!!!!!"0Y!!!!!!!!!!$`````!!!!!!!!&amp;(Q!!!!!!!!!!0````]!!!!!!!!7A!!!!!!!!!!!`````Q!!!!!!!"K%!!!!!!!!!!$`````!!!!!!!!)?!!!!!!!!!!!0````]!!!!!!!"*]!!!!!!!!!!!`````Q!!!!!!!%HY!!!!!!!!!!$`````!!!!!!!!3A!!!!!!!!!!!0````]!!!!!!!"+%!!!!!!!!!!!`````Q!!!!!!!%JY!!!!!!!!!!$`````!!!!!!!!3I!!!!!!!!!!!0````]!!!!!!!"_C!!!!!!!!!!!`````Q!!!!!!!([1!!!!!!!!!!$`````!!!!!!!!@JA!!!!!!!!!!0````]!!!!!!!"_R!!!!!!!!!#!`````Q!!!!!!!)2!!!!!!!V5?#"0&gt;82Q&gt;81O9X2M!!!!!!</Property>
	<Property Name="NI.LVClass.Geneology" Type="Xml"><String>

<Name></Name>

<Val>!!!!!B2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;5?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q"16%AQ!!!!!!!!!!!!!!!"!!%!!!!!!!%!!!!!2A!.1!-!"U.I97ZO:7Q!3E"Q!"Y!!#M55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)515:433V/.T2$.&amp;.Y,GRW9WRB=X-!&amp;%&amp;'5UEN4D=U1T24?#ZM&gt;G.M98.T!!"%1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=Q!!%5V"4UUT/$!V-SZM&gt;G.M98.T!!F!"Q!$6'^Q!!F!"Q!$47FE!!F!"Q!$1G^U!')!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!;1&amp;!!!Q!$!!1!"1F.97FO)%.))$!!9A$R!!!!!!!!!!-55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)245&amp;044-Y-$5T,GRW9WRB=X-94X6U=(6U)&amp;:B&lt;(6F,5.M&gt;8.U:8)O9X2M!"J!5!!$!!-!"!!&amp;#5VB;7YA1UAA-1"C!0%!!!!!!!!!!R2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=RB0&gt;82Q&gt;81A6G&amp;M&gt;75N1WRV=X2F=CZD&gt;'Q!'E"1!!-!!Q!%!!5*47&amp;J&lt;C"$3#!S!')!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!;1&amp;!!!Q!$!!1!"1F.97FO)%.))$-!9A$R!!!!!!!!!!-55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)245&amp;044-Y-$5T,GRW9WRB=X-94X6U=(6U)&amp;:B&lt;(6F,5.M&gt;8.U:8)O9X2M!"J!5!!$!!-!"!!&amp;#5VB;7YA1UAA.!"C!0%!!!!!!!!!!R2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=RB0&gt;82Q&gt;81A6G&amp;M&gt;75N1WRV=X2F=CZD&gt;'Q!'E"1!!-!!Q!%!!5*47&amp;J&lt;C"$3#!V!')!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!;1&amp;!!!Q!$!!1!"1F.97FO)%.))$9!9A$R!!!!!!!!!!-55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)245&amp;044-Y-$5T,GRW9WRB=X-94X6U=(6U)&amp;:B&lt;(6F,5.M&gt;8.U:8)O9X2M!"J!5!!$!!-!"!!&amp;#5VB;7YA1UAA.Q!=1&amp;!!#!!'!!=!#!!*!!I!#Q!-!!U%47&amp;J&lt;A!!$U!+!!F#;7&amp;T)%.))$!!$U!+!!F#;7&amp;T)%.))$%!$U!+!!F#;7&amp;T)%.))$)!$U!+!!F#;7&amp;T)%.))$-!$U!+!!F#;7&amp;T)%.))$1!$U!+!!F#;7&amp;T)%.))$5!$U!+!!F#;7&amp;T)%.))$9!$U!+!!F#;7&amp;T)%.))$=!9!$R!!!!!!!!!!-55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)26(AA4X6U=(6U,GRW9WRB=X-11GFB=SV$&lt;(6T&gt;'6S,G.U&lt;!!A1&amp;!!#!!0!"!!%1!3!"-!&amp;!!6!"9%1GFB=Q!!$U!+!!B.&lt;W1A1UAA-!!!$U!+!!B.&lt;W1A1UAA-1!!$U!+!!B.&lt;W1A1UAA-A!!$U!+!!B.&lt;W1A1UAA-Q!!$U!+!!B.&lt;W1A1UAA.!!!$U!+!!B.&lt;W1A1UAA.1!!$U!+!!B.&lt;W1A1UAA.A!!$U!+!!B.&lt;W1A1UAA.Q!!81$R!!!!!!!!!!-55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)26(AA4X6U=(6U,GRW9WRB=X-047^E,5.M&gt;8.U:8)O9X2M!"Z!5!!)!"A!'1!;!"M!(!!&gt;!"Y!(Q..&lt;W1!&amp;5!&amp;!!Z#97ZE&gt;WFE&gt;'AA1UAA-!!!&amp;5!&amp;!!Z#97ZE&gt;WFE&gt;'AA1UAA-1!!&amp;5!&amp;!!Z#97ZE&gt;WFE&gt;'AA1UAA-A!!&amp;5!&amp;!!Z#97ZE&gt;WFE&gt;'AA1UAA-Q!!&amp;5!&amp;!!Z#97ZE&gt;WFE&gt;'AA1UAA.!!!&amp;5!&amp;!!Z#97ZE&gt;WFE&gt;'AA1UAA.1!!&amp;5!&amp;!!Z#97ZE&gt;WFE&gt;'AA1UAA.A!!&amp;5!&amp;!!Z#97ZE&gt;WFE&gt;'AA1UAA.Q!!;1$R!!!!!!!!!!-55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)26(AA4X6U=(6U,GRW9WRB=X-61G&amp;O:(&gt;J:(2I,5.M&gt;8.U:8)O9X2M!#2!5!!)!#%!)A!D!#1!*1!G!#=!+!F#97ZE&gt;WFE&gt;'A!3E"Q!"Y!!$!74X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC="&gt;0=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:D&lt;'&amp;T=Q!!$V"S&lt;W2V9X1O&lt;(:D&lt;'&amp;T=Q!31$$`````#%RP&gt;#"$&lt;W2F!!"C!0%!!!!!!!!!!R2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=RB0&gt;82Q&gt;81A6G&amp;M&gt;75N1WRV=X2F=CZD&gt;'Q!'E"1!!-!!Q!%!!5)5(*F)%.))$!!!')!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!;1&amp;!!!Q!$!!1!"1B1=G5A1UAA-1!!9A$R!!!!!!!!!!-55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)245&amp;044-Y-$5T,GRW9WRB=X-94X6U=(6U)&amp;:B&lt;(6F,5.M&gt;8.U:8)O9X2M!"J!5!!$!!-!"!!&amp;#&amp;"S:3"$3#!S!!"C!0%!!!!!!!!!!R2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=RB0&gt;82Q&gt;81A6G&amp;M&gt;75N1WRV=X2F=CZD&gt;'Q!'E"1!!-!!Q!%!!5)5(*F)%.))$-!!')!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!;1&amp;!!!Q!$!!1!"1B1=G5A1UAA.!!!9A$R!!!!!!!!!!-55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)245&amp;044-Y-$5T,GRW9WRB=X-94X6U=(6U)&amp;:B&lt;(6F,5.M&gt;8.U:8)O9X2M!"J!5!!$!!-!"!!&amp;#&amp;"S:3"$3#!V!!"C!0%!!!!!!!!!!R2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=RB0&gt;82Q&gt;81A6G&amp;M&gt;75N1WRV=X2F=CZD&gt;'Q!'E"1!!-!!Q!%!!5)5(*F)%.))$9!!')!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!;1&amp;!!!Q!$!!1!"1B1=G5A1UAA.Q!!'E"1!!A!,!!N!#Y!,Q!Q!$%!-A!T!V"S:1"E!0%!!!!!!!!!!R2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=RB0&gt;82Q&gt;81A6G&amp;M&gt;75N1WRV=X2F=CZD&gt;'Q!(%"1!!-!!Q!%!!5+2'6M98EA1UAA-!!!:!$R!!!!!!!!!!-55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)245&amp;044-Y-$5T,GRW9WRB=X-94X6U=(6U)&amp;:B&lt;(6F,5.M&gt;8.U:8)O9X2M!"R!5!!$!!-!"!!&amp;#E2F&lt;'&amp;Z)%.))$%!!'1!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!=1&amp;!!!Q!$!!1!"1J%:7RB?3"$3#!S!!"E!0%!!!!!!!!!!R2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=RB0&gt;82Q&gt;81A6G&amp;M&gt;75N1WRV=X2F=CZD&gt;'Q!(%"1!!-!!Q!%!!5+2'6M98EA1UAA-Q!!:!$R!!!!!!!!!!-55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)245&amp;044-Y-$5T,GRW9WRB=X-94X6U=(6U)&amp;:B&lt;(6F,5.M&gt;8.U:8)O9X2M!"R!5!!$!!-!"!!&amp;#E2F&lt;'&amp;Z)%.))$1!!'1!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!=1&amp;!!!Q!$!!1!"1J%:7RB?3"$3#!V!!"E!0%!!!!!!!!!!R2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=RB0&gt;82Q&gt;81A6G&amp;M&gt;75N1WRV=X2F=CZD&gt;'Q!(%"1!!-!!Q!%!!5+2'6M98EA1UAA.A!!:!$R!!!!!!!!!!-55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)245&amp;044-Y-$5T,GRW9WRB=X-94X6U=(6U)&amp;:B&lt;(6F,5.M&gt;8.U:8)O9X2M!"R!5!!$!!-!"!!&amp;#E2F&lt;'&amp;Z)%.))$=!!"R!5!!)!$5!.A!X!$A!/1![!$M!0!6%:7RB?1!-1#%'1WBJ=#!R!!!-1#%'1WBJ=#!S!!"B!0%!!!!!!!!!!R2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;5?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=R&gt;$;'FQ)&amp;.U982V=SV$&lt;(6T&gt;'6S,G.U&lt;!!;1&amp;!!!A!_!$]+1U23)&amp;.U982V=Q!!91$R!!!!!!!!!!-55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)26(AA4X6U=(6U,GRW9WRB=X-81WBJ=#"4&gt;'&amp;U&gt;8-N1WRV=X2F=CZD&gt;'Q!'E"1!!)!0A!`#ER%2#"4&gt;'&amp;U&gt;8-!!*]!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T&amp;F2V&lt;G5A5'^M98*J&gt;(EN27ZV&lt;3ZD&gt;'Q!75!7!!114G^S&lt;7&amp;M)%^Q:8*B&gt;'FP&lt;B6115UU)%FO&gt;G6S&gt;#"197FS)&amp;.X98!/5%&amp;..#"*&lt;H:F=H.J&lt;WY*5'&amp;J=C"4&gt;W&amp;Q!!V5&gt;7ZF)&amp;"P&lt;'&amp;S;82Z!!Z!)1F4&gt;'^Q)&amp;2V&lt;G5!&amp;%"Q!"E!!1"$#6.U&lt;X!A6(6O:1#$!0(;HT$8!!!!!R2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;5?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=QV5?#"0&gt;82Q&gt;81O9X2M!%:!5!!0!!!!!1!#!!Y!&amp;Q!A!#E!+A!L!$1!01"!!%%!1A"%(5.M&gt;8.U:8)A&lt;W9A9WRB=X-A=(*J&gt;G&amp;U:3"E982B!!%!21!!!)M!!!!!``````````]!!!!$!!!!"!!!!!5!!!!'!!!!"Q!!!!A!!!!*!!!!#A!!!!M!!!!-!!!!$1!!!!Y!!!!0!!!!%!!!!"%!!!!3!!!!%Q!!!"1!!!!6!!!!&amp;A!!!"=!!!!9!!!!'1!!!"I!!!!&lt;!!!!(!!!!"U!!!!?!!!!(Q!!!#!!!!!B!!!!)A!!!#-!!!!E!!!!*1!!!#9!!!!H!!!!+!!!!#E!!!!K!!!!+Q!!!#Q!!!!N!!!!,A!!!#]!!!!Q!!!!-1!!!$)!!!!T!!!!.!!!!$5!!!!W!!!!.Q!!!$A!!!!Z!!!!/A!!!$M!!!!]!!!!01!!!$Y!!!!`!!!!1!!!!%%!!!"#!!!!1Q!!!%1!!!"&amp;!!!!2A!!!%=!!!")!!!!31!!!%I!!!",!!!!4!!!!%U!!!"/!!!!4Q!!!&amp;!!!!"2!!!!5A!!!&amp;-!!!"5!!!!61!!!&amp;9!!!"8!!!!7!!!!&amp;E!!!";!!!!7Q!!!&amp;Q!!!"&gt;!!!!8A!!!&amp;]!!!"A!!!!91!!!')!!!"D!!!!:!!!!'5!!!"G!!!!:Q!!!'A!!!"J!!!!;A!!!'M!!!"M!!!!&lt;1!!!'Y!!!"P!!!!=!!!!(%!!!"S!!!!=Q!!!(1!!!"V!!!!&gt;A!!!(=!!!"Y!!!!?1!!!(I!!!"\!!!!@!!!!(U!!!"_!!!!@Q!!!)!!!!#"!!!!AA!!!)-!!!#%!!!!B1!!!)9!!!#(!!!!C!!!!)E!!!#+!!!!!!!!!!%L&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC&amp;%&amp;'5UEN4D=U1T24?#ZM&gt;G.M98.T!!!!!!!!!!!!!!!!!3A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)245&amp;044-Y-$5T,GRW9WRB=X-!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!4!74X"U;7.B&lt;#"1=G^E&gt;7.U,GRW&lt;'FC="&gt;0=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:D&lt;'&amp;T=Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!%/4'&amp;C6EF&amp;6S"09GJF9X1!5&amp;2)-!!!!!!!!!!!!"=!A!!!!!!!!!!!!!!(!!!!*6""441A2'6W;7.F,GRW&lt;'FC/F""441A2'6W;7.F,GRW9WRB=X-!!!!I5%&amp;..#"%:8:J9W5O&lt;(:M;7)[5%&amp;..#"5?#"%:8:J9W5O&lt;(:D&lt;'&amp;T=Q!!!#N115UU)&amp;2Y)%2F&gt;GFD:3ZM&gt;GRJ9DJ115UU)&amp;2Y)%2F&gt;GFD:3ZM&gt;G.M98.T!!!!,&amp;""441A1U23)%2F&gt;GFD:3ZM&gt;GRJ9DJ115UU)&amp;2Y)%2F&gt;GFD:3ZM&gt;G.M98.T!!!!*V""441A1U23)%2F&gt;GFD:3ZM&gt;GRJ9DJ5?#"%:8:J9W5O&lt;(:D&lt;'&amp;T=Q!!!#&gt;115UU)%.%5C"%:8:J9W5O&lt;(:M;7)[6(AA4X6U=(6U,GRW9WRB=X-!!!!P56.'5#V%2#"115UU)%.%5C"%:8:J9W5O&lt;(:M;7)[6(AA4X6U=(6U,GRW9WRB=X-</Val>

</String>

</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">false</Property>
	<Property Name="NI.LVClass.LowestCompatibleVersion" Type="Str">1.0.0.0</Property>
	<Property Name="NI_IconEditor" Type="Str">49 55 48 49 56 48 49 48 13 0 0 0 0 1 23 21 76 111 97 100 32 38 32 85 110 108 111 97 100 46 108 118 99 108 97 115 115 0 0 1 0 0 0 0 0 9 0 0 25 221 1 100 1 100 80 84 72 48 0 0 0 33 0 1 0 3 7 76 105 98 114 97 114 121 9 51 114 100 32 112 97 114 116 121 10 95 98 108 97 110 107 46 112 110 103 0 0 12 158 0 40 0 0 12 152 0 0 12 0 0 0 0 0 0 32 0 32 0 24 0 0 0 0 0 255 255 255 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 255 255 255 255 128 0 0 1 128 0 0 1 128 0 0 1 128 0 0 1 128 0 0 1 128 0 0 1 128 0 0 1 255 255 255 255 128 0 0 1 128 0 0 1 128 0 0 1 128 0 0 1 128 0 0 1 128 0 0 1 128 0 0 1 128 0 0 1 128 0 0 1 128 0 0 1 128 0 0 1 128 0 0 1 128 0 0 1 128 0 0 1 128 0 0 1 128 0 0 1 128 0 0 1 128 0 0 1 128 0 0 1 128 0 0 1 128 0 0 1 128 0 0 1 255 255 255 255 0 0 0 1 0 0 0 1 15 13 76 97 121 101 114 46 108 118 99 108 97 115 115 0 0 1 0 0 0 0 0 7 0 0 12 182 0 0 0 0 0 0 0 0 0 0 12 158 0 40 0 0 12 152 0 0 12 0 0 0 0 0 0 32 0 32 0 24 0 0 0 0 0 255 255 255 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 192 18 255 192 18 255 192 18 255 192 18 255 192 18 255 192 18 255 192 18 255 192 18 255 192 18 255 192 18 255 192 18 255 192 18 255 192 18 255 192 18 255 192 18 255 192 18 255 192 18 255 192 18 255 192 18 255 192 18 255 192 18 255 192 18 255 192 18 255 192 18 255 192 18 255 192 18 255 192 18 255 192 18 255 192 18 255 192 18 255 255 255 255 255 255 255 192 18 255 246 246 246 246 246 246 246 246 246 192 18 255 192 18 255 192 18 255 246 246 246 246 246 246 192 18 255 192 18 255 246 246 246 192 18 255 192 18 255 192 18 255 246 246 246 192 18 255 192 18 255 192 18 255 246 246 246 192 18 255 192 18 255 246 246 246 246 246 246 246 246 246 192 18 255 246 246 246 192 18 255 246 246 246 192 18 255 255 255 255 255 255 255 192 18 255 246 246 246 192 18 255 192 18 255 246 246 246 192 18 255 192 18 255 246 246 246 246 246 246 192 18 255 192 18 255 246 246 246 246 246 246 192 18 255 246 246 246 246 246 246 192 18 255 192 18 255 246 246 246 246 246 246 192 18 255 192 18 255 192 18 255 246 246 246 192 18 255 192 18 255 246 246 246 192 18 255 246 246 246 192 18 255 255 255 255 255 255 255 192 18 255 246 246 246 246 246 246 246 246 246 192 18 255 192 18 255 246 246 246 192 18 255 192 18 255 246 246 246 192 18 255 246 246 246 192 18 255 246 246 246 192 18 255 246 246 246 192 18 255 246 246 246 192 18 255 246 246 246 192 18 255 192 18 255 192 18 255 246 246 246 192 18 255 192 18 255 192 18 255 246 246 246 192 18 255 192 18 255 255 255 255 255 255 255 192 18 255 246 246 246 192 18 255 192 18 255 192 18 255 192 18 255 246 246 246 246 246 246 246 246 246 246 246 246 192 18 255 246 246 246 192 18 255 192 18 255 192 18 255 246 246 246 192 18 255 246 246 246 246 246 246 246 246 246 192 18 255 192 18 255 192 18 255 246 246 246 192 18 255 192 18 255 246 246 246 192 18 255 246 246 246 192 18 255 255 255 255 255 255 255 192 18 255 246 246 246 192 18 255 192 18 255 192 18 255 192 18 255 246 246 246 192 18 255 192 18 255 246 246 246 192 18 255 246 246 246 192 18 255 192 18 255 192 18 255 246 246 246 192 18 255 192 18 255 192 18 255 246 246 246 192 18 255 192 18 255 192 18 255 246 246 246 192 18 255 192 18 255 246 246 246 192 18 255 246 246 246 192 18 255 255 255 255 255 255 255 192 18 255 192 18 255 192 18 255 192 18 255 192 18 255 192 18 255 192 18 255 192 18 255 192 18 255 192 18 255 192 18 255 192 18 255 192 18 255 192 18 255 192 18 255 192 18 255 192 18 255 192 18 255 192 18 255 192 18 255 192 18 255 192 18 255 192 18 255 192 18 255 192 18 255 192 18 255 192 18 255 192 18 255 192 18 255 192 18 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 127 255 255 254 127 255 255 254 127 255 255 254 127 255 255 254 127 255 255 254 127 255 255 254 127 255 255 254 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 4 70 105 108 108 100 1 0 2 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 11 83 109 97 108 108 32 70 111 110 116 115 0 1 8 1 1

</Property>
	<Item Name="Tx Output.ctl" Type="Class Private Data" URL="Tx Output.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="Accessor" Type="Folder">
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Item Name="AFSI-N74C4Sx" Type="Folder">
			<Property Name="NI.SortType" Type="Int">3</Property>
			<Item Name="Bias" Type="Folder">
				<Item Name="Bias" Type="Property Definition">
					<Property Name="NI.ClassItem.Property.LongName" Type="Str">Data:LDD:Bias:All</Property>
					<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Bias</Property>
					<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
					<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
					<Item Name="Read Bias.vi" Type="VI" URL="../Accessor/Bias Property/Read Bias.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)G!!!!%A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!^!#A!*1GFB=S"$3#!Q!!^!#A!*1GFB=S"$3#!R!!^!#A!*1GFB=S"$3#!S!!^!#A!*1GFB=S"$3#!T!!^!#A!*1GFB=S"$3#!U!!^!#A!*1GFB=S"$3#!V!!^!#A!*1GFB=S"$3#!W!!^!#A!*1GFB=S"$3#!X!'1!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T%%*J98-N1WRV=X2F=CZD&gt;'Q!*%"1!!A!"1!'!!=!#!!*!!I!#Q!-#5*J98-A2'&amp;U91"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;5?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q!!$62Y)%2F&gt;GFD:3"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;5?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q!!$&amp;2Y)%2F&gt;GFD:3"J&lt;A!!91$Q!!Q!!Q!%!!U!$A!%!!1!"!!%!!]!"!!%!"!#!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!%1!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">276832256</Property>
					</Item>
					<Item Name="Write Bias.vi" Type="VI" URL="../Accessor/Bias Property/Write Bias.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)G!!!!%A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA2'6W;7.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!^!#A!*1GFB=S"$3#!Q!!^!#A!*1GFB=S"$3#!R!!^!#A!*1GFB=S"$3#!S!!^!#A!*1GFB=S"$3#!T!!^!#A!*1GFB=S"$3#!U!!^!#A!*1GFB=S"$3#!V!!^!#A!*1GFB=S"$3#!W!!^!#A!*1GFB=S"$3#!X!'1!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T%%*J98-N1WRV=X2F=CZD&gt;'Q!*%"1!!A!"Q!)!!E!#A!,!!Q!$1!/#5*J98-A2'&amp;U91"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;5?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q!!$&amp;2Y)%2F&gt;GFD:3"J&lt;A!!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!0!"!#!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!%1!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">276832256</Property>
					</Item>
				</Item>
				<Item Name="Bias CH 0" Type="Property Definition">
					<Property Name="NI.ClassItem.Property.LongName" Type="Str">Data:LDD:Bias:CH 0</Property>
					<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Bias CH 0</Property>
					<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
					<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
					<Item Name="Read Bias CH 0.vi" Type="VI" URL="../Accessor/Bias CH 0 Property/Read Bias CH 0.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;:!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!^!#A!*1GFB=S"$3#!Q!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA2'6W;7.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA2'6W;7.F)'FO!!"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!)!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
					</Item>
					<Item Name="Write Bias CH 0.vi" Type="VI" URL="../Accessor/Bias CH 0 Property/Write Bias CH 0.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;:!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA2'6W;7.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!^!#A!*1GFB=S"$3#!Q!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA2'6W;7.F)'FO!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
					</Item>
				</Item>
				<Item Name="Bias CH 1" Type="Property Definition">
					<Property Name="NI.ClassItem.Property.LongName" Type="Str">Data:LDD:Bias:CH 1</Property>
					<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Bias CH 1</Property>
					<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
					<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
					<Item Name="Read Bias CH 1.vi" Type="VI" URL="../Accessor/Bias CH 1 Property/Read Bias CH 1.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;:!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!^!#A!*1GFB=S"$3#!R!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA2'6W;7.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA2'6W;7.F)'FO!!"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!)!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
					</Item>
					<Item Name="Write Bias CH 1.vi" Type="VI" URL="../Accessor/Bias CH 1 Property/Write Bias CH 1.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;:!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA2'6W;7.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!^!#A!*1GFB=S"$3#!R!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA2'6W;7.F)'FO!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
					</Item>
				</Item>
				<Item Name="Bias CH 2" Type="Property Definition">
					<Property Name="NI.ClassItem.Property.LongName" Type="Str">Data:LDD:Bias:CH 2</Property>
					<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Bias CH 2</Property>
					<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
					<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
					<Item Name="Read Bias CH 2.vi" Type="VI" URL="../Accessor/Bias CH 2 Property/Read Bias CH 2.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;:!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!^!#A!*1GFB=S"$3#!S!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA2'6W;7.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA2'6W;7.F)'FO!!"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!)!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
					</Item>
					<Item Name="Write Bias CH 2.vi" Type="VI" URL="../Accessor/Bias CH 2 Property/Write Bias CH 2.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;:!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA2'6W;7.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!^!#A!*1GFB=S"$3#!S!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA2'6W;7.F)'FO!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
					</Item>
				</Item>
				<Item Name="Bias CH 3" Type="Property Definition">
					<Property Name="NI.ClassItem.Property.LongName" Type="Str">Data:LDD:Bias:CH 3</Property>
					<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Bias CH 3</Property>
					<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
					<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
					<Item Name="Read Bias CH 3.vi" Type="VI" URL="../Accessor/Bias CH 3 Property/Read Bias CH 3.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;:!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!^!#A!*1GFB=S"$3#!T!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA2'6W;7.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA2'6W;7.F)'FO!!"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!)!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
					</Item>
					<Item Name="Write Bias CH 3.vi" Type="VI" URL="../Accessor/Bias CH 3 Property/Write Bias CH 3.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;:!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA2'6W;7.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!^!#A!*1GFB=S"$3#!T!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA2'6W;7.F)'FO!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
					</Item>
				</Item>
				<Item Name="Bias CH 4" Type="Property Definition">
					<Property Name="NI.ClassItem.Property.LongName" Type="Str">Data:LDD:Bias:CH 4</Property>
					<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Bias CH 4</Property>
					<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
					<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
					<Item Name="Read Bias CH 4.vi" Type="VI" URL="../Accessor/Bias CH 4 Property/Read Bias CH 4.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;:!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!^!#A!*1GFB=S"$3#!U!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA2'6W;7.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA2'6W;7.F)'FO!!"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!)!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
					</Item>
					<Item Name="Write Bias CH 4.vi" Type="VI" URL="../Accessor/Bias CH 4 Property/Write Bias CH 4.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;:!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA2'6W;7.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!^!#A!*1GFB=S"$3#!U!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA2'6W;7.F)'FO!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
					</Item>
				</Item>
				<Item Name="Bias CH 5" Type="Property Definition">
					<Property Name="NI.ClassItem.Property.LongName" Type="Str">Data:LDD:Bias:CH 5</Property>
					<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Bias CH 5</Property>
					<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
					<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
					<Item Name="Read Bias CH 5.vi" Type="VI" URL="../Accessor/Bias CH 5 Property/Read Bias CH 5.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;:!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!^!#A!*1GFB=S"$3#!V!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA2'6W;7.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA2'6W;7.F)'FO!!"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!)!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
					</Item>
					<Item Name="Write Bias CH 5.vi" Type="VI" URL="../Accessor/Bias CH 5 Property/Write Bias CH 5.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;:!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA2'6W;7.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!^!#A!*1GFB=S"$3#!V!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA2'6W;7.F)'FO!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
					</Item>
				</Item>
				<Item Name="Bias CH 6" Type="Property Definition">
					<Property Name="NI.ClassItem.Property.LongName" Type="Str">Data:LDD:Bias:CH 6</Property>
					<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Bias CH 6</Property>
					<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
					<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
					<Item Name="Read Bias CH 6.vi" Type="VI" URL="../Accessor/Bias CH 6 Property/Read Bias CH 6.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;:!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!^!#A!*1GFB=S"$3#!W!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA2'6W;7.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA2'6W;7.F)'FO!!"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!)!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
					</Item>
					<Item Name="Write Bias CH 6.vi" Type="VI" URL="../Accessor/Bias CH 6 Property/Write Bias CH 6.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;:!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA2'6W;7.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!^!#A!*1GFB=S"$3#!W!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA2'6W;7.F)'FO!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
					</Item>
				</Item>
				<Item Name="Bias CH 7" Type="Property Definition">
					<Property Name="NI.ClassItem.Property.LongName" Type="Str">Data:LDD:Bias:CH 7</Property>
					<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Bias CH 7</Property>
					<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
					<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
					<Item Name="Read Bias CH 7.vi" Type="VI" URL="../Accessor/Bias CH 7 Property/Read Bias CH 7.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;:!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!^!#A!*1GFB=S"$3#!X!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA2'6W;7.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA2'6W;7.F)'FO!!"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!)!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
					</Item>
					<Item Name="Write Bias CH 7.vi" Type="VI" URL="../Accessor/Bias CH 7 Property/Write Bias CH 7.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;:!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA2'6W;7.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!^!#A!*1GFB=S"$3#!X!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA2'6W;7.F)'FO!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
					</Item>
				</Item>
			</Item>
			<Item Name="Mod" Type="Folder">
				<Item Name="Mod" Type="Property Definition">
					<Property Name="NI.ClassItem.Property.LongName" Type="Str">Data:LDD:Mod:All</Property>
					<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Mod</Property>
					<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
					<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
					<Item Name="Read Mod.vi" Type="VI" URL="../Accessor/Mod Property/Read Mod.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)F!!!!%A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!^!#A!)47^E)%.))$!!!!^!#A!)47^E)%.))$%!!!^!#A!)47^E)%.))$)!!!^!#A!)47^E)%.))$-!!!^!#A!)47^E)%.))$1!!!^!#A!)47^E)%.))$5!!!^!#A!)47^E)%.))$9!!!^!#A!)47^E)%.))$=!!'-!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T$UVP:#V$&lt;(6T&gt;'6S,G.U&lt;!!E1&amp;!!#!!&amp;!!9!"Q!)!!E!#A!,!!Q)47^E)%2B&gt;'%!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA2'6W;7.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA2'6W;7.F)'FO!!"B!0!!$!!$!!1!$1!/!!1!"!!%!!1!$Q!%!!1!%!)!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!2!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">276832256</Property>
					</Item>
					<Item Name="Write Mod.vi" Type="VI" URL="../Accessor/Mod Property/Write Mod.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)F!!!!%A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA2'6W;7.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!^!#A!)47^E)%.))$!!!!^!#A!)47^E)%.))$%!!!^!#A!)47^E)%.))$)!!!^!#A!)47^E)%.))$-!!!^!#A!)47^E)%.))$1!!!^!#A!)47^E)%.))$5!!!^!#A!)47^E)%.))$9!!!^!#A!)47^E)%.))$=!!'-!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T$UVP:#V$&lt;(6T&gt;'6S,G.U&lt;!!E1&amp;!!#!!(!!A!#1!+!!M!$!!.!!Y)47^E)%2B&gt;'%!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA2'6W;7.F)'FO!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!]!%!)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!2!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">276832256</Property>
					</Item>
				</Item>
				<Item Name="Mod CH 0" Type="Property Definition">
					<Property Name="NI.ClassItem.Property.LongName" Type="Str">Data:LDD:Mod:CH 0</Property>
					<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Mod CH 0</Property>
					<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
					<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
					<Item Name="Read Mod CH 0.vi" Type="VI" URL="../Accessor/Mod CH 0 Property/Read Mod CH 0.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;:!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!^!#A!)47^E)%.))$!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA2'6W;7.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA2'6W;7.F)'FO!!"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!)!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
					</Item>
					<Item Name="Write Mod CH 0.vi" Type="VI" URL="../Accessor/Mod CH 0 Property/Write Mod CH 0.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;:!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA2'6W;7.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!^!#A!)47^E)%.))$!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA2'6W;7.F)'FO!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
					</Item>
				</Item>
				<Item Name="Mod CH 1" Type="Property Definition">
					<Property Name="NI.ClassItem.Property.LongName" Type="Str">Data:LDD:Mod:CH 1</Property>
					<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Mod CH 1</Property>
					<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
					<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
					<Item Name="Read Mod CH 1.vi" Type="VI" URL="../Accessor/Mod CH 1 Property/Read Mod CH 1.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;:!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!^!#A!)47^E)%.))$%!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA2'6W;7.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA2'6W;7.F)'FO!!"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!)!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
					</Item>
					<Item Name="Write Mod CH 1.vi" Type="VI" URL="../Accessor/Mod CH 1 Property/Write Mod CH 1.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;:!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA2'6W;7.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!^!#A!)47^E)%.))$%!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA2'6W;7.F)'FO!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
					</Item>
				</Item>
				<Item Name="Mod CH 2" Type="Property Definition">
					<Property Name="NI.ClassItem.Property.LongName" Type="Str">Data:LDD:Mod:CH 2</Property>
					<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Mod CH 2</Property>
					<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
					<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
					<Item Name="Read Mod CH 2.vi" Type="VI" URL="../Accessor/Mod CH 2 Property/Read Mod CH 2.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;:!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!^!#A!)47^E)%.))$)!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA2'6W;7.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA2'6W;7.F)'FO!!"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!)!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
					</Item>
					<Item Name="Write Mod CH 2.vi" Type="VI" URL="../Accessor/Mod CH 2 Property/Write Mod CH 2.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;:!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA2'6W;7.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!^!#A!)47^E)%.))$)!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA2'6W;7.F)'FO!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
					</Item>
				</Item>
				<Item Name="Mod CH 3" Type="Property Definition">
					<Property Name="NI.ClassItem.Property.LongName" Type="Str">Data:LDD:Mod:CH 3</Property>
					<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Mod CH 3</Property>
					<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
					<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
					<Item Name="Read Mod CH 3.vi" Type="VI" URL="../Accessor/Mod CH 3 Property/Read Mod CH 3.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;:!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!^!#A!)47^E)%.))$-!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA2'6W;7.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA2'6W;7.F)'FO!!"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!)!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
					</Item>
					<Item Name="Write Mod CH 3.vi" Type="VI" URL="../Accessor/Mod CH 3 Property/Write Mod CH 3.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;:!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA2'6W;7.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!^!#A!)47^E)%.))$-!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA2'6W;7.F)'FO!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
					</Item>
				</Item>
				<Item Name="Mod CH 4" Type="Property Definition">
					<Property Name="NI.ClassItem.Property.LongName" Type="Str">Data:LDD:Mod:CH 4</Property>
					<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Mod CH 4</Property>
					<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
					<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
					<Item Name="Read Mod CH 4.vi" Type="VI" URL="../Accessor/Mod CH 4 Property/Read Mod CH 4.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;:!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!^!#A!)47^E)%.))$1!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA2'6W;7.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA2'6W;7.F)'FO!!"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!)!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
					</Item>
					<Item Name="Write Mod CH 4.vi" Type="VI" URL="../Accessor/Mod CH 4 Property/Write Mod CH 4.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;:!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA2'6W;7.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!^!#A!)47^E)%.))$1!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA2'6W;7.F)'FO!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
					</Item>
				</Item>
				<Item Name="Mod CH 5" Type="Property Definition">
					<Property Name="NI.ClassItem.Property.LongName" Type="Str">Data:LDD:Mod:CH 5</Property>
					<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Mod CH 5</Property>
					<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
					<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
					<Item Name="Read Mod CH 5.vi" Type="VI" URL="../Accessor/Mod CH 5 Property/Read Mod CH 5.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;:!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!^!#A!)47^E)%.))$5!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA2'6W;7.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA2'6W;7.F)'FO!!"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!)!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
					</Item>
					<Item Name="Write Mod CH 5.vi" Type="VI" URL="../Accessor/Mod CH 5 Property/Write Mod CH 5.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;:!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA2'6W;7.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!^!#A!)47^E)%.))$5!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA2'6W;7.F)'FO!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
					</Item>
				</Item>
				<Item Name="Mod CH 6" Type="Property Definition">
					<Property Name="NI.ClassItem.Property.LongName" Type="Str">Data:LDD:Mod:CH 6</Property>
					<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Mod CH 6</Property>
					<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
					<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
					<Item Name="Read Mod CH 6.vi" Type="VI" URL="../Accessor/Mod CH 6 Property/Read Mod CH 6.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;:!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!^!#A!)47^E)%.))$9!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA2'6W;7.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA2'6W;7.F)'FO!!"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!)!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
					</Item>
					<Item Name="Write Mod CH 6.vi" Type="VI" URL="../Accessor/Mod CH 6 Property/Write Mod CH 6.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;:!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA2'6W;7.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!^!#A!)47^E)%.))$9!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA2'6W;7.F)'FO!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
					</Item>
				</Item>
				<Item Name="Mod CH 7" Type="Property Definition">
					<Property Name="NI.ClassItem.Property.LongName" Type="Str">Data:LDD:Mod:CH 7</Property>
					<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Mod CH 7</Property>
					<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
					<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
					<Item Name="Read Mod CH 7.vi" Type="VI" URL="../Accessor/Mod CH 7 Property/Read Mod CH 7.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;:!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!^!#A!)47^E)%.))$=!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA2'6W;7.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA2'6W;7.F)'FO!!"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!)!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
					</Item>
					<Item Name="Write Mod CH 7.vi" Type="VI" URL="../Accessor/Mod CH 7 Property/Write Mod CH 7.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;:!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA2'6W;7.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!^!#A!)47^E)%.))$=!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA2'6W;7.F)'FO!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
					</Item>
				</Item>
			</Item>
		</Item>
		<Item Name="MAOM38053" Type="Folder">
			<Item Name="Main" Type="Folder">
				<Item Name="Main" Type="Property Definition">
					<Property Name="NI.ClassItem.Property.LongName" Type="Str">Data:CDR:Main:All</Property>
					<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Main</Property>
					<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
					<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
					<Item Name="Read Main.vi" Type="VI" URL="../Accessor/Main Property/Read Main.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!32!!!!&amp;1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!F!"Q!$6'^Q!!F!"Q!$47FE!!F!"Q!$1G^U!')!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!;1&amp;!!!Q!&amp;!!9!"QF.97FO)%.))$!!9A$R!!!!!!!!!!-55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)245&amp;044-Y-$5T,GRW9WRB=X-94X6U=(6U)&amp;:B&lt;(6F,5.M&gt;8.U:8)O9X2M!"J!5!!$!!5!"A!(#5VB;7YA1UAA-1"C!0%!!!!!!!!!!R2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=RB0&gt;82Q&gt;81A6G&amp;M&gt;75N1WRV=X2F=CZD&gt;'Q!'E"1!!-!"1!'!!=*47&amp;J&lt;C"$3#!S!')!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!;1&amp;!!!Q!&amp;!!9!"QF.97FO)%.))$-!9A$R!!!!!!!!!!-55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)245&amp;044-Y-$5T,GRW9WRB=X-94X6U=(6U)&amp;:B&lt;(6F,5.M&gt;8.U:8)O9X2M!"J!5!!$!!5!"A!(#5VB;7YA1UAA.!"C!0%!!!!!!!!!!R2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=RB0&gt;82Q&gt;81A6G&amp;M&gt;75N1WRV=X2F=CZD&gt;'Q!'E"1!!-!"1!'!!=*47&amp;J&lt;C"$3#!V!')!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!;1&amp;!!!Q!&amp;!!9!"QF.97FO)%.))$9!9A$R!!!!!!!!!!-55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)245&amp;044-Y-$5T,GRW9WRB=X-94X6U=(6U)&amp;:B&lt;(6F,5.M&gt;8.U:8)O9X2M!"J!5!!$!!5!"A!(#5VB;7YA1UAA.Q!=1&amp;!!#!!)!!E!#A!,!!Q!$1!/!!]%47&amp;J&lt;A!!1%"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)26(AA4X6U=(6U,GRW9WRB=X-!!!V5?#"%:8:J9W5A&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!1%"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)26(AA4X6U=(6U,GRW9WRB=X-!!!R5?#"%:8:J9W5A;7Y!!'%!]!!-!!-!"!!1!"%!"!!%!!1!"!!3!!1!"!!4!A!!?!!!$1A!!!!!!!!*!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!"1!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">276832256</Property>
					</Item>
					<Item Name="Write Main.vi" Type="VI" URL="../Accessor/Main Property/Write Main.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!32!!!!&amp;1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA2'6W;7.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!F!"Q!$6'^Q!!F!"Q!$47FE!!F!"Q!$1G^U!')!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!;1&amp;!!!Q!(!!A!#1F.97FO)%.))$!!9A$R!!!!!!!!!!-55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)245&amp;044-Y-$5T,GRW9WRB=X-94X6U=(6U)&amp;:B&lt;(6F,5.M&gt;8.U:8)O9X2M!"J!5!!$!!=!#!!*#5VB;7YA1UAA-1"C!0%!!!!!!!!!!R2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=RB0&gt;82Q&gt;81A6G&amp;M&gt;75N1WRV=X2F=CZD&gt;'Q!'E"1!!-!"Q!)!!E*47&amp;J&lt;C"$3#!S!')!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!;1&amp;!!!Q!(!!A!#1F.97FO)%.))$-!9A$R!!!!!!!!!!-55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)245&amp;044-Y-$5T,GRW9WRB=X-94X6U=(6U)&amp;:B&lt;(6F,5.M&gt;8.U:8)O9X2M!"J!5!!$!!=!#!!*#5VB;7YA1UAA.!"C!0%!!!!!!!!!!R2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=RB0&gt;82Q&gt;81A6G&amp;M&gt;75N1WRV=X2F=CZD&gt;'Q!'E"1!!-!"Q!)!!E*47&amp;J&lt;C"$3#!V!')!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!;1&amp;!!!Q!(!!A!#1F.97FO)%.))$9!9A$R!!!!!!!!!!-55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)245&amp;044-Y-$5T,GRW9WRB=X-94X6U=(6U)&amp;:B&lt;(6F,5.M&gt;8.U:8)O9X2M!"J!5!!$!!=!#!!*#5VB;7YA1UAA.Q!=1&amp;!!#!!+!!M!$!!.!!Y!$Q!1!"%%47&amp;J&lt;A!!1%"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)26(AA4X6U=(6U,GRW9WRB=X-!!!R5?#"%:8:J9W5A;7Y!!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!%A!4!A!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!"!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!"1!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">276832256</Property>
					</Item>
				</Item>
				<Item Name="Main CH 0" Type="Property Definition">
					<Property Name="NI.ClassItem.Property.LongName" Type="Str">Data:CDR:Main:CH 0</Property>
					<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Main CH 0</Property>
					<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
					<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
					<Item Name="Read Main CH 0.vi" Type="VI" URL="../Accessor/Main CH 0 Property/Read Main CH 0.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(*!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!F!"Q!$6'^Q!!F!"Q!$47FE!!F!"Q!$1G^U!'1!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!=1&amp;!!!Q!&amp;!!9!"QJ.97FO)&amp;:B&lt;(6F!!"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;5?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q!!$62Y)%2F&gt;GFD:3"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;5?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q!!$&amp;2Y)%2F&gt;GFD:3"J&lt;A!!91$Q!!Q!!Q!%!!A!#1!%!!1!"!!%!!I!"!!%!!M#!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!$!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">276832256</Property>
					</Item>
					<Item Name="Write Main CH 0.vi" Type="VI" URL="../Accessor/Main CH 0 Property/Write Main CH 0.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(*!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA2'6W;7.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!F!"Q!$6'^Q!!F!"Q!$47FE!!F!"Q!$1G^U!'1!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!=1&amp;!!!Q!(!!A!#1J.97FO)&amp;:B&lt;(6F!!"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;5?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q!!$&amp;2Y)%2F&gt;GFD:3"J&lt;A!!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!+!!M#!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!$!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">276832256</Property>
					</Item>
				</Item>
				<Item Name="Main CH 1" Type="Property Definition">
					<Property Name="NI.ClassItem.Property.LongName" Type="Str">Data:CDR:Main:CH 1</Property>
					<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Main CH 1</Property>
					<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
					<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
					<Item Name="Read Main CH 1.vi" Type="VI" URL="../Accessor/Main CH 1 Property/Read Main CH 1.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(*!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!F!"Q!$6'^Q!!F!"Q!$47FE!!F!"Q!$1G^U!'1!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!=1&amp;!!!Q!&amp;!!9!"QJ.97FO)&amp;:B&lt;(6F!!"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;5?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q!!$62Y)%2F&gt;GFD:3"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;5?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q!!$&amp;2Y)%2F&gt;GFD:3"J&lt;A!!91$Q!!Q!!Q!%!!A!#1!%!!1!"!!%!!I!"!!%!!M#!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!$!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">276832256</Property>
					</Item>
					<Item Name="Write Main CH 1.vi" Type="VI" URL="../Accessor/Main CH 1 Property/Write Main CH 1.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(*!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA2'6W;7.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!F!"Q!$6'^Q!!F!"Q!$47FE!!F!"Q!$1G^U!'1!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!=1&amp;!!!Q!(!!A!#1J.97FO)&amp;:B&lt;(6F!!"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;5?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q!!$&amp;2Y)%2F&gt;GFD:3"J&lt;A!!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!+!!M#!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!$!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">276832256</Property>
					</Item>
				</Item>
				<Item Name="Main CH 2" Type="Property Definition">
					<Property Name="NI.ClassItem.Property.LongName" Type="Str">Data:CDR:Main:CH 2</Property>
					<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Main CH 2</Property>
					<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
					<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
					<Item Name="Read Main CH 2.vi" Type="VI" URL="../Accessor/Main CH 2 Property/Read Main CH 2.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(*!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!F!"Q!$6'^Q!!F!"Q!$47FE!!F!"Q!$1G^U!'1!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!=1&amp;!!!Q!&amp;!!9!"QJ.97FO)&amp;:B&lt;(6F!!"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;5?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q!!$62Y)%2F&gt;GFD:3"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;5?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q!!$&amp;2Y)%2F&gt;GFD:3"J&lt;A!!91$Q!!Q!!Q!%!!A!#1!%!!1!"!!%!!I!"!!%!!M#!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!$!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">276832256</Property>
					</Item>
					<Item Name="Write Main CH 2.vi" Type="VI" URL="../Accessor/Main CH 2 Property/Write Main CH 2.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(*!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA2'6W;7.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!F!"Q!$6'^Q!!F!"Q!$47FE!!F!"Q!$1G^U!'1!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!=1&amp;!!!Q!(!!A!#1J.97FO)&amp;:B&lt;(6F!!"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;5?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q!!$&amp;2Y)%2F&gt;GFD:3"J&lt;A!!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!+!!M#!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!$!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">276832256</Property>
					</Item>
				</Item>
				<Item Name="Main CH 3" Type="Property Definition">
					<Property Name="NI.ClassItem.Property.LongName" Type="Str">Data:CDR:Main:CH 3</Property>
					<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Main CH 3</Property>
					<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
					<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
					<Item Name="Read Main CH 3.vi" Type="VI" URL="../Accessor/Main CH 3 Property/Read Main CH 3.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(*!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!F!"Q!$6'^Q!!F!"Q!$47FE!!F!"Q!$1G^U!'1!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!=1&amp;!!!Q!&amp;!!9!"QJ.97FO)&amp;:B&lt;(6F!!"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;5?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q!!$62Y)%2F&gt;GFD:3"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;5?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q!!$&amp;2Y)%2F&gt;GFD:3"J&lt;A!!91$Q!!Q!!Q!%!!A!#1!%!!1!"!!%!!I!"!!%!!M#!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!$!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">276832256</Property>
					</Item>
					<Item Name="Write Main CH 3.vi" Type="VI" URL="../Accessor/Main CH 3 Property/Write Main CH 3.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(*!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA2'6W;7.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!F!"Q!$6'^Q!!F!"Q!$47FE!!F!"Q!$1G^U!'1!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!=1&amp;!!!Q!(!!A!#1J.97FO)&amp;:B&lt;(6F!!"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;5?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q!!$&amp;2Y)%2F&gt;GFD:3"J&lt;A!!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!+!!M#!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!$!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">276832256</Property>
					</Item>
				</Item>
				<Item Name="Main CH 4" Type="Property Definition">
					<Property Name="NI.ClassItem.Property.LongName" Type="Str">Data:CDR:Main:CH 4</Property>
					<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Main CH 4</Property>
					<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
					<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
					<Item Name="Read Main CH 4.vi" Type="VI" URL="../Accessor/Main CH 4 Property/Read Main CH 4.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(*!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!F!"Q!$6'^Q!!F!"Q!$47FE!!F!"Q!$1G^U!'1!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!=1&amp;!!!Q!&amp;!!9!"QJ.97FO)&amp;:B&lt;(6F!!"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;5?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q!!$62Y)%2F&gt;GFD:3"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;5?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q!!$&amp;2Y)%2F&gt;GFD:3"J&lt;A!!91$Q!!Q!!Q!%!!A!#1!%!!1!"!!%!!I!"!!%!!M#!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!$!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">276832256</Property>
					</Item>
					<Item Name="Write Main CH 4.vi" Type="VI" URL="../Accessor/Main CH 4 Property/Write Main CH 4.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(*!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA2'6W;7.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!F!"Q!$6'^Q!!F!"Q!$47FE!!F!"Q!$1G^U!'1!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!=1&amp;!!!Q!(!!A!#1J.97FO)&amp;:B&lt;(6F!!"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;5?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q!!$&amp;2Y)%2F&gt;GFD:3"J&lt;A!!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!+!!M#!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!$!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">276832256</Property>
					</Item>
				</Item>
				<Item Name="Main CH 5" Type="Property Definition">
					<Property Name="NI.ClassItem.Property.LongName" Type="Str">Data:CDR:Main:CH 5</Property>
					<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Main CH 5</Property>
					<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
					<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
					<Item Name="Read Main CH 5.vi" Type="VI" URL="../Accessor/Main CH 5 Property/Read Main CH 5.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(*!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!F!"Q!$6'^Q!!F!"Q!$47FE!!F!"Q!$1G^U!'1!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!=1&amp;!!!Q!&amp;!!9!"QJ.97FO)&amp;:B&lt;(6F!!"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;5?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q!!$62Y)%2F&gt;GFD:3"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;5?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q!!$&amp;2Y)%2F&gt;GFD:3"J&lt;A!!91$Q!!Q!!Q!%!!A!#1!%!!1!"!!%!!I!"!!%!!M#!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!$!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">276832256</Property>
					</Item>
					<Item Name="Write Main CH 5.vi" Type="VI" URL="../Accessor/Main CH 5 Property/Write Main CH 5.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(*!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA2'6W;7.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!F!"Q!$6'^Q!!F!"Q!$47FE!!F!"Q!$1G^U!'1!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!=1&amp;!!!Q!(!!A!#1J.97FO)&amp;:B&lt;(6F!!"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;5?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q!!$&amp;2Y)%2F&gt;GFD:3"J&lt;A!!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!+!!M#!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!$!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">276832256</Property>
					</Item>
				</Item>
				<Item Name="Main CH 6" Type="Property Definition">
					<Property Name="NI.ClassItem.Property.LongName" Type="Str">Data:CDR:Main:CH 6</Property>
					<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Main CH 6</Property>
					<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
					<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
					<Item Name="Read Main CH 6.vi" Type="VI" URL="../Accessor/Main CH 6 Property/Read Main CH 6.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(*!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!F!"Q!$6'^Q!!F!"Q!$47FE!!F!"Q!$1G^U!'1!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!=1&amp;!!!Q!&amp;!!9!"QJ.97FO)&amp;:B&lt;(6F!!"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;5?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q!!$62Y)%2F&gt;GFD:3"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;5?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q!!$&amp;2Y)%2F&gt;GFD:3"J&lt;A!!91$Q!!Q!!Q!%!!A!#1!%!!1!"!!%!!I!"!!%!!M#!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!$!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">276832256</Property>
					</Item>
					<Item Name="Write Main CH 6.vi" Type="VI" URL="../Accessor/Main CH 6 Property/Write Main CH 6.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(*!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA2'6W;7.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!F!"Q!$6'^Q!!F!"Q!$47FE!!F!"Q!$1G^U!'1!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!=1&amp;!!!Q!(!!A!#1J.97FO)&amp;:B&lt;(6F!!"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;5?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q!!$&amp;2Y)%2F&gt;GFD:3"J&lt;A!!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!+!!M#!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!$!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">276832256</Property>
					</Item>
				</Item>
				<Item Name="Main CH 7" Type="Property Definition">
					<Property Name="NI.ClassItem.Property.LongName" Type="Str">Data:CDR:Main:CH 7</Property>
					<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Main CH 7</Property>
					<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
					<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
					<Item Name="Read Main CH 7.vi" Type="VI" URL="../Accessor/Main CH 7 Property/Read Main CH 7.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(*!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!F!"Q!$6'^Q!!F!"Q!$47FE!!F!"Q!$1G^U!'1!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!=1&amp;!!!Q!&amp;!!9!"QJ.97FO)&amp;:B&lt;(6F!!"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;5?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q!!$62Y)%2F&gt;GFD:3"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;5?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q!!$&amp;2Y)%2F&gt;GFD:3"J&lt;A!!91$Q!!Q!!Q!%!!A!#1!%!!1!"!!%!!I!"!!%!!M#!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!$!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">276832256</Property>
					</Item>
					<Item Name="Write Main CH 7.vi" Type="VI" URL="../Accessor/Main CH 7 Property/Write Main CH 7.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(*!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA2'6W;7.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!F!"Q!$6'^Q!!F!"Q!$47FE!!F!"Q!$1G^U!'1!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!=1&amp;!!!Q!(!!A!#1J.97FO)&amp;:B&lt;(6F!!"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;5?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q!!$&amp;2Y)%2F&gt;GFD:3"J&lt;A!!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!+!!M#!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!$!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">276832256</Property>
					</Item>
				</Item>
			</Item>
			<Item Name="Bandwidth" Type="Folder">
				<Item Name="Bandwidth" Type="Property Definition">
					<Property Name="NI.ClassItem.Property.LongName" Type="Str">Data:CDR:Bandwidth:All</Property>
					<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Bandwidth</Property>
					<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
					<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
					<Item Name="Read Bandwidth.vi" Type="VI" URL="../Accessor/Bandwidth Property/Read Bandwidth.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*&lt;!!!!%A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"6!"1!/1G&amp;O:(&gt;J:(2I)%.))$!!!"6!"1!/1G&amp;O:(&gt;J:(2I)%.))$%!!"6!"1!/1G&amp;O:(&gt;J:(2I)%.))$)!!"6!"1!/1G&amp;O:(&gt;J:(2I)%.))$-!!"6!"1!/1G&amp;O:(&gt;J:(2I)%.))$1!!"6!"1!/1G&amp;O:(&gt;J:(2I)%.))$5!!"6!"1!/1G&amp;O:(&gt;J:(2I)%.))$9!!"6!"1!/1G&amp;O:(&gt;J:(2I)%.))$=!!'E!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T&amp;5*B&lt;G2X;72U;#V$&lt;(6T&gt;'6S,G.U&lt;!!E1&amp;!!#!!&amp;!!9!"Q!)!!E!#A!,!!Q*1G&amp;O:(&gt;J:(2I!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA2'6W;7.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA2'6W;7.F)'FO!!"B!0!!$!!$!!1!$1!/!!1!"!!%!!1!$Q!%!!1!%!)!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!2!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">276832256</Property>
					</Item>
					<Item Name="Write Bandwidth.vi" Type="VI" URL="../Accessor/Bandwidth Property/Write Bandwidth.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*&lt;!!!!%A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA2'6W;7.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"6!"1!/1G&amp;O:(&gt;J:(2I)%.))$!!!"6!"1!/1G&amp;O:(&gt;J:(2I)%.))$%!!"6!"1!/1G&amp;O:(&gt;J:(2I)%.))$)!!"6!"1!/1G&amp;O:(&gt;J:(2I)%.))$-!!"6!"1!/1G&amp;O:(&gt;J:(2I)%.))$1!!"6!"1!/1G&amp;O:(&gt;J:(2I)%.))$5!!"6!"1!/1G&amp;O:(&gt;J:(2I)%.))$9!!"6!"1!/1G&amp;O:(&gt;J:(2I)%.))$=!!'E!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T&amp;5*B&lt;G2X;72U;#V$&lt;(6T&gt;'6S,G.U&lt;!!E1&amp;!!#!!(!!A!#1!+!!M!$!!.!!Y*1G&amp;O:(&gt;J:(2I!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA2'6W;7.F)'FO!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!]!%!)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!2!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">276832256</Property>
					</Item>
				</Item>
				<Item Name="Bandwidth CH 0" Type="Property Definition">
					<Property Name="NI.ClassItem.Property.LongName" Type="Str">Data:CDR:Bandwidth:CH 0</Property>
					<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Bandwidth CH 0</Property>
					<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
					<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
					<Item Name="Read Bandwidth CH 0.vi" Type="VI" URL="../Accessor/Bandwidth CH 0 Property/Read Bandwidth CH 0.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;@!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"6!"1!/1G&amp;O:(&gt;J:(2I)%.))$!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA2'6W;7.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA2'6W;7.F)'FO!!"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!)!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
					</Item>
					<Item Name="Write Bandwidth CH 0.vi" Type="VI" URL="../Accessor/Bandwidth CH 0 Property/Write Bandwidth CH 0.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;@!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA2'6W;7.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"6!"1!/1G&amp;O:(&gt;J:(2I)%.))$!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA2'6W;7.F)'FO!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
					</Item>
				</Item>
				<Item Name="Bandwidth CH 1" Type="Property Definition">
					<Property Name="NI.ClassItem.Property.LongName" Type="Str">Data:CDR:Bandwidth:CH 1</Property>
					<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Bandwidth CH 1</Property>
					<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
					<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
					<Item Name="Read Bandwidth CH 1.vi" Type="VI" URL="../Accessor/Bandwidth CH 1 Property/Read Bandwidth CH 1.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;@!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"6!"1!/1G&amp;O:(&gt;J:(2I)%.))$%!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA2'6W;7.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA2'6W;7.F)'FO!!"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!)!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
					</Item>
					<Item Name="Write Bandwidth CH 1.vi" Type="VI" URL="../Accessor/Bandwidth CH 1 Property/Write Bandwidth CH 1.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;@!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA2'6W;7.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"6!"1!/1G&amp;O:(&gt;J:(2I)%.))$%!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA2'6W;7.F)'FO!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
					</Item>
				</Item>
				<Item Name="Bandwidth CH 2" Type="Property Definition">
					<Property Name="NI.ClassItem.Property.LongName" Type="Str">Data:CDR:Bandwidth:CH 2</Property>
					<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Bandwidth CH 2</Property>
					<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
					<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
					<Item Name="Read Bandwidth CH 2.vi" Type="VI" URL="../Accessor/Bandwidth CH 2 Property/Read Bandwidth CH 2.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;@!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"6!"1!/1G&amp;O:(&gt;J:(2I)%.))$)!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA2'6W;7.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA2'6W;7.F)'FO!!"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!)!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
					</Item>
					<Item Name="Write Bandwidth CH 2.vi" Type="VI" URL="../Accessor/Bandwidth CH 2 Property/Write Bandwidth CH 2.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;@!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA2'6W;7.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"6!"1!/1G&amp;O:(&gt;J:(2I)%.))$)!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA2'6W;7.F)'FO!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
					</Item>
				</Item>
				<Item Name="Bandwidth CH 3" Type="Property Definition">
					<Property Name="NI.ClassItem.Property.LongName" Type="Str">Data:CDR:Bandwidth:CH 3</Property>
					<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Bandwidth CH 3</Property>
					<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
					<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
					<Item Name="Read Bandwidth CH 3.vi" Type="VI" URL="../Accessor/Bandwidth CH 3 Property/Read Bandwidth CH 3.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;@!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"6!"1!/1G&amp;O:(&gt;J:(2I)%.))$-!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA2'6W;7.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA2'6W;7.F)'FO!!"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!)!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
					</Item>
					<Item Name="Write Bandwidth CH 3.vi" Type="VI" URL="../Accessor/Bandwidth CH 3 Property/Write Bandwidth CH 3.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;@!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA2'6W;7.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"6!"1!/1G&amp;O:(&gt;J:(2I)%.))$-!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA2'6W;7.F)'FO!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
					</Item>
				</Item>
				<Item Name="Bandwidth CH 4" Type="Property Definition">
					<Property Name="NI.ClassItem.Property.LongName" Type="Str">Data:CDR:Bandwidth:CH 4</Property>
					<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Bandwidth CH 4</Property>
					<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
					<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
					<Item Name="Read Bandwidth CH 4.vi" Type="VI" URL="../Accessor/Bandwidth CH 4 Property/Read Bandwidth CH 4.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;@!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"6!"1!/1G&amp;O:(&gt;J:(2I)%.))$1!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA2'6W;7.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA2'6W;7.F)'FO!!"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!)!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
					</Item>
					<Item Name="Write Bandwidth CH 4.vi" Type="VI" URL="../Accessor/Bandwidth CH 4 Property/Write Bandwidth CH 4.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;@!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA2'6W;7.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"6!"1!/1G&amp;O:(&gt;J:(2I)%.))$1!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA2'6W;7.F)'FO!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
					</Item>
				</Item>
				<Item Name="Bandwidth CH 5" Type="Property Definition">
					<Property Name="NI.ClassItem.Property.LongName" Type="Str">Data:CDR:Bandwidth:CH 5</Property>
					<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Bandwidth CH 5</Property>
					<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
					<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
					<Item Name="Read Bandwidth CH 5.vi" Type="VI" URL="../Accessor/Bandwidth CH 5 Property/Read Bandwidth CH 5.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;@!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"6!"1!/1G&amp;O:(&gt;J:(2I)%.))$5!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA2'6W;7.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA2'6W;7.F)'FO!!"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!)!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
					</Item>
					<Item Name="Write Bandwidth CH 5.vi" Type="VI" URL="../Accessor/Bandwidth CH 5 Property/Write Bandwidth CH 5.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;@!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA2'6W;7.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"6!"1!/1G&amp;O:(&gt;J:(2I)%.))$5!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA2'6W;7.F)'FO!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
					</Item>
				</Item>
				<Item Name="Bandwidth CH 6" Type="Property Definition">
					<Property Name="NI.ClassItem.Property.LongName" Type="Str">Data:CDR:Bandwidth:CH 6</Property>
					<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Bandwidth CH 6</Property>
					<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
					<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
					<Item Name="Read Bandwidth CH 6.vi" Type="VI" URL="../Accessor/Bandwidth CH 6 Property/Read Bandwidth CH 6.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;@!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"6!"1!/1G&amp;O:(&gt;J:(2I)%.))$9!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA2'6W;7.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA2'6W;7.F)'FO!!"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!)!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
					</Item>
					<Item Name="Write Bandwidth CH 6.vi" Type="VI" URL="../Accessor/Bandwidth CH 6 Property/Write Bandwidth CH 6.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;@!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA2'6W;7.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"6!"1!/1G&amp;O:(&gt;J:(2I)%.))$9!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA2'6W;7.F)'FO!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
					</Item>
				</Item>
				<Item Name="Bandwidth CH 7" Type="Property Definition">
					<Property Name="NI.ClassItem.Property.LongName" Type="Str">Data:CDR:Bandwidth:CH 7</Property>
					<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Bandwidth CH 7</Property>
					<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
					<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
					<Item Name="Read Bandwidth CH 7.vi" Type="VI" URL="../Accessor/Bandwidth CH 7 Property/Read Bandwidth CH 7.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;@!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"6!"1!/1G&amp;O:(&gt;J:(2I)%.))$=!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA2'6W;7.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA2'6W;7.F)'FO!!"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!)!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
					</Item>
					<Item Name="Write Bandwidth CH 7.vi" Type="VI" URL="../Accessor/Bandwidth CH 7 Property/Write Bandwidth CH 7.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;@!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA2'6W;7.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"6!"1!/1G&amp;O:(&gt;J:(2I)%.))$=!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA2'6W;7.F)'FO!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
					</Item>
				</Item>
			</Item>
			<Item Name="Pre" Type="Folder">
				<Item Name="Pre" Type="Property Definition">
					<Property Name="NI.ClassItem.Property.LongName" Type="Str">Data:CDR:Pre:All</Property>
					<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Pre</Property>
					<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
					<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
					<Item Name="Read Pre.vi" Type="VI" URL="../Accessor/Pre Property/Read Pre.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!30!!!!&amp;1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!F!"Q!$6'^Q!!F!"Q!$47FE!!F!"Q!$1G^U!')!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!;1&amp;!!!Q!&amp;!!9!"QB1=G5A1UAA-!!!9A$R!!!!!!!!!!-55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)245&amp;044-Y-$5T,GRW9WRB=X-94X6U=(6U)&amp;:B&lt;(6F,5.M&gt;8.U:8)O9X2M!"J!5!!$!!5!"A!(#&amp;"S:3"$3#!R!!"C!0%!!!!!!!!!!R2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=RB0&gt;82Q&gt;81A6G&amp;M&gt;75N1WRV=X2F=CZD&gt;'Q!'E"1!!-!"1!'!!=)5(*F)%.))$)!!')!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!;1&amp;!!!Q!&amp;!!9!"QB1=G5A1UAA-Q!!9A$R!!!!!!!!!!-55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)245&amp;044-Y-$5T,GRW9WRB=X-94X6U=(6U)&amp;:B&lt;(6F,5.M&gt;8.U:8)O9X2M!"J!5!!$!!5!"A!(#&amp;"S:3"$3#!U!!"C!0%!!!!!!!!!!R2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=RB0&gt;82Q&gt;81A6G&amp;M&gt;75N1WRV=X2F=CZD&gt;'Q!'E"1!!-!"1!'!!=)5(*F)%.))$5!!')!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!;1&amp;!!!Q!&amp;!!9!"QB1=G5A1UAA.A!!9A$R!!!!!!!!!!-55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)245&amp;044-Y-$5T,GRW9WRB=X-94X6U=(6U)&amp;:B&lt;(6F,5.M&gt;8.U:8)O9X2M!"J!5!!$!!5!"A!(#&amp;"S:3"$3#!X!!!;1&amp;!!#!!)!!E!#A!,!!Q!$1!/!!]$5(*F!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA4X6U=(6U)'FO!!"B!0!!$!!$!!1!%!!2!!1!"!!%!!1!%A!%!!1!%Q)!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!5!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">276832256</Property>
					</Item>
					<Item Name="Write Pre.vi" Type="VI" URL="../Accessor/Pre Property/Write Pre.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!30!!!!&amp;1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!F!"Q!$6'^Q!!F!"Q!$47FE!!F!"Q!$1G^U!')!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!;1&amp;!!!Q!(!!A!#1B1=G5A1UAA-!!!9A$R!!!!!!!!!!-55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)245&amp;044-Y-$5T,GRW9WRB=X-94X6U=(6U)&amp;:B&lt;(6F,5.M&gt;8.U:8)O9X2M!"J!5!!$!!=!#!!*#&amp;"S:3"$3#!R!!"C!0%!!!!!!!!!!R2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=RB0&gt;82Q&gt;81A6G&amp;M&gt;75N1WRV=X2F=CZD&gt;'Q!'E"1!!-!"Q!)!!E)5(*F)%.))$)!!')!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!;1&amp;!!!Q!(!!A!#1B1=G5A1UAA-Q!!9A$R!!!!!!!!!!-55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)245&amp;044-Y-$5T,GRW9WRB=X-94X6U=(6U)&amp;:B&lt;(6F,5.M&gt;8.U:8)O9X2M!"J!5!!$!!=!#!!*#&amp;"S:3"$3#!U!!"C!0%!!!!!!!!!!R2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=RB0&gt;82Q&gt;81A6G&amp;M&gt;75N1WRV=X2F=CZD&gt;'Q!'E"1!!-!"Q!)!!E)5(*F)%.))$5!!')!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!;1&amp;!!!Q!(!!A!#1B1=G5A1UAA.A!!9A$R!!!!!!!!!!-55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)245&amp;044-Y-$5T,GRW9WRB=X-94X6U=(6U)&amp;:B&lt;(6F,5.M&gt;8.U:8)O9X2M!"J!5!!$!!=!#!!*#&amp;"S:3"$3#!X!!!;1&amp;!!#!!+!!M!$!!.!!Y!$Q!1!"%$5(*F!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA4X6U=(6U)'FO!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!")!%Q)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!5!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">276832256</Property>
					</Item>
				</Item>
				<Item Name="Pre CH 0" Type="Property Definition">
					<Property Name="NI.ClassItem.Property.LongName" Type="Str">Data:CDR:Pre:CH 0</Property>
					<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Pre CH 0</Property>
					<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
					<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
					<Item Name="Read Pre CH 0.vi" Type="VI" URL="../Accessor/Pre CH 0 Property/Read Pre CH 0.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(,!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!F!"Q!$6'^Q!!F!"Q!$47FE!!F!"Q!$1G^U!'9!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!?1&amp;!!!Q!&amp;!!9!"QR0&gt;82Q&gt;81A6G&amp;M&gt;75!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA4X6U=(6U)'FO!!"B!0!!$!!$!!1!#!!*!!1!"!!%!!1!#A!%!!1!#Q)!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!-!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">276832256</Property>
					</Item>
					<Item Name="Write Pre CH 0.vi" Type="VI" URL="../Accessor/Pre CH 0 Property/Write Pre CH 0.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(,!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!F!"Q!$6'^Q!!F!"Q!$47FE!!F!"Q!$1G^U!'9!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!?1&amp;!!!Q!(!!A!#1R0&gt;82Q&gt;81A6G&amp;M&gt;75!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA4X6U=(6U)'FO!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!I!#Q)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!-!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">276832256</Property>
					</Item>
				</Item>
				<Item Name="Pre CH 1" Type="Property Definition">
					<Property Name="NI.ClassItem.Property.LongName" Type="Str">Data:CDR:Pre:CH 1</Property>
					<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Pre CH 1</Property>
					<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
					<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
					<Item Name="Read Pre CH 1.vi" Type="VI" URL="../Accessor/Pre CH 1 Property/Read Pre CH 1.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(,!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!F!"Q!$6'^Q!!F!"Q!$47FE!!F!"Q!$1G^U!'9!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!?1&amp;!!!Q!&amp;!!9!"QR0&gt;82Q&gt;81A6G&amp;M&gt;75!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA4X6U=(6U)'FO!!"B!0!!$!!$!!1!#!!*!!1!"!!%!!1!#A!%!!1!#Q)!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!-!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">276832256</Property>
					</Item>
					<Item Name="Write Pre CH 1.vi" Type="VI" URL="../Accessor/Pre CH 1 Property/Write Pre CH 1.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(,!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!F!"Q!$6'^Q!!F!"Q!$47FE!!F!"Q!$1G^U!'9!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!?1&amp;!!!Q!(!!A!#1R0&gt;82Q&gt;81A6G&amp;M&gt;75!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA4X6U=(6U)'FO!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!I!#Q)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!-!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">276832256</Property>
					</Item>
				</Item>
				<Item Name="Pre CH 2" Type="Property Definition">
					<Property Name="NI.ClassItem.Property.LongName" Type="Str">Data:CDR:Pre:CH 2</Property>
					<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Pre CH 2</Property>
					<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
					<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
					<Item Name="Read Pre CH 2.vi" Type="VI" URL="../Accessor/Pre CH 2 Property/Read Pre CH 2.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(,!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!F!"Q!$6'^Q!!F!"Q!$47FE!!F!"Q!$1G^U!'9!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!?1&amp;!!!Q!&amp;!!9!"QR0&gt;82Q&gt;81A6G&amp;M&gt;75!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA4X6U=(6U)'FO!!"B!0!!$!!$!!1!#!!*!!1!"!!%!!1!#A!%!!1!#Q)!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!-!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">276832256</Property>
					</Item>
					<Item Name="Write Pre CH 2.vi" Type="VI" URL="../Accessor/Pre CH 2 Property/Write Pre CH 2.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(,!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!F!"Q!$6'^Q!!F!"Q!$47FE!!F!"Q!$1G^U!'9!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!?1&amp;!!!Q!(!!A!#1R0&gt;82Q&gt;81A6G&amp;M&gt;75!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA4X6U=(6U)'FO!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!I!#Q)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!-!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">276832256</Property>
					</Item>
				</Item>
				<Item Name="Pre CH 3" Type="Property Definition">
					<Property Name="NI.ClassItem.Property.LongName" Type="Str">Data:CDR:Pre:CH 3</Property>
					<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Pre CH 3</Property>
					<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
					<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
					<Item Name="Read Pre CH 3.vi" Type="VI" URL="../Accessor/Pre CH 3 Property/Read Pre CH 3.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(,!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!F!"Q!$6'^Q!!F!"Q!$47FE!!F!"Q!$1G^U!'9!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!?1&amp;!!!Q!&amp;!!9!"QR0&gt;82Q&gt;81A6G&amp;M&gt;75!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA4X6U=(6U)'FO!!"B!0!!$!!$!!1!#!!*!!1!"!!%!!1!#A!%!!1!#Q)!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!-!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">276832256</Property>
					</Item>
					<Item Name="Write Pre CH 3.vi" Type="VI" URL="../Accessor/Pre CH 3 Property/Write Pre CH 3.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(,!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!F!"Q!$6'^Q!!F!"Q!$47FE!!F!"Q!$1G^U!'9!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!?1&amp;!!!Q!(!!A!#1R0&gt;82Q&gt;81A6G&amp;M&gt;75!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA4X6U=(6U)'FO!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!I!#Q)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!-!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">276832256</Property>
					</Item>
				</Item>
				<Item Name="Pre CH 4" Type="Property Definition">
					<Property Name="NI.ClassItem.Property.LongName" Type="Str">Data:CDR:Pre:CH 4</Property>
					<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Pre CH 4</Property>
					<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
					<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
					<Item Name="Read Pre CH 4.vi" Type="VI" URL="../Accessor/Pre CH 4 Property/Read Pre CH 4.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(,!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!F!"Q!$6'^Q!!F!"Q!$47FE!!F!"Q!$1G^U!'9!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!?1&amp;!!!Q!&amp;!!9!"QR0&gt;82Q&gt;81A6G&amp;M&gt;75!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA4X6U=(6U)'FO!!"B!0!!$!!$!!1!#!!*!!1!"!!%!!1!#A!%!!1!#Q)!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!-!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">276832256</Property>
					</Item>
					<Item Name="Write Pre CH 4.vi" Type="VI" URL="../Accessor/Pre CH 4 Property/Write Pre CH 4.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(,!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!F!"Q!$6'^Q!!F!"Q!$47FE!!F!"Q!$1G^U!'9!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!?1&amp;!!!Q!(!!A!#1R0&gt;82Q&gt;81A6G&amp;M&gt;75!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA4X6U=(6U)'FO!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!I!#Q)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!-!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">276832256</Property>
					</Item>
				</Item>
				<Item Name="Pre CH 5" Type="Property Definition">
					<Property Name="NI.ClassItem.Property.LongName" Type="Str">Data:CDR:Pre:CH 5</Property>
					<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Pre CH 5</Property>
					<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
					<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
					<Item Name="Read Pre CH 5.vi" Type="VI" URL="../Accessor/Pre CH 5 Property/Read Pre CH 5.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(,!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!F!"Q!$6'^Q!!F!"Q!$47FE!!F!"Q!$1G^U!'9!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!?1&amp;!!!Q!&amp;!!9!"QR0&gt;82Q&gt;81A6G&amp;M&gt;75!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA4X6U=(6U)'FO!!"B!0!!$!!$!!1!#!!*!!1!"!!%!!1!#A!%!!1!#Q)!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!-!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">276832256</Property>
					</Item>
					<Item Name="Write Pre CH 5.vi" Type="VI" URL="../Accessor/Pre CH 5 Property/Write Pre CH 5.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(,!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!F!"Q!$6'^Q!!F!"Q!$47FE!!F!"Q!$1G^U!'9!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!?1&amp;!!!Q!(!!A!#1R0&gt;82Q&gt;81A6G&amp;M&gt;75!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA4X6U=(6U)'FO!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!I!#Q)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!-!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">276832256</Property>
					</Item>
				</Item>
				<Item Name="Pre CH 6" Type="Property Definition">
					<Property Name="NI.ClassItem.Property.LongName" Type="Str">Data:CDR:Pre:CH 6</Property>
					<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Pre CH 6</Property>
					<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
					<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
					<Item Name="Read Pre CH 6.vi" Type="VI" URL="../Accessor/Pre CH 6 Property/Read Pre CH 6.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(,!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!F!"Q!$6'^Q!!F!"Q!$47FE!!F!"Q!$1G^U!'9!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!?1&amp;!!!Q!&amp;!!9!"QR0&gt;82Q&gt;81A6G&amp;M&gt;75!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA4X6U=(6U)'FO!!"B!0!!$!!$!!1!#!!*!!1!"!!%!!1!#A!%!!1!#Q)!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!-!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">276832256</Property>
					</Item>
					<Item Name="Write Pre CH 6.vi" Type="VI" URL="../Accessor/Pre CH 6 Property/Write Pre CH 6.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(,!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!F!"Q!$6'^Q!!F!"Q!$47FE!!F!"Q!$1G^U!'9!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!?1&amp;!!!Q!(!!A!#1R0&gt;82Q&gt;81A6G&amp;M&gt;75!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA4X6U=(6U)'FO!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!I!#Q)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!-!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">276832256</Property>
					</Item>
				</Item>
				<Item Name="Pre CH 7" Type="Property Definition">
					<Property Name="NI.ClassItem.Property.LongName" Type="Str">Data:CDR:Pre:CH 7</Property>
					<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Pre CH 7</Property>
					<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
					<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
					<Item Name="Read Pre CH 7.vi" Type="VI" URL="../Accessor/Pre CH 7 Property/Read Pre CH 7.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(,!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!F!"Q!$6'^Q!!F!"Q!$47FE!!F!"Q!$1G^U!'9!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!?1&amp;!!!Q!&amp;!!9!"QR0&gt;82Q&gt;81A6G&amp;M&gt;75!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA4X6U=(6U)'FO!!"B!0!!$!!$!!1!#!!*!!1!"!!%!!1!#A!%!!1!#Q)!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!-!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">276832256</Property>
					</Item>
					<Item Name="Write Pre CH 7.vi" Type="VI" URL="../Accessor/Pre CH 7 Property/Write Pre CH 7.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(,!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!F!"Q!$6'^Q!!F!"Q!$47FE!!F!"Q!$1G^U!'9!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!?1&amp;!!!Q!(!!A!#1R0&gt;82Q&gt;81A6G&amp;M&gt;75!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA4X6U=(6U)'FO!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!I!#Q)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!-!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">276832256</Property>
					</Item>
				</Item>
			</Item>
			<Item Name="Delay" Type="Folder">
				<Item Name="Delay" Type="Property Definition">
					<Property Name="NI.ClassItem.Property.LongName" Type="Str">Data:CDR:Delay:All</Property>
					<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Delay</Property>
					<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
					<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
					<Item Name="Read Delay.vi" Type="VI" URL="../Accessor/Delay Property/Read Delay.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!3B!!!!&amp;1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!F!"Q!$6'^Q!!F!"Q!$47FE!!F!"Q!$1G^U!'1!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!=1&amp;!!!Q!&amp;!!9!"QJ%:7RB?3"$3#!Q!!"E!0%!!!!!!!!!!R2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=RB0&gt;82Q&gt;81A6G&amp;M&gt;75N1WRV=X2F=CZD&gt;'Q!(%"1!!-!"1!'!!=+2'6M98EA1UAA-1!!:!$R!!!!!!!!!!-55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)245&amp;044-Y-$5T,GRW9WRB=X-94X6U=(6U)&amp;:B&lt;(6F,5.M&gt;8.U:8)O9X2M!"R!5!!$!!5!"A!(#E2F&lt;'&amp;Z)%.))$)!!'1!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!=1&amp;!!!Q!&amp;!!9!"QJ%:7RB?3"$3#!T!!"E!0%!!!!!!!!!!R2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=RB0&gt;82Q&gt;81A6G&amp;M&gt;75N1WRV=X2F=CZD&gt;'Q!(%"1!!-!"1!'!!=+2'6M98EA1UAA.!!!:!$R!!!!!!!!!!-55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)245&amp;044-Y-$5T,GRW9WRB=X-94X6U=(6U)&amp;:B&lt;(6F,5.M&gt;8.U:8)O9X2M!"R!5!!$!!5!"A!(#E2F&lt;'&amp;Z)%.))$5!!'1!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!=1&amp;!!!Q!&amp;!!9!"QJ%:7RB?3"$3#!W!!"E!0%!!!!!!!!!!R2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=RB0&gt;82Q&gt;81A6G&amp;M&gt;75N1WRV=X2F=CZD&gt;'Q!(%"1!!-!"1!'!!=+2'6M98EA1UAA.Q!!(%"1!!A!#!!*!!I!#Q!-!!U!$A!0"52F&lt;'&amp;Z!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA4X6U=(6U)'FO!!"B!0!!$!!$!!1!%!!2!!1!"!!%!!1!%A!%!!1!%Q)!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!5!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">276832256</Property>
					</Item>
					<Item Name="Write Delay.vi" Type="VI" URL="../Accessor/Delay Property/Write Delay.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!3B!!!!&amp;1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!F!"Q!$6'^Q!!F!"Q!$47FE!!F!"Q!$1G^U!'1!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!=1&amp;!!!Q!(!!A!#1J%:7RB?3"$3#!Q!!"E!0%!!!!!!!!!!R2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=RB0&gt;82Q&gt;81A6G&amp;M&gt;75N1WRV=X2F=CZD&gt;'Q!(%"1!!-!"Q!)!!E+2'6M98EA1UAA-1!!:!$R!!!!!!!!!!-55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)245&amp;044-Y-$5T,GRW9WRB=X-94X6U=(6U)&amp;:B&lt;(6F,5.M&gt;8.U:8)O9X2M!"R!5!!$!!=!#!!*#E2F&lt;'&amp;Z)%.))$)!!'1!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!=1&amp;!!!Q!(!!A!#1J%:7RB?3"$3#!T!!"E!0%!!!!!!!!!!R2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=RB0&gt;82Q&gt;81A6G&amp;M&gt;75N1WRV=X2F=CZD&gt;'Q!(%"1!!-!"Q!)!!E+2'6M98EA1UAA.!!!:!$R!!!!!!!!!!-55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)245&amp;044-Y-$5T,GRW9WRB=X-94X6U=(6U)&amp;:B&lt;(6F,5.M&gt;8.U:8)O9X2M!"R!5!!$!!=!#!!*#E2F&lt;'&amp;Z)%.))$5!!'1!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!=1&amp;!!!Q!(!!A!#1J%:7RB?3"$3#!W!!"E!0%!!!!!!!!!!R2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.15^.-TAQ.4-O&lt;(:D&lt;'&amp;T=RB0&gt;82Q&gt;81A6G&amp;M&gt;75N1WRV=X2F=CZD&gt;'Q!(%"1!!-!"Q!)!!E+2'6M98EA1UAA.Q!!(%"1!!A!#A!,!!Q!$1!/!!]!%!!2"52F&lt;'&amp;Z!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA4X6U=(6U)'FO!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!")!%Q)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!5!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">276832256</Property>
					</Item>
				</Item>
				<Item Name="Delay CH 0" Type="Property Definition">
					<Property Name="NI.ClassItem.Property.LongName" Type="Str">Data:CDR:Delay:CH 0</Property>
					<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Delay CH 0</Property>
					<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
					<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
					<Item Name="Read Delay CH 0.vi" Type="VI" URL="../Accessor/Delay CH 0 Property/Read Delay CH 0.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(,!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!F!"Q!$6'^Q!!F!"Q!$47FE!!F!"Q!$1G^U!'9!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!?1&amp;!!!Q!&amp;!!9!"QR0&gt;82Q&gt;81A6G&amp;M&gt;75!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA4X6U=(6U)'FO!!"B!0!!$!!$!!1!#!!*!!1!"!!%!!1!#A!%!!1!#Q)!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!-!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">276832256</Property>
					</Item>
					<Item Name="Write Delay CH 0.vi" Type="VI" URL="../Accessor/Delay CH 0 Property/Write Delay CH 0.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(,!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!F!"Q!$6'^Q!!F!"Q!$47FE!!F!"Q!$1G^U!'9!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!?1&amp;!!!Q!(!!A!#1R0&gt;82Q&gt;81A6G&amp;M&gt;75!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA4X6U=(6U)'FO!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!I!#Q)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!-!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">276832256</Property>
					</Item>
				</Item>
				<Item Name="Delay CH 1" Type="Property Definition">
					<Property Name="NI.ClassItem.Property.LongName" Type="Str">Data:CDR:Delay:CH 1</Property>
					<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Delay CH 1</Property>
					<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
					<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
					<Item Name="Read Delay CH 1.vi" Type="VI" URL="../Accessor/Delay CH 1 Property/Read Delay CH 1.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(,!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!F!"Q!$6'^Q!!F!"Q!$47FE!!F!"Q!$1G^U!'9!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!?1&amp;!!!Q!&amp;!!9!"QR0&gt;82Q&gt;81A6G&amp;M&gt;75!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA4X6U=(6U)'FO!!"B!0!!$!!$!!1!#!!*!!1!"!!%!!1!#A!%!!1!#Q)!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!-!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">276832256</Property>
					</Item>
					<Item Name="Write Delay CH 1.vi" Type="VI" URL="../Accessor/Delay CH 1 Property/Write Delay CH 1.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(,!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!F!"Q!$6'^Q!!F!"Q!$47FE!!F!"Q!$1G^U!'9!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!?1&amp;!!!Q!(!!A!#1R0&gt;82Q&gt;81A6G&amp;M&gt;75!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA4X6U=(6U)'FO!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!I!#Q)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!-!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">276832256</Property>
					</Item>
				</Item>
				<Item Name="Delay CH 2" Type="Property Definition">
					<Property Name="NI.ClassItem.Property.LongName" Type="Str">Data:CDR:Delay:CH 2</Property>
					<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Delay CH 2</Property>
					<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
					<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
					<Item Name="Read Delay CH 2.vi" Type="VI" URL="../Accessor/Delay CH 2 Property/Read Delay CH 2.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(,!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!F!"Q!$6'^Q!!F!"Q!$47FE!!F!"Q!$1G^U!'9!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!?1&amp;!!!Q!&amp;!!9!"QR0&gt;82Q&gt;81A6G&amp;M&gt;75!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA4X6U=(6U)'FO!!"B!0!!$!!$!!1!#!!*!!1!"!!%!!1!#A!%!!1!#Q)!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!-!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">276832256</Property>
					</Item>
					<Item Name="Write Delay CH 2.vi" Type="VI" URL="../Accessor/Delay CH 2 Property/Write Delay CH 2.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(,!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!F!"Q!$6'^Q!!F!"Q!$47FE!!F!"Q!$1G^U!'9!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!?1&amp;!!!Q!(!!A!#1R0&gt;82Q&gt;81A6G&amp;M&gt;75!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA4X6U=(6U)'FO!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!I!#Q)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!-!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">276832256</Property>
					</Item>
				</Item>
				<Item Name="Delay CH 3" Type="Property Definition">
					<Property Name="NI.ClassItem.Property.LongName" Type="Str">Data:CDR:Delay:CH 3</Property>
					<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Delay CH 3</Property>
					<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
					<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
					<Item Name="Read Delay CH 3.vi" Type="VI" URL="../Accessor/Delay CH 3 Property/Read Delay CH 3.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(,!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!F!"Q!$6'^Q!!F!"Q!$47FE!!F!"Q!$1G^U!'9!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!?1&amp;!!!Q!&amp;!!9!"QR0&gt;82Q&gt;81A6G&amp;M&gt;75!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA4X6U=(6U)'FO!!"B!0!!$!!$!!1!#!!*!!1!"!!%!!1!#A!%!!1!#Q)!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!-!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">276832256</Property>
					</Item>
					<Item Name="Write Delay CH 3.vi" Type="VI" URL="../Accessor/Delay CH 3 Property/Write Delay CH 3.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(,!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!F!"Q!$6'^Q!!F!"Q!$47FE!!F!"Q!$1G^U!'9!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!?1&amp;!!!Q!(!!A!#1R0&gt;82Q&gt;81A6G&amp;M&gt;75!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA4X6U=(6U)'FO!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!I!#Q)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!-!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">276832256</Property>
					</Item>
				</Item>
				<Item Name="Delay CH 4" Type="Property Definition">
					<Property Name="NI.ClassItem.Property.LongName" Type="Str">Data:CDR:Delay:CH 4</Property>
					<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Delay CH 4</Property>
					<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
					<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
					<Item Name="Read Delay CH 4.vi" Type="VI" URL="../Accessor/Delay CH 4 Property/Read Delay CH 4.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(,!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!F!"Q!$6'^Q!!F!"Q!$47FE!!F!"Q!$1G^U!'9!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!?1&amp;!!!Q!&amp;!!9!"QR0&gt;82Q&gt;81A6G&amp;M&gt;75!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA4X6U=(6U)'FO!!"B!0!!$!!$!!1!#!!*!!1!"!!%!!1!#A!%!!1!#Q)!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!-!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">276832256</Property>
					</Item>
					<Item Name="Write Delay CH 4.vi" Type="VI" URL="../Accessor/Delay CH 4 Property/Write Delay CH 4.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(,!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!F!"Q!$6'^Q!!F!"Q!$47FE!!F!"Q!$1G^U!'9!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!?1&amp;!!!Q!(!!A!#1R0&gt;82Q&gt;81A6G&amp;M&gt;75!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA4X6U=(6U)'FO!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!I!#Q)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!-!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">276832256</Property>
					</Item>
				</Item>
				<Item Name="Delay CH 5" Type="Property Definition">
					<Property Name="NI.ClassItem.Property.LongName" Type="Str">Data:CDR:Delay:CH 5</Property>
					<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Delay CH 5</Property>
					<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
					<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
					<Item Name="Read Delay CH 5.vi" Type="VI" URL="../Accessor/Delay CH 5 Property/Read Delay CH 5.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(,!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!F!"Q!$6'^Q!!F!"Q!$47FE!!F!"Q!$1G^U!'9!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!?1&amp;!!!Q!&amp;!!9!"QR0&gt;82Q&gt;81A6G&amp;M&gt;75!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA4X6U=(6U)'FO!!"B!0!!$!!$!!1!#!!*!!1!"!!%!!1!#A!%!!1!#Q)!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!-!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">276832256</Property>
					</Item>
					<Item Name="Write Delay CH 5.vi" Type="VI" URL="../Accessor/Delay CH 5 Property/Write Delay CH 5.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(,!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!F!"Q!$6'^Q!!F!"Q!$47FE!!F!"Q!$1G^U!'9!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!?1&amp;!!!Q!(!!A!#1R0&gt;82Q&gt;81A6G&amp;M&gt;75!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA4X6U=(6U)'FO!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!I!#Q)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!-!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">276832256</Property>
					</Item>
				</Item>
				<Item Name="Delay CH 6" Type="Property Definition">
					<Property Name="NI.ClassItem.Property.LongName" Type="Str">Data:CDR:Delay:CH 6</Property>
					<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Delay CH 6</Property>
					<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
					<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
					<Item Name="Read Delay CH 6.vi" Type="VI" URL="../Accessor/Delay CH 6 Property/Read Delay CH 6.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(,!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!F!"Q!$6'^Q!!F!"Q!$47FE!!F!"Q!$1G^U!'9!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!?1&amp;!!!Q!&amp;!!9!"QR0&gt;82Q&gt;81A6G&amp;M&gt;75!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA4X6U=(6U)'FO!!"B!0!!$!!$!!1!#!!*!!1!"!!%!!1!#A!%!!1!#Q)!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!-!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">276832256</Property>
					</Item>
					<Item Name="Write Delay CH 6.vi" Type="VI" URL="../Accessor/Delay CH 6 Property/Write Delay CH 6.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(,!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!F!"Q!$6'^Q!!F!"Q!$47FE!!F!"Q!$1G^U!'9!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!?1&amp;!!!Q!(!!A!#1R0&gt;82Q&gt;81A6G&amp;M&gt;75!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA4X6U=(6U)'FO!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!I!#Q)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!-!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">276832256</Property>
					</Item>
				</Item>
				<Item Name="Delay CH 7" Type="Property Definition">
					<Property Name="NI.ClassItem.Property.LongName" Type="Str">Data:CDR:Delay:CH 7</Property>
					<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Delay CH 7</Property>
					<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
					<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
					<Item Name="Read Delay CH 7.vi" Type="VI" URL="../Accessor/Delay CH 7 Property/Read Delay CH 7.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(,!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!F!"Q!$6'^Q!!F!"Q!$47FE!!F!"Q!$1G^U!'9!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!?1&amp;!!!Q!&amp;!!9!"QR0&gt;82Q&gt;81A6G&amp;M&gt;75!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA4X6U=(6U)'FO!!"B!0!!$!!$!!1!#!!*!!1!"!!%!!1!#A!%!!1!#Q)!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!-!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">276832256</Property>
					</Item>
					<Item Name="Write Delay CH 7.vi" Type="VI" URL="../Accessor/Delay CH 7 Property/Write Delay CH 7.vi">
						<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(,!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!F!"Q!$6'^Q!!F!"Q!$47FE!!F!"Q!$1G^U!'9!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!?1&amp;!!!Q!(!!A!#1R0&gt;82Q&gt;81A6G&amp;M&gt;75!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA4X6U=(6U)'FO!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!I!#Q)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!-!!!!!!</Property>
						<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
						<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
						<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
						<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
						<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
						<Property Name="NI.ClassItem.State" Type="Int">276832256</Property>
					</Item>
				</Item>
			</Item>
		</Item>
		<Item Name="Class" Type="Folder">
			<Item Name="AFSI-N74C4Sx.lvclass" Type="Property Definition">
				<Property Name="NI.ClassItem.Property.LongName" Type="Str">Class:AFSI-N74C4Sx.lvclass</Property>
				<Property Name="NI.ClassItem.Property.ShortName" Type="Str">AFSI-N74C4Sx.lvclass</Property>
				<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
				<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
				<Item Name="Read AFSI-N74C4Sx.vi" Type="VI" URL="../Accessor/AFSI-N74C4Sx Property/Read AFSI-N74C4Sx.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'K!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!'"!=!!?!!!L&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC&amp;%&amp;'5UEN4D=U1T24?#ZM&gt;G.M98.T!#J115UU)%.%5C"%:8:J9W5O&lt;(:M;7)[15:433V/.T2$.&amp;.Y,GRW9WRB=X-!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA2'6W;7.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA2'6W;7.F)'FO!!"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!)!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
				</Item>
				<Item Name="Write AFSI-N74C4Sx.vi" Type="VI" URL="../Accessor/AFSI-N74C4Sx Property/Write AFSI-N74C4Sx.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'K!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA2'6W;7.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!'"!=!!?!!!L&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC&amp;%&amp;'5UEN4D=U1T24?#ZM&gt;G.M98.T!#J115UU)%.%5C"%:8:J9W5O&lt;(:M;7)[15:433V/.T2$.&amp;.Y,GRW9WRB=X-!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA2'6W;7.F)'FO!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
				</Item>
			</Item>
			<Item Name="MAOM38053.lvclass" Type="Property Definition">
				<Property Name="NI.ClassItem.Property.LongName" Type="Str">Class:MAOM38053.lvclass</Property>
				<Property Name="NI.ClassItem.Property.ShortName" Type="Str">MAOM38053.lvclass</Property>
				<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
				<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
				<Item Name="Read MAOM38053.lvclass.vi" Type="VI" URL="../Accessor/MAOM38053.lvclass Property/Read MAOM38053.lvclass.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'E!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;J!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T!!!H5%&amp;..#"$2&amp;)A2'6W;7.F,GRW&lt;'FC/EV"4UUT/$!V-SZM&gt;G.M98.T!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA2'6W;7.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA2'6W;7.F)'FO!!"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!)!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
				</Item>
				<Item Name="Write MAOM38053.lvclass.vi" Type="VI" URL="../Accessor/MAOM38053.lvclass Property/Write MAOM38053.lvclass.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'E!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA2'6W;7.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!&amp;J!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T!!!H5%&amp;..#"$2&amp;)A2'6W;7.F,GRW&lt;'FC/EV"4UUT/$!V-SZM&gt;G.M98.T!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA2'6W;7.F)'FO!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
				</Item>
			</Item>
			<Item Name="Product.lvclass" Type="Property Definition">
				<Property Name="NI.ClassItem.Property.LongName" Type="Str">Class:Product.lvclass</Property>
				<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Product.lvclass</Property>
				<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
				<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
				<Item Name="Read Product.lvclass.vi" Type="VI" URL="../Accessor/Product.lvclass Property/Read Product.lvclass.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'U!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!'J!=!!?!!!Q&amp;E^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9H!84X"U;7.B&lt;#"1=G^E&gt;7.U,GRW9WRB=X-!!#Z0=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7*Q/E^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;G.M98.T!!"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;5?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q!!$62Y)%^V&gt;("V&gt;#"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;5?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q!!$&amp;2Y)%^V&gt;("V&gt;#"J&lt;A!!91$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A#!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
				</Item>
				<Item Name="Write Product.lvclass.vi" Type="VI" URL="../Accessor/Product.lvclass Property/Write Product.lvclass.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'U!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!'J!=!!?!!!Q&amp;E^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9H!84X"U;7.B&lt;#"1=G^E&gt;7.U,GRW9WRB=X-!!#Z0=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:M;7*Q/E^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;G.M98.T!!"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;5?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q!!$&amp;2Y)%^V&gt;("V&gt;#"J&lt;A!!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A#!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
				</Item>
			</Item>
		</Item>
		<Item Name="Chip Status" Type="Folder">
			<Item Name="CDR Status" Type="Property Definition">
				<Property Name="NI.ClassItem.Property.LongName" Type="Str">Chip Status:CDR</Property>
				<Property Name="NI.ClassItem.Property.ShortName" Type="Str">CDR Status</Property>
				<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
				<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
				<Item Name="Write CDR Status.vi" Type="VI" URL="../Accessor/CDR Status Property/Write CDR Status.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!($!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!R!)1:$;'FQ)$%!!!R!)1:$;'FQ)$)!!'%!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T&amp;U.I;8!A5X2B&gt;(6T,5.M&gt;8.U:8)O9X2M!"J!5!!#!!=!#!N$;'FQ)&amp;.U982V=Q"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;5?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q!!$&amp;2Y)%^V&gt;("V&gt;#"J&lt;A!!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!*!!I#!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#Q!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">276832256</Property>
				</Item>
				<Item Name="Read CDR Status.vi" Type="VI" URL="../Accessor/CDR Status Property/Read CDR Status.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!($!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!R!)1:$;'FQ)$%!!!R!)1:$;'FQ)$)!!'%!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T&amp;U.I;8!A5X2B&gt;(6T,5.M&gt;8.U:8)O9X2M!"J!5!!#!!5!"AN$;'FQ)&amp;.U982V=Q"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;5?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q!!$62Y)%^V&gt;("V&gt;#"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;5?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q!!$&amp;2Y)%^V&gt;("V&gt;#"J&lt;A!!91$Q!!Q!!Q!%!!=!#!!%!!1!"!!%!!E!"!!%!!I#!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!#Q!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">276832256</Property>
				</Item>
			</Item>
			<Item Name="LDD Status" Type="Property Definition">
				<Property Name="NI.ClassItem.Property.LongName" Type="Str">Chip Status:LDD</Property>
				<Property Name="NI.ClassItem.Property.ShortName" Type="Str">LDD Status</Property>
				<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
				<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
				<Item Name="Read LDD Status.vi" Type="VI" URL="../Accessor/LDD Status Property/Read LDD Status.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!($!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!R!)1:$;'FQ)$%!!!R!)1:$;'FQ)$)!!'%!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T&amp;U.I;8!A5X2B&gt;(6T,5.M&gt;8.U:8)O9X2M!"J!5!!#!!5!"AN$;'FQ)&amp;.U982V=Q"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;5?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q!!$62Y)%^V&gt;("V&gt;#"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;5?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q!!$&amp;2Y)%^V&gt;("V&gt;#"J&lt;A!!91$Q!!Q!!Q!%!!=!#!!%!!1!"!!%!!E!"!!%!!I#!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!#Q!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">276832256</Property>
				</Item>
				<Item Name="Write LDD Status.vi" Type="VI" URL="../Accessor/LDD Status Property/Write LDD Status.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!($!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!R!)1:$;'FQ)$%!!!R!)1:$;'FQ)$)!!'%!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T&amp;U.I;8!A5X2B&gt;(6T,5.M&gt;8.U:8)O9X2M!"J!5!!#!!=!#!N$;'FQ)&amp;.U982V=Q"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;5?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q!!$&amp;2Y)%^V&gt;("V&gt;#"J&lt;A!!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!*!!I#!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#Q!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">276832256</Property>
				</Item>
			</Item>
		</Item>
		<Item Name="Configure" Type="Folder">
			<Item Name="Channel" Type="Property Definition">
				<Property Name="NI.ClassItem.Property.LongName" Type="Str">Configure:Channel</Property>
				<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Channel</Property>
				<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
				<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
				<Item Name="Read Channel.vi" Type="VI" URL="../Accessor/Channel Property/Read Channel.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;B!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!V!!Q!(1WBB&lt;GZF&lt;!"'1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;5?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q!!%F""441A6(AA2'6W;7.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"%1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;5?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q!!%6""441A6(AA2'6W;7.F)'FO!'%!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!A!!?!!!$1A!!!!!!!!*!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
				</Item>
				<Item Name="Write Channel.vi" Type="VI" URL="../Accessor/Channel Property/Write Channel.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;B!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%:!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!35%&amp;..#"5?#"%:8:J9W5A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!V!!Q!(1WBB&lt;GZF&lt;!"%1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;5?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q!!%6""441A6(AA2'6W;7.F)'FO!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!A!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!"!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
				</Item>
			</Item>
			<Item Name="Tune Polarity" Type="Property Definition">
				<Property Name="NI.ClassItem.Property.LongName" Type="Str">Configure:Tune Polarity</Property>
				<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Tune Polarity</Property>
				<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
				<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
				<Item Name="Read Tune Polarity.vi" Type="VI" URL="../Accessor/Tune Polarity Property/Read Tune Polarity.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(J!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!*]!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T&amp;F2V&lt;G5A5'^M98*J&gt;(EN27ZV&lt;3ZD&gt;'Q!75!7!!114G^S&lt;7&amp;M)%^Q:8*B&gt;'FP&lt;B6115UU)%FO&gt;G6S&gt;#"197FS)&amp;.X98!/5%&amp;..#"*&lt;H:F=H.J&lt;WY*5'&amp;J=C"4&gt;W&amp;Q!!V5&gt;7ZF)&amp;"P&lt;'&amp;S;82Z!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA4X6U=(6U)'FO!!"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!)!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">276832256</Property>
				</Item>
				<Item Name="Write Tune Polarity.vi" Type="VI" URL="../Accessor/Tune Polarity Property/Write Tune Polarity.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(J!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!*]!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T&amp;F2V&lt;G5A5'^M98*J&gt;(EN27ZV&lt;3ZD&gt;'Q!75!7!!114G^S&lt;7&amp;M)%^Q:8*B&gt;'FP&lt;B6115UU)%FO&gt;G6S&gt;#"197FS)&amp;.X98!/5%&amp;..#"*&lt;H:F=H.J&lt;WY*5'&amp;J=C"4&gt;W&amp;Q!!V5&gt;7ZF)&amp;"P&lt;'&amp;S;82Z!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA4X6U=(6U)'FO!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">276832256</Property>
				</Item>
			</Item>
		</Item>
		<Item Name="Lot Code" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">Lot Code</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Lot Code</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="Read Lot Code.vi" Type="VI" URL="../Accessor/Lot Code Property/Read Lot Code.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;=!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"*!-0````])4'^U)%.P:'5!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA4X6U=(6U)'FO!!"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!)!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
			</Item>
			<Item Name="Write Lot Code.vi" Type="VI" URL="../Accessor/Lot Code Property/Write Lot Code.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;=!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"*!-0````])4'^U)%.P:'5!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA4X6U=(6U)'FO!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!)1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
			</Item>
		</Item>
		<Item Name="Stop Tune" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">Stop Tune</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Stop Tune</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="Read Stop Tune.vi" Type="VI" URL="../Accessor/Stop Tune Property/Read Stop Tune.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;M!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!Z!)1F4&gt;'^Q)&amp;2V&lt;G5!&amp;%"Q!"E!!1!&amp;#6.U&lt;X!A6(6O:1"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;5?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q!!$62Y)%^V&gt;("V&gt;#"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;5?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q!!$&amp;2Y)%^V&gt;("V&gt;#"J&lt;A!!91$Q!!Q!!Q!%!!9!"Q!%!!1!"!!%!!A!"!!%!!E#!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!#A!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
			</Item>
			<Item Name="Write Stop Tune.vi" Type="VI" URL="../Accessor/Stop Tune Property/Write Stop Tune.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;M!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!Z!)1F4&gt;'^Q)&amp;2V&lt;G5!&amp;%"Q!"E!!1!(#6.U&lt;X!A6(6O:1"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;5?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q!!$&amp;2Y)%^V&gt;("V&gt;#"J&lt;A!!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!)!!E#!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#A!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
			</Item>
		</Item>
	</Item>
	<Item Name="Control" Type="Folder">
		<Item Name="AC Gain-Enum.ctl" Type="VI" URL="../Control/AC Gain-Enum.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#\!!!!!1#T!0%!!!!!!!!!!R2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;5?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=R""1S"(97FO,56O&gt;7UO9X2M!(.!&amp;A!1"4!O-'2#"4!O-72#"4!O-W2#"4!O.'2#"4!O.72#"4!O.W2#"4!O/'2#"4!O/72#"4%O-72#"4%O-G2#"4%O-W2#"4%O.72#"4%O.G2#"4%O.W2#"4%O/72#"4)O-'2#!!&gt;"1S"(97FO!!%!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">1048576</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074278912</Property>
		</Item>
		<Item Name="Bandwidth-Cluster.ctl" Type="VI" URL="../Control/Bandwidth-Cluster.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%;!!!!#1!61!5!$E*B&lt;G2X;72U;#"$3#!Q!!!61!5!$E*B&lt;G2X;72U;#"$3#!R!!!61!5!$E*B&lt;G2X;72U;#"$3#!S!!!61!5!$E*B&lt;G2X;72U;#"$3#!T!!!61!5!$E*B&lt;G2X;72U;#"$3#!U!!!61!5!$E*B&lt;G2X;72U;#"$3#!V!!!61!5!$E*B&lt;G2X;72U;#"$3#!W!!!61!5!$E*B&lt;G2X;72U;#"$3#!X!!"K!0%!!!!!!!!!!R6115UU)%.%5C"%:8:J9W5O&lt;(:M;7)26(AA2'6W;7.F,GRW9WRB=X-61G&amp;O:(&gt;J:(2I,5.M&gt;8.U:8)O9X2M!#2!5!!)!!!!!1!#!!-!"!!&amp;!!9!"QF#97ZE&gt;WFE&gt;'A!!1!)!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">1048576</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082143232</Property>
		</Item>
		<Item Name="Bias-Cluster.ctl" Type="VI" URL="../Control/Bias-Cluster.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$F!!!!#1!01!I!#5*J98-A1UAA-!!01!I!#5*J98-A1UAA-1!01!I!#5*J98-A1UAA-A!01!I!#5*J98-A1UAA-Q!01!I!#5*J98-A1UAA.!!01!I!#5*J98-A1UAA.1!01!I!#5*J98-A1UAA.A!01!I!#5*J98-A1UAA.Q"F!0%!!!!!!!!!!R6115UU)%.%5C"%:8:J9W5O&lt;(:M;7)26(AA2'6W;7.F,GRW9WRB=X-11GFB=SV$&lt;(6T&gt;'6S,G.U&lt;!!E1&amp;!!#!!!!!%!!A!$!!1!"1!'!!=*1GFB=S"%982B!!%!#!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">1048576</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082143234</Property>
		</Item>
		<Item Name="Chip Status-Cluster.ctl" Type="VI" URL="../Control/Chip Status-Cluster.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!##!!!!!Q!)1#%$1U23!!B!)1.-2%1!;A$R!!!!!!!!!!-&gt;56.'5#V%2#"115UU)%.%5C"%:8:J9W5O&lt;(:M;7)26(AA4X6U=(6U,GRW9WRB=X-81WBJ=#"4&gt;'&amp;U&gt;8-N1WRV=X2F=CZD&gt;'Q!'E"1!!)!!!!"#U.I;8!A5X2B&gt;(6T!!%!!A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">1048576</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082143232</Property>
		</Item>
		<Item Name="Mod-Cluster.ctl" Type="VI" URL="../Control/Mod-Cluster.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$E!!!!#1!01!I!#%VP:#"$3#!Q!!!01!I!#%VP:#"$3#!R!!!01!I!#%VP:#"$3#!S!!!01!I!#%VP:#"$3#!T!!!01!I!#%VP:#"$3#!U!!!01!I!#%VP:#"$3#!V!!!01!I!#%VP:#"$3#!W!!!01!I!#%VP:#"$3#!X!!"E!0%!!!!!!!!!!R6115UU)%.%5C"%:8:J9W5O&lt;(:M;7)26(AA2'6W;7.F,GRW9WRB=X-047^E,5.M&gt;8.U:8)O9X2M!#2!5!!)!!!!!1!#!!-!"!!&amp;!!9!"QB.&lt;W1A2'&amp;U91!!!1!)!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">1048576</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082143234</Property>
		</Item>
		<Item Name="Rvcsel-Enum.ctl" Type="VI" URL="../Control/Rvcsel-Enum.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$Q!!!!!1$I!0%!!!!!!!!!!R2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;5?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q^3&gt;G.T:7QN27ZV&lt;3ZD&gt;'Q!K5!7!!]*.4!O-#"P;'VT#45S,D5A&lt;WBN=QEV.3YQ)'^I&lt;8-*.4=O.3"P;'VT#49Q,D!A&lt;WBN=QEW-CYV)'^I&lt;8-*.T!O-#"P;'VT#4=S,D5A&lt;WBN=QEX.3YQ)'^I&lt;8-*/$!O-#"P;'VT#4AS,D5A&lt;WBN=QEY.3YQ)'^I&lt;8-*/4!O-#"P;'VT#4EV,D!A&lt;WBN=QAR-$!A&lt;WBN=Q!!"F*W9X.F&lt;!!!!1!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">9437184</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074541056</Property>
		</Item>
		<Item Name="Tune Polarity-Enum.ctl" Type="VI" URL="../Control/Tune Polarity-Enum.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#Q!!!!!1#I!0%!!!!!!!!!!RV25U:1,52%)&amp;""441A1U23)%2F&gt;GFD:3ZM&gt;GRJ9B&amp;5?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=R:1&lt;WRB=GFU?3".&lt;W2F,56O&gt;7UO9X2M!&amp;F!&amp;A!%%%ZP=GVB&lt;#"0='6S982J&lt;WY65%&amp;..#"*&lt;H:F=H1A5'&amp;J=C"4&gt;W&amp;Q$F""441A37ZW:8*T;7^O#6"B;8)A5X&gt;B=!!.5'^M98*J&gt;(EA47^E:1!"!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">9437184</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074541056</Property>
		</Item>
	</Item>
	<Item Name="Override" Type="Folder">
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Item Name="CDR" Type="Folder">
			<Item Name="Set NRZ 3D CTLE Center.vi" Type="VI" URL="../Override/Set NRZ 3D CTLE Center.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;)!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!N!"1!%1V2-21!!1%"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)26(AA4X6U=(6U,GRW9WRB=X-!!!R5?#"0&gt;82Q&gt;81A;7Y!!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!Q!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!A!!!#1!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1082139152</Property>
			</Item>
			<Item Name="Set PAM4 3D CTLE Center.vi" Type="VI" URL="../Override/Set PAM4 3D CTLE Center.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;)!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!N!"1!%1V2-21!!1%"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)26(AA4X6U=(6U,GRW9WRB=X-!!!R5?#"0&gt;82Q&gt;81A;7Y!!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!Q!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!A!!!#1!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1082139152</Property>
			</Item>
			<Item Name="Get NRZ 3D CTLE Center.vi" Type="VI" URL="../Override/Get NRZ 3D CTLE Center.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;)!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!N!"1!%1V2-21!!1%"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)26(AA4X6U=(6U,GRW9WRB=X-!!!V5?#"0&gt;82Q&gt;81A&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!1%"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)26(AA4X6U=(6U,GRW9WRB=X-!!!R5?#"0&gt;82Q&gt;81A;7Y!!&amp;1!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!Q!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!#1!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1082139152</Property>
			</Item>
			<Item Name="Get PAM4 3D CTLE Center.vi" Type="VI" URL="../Override/Get PAM4 3D CTLE Center.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;)!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!N!"1!%1V2-21!!1%"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)26(AA4X6U=(6U,GRW9WRB=X-!!!V5?#"0&gt;82Q&gt;81A&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!1%"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)26(AA4X6U=(6U,GRW9WRB=X-!!!R5?#"0&gt;82Q&gt;81A;7Y!!&amp;1!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!Q!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!#1!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1082139152</Property>
			</Item>
			<Item Name="Get IC Channel.vi" Type="VI" URL="../Override/Get IC Channel.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;=!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"&amp;!"1!+35-A1WBB&lt;GZF&lt;!!!1%"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)26(AA4X6U=(6U,GRW9WRB=X-!!!V5?#"0&gt;82Q&gt;81A&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!$E!B#5F$)&amp;.F&lt;'6D&gt;!"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;5?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q!!$&amp;2Y)%^V&gt;("V&gt;#"J&lt;A!!6!$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!)!!E#!!"Y!!!.#!!!!!!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!#!!!!*!!!!!!!1!+!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1082139136</Property>
			</Item>
			<Item Name="Write Channel Setting.vi" Type="VI" URL="../Override/Write Channel Setting.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;/!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"&amp;!"1!+35-A1WBB&lt;GZF&lt;!!!1%"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)26(AA4X6U=(6U,GRW9WRB=X-!!!R5?#"0&gt;82Q&gt;81A;7Y!!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!Q!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!A!!!#1!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1082139152</Property>
			</Item>
			<Item Name="Set Default Value.vi" Type="VI" URL="../Override/Set Default Value.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%^!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA4X6U=(6U)'FO!!"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!E!!!!!!"!!A!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1082139152</Property>
			</Item>
			<Item Name="Set AFE1.vi" Type="VI" URL="../Override/Set AFE1.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;)!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!N!"1!%15:&amp;-1!!1%"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)26(AA4X6U=(6U,GRW9WRB=X-!!!R5?#"0&gt;82Q&gt;81A;7Y!!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!Q!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!A!!!#3!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
			</Item>
			<Item Name="Set CTLE BOOST.vi" Type="VI" URL="../Override/Set CTLE BOOST.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;/!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"&amp;!"1!+1V2-23"#4U^46!!!1%"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)26(AA4X6U=(6U,GRW9WRB=X-!!!R5?#"0&gt;82Q&gt;81A;7Y!!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!Q!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!A!!!#3!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
			</Item>
			<Item Name="Save Autotune Data.vi" Type="VI" URL="../Override/Save Autotune Data.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%^!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA4X6U=(6U)'FO!!"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!EA!!!!!"!!A!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
			</Item>
			<Item Name="Software Reset.vi" Type="VI" URL="../Override/Software Reset.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;,!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!Z!)1F*1S"4:7RF9X1!1%"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)26(AA4X6U=(6U,GRW9WRB=X-!!!R5?#"0&gt;82Q&gt;81A;7Y!!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!Q!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!A!!!#3!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
			</Item>
			<Item Name="Get Bandwidth.vi" Type="VI" URL="../Override/Get Bandwidth.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;-!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!^!"1!*1G&amp;O:(&gt;J:(2I!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA2'6W;7.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA2'6W;7.F)'FO!!"5!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!-!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!EA!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
			</Item>
			<Item Name="Set Bandwidth.vi" Type="VI" URL="../Override/Set Bandwidth.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;-!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA2'6W;7.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!^!"1!*1G&amp;O:(&gt;J:(2I!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA2'6W;7.F)'FO!!"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!-!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!)!!!!EA!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
			</Item>
			<Item Name="Set Main Value.vi" Type="VI" URL="../Override/Set Main Value.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(!!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%:!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!35%&amp;..#"5?#"%:8:J9W5A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!F!"Q!$6'^Q!!F!"Q!$47FE!!F!"Q!$1G^U!&amp;Y!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!71&amp;!!!Q!(!!A!#12.97FO!!"%1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;5?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q!!%6""441A6(AA2'6W;7.F)'FO!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!#A!,!Q!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!A!!!#3!!!!!!%!$!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
			</Item>
			<Item Name="Get Main Value.vi" Type="VI" URL="../Override/Get Main Value.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(!!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!F!"Q!$6'^Q!!F!"Q!$47FE!!F!"Q!$1G^U!&amp;Y!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!71&amp;!!!Q!&amp;!!9!"Q2.97FO!!"'1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;5?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q!!%F""441A6(AA2'6W;7.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"%1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;5?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q!!%6""441A6(AA2'6W;7.F)'FO!&amp;1!]!!-!!-!"!!)!!E!"!!%!!1!"!!+!!1!"!!,!Q!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!#3!!!!!!%!$!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
			</Item>
			<Item Name="Set Main Polarity.vi" Type="VI" URL="../Override/Set Main Polarity.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'`!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%:!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!35%&amp;..#"5?#"%:8:J9W5A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!B!)1.5&lt;X!!#%!B!UVJ:!!)1#%$1G^U!'!!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'E^V&gt;("V&gt;#"#&lt;W^M:7&amp;O,5.M&gt;8.U:8)O9X2M!":!5!!$!!=!#!!*"%VB;7Y!!%2!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!25%&amp;..#"5?#"%:8:J9W5A;7Y!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!+!!M$!!"Y!!!.#!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!#!!!!*)!!!!!!1!-!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
			</Item>
			<Item Name="Get Main Polarity.vi" Type="VI" URL="../Override/Get Main Polarity.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'`!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!B!)1.5&lt;X!!#%!B!UVJ:!!)1#%$1G^U!'!!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'E^V&gt;("V&gt;#"#&lt;W^M:7&amp;O,5.M&gt;8.U:8)O9X2M!":!5!!$!!5!"A!("%VB;7Y!!%:!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!35%&amp;..#"5?#"%:8:J9W5A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%2!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!25%&amp;..#"5?#"%:8:J9W5A;7Y!6!$Q!!Q!!Q!%!!A!#1!%!!1!"!!%!!I!"!!%!!M$!!"Y!!!.#!!!!!!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!*)!!!!!!1!-!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
			</Item>
			<Item Name="Set Pre Value.vi" Type="VI" URL="../Override/Set Pre Value.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'_!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%:!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!35%&amp;..#"5?#"%:8:J9W5A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!F!"Q!$6'^Q!!F!"Q!$47FE!!F!"Q!$1G^U!&amp;Q!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!51&amp;!!!Q!(!!A!#1.1=G5!2%"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)26(AA4X6U=(6U,GRW9WRB=X-!!"&amp;115UU)&amp;2Y)%2F&gt;GFD:3"J&lt;A"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!I!#Q-!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!)!!!!EA!!!!!"!!Q!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
			</Item>
			<Item Name="Get Pre Value.vi" Type="VI" URL="../Override/Get Pre Value.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'_!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!F!"Q!$6'^Q!!F!"Q!$47FE!!F!"Q!$1G^U!&amp;Q!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!51&amp;!!!Q!&amp;!!9!"Q.1=G5!2E"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)26(AA4X6U=(6U,GRW9WRB=X-!!"*115UU)&amp;2Y)%2F&gt;GFD:3"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!2%"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)26(AA4X6U=(6U,GRW9WRB=X-!!"&amp;115UU)&amp;2Y)%2F&gt;GFD:3"J&lt;A"5!0!!$!!$!!1!#!!*!!1!"!!%!!1!#A!%!!1!#Q-!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!EA!!!!!"!!Q!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
			</Item>
			<Item Name="Set Delay Value.vi" Type="VI" URL="../Override/Set Delay Value.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(!!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%:!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!35%&amp;..#"5?#"%:8:J9W5A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!F!"Q!$6'^Q!!F!"Q!$47FE!!F!"Q!$1G^U!&amp;Y!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!71&amp;!!!Q!(!!A!#16%:7RB?1"%1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;5?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q!!%6""441A6(AA2'6W;7.F)'FO!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!#A!,!Q!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!A!!!#3!!!!!!%!$!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
			</Item>
			<Item Name="Get Delay Value.vi" Type="VI" URL="../Override/Get Delay Value.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(!!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!F!"Q!$6'^Q!!F!"Q!$47FE!!F!"Q!$1G^U!&amp;Y!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"4UUT/$!V-SZM&gt;G.M98.T'%^V&gt;("V&gt;#"797RV:3V$&lt;(6T&gt;'6S,G.U&lt;!!71&amp;!!!Q!&amp;!!9!"Q6%:7RB?1"'1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;5?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q!!%F""441A6(AA2'6W;7.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"%1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;5?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q!!%6""441A6(AA2'6W;7.F)'FO!&amp;1!]!!-!!-!"!!)!!E!"!!%!!1!"!!+!!1!"!!,!Q!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!#3!!!!!!%!$!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
			</Item>
			<Item Name="Get Tx Algrethom.vi" Type="VI" URL="../Override/Get Tx Algrethom.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;0!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"*!)1R5?#""&lt;'&gt;S:82I&lt;WU!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA2'6W;7.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA2'6W;7.F)'FO!!"5!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!-!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!E!!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
			</Item>
			<Item Name="Set Tx Algrethom.vi" Type="VI" URL="../Override/Set Tx Algrethom.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;0!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA2'6W;7.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"*!)1R5?#""&lt;'&gt;S:82I&lt;WU!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA2'6W;7.F)'FO!!"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!-!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!+!!!!E!!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
			</Item>
		</Item>
		<Item Name="LDD" Type="Folder">
			<Item Name="Get Tx Power Down.vi" Type="VI" URL="../Override/Get Tx Power Down.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;?!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"2!)1Z&amp;&lt;G&amp;C&lt;'5P2'FT97*M:1!!1%"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)26(AA4X6U=(6U,GRW9WRB=X-!!!V5?#"0&gt;82Q&gt;81A&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!$5!$!!&gt;$;'&amp;O&lt;G6M!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA4X6U=(6U)'FO!!"5!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!A!#1-!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!+!!!!EA!!!!!"!!I!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
			</Item>
			<Item Name="Set Tx Power Down.vi" Type="VI" URL="../Override/Set Tx Power Down.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;2!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"2!)1Z&amp;&lt;G&amp;C&lt;'5P2'FT97*M:1!!1%"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)26(AA4X6U=(6U,GRW9WRB=X-!!!R5?#"0&gt;82Q&gt;81A;7Y!!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!=!"!!)!Q!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!#!!!!!!!!!#3!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
			</Item>
			<Item Name="Set Tx Disable.vi" Type="VI" URL="../Override/Set Tx Disable.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;2!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"2!)1Z&amp;&lt;G&amp;C&lt;'5P2'FT97*M:1!!1%"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)26(AA4X6U=(6U,GRW9WRB=X-!!!R5?#"0&gt;82Q&gt;81A;7Y!!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!=!"!!)!Q!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!#!!!!!!!!!#3!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
			</Item>
			<Item Name="Get Tx Disable.vi" Type="VI" URL="../Override/Get Tx Disable.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;;!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!""!)1J5?#"%;8.B9GRF!!"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;5?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q!!$62Y)%^V&gt;("V&gt;#"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!.1!-!"U.I97ZO:7Q!1%"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)26(AA4X6U=(6U,GRW9WRB=X-!!!R5?#"0&gt;82Q&gt;81A;7Y!!&amp;1!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!#!!*!Q!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!I!!!#3!!!!!!%!#A!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
			</Item>
			<Item Name="Set Burn-In Current.vi" Type="VI" URL="../Override/Set Burn-In Current.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;3!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"6!#A!01H6S&lt;CV*&lt;C"$&gt;8*S:7ZU!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA4X6U=(6U)'FO!!"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!(!!1!#!-!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!A!!!!!!!!!EA!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
			</Item>
			<Item Name="Get Burn-In Current.vi" Type="VI" URL="../Override/Get Burn-In Current.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;N!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"6!#A!01H6S&lt;CV*&lt;C"$&gt;8*S:7ZU!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"N!&amp;A!#"E.I;8!A-!:$;'FQ)$%!"%.I;8!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA4X6U=(6U)'FO!!"5!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!A!#1-!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!)!!!!EA!!!!!"!!I!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
			</Item>
			<Item Name="Set Burn-In Mode.vi" Type="VI" URL="../Override/Set Burn-In Mode.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;2!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"2!)1Z&amp;&lt;G&amp;C&lt;'5P2'FT97*M:1!!1%"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)26(AA4X6U=(6U,GRW9WRB=X-!!!R5?#"0&gt;82Q&gt;81A;7Y!!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!=!"!!)!Q!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!#!!!!!!!!!#3!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
			</Item>
			<Item Name="Get Burn-In Mode.vi" Type="VI" URL="../Override/Get Burn-In Mode.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;=!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"*!)1R#&gt;8*O,5FO)%VP:'5!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!V!!Q!(1WBB&lt;GZF&lt;!"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;5?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q!!$&amp;2Y)%^V&gt;("V&gt;#"J&lt;A!!6!$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!)!!E$!!"Y!!!.#!!!!!!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!#A!!!*)!!!!!!1!+!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
			</Item>
			<Item Name="Get Bias Value.vi" Type="VI" URL="../Override/Get Bias Value.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;3!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!N!#A!%1GFB=Q!!2E"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)26(AA4X6U=(6U,GRW9WRB=X-!!"*115UU)&amp;2Y)%2F&gt;GFD:3"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!2%"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)26(AA4X6U=(6U,GRW9WRB=X-!!"&amp;115UU)&amp;2Y)%2F&gt;GFD:3"J&lt;A"5!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!-!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!EA!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
			</Item>
			<Item Name="Get Mod Value.vi" Type="VI" URL="../Override/Get Mod Value.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;1!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!F!#A!$47^E!%:!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!35%&amp;..#"5?#"%:8:J9W5A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%2!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!25%&amp;..#"5?#"%:8:J9W5A;7Y!6!$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A$!!"Y!!!.#!!!!!!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!*)!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
			</Item>
			<Item Name="Get DC Gain.vi" Type="VI" URL="../Override/Get DC Gain.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;5!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!V!"1!(2%-A2W&amp;J&lt;A"'1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;5?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q!!%F""441A6(AA2'6W;7.F)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"%1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;5?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q!!%6""441A6(AA2'6W;7.F)'FO!&amp;1!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!Q!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!#3!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
			</Item>
			<Item Name="Get AC Gain.vi" Type="VI" URL="../Override/Get AC Gain.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!([!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!,-!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T%%&amp;$)%&gt;B;7YN27ZV&lt;3ZD&gt;'Q!=U!7!"!&amp;-#YQ:%)&amp;-#YR:%)&amp;-#YT:%)&amp;-#YU:%)&amp;-#YV:%)&amp;-#YX:%)&amp;-#YY:%)&amp;-#YZ:%)&amp;-3YR:%)&amp;-3YS:%)&amp;-3YT:%)&amp;-3YV:%)&amp;-3YW:%)&amp;-3YX:%)&amp;-3YZ:%)&amp;-CYQ:%)!"U&amp;$)%&gt;B;7Y!2E"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)26(AA4X6U=(6U,GRW9WRB=X-!!"*115UU)&amp;2Y)%2F&gt;GFD:3"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!2%"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)26(AA4X6U=(6U,GRW9WRB=X-!!"&amp;115UU)&amp;2Y)%2F&gt;GFD:3"J&lt;A"5!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!-!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!EA!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
			</Item>
			<Item Name="Get Rvcsel.vi" Type="VI" URL="../Override/Get Rvcsel.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)Z!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!0)!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T$V*W9X.F&lt;#V&amp;&lt;H6N,G.U&lt;!#T1"9!%!EV-#YQ)'^I&lt;8-*.4)O.3"P;'VT#45V,D!A&lt;WBN=QEV.SYV)'^I&lt;8-*.D!O-#"P;'VT#49S,D5A&lt;WBN=QEW.3YQ)'^I&lt;8-*.T!O-#"P;'VT#4=S,D5A&lt;WBN=QEX.3YQ)'^I&lt;8-*/$!O-#"P;'VT#4AS,D5A&lt;WBN=QEY.3YQ)'^I&lt;8-*/4!O-#"P;'VT#4EV,D!A&lt;WBN=QAR-$!A&lt;WBN=Q!!"F*W9X.F&lt;!!!2E"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)26(AA4X6U=(6U,GRW9WRB=X-!!"*115UU)&amp;2Y)%2F&gt;GFD:3"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!2%"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)26(AA4X6U=(6U,GRW9WRB=X-!!"&amp;115UU)&amp;2Y)%2F&gt;GFD:3"J&lt;A"5!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!-!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!EA!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
			</Item>
			<Item Name="Set Bias Value.vi" Type="VI" URL="../Override/Set Bias Value.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;3!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%:!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!35%&amp;..#"5?#"%:8:J9W5A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!N!#A!%1GFB=Q!!2%"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)26(AA4X6U=(6U,GRW9WRB=X-!!"&amp;115UU)&amp;2Y)%2F&gt;GFD:3"J&lt;A"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!-!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!+!!!!EA!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
			</Item>
			<Item Name="Set Mod Value.vi" Type="VI" URL="../Override/Set Mod Value.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;1!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%:!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!35%&amp;..#"5?#"%:8:J9W5A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!F!#A!$47^E!%2!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!25%&amp;..#"5?#"%:8:J9W5A;7Y!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A$!!"Y!!!.#!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!#A!!!*)!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
			</Item>
			<Item Name="Set DC Gain.vi" Type="VI" URL="../Override/Set DC Gain.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;5!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%:!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!35%&amp;..#"5?#"%:8:J9W5A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!V!"1!(2%-A2W&amp;J&lt;A"%1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;5?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q!!%6""441A6(AA2'6W;7.F)'FO!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!Q!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!A!!!#3!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
			</Item>
			<Item Name="Set AC Gain.vi" Type="VI" URL="../Override/Set AC Gain.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!([!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%:!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!35%&amp;..#"5?#"%:8:J9W5A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!,-!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T%%&amp;$)%&gt;B;7YN27ZV&lt;3ZD&gt;'Q!=U!7!"!&amp;-#YQ:%)&amp;-#YR:%)&amp;-#YT:%)&amp;-#YU:%)&amp;-#YV:%)&amp;-#YX:%)&amp;-#YY:%)&amp;-#YZ:%)&amp;-3YR:%)&amp;-3YS:%)&amp;-3YT:%)&amp;-3YV:%)&amp;-3YW:%)&amp;-3YX:%)&amp;-3YZ:%)&amp;-CYQ:%)!"U&amp;$)%&gt;B;7Y!2%"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)26(AA4X6U=(6U,GRW9WRB=X-!!"&amp;115UU)&amp;2Y)%2F&gt;GFD:3"J&lt;A"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!-!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!)!!!!EA!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
			</Item>
			<Item Name="Set Rvcsel.vi" Type="VI" URL="../Override/Set Rvcsel.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)Z!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%:!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!35%&amp;..#"5?#"%:8:J9W5A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!0)!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T$V*W9X.F&lt;#V&amp;&lt;H6N,G.U&lt;!#T1"9!%!EV-#YQ)'^I&lt;8-*.4)O.3"P;'VT#45V,D!A&lt;WBN=QEV.SYV)'^I&lt;8-*.D!O-#"P;'VT#49S,D5A&lt;WBN=QEW.3YQ)'^I&lt;8-*.T!O-#"P;'VT#4=S,D5A&lt;WBN=QEX.3YQ)'^I&lt;8-*/$!O-#"P;'VT#4AS,D5A&lt;WBN=QEY.3YQ)'^I&lt;8-*/4!O-#"P;'VT#4EV,D!A&lt;WBN=QAR-$!A&lt;WBN=Q!!"F*W9X.F&lt;!!!2%"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)26(AA4X6U=(6U,GRW9WRB=X-!!"&amp;115UU)&amp;2Y)%2F&gt;GFD:3"J&lt;A"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!-!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!)!!!!EA!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
			</Item>
		</Item>
		<Item Name="Page" Type="Folder">
			<Item Name="Get Device Pages.vi" Type="VI" URL="../Override/Get Device Pages.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;E!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!V!"1!(4H6N:8*J9Q!;1%!!!@````]!"1R%:8:J9W5A5'&amp;H:8-!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA4X6U=(6U)'FO!!"5!0!!$!!$!!1!"A!(!!1!"!!%!!1!#!!%!!1!#1)!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!E!!!!!!"!!I!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1082139136</Property>
			</Item>
			<Item Name="Set CDR Input Page.vi" Type="VI" URL="../Override/Set CDR Input Page.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%^!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA4X6U=(6U)'FO!!"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!E!!!!!!"!!A!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1082139152</Property>
			</Item>
			<Item Name="Set CDR Output Page.vi" Type="VI" URL="../Override/Set CDR Output Page.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%^!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA4X6U=(6U)'FO!!"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!E!!!!!!"!!A!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1082139152</Property>
			</Item>
			<Item Name="Set VCSEL Driver Page.vi" Type="VI" URL="../Override/Set VCSEL Driver Page.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%^!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA4X6U=(6U)'FO!!"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!E!!!!!!"!!A!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1082139152</Property>
			</Item>
		</Item>
		<Item Name="Tune" Type="Folder">
			<Item Name="Get CDR Pages.vi" Type="VI" URL="../Override/Get CDR Pages.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;A!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!$5!&amp;!!&gt;/&gt;7VF=GFD!":!1!!"`````Q!%#5.%5C"197&gt;F=Q!%!!!!1%"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)26(AA4X6U=(6U,GRW9WRB=X-!!!V5?#"0&gt;82Q&gt;81A&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!1%"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)26(AA4X6U=(6U,GRW9WRB=X-!!!R5?#"0&gt;82Q&gt;81A;7Y!!&amp;1!]!!-!!-!"1!'!!=!"A!'!!9!"A!)!!9!"A!*!A!!?!!!$1A!!!E!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!#1!!!!!!%!#A!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1082139136</Property>
			</Item>
			<Item Name="Save Tune Data.vi" Type="VI" URL="../Override/Save Tune Data.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%^!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA4X6U=(6U)'FO!!"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!EA!!!!!"!!A!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
			</Item>
			<Item Name="Get Tune Polarity.vi" Type="VI" URL="../Override/Get Tune Polarity.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%^!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA4X6U=(6U)'FO!!"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!EA!!!!!"!!A!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
			</Item>
			<Item Name="Wait Tuning.vi" Type="VI" URL="../Override/Wait Tuning.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;&gt;!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!&amp;5!'!!^#;81A28*S&lt;X)A1W^V&lt;H1!#U!&amp;!!65&lt;X2B&lt;!"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;5?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q!!$62Y)%^V&gt;("V&gt;#"P&gt;81!"!!!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!1%"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)26(AA4X6U=(6U,GRW9WRB=X-!!!R5?#"0&gt;82Q&gt;81A;7Y!!&amp;1!]!!-!!-!"!!&amp;!!9!"Q!(!!=!"Q!)!!=!"Q!*!Q!!?!!!$1A!!!E!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!#1!!!!!!%!#A!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1082139152</Property>
			</Item>
			<Item Name="Get Tune IC Channel.vi" Type="VI" URL="../Override/Get Tune IC Channel.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;/!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"&amp;!"1!+35-A1WBB&lt;GZF&lt;!!!1%"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)26(AA4X6U=(6U,GRW9WRB=X-!!!V5?#"0&gt;82Q&gt;81A&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!1%"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)26(AA4X6U=(6U,GRW9WRB=X-!!!R5?#"0&gt;82Q&gt;81A;7Y!!&amp;1!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!Q!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!#3!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
			</Item>
			<Item Name="Run Tuning.vi" Type="VI" URL="../Override/Run Tuning.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%^!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA4X6U=(6U)'FO!!"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!EA!!!!!"!!A!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
			</Item>
		</Item>
		<Item Name="Get Chip Status.vi" Type="VI" URL="../Override/Get Chip Status.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%^!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA4X6U=(6U)'FO!!"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!EA!!!!!"!!A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
		</Item>
	</Item>
	<Item Name="Public" Type="Folder">
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Item Name="Get Product Channel.vi" Type="VI" URL="../Public/Get Product Channel.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;8!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!V!!Q!(1WBB&lt;GZF&lt;!"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;5?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q!!$62Y)%^V&gt;("V&gt;#"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;5?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q!!$&amp;2Y)%^V&gt;("V&gt;#"J&lt;A!!91$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A$!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
		</Item>
		<Item Name="Prepare Tx Device Class.vi" Type="VI" URL="../Public/Prepare Tx Device Class.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;0!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA2'6W;7.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!&amp;*!=!!?!!!Q&amp;E^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9H!84X"U;7.B&lt;#"1=G^E&gt;7.U,GRW9WRB=X-!!"&gt;0=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:D&lt;'&amp;T=Q"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!U)!!!!!!!!!!!!!!E!!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!#A!!!!!"!!A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082139152</Property>
		</Item>
		<Item Name="Convert Output Channel.vi" Type="VI" URL="../Public/Convert Output Channel.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;+!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA4X6U=(6U)'FO!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!)!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082139152</Property>
		</Item>
		<Item Name="Get CDR Channel Invert.vi" Type="VI" URL="../Public/Get CDR Channel Invert.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;+!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA4X6U=(6U)'FO!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!#A!!$1!!!!Q!!!!!!!!!!!!!!1!)!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082139152</Property>
		</Item>
		<Item Name="Get Driver Channel Invert.vi" Type="VI" URL="../Public/Get Driver Channel Invert.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;+!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.6(AA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-6(AA4X6U=(6U)'FO!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!#A!!$1!!!!Q!!!!!!!!!!!!!!1!)!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082139152</Property>
		</Item>
		<Item Name="Get S-DD Channel.vi" Type="VI" URL="../Public/Get S-DD Channel.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#:!!!!"1!%!!!!%5!$!!N$;'&amp;O&lt;G6M)%^V&gt;!!41!-!$62P&gt;'&amp;M)%.I97ZO:7Q!&amp;5!$!!^$&gt;8*S:7ZU)%.I97ZO:7Q!6!$Q!!Q!!!!!!!!!!1!!!!!!!!!!!!!!!A!!!!-#!!"Y!!!!!!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!I!!!!!!1!%!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342710272</Property>
		</Item>
	</Item>
</LVClass>
