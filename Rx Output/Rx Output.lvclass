﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="17008000">
	<Property Name="EndevoGOOP_ClassProvider" Type="Str">Endevo LabVIEW Native</Property>
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">16769464</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorProtected" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">16448250</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ConnectorPanePattern" Type="Str">0</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="EndevoGOOP_PlugIns" Type="Bin">&amp;Q#!!!!!!!A!4A$R!!!!!!!!!!%;2U244(:$&lt;'&amp;T=V^1&lt;(6H37Z5?8"F=SZD&gt;'Q!+U!7!!).2'6T;7&gt;O5'&amp;U&gt;'6S&lt;AN.:82I&lt;W25?8"F=Q!%6(FQ:1!!$%!Q`````Q**2!!!%%!Q`````Q&gt;7:8*T;7^O!!Z!-P````]%5'&amp;U;!!!$E!Q`````Q2/97VF!!!+1&amp;-%2'&amp;U91!!0!$R!!!!!!!!!!%62U244(:$&lt;'&amp;T=V^1&lt;(6H37YO9X2M!"Z!5!!'!!!!!1!#!!-!"!!&amp;"F"M&gt;7&gt;*&lt;A!!&amp;%"!!!(`````!!9(5'RV:UFO=Q!"!!=!!!!!!!!!!!</Property>
	<Property Name="EndevoGOOP_TemplateUsed" Type="Str">NativeSubTemplate</Property>
	<Property Name="EndevoGOOP_TemplateVersion" Type="Str"></Property>
	<Property Name="NI.Lib.ContainingLib" Type="Str">PAM4 Device IC.lvlib</Property>
	<Property Name="NI.Lib.ContainingLibPath" Type="Str">../../PAM4 Device IC.lvlib</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!,@!!!*Q(C=T:2.;BN"%%:L\)#]#!E[19R/%+;W)7$1"7T1=C#LOI+SCG=J1`!CO\K#G"NIE_TL#C,Y!NK',*,*5U^,VPB(=C!"&gt;[MUGO_LLH\4X2K2V%Z%8EB\K!XWNO3HL(&lt;1ZPPOJBP:_:W\N@+V0X[QEXH80T2_[W`K$8L_8PZ$4@;WP`3LMKI?^[NSD&gt;4,W0%\^U\'VL^V?RF&lt;P_TZT8W`OKX1.%XZA*]TGO:2HYSKW?NH_`HZ6=^0V`\[F_87ZT'\N0\_F:W`Y^\&gt;`Z1R;'\N?_?H\,E0HK]&gt;^SHH\`$RLA``#@ZP%SE3+2'%%U:I?LIWU!-^U!-^U!0&gt;U2X&gt;U2X&gt;U2X&gt;U!X&gt;U!X&gt;U!V&gt;U26&gt;U26&gt;U25^&gt;82"&amp;X2":V7#S9/*AK*"A3!:&amp;!G_!J[!*_!*?,C6A#@A#8A#HI#(&amp;!FY!J[!*_!*?"AG!5`!%`!%0!%0J2**J)Y/4]"$?8&amp;Y("[(R_&amp;R?*B3("Y(Q*H-+?Q5!5-=U`HB]$A]$A]`R?&amp;R?"Q?B]@BQ2;(R_&amp;R?"Q?BY=B;65]U;Q\/DS5%90(Y$&amp;Y$"[$B^*C]"A]"I`"9`!QH2A]"I]"95RI&amp;!&gt;"D%&amp;'AH&amp;D]"A]8-4A-8A-(I0(Y-&amp;+/W2J:&gt;9U[YY/D]+D]#A]#I`#1QF2?"1?B5@B58AI+QK0QK0Q+$Q+$V/*QK0Q+$Q+C$)JUYN34"GI*#G#QM-HH2:.O_3*2&amp;00@]XOI%I_A*)0FO1$)`EA3.ZAS2MH?5-E,\4E":3]-*)@70+$3!;50,(EAJ)4:=8XEFA1=W*'4)E*-3:'R(!^^"]HLF9L73[8MFAM:$[@SWQWE_FU+J0*2-&lt;DM9R')RE/B^OXV3G^;]8GP64T_`L]S]X6R=8:^?DC&lt;(W^/P^]NN9X@IKDLU6^]L;I4V]7&gt;8W-2HRY6&gt;1@XR@VJ^^&amp;@@F!N/V.`?&lt;8T`LVDZP[_0OX7CY:^[\9V(Y?\U9ZEH&lt;H.=];`1(-J-\^!!!!!!</Property>
	<Property Name="NI.Lib.LocalName" Type="Str">Rx Output</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.1</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.DataValRefToSelfLimitedLibFlag" Type="Bool">true</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!#!75F.31QU+!!.-6E.$4%*76Q!!'[1!!!2S!!!!)!!!'Y1!!!!L!!!!!B2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;3?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q!!!!#A&amp;Q#!!!!Q!!!I!!!!!!!!"!!$!$Q!P!!@1)!#!!!!!!%!!1!'`````Q!!!!!!!!!!!!!!!0OL70MI=86&amp;NNS_3"EDK2%!!!!-!!!!%!!!!!!X/KMFR#O-4YQ%7EM+NO.8V"W-W9]!MA4JA!G9\0B#@A!!%!!!!!!!(T)=H6$A057L`HV847UE:1%!!!$`````V"W-W9]!MA4JA!G9\0B#@A!!!"#]E,K14P_=(:J&gt;$,Y_3.+H!!!!"!!!!!!!!!%E!!&amp;-6E.$!!!!!Q!#6EF-1A!!!!"16%AQ!!!!"1!"!!%!!!!!!A!#6EF131!!!!!#%5V"6%%T/$1T.#ZM&gt;G.M98.T!A=!!&amp;"53$!!!!!J!!%!"A!!!!6$&lt;'&amp;T=QF.162"-TAU-T1245&amp;514-Y.$-U,GRW9WRB=X-!!!!$!!!"!!!!!!!!!!%!!!!!!A!!!!!!!!!!!F:*1U-!!!!#%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T&amp;U.I;8!A5X2B&gt;(6T,5.M&gt;8.U:8)O9X2M5&amp;2)-!!!!$%!!1!'!!!!#62Y)%^V&gt;("V&gt;!&gt;$&lt;WZU=G^M&amp;U.I;8!A5X2B&gt;(6T,5.M&gt;8.U:8)O9X2M!!!!!Q!"`Q!!!!%!!1!!!!!!!1!!!!!!!!!!!!!!!!!!!!!!!Q!!!!)!!A!!!!!!*A!!!"ZYH'0A9'"O9,D!!-3-$%Q.8%!7EQ;1^Y'"A5/!!1"CQ!9\!!!!!!!5!!!!#HC=9W"GY'&lt;A!%)'!!#S!"]!!!"*!!!"'(C=9W$!"0_"!%AR-D!Q8103L'DC9"L'JC&lt;!:3YOO[$CT%$-AO2/I"D4(C$.""+(KG'$3$&amp;&gt;!/)4[/&lt;Q1_E(3')!ERUJ'Q!!!!!!!!Q!!6:*2&amp;-!!!!!!!-!!!(*!!!$V(C=W]$)Q*"J&lt;'('Q-4!Q!RECT-U-#4HJ[4S-A$Z$"#A!W.1!!+AZGGBC2M?/*Q'"(L]]CVA@P-&lt;HGY8&amp;9(G'B5*JF+2&lt;B]6E5Y@&amp;::/&amp;J58@`\``^^]B/&gt;QNU@/=5=&lt;E.JO$K$Y=2=6$B!(3,/![0_"'3"6K/&lt;*&gt;!*FA&lt;1%EA;YA3DW"Q"6=425+$/5M"A?C$J]P-'%%?*1G"/CM,G8?0/&lt;XX!!036Q]#&amp;,&gt;[-'E.]\%51#B8A[1TAEDLNQ[)A"_9QH1!:W]M"]T1(X4RD)A")6A5Y4E%5MD$#,ONG//WC!Q]&amp;""%*F1+A+#&amp;5!IH;!88#%)_YQ00T8PL[XCR6)MS(&amp;C1-1.Y!94+B9DY'2A2(-:'29#V6L!W1T1=6A=1NC+U#$D:("(K\H.F2?!]E=&amp;U;9(I3[;C2X-)(.9'4YQQ!T$WA@6%]$V.UA-6_AW!%I/Q4)HA"F2Q0:([$M*#"&lt;!-L/",).'#(M0#A&lt;&lt;"E$&lt;NL:X]56+:D!_1+7.2S"/$GXQ-"!L^J:J^J*R[GWVLL;W3&lt;!U&gt;&gt;%Q37V,$-Z6=(473_H,#=TS=L8-=42W-,%W!4)4]Z*,#[WA_K!_DGZ),G-%G-!:$[S]Q!!!!!!!2I!!!&amp;U?*RT9'"AS$3W-(M!J*E:'2D%'2I9EP.45BG1A!AD!UY1("\7`%;COU2&amp;I\.%2;3\2E7HMU:&amp;!)B:/EV58PTZ``^`[Q(_+1&gt;+70GX(8C&gt;UOP'UOGC)N,LT^,J!V4FIM)$J$E;[FE9_+=?;KA$59=\\BF=-\B=QM'`\6DT"]&lt;8"LU"D*UB+DS^A3#+I['3%;DI9%-&amp;C$L1==DA&amp;0_W3]W@'!V/\!#ZEN`V50-2C&lt;DY[-.1^[V^@7]83!,:#QZ!`"EI!B*D!G)Z*(%1=0:X=58X0UCN)R!HZR99'/B6/_N5/_EYV&gt;:;6TP&lt;"$D[GCCYJ*:F*K=K?$LLZ:4F:#::_4K'/"J&lt;G"C&lt;!0H*/9H&amp;R8:1(5!T!.:?7E1!!!!!!-A!!!%A?*RT9'"AS$3W-*M!J*E:'2D%'2I9EP.45BG1Q"%'X#!]L0G.2(?*CEJHC9J)&gt;YW+2G?.CA!1MX3;K,TY]````^9$J2+^&lt;CS&gt;,CICP?YA3C#NWYUFL&gt;/&gt;R@"!]R'*O0D$+/;N@8VP&amp;^!:$)R)9AZ!`"9I!B*D!G*:*(%1=0:X=56X,S.50LEAO5SPWFGHWEH(K&lt;&lt;7ONL:*M$2VU4"*&lt;5M-TF6Q&gt;.:,[=M*T0*SN=RR.(9QM49"-B0TEEM,L;$[A!!5A][6Q!!!!Y8!9!1!!!'-4=O-#YR!!!!!!!!$"=!A!!!!!1R.SYQ!!!!!!Y8!9!1!!!'-4=O-#YR!!!!!!!!$"=!A!!!!!1R.SYQ!!!!!!Y8!9!1!!!'-4=O-#YR!!!!!!!!&amp;!%!!!$V6T7#?3;CD#ZT5EY'34G&gt;!!!!$1!!!!!!!!!!!!!!!!!!!!!!!!#!`````Y!!!!'/5:,BC6*319YC5E'*5F*"C6'-19!!!!(`````A!!!!9!!!!'!!M!"A!AQ!9!A$!'!A!-"A!!$!9"A$A'!?$Y"A(\_!9"``A'!@`Y"A(`_!9"``A'!@`Y"A0``Y9!@`@G!$`@BA!0@Q9!!@Q'!!$Q"A!!!!@````]!!!)!``````````````````````!!!!!!!!!!!!!!!!!!!!`Q!0`Q$Q]!$`!0!0$`]!!0]!$Q$Q]0!0!0$Q$Q$Q!!$`!!``!!]!$Q$Q]!]!]!!!`Q!0!0$Q]!]!]0!0!0!!!0]!$Q$Q]0!!`Q!0]!$Q!!$`!!!!!!!!!!!!!!!!!!!!``````````````````````]!!!!!!!!!!!!!!!!!!!$`!!!!!!!!!!!!!!!!!!!!`Q!!!!!!!!X.U!!!!!!!!0]!!!!!!!X!!!X1!!!!!!$`!!!!!!X!!!!!$&gt;!!!!!!`Q!!!!X!!!!!!!!.U!!!!0]!!!!-Q!!!!!!!$^!!!!$`!!!!$.X!!!!!$``!!!!!`Q!!!!T&gt;X=!!$```Q!!!!0]!!!!-X&gt;X&gt;T@```]!!!!$`!!!!$.X&gt;X&gt;`````!!!!!`Q!!!!T&gt;X&gt;X@````Q!!!!0]!!!!-X&gt;X&gt;X````]!!!!$`!!!!$.X&gt;X&gt;`````!!!!!`Q!!!!T&gt;X&gt;X@````Q!!!!0]!!!!.X&gt;X&gt;X````&gt;``!!$`!!!!!-T&gt;X&gt;```&gt;T```]!`Q!!!!!!T&gt;X@`&gt;T```]!!0]!!!!!!!$.X&gt;$````Q!!$`!!!!!!!!!-$````Q!!!!`Q!!!!!!!!!!$``Q!!!!!0]!!!!!!!!!!!!!!!!!!!$`````````````````````]!!!1!````````````````````````````````````````````"Q=("Q=("Q=("Q=("Q=("Q=("Q=("Q=("Q=("Q=(``]("Q@```]("`](`Q=("```"Q@`"Q@`"````Q=("Q@``Q=("`]("`](`Q@`"Q@`"Q@`"`]("`]("`]("Q=("```"Q=(````"Q=(`Q=("`]("`](`Q=(`Q=(`Q=("Q=(``]("Q@`"Q@`"`](`Q=(`Q=(`Q@`"Q@`"Q@`"Q=("Q@``Q=("`]("`](`Q@`"Q=(``]("Q@``Q=("`]("Q=("```"Q=("Q=("Q=("Q=("Q=("Q=("Q=("Q=("Q=("Q=(`````````````````````````````````````````````Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!84*&gt;81!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!84)("Q=(86U!!!!!!!!!!!!!!0``!!!!!!!!!!!!84)("Q=("Q=("VV&gt;!!!!!!!!!!!!``]!!!!!!!!!84)("Q=("Q=("Q=("Q&gt;&gt;81!!!!!!!!$``Q!!!!!!!!!S-A=("Q=("Q=("Q=("[R&gt;!!!!!!!!!0``!!!!!!!!!$*&gt;84)("Q=("Q=("[SML$)!!!!!!!!!``]!!!!!!!!!-FV&gt;86US"Q=("[SML+SM-A!!!!!!!!$``Q!!!!!!!!!S86V&gt;86V&gt;-FWML+SML+QS!!!!!!!!!0``!!!!!!!!!$*&gt;86V&gt;86V&gt;L+SML+SML$)!!!!!!!!!``]!!!!!!!!!-FV&gt;86V&gt;86WML+SML+SM-A!!!!!!!!$``Q!!!!!!!!!S86V&gt;86V&gt;8;SML+SML+QS!!!!!!!!!0``!!!!!!!!!$*&gt;86V&gt;86V&gt;L+SML+SML$)!!!!!!!!!``]!!!!!!!!!-FV&gt;86V&gt;86WML+SML+SM-A!!!!!!!!$``Q!!!!!!!!"&gt;86V&gt;86V&gt;8;SML+SML&amp;V&gt;L+SM!!!!!0``!!!!!!!!!!!S-FV&gt;86V&gt;L+SML&amp;V&gt;-KSML+SML!!!``]!!!!!!!!!!!!!-FV&gt;86WML&amp;V&gt;-KSML+SML!!!!!$``Q!!!!!!!!!!!!!!!$*&gt;86V&gt;"[SML+SML+Q!!!!!!0``!!!!!!!!!!!!!!!!!!!S"[SML+SML+Q!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!+SML+Q!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0```````````````````````````````````````````Q!!!=-!!5:13&amp;!!!!!$!!*'5&amp;"*!!!!!B&amp;.162"-TAU-T1O&lt;(:D&lt;'&amp;T=Q)(!!"16%AQ!!!!+1!"!!9!!!!&amp;1WRB=X-*45&amp;514-Y.$-U%5V"6%%T/$1T.#ZM&gt;G.M98.T!!!!!Q!!!1!!!!!!!!!"!!!!!!)!!!!!!!!!!!!!!!!"!!!!11!#2%2131!!!!!!!B&amp;.162"-TAU-T1O&lt;(:D&lt;'&amp;T=Q)(!!"16%AQ!!!!+1!"!!9!!!!&amp;1WRB=X-*45&amp;514-Y.$-U%5V"6%%T/$1T.#ZM&gt;G.M98.T!!!!!Q!!!1!!!!!!!!!"!!!!!!)!!!!!!!"E!!!!!!!#!!!!11!!!#I!!F2%1U-!!!!!!!)26(AA4X6U=(6U,GRW9WRB=X-81WBJ=#"4&gt;'&amp;U&gt;8-N1WRV=X2F=CZD&gt;'R16%AQ!!!!-1!"!!9!!!!*6(AA4X6U=(6U"U.P&lt;H2S&lt;WQ81WBJ=#"4&gt;'&amp;U&gt;8-N1WRV=X2F=CZD&gt;'Q!!!!$!!$`!!!!!1!"!!!!!!!"!!!!!!!!!!!!!!!!!!!!!!!!!!%!!!!,5&amp;2)-!!!!#-!!1!&amp;!!!!#62Y)%^V&gt;("V&gt;"&amp;5?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q!$!!!!"+%!!!YM?*TF6UNM'V55P=_?B(&amp;)[%Q3UU:K]-3;"!MV+%U,43-+-:H3ZE0KVK\[!1IO&gt;NK#;;JY5H7"7K'YF&lt;+I7&amp;!C@J6A9\&amp;![C),&amp;IA&amp;S,#:"6EB12;G7=!'")M5)OBYO/_.R_0R//-+MI+*]G1Z^^R\X\HHT(M"?#!C&lt;0/6Y,)/2,C$(S:V;%FJ"+$1TU0Z[&lt;M'QF(S&amp;Z$/,K,$#(^570'6S(9&gt;7F.;(T_A,-"P''X]9(R!60^(QGU-&lt;2;[-&amp;G,$FN3WF:R8#Y+]KXN]E+4F67%(O%[+@G/S[%`_/OZ'3Q)O1B&gt;R8Z3!K,U=&amp;QO(%O?4_&gt;E_GWAH_^C+1-[#)L7.CM8(];-70J,FN+`2O;Z*CMF9-I),#UNW3$2"07R.I911_9"S!X`GA?G8&gt;'[MX*R"]/U-!T7O7(655+FNWDP&amp;&amp;1,\6!U!;')OV4?-M6;/(W%Y6:86R'(;RFX6I&gt;/O@AUX]8@4HX@V(9Y^SE1))58??.$QU`RYB3&gt;!INNRU(U$B-BDJ`D/DS3UXQP!7@2_R8EW2AY;QR0Y"D)-WQ-!76"!(M/_1:T5,B#O(MU-Z&gt;6U\03T,4U=C;:T5I8:M^&gt;4+JJ+:65E_Y*\6/UQ"[[?VK-C1/#Q-%8=+G;\2H)Z`.)!+YW^%G%&lt;J7,&amp;:T)&gt;P.[B@'5T4CN;D0X&amp;$+HL#@/50:[BXW7:HV-MV'WXG]L&gt;R#6+QKVT4G5_^DG+`&gt;R&amp;.4F'O7##J`!61]6\D&amp;"NH)R(O"T5$UQ1YCZ5KV=R+C)O&gt;J9O8N&gt;SK89'O5O,CY[=-D`=%7Z(#'G=H0,RFXD,N8P&gt;]:0M!*P-`X['#+&amp;YZ!VC:99Q",PQQBDXW_R0W%*6LE6'H=K&gt;M3&lt;@]E(JGJ&lt;%G.2+;YGV&lt;FMT3CQX5FMN`=%^*Y%:&lt;VNIPQ^'OG1;33F'"KH&amp;4M1^CI-/-VUG#JDSG%GYR=!!``95"JS'.S[//,W#Y=`#&gt;D2U#`R'L`1ZTAWX.!PC3I#,.@YH;YZT&gt;:8&lt;.===\D';N(BGB/&lt;\ZK4^6Q4A2DM^8$!=S\8R"#6B)A(ZHG8;W*9*WH6]8,.#W\8R+R;8KYZ6:H#BNZZR`A&gt;""CM]MYB(!L6(!V:"^EJTAQ&gt;Q!%WA+$4.Z&amp;\^%XT[.FT&amp;[4"GE'U[P!;;AU0K+$^WP2"(X4ATB[;Q&amp;RU&amp;5.G,[4EFQIBK\+9EVB)$VW&amp;=*H-$"_GO&gt;WB+9/'YNIY^.^H$94M$:YX@8C-5&lt;=&amp;L"&gt;!")+6K=.UX4U'-52C)4UUJ&amp;)C\?\'%7L=?_C\`TSUME&gt;5X%S6\Y^UGL\HU0@84F'?SH'IM..592_DQJ:L&amp;:&lt;;*)8N&gt;#MM`6^8W04`1'&amp;H[CCMK5JBA;GK&gt;T#`]P5=&lt;XS'/M.@-\9:9X^]U]R(E,/IIEHMD0U6ORD!OYB$D1=WP.GK$&gt;498&amp;;D_'QU%&gt;UVN(P8\E=T&amp;^HVVHU'(61U@K&gt;=4'+2]KH7D;@@0L4'RM@*G$HLA(7.D&gt;L(]NL"OA=*ED&gt;?@3^ZQS4E0C3E(GV1@PCYL"B?!=*_4(Z&amp;B[!SWP[.-)&lt;UN@*D`(\B*B+-*`S$`%X_4O(&lt;SH^QB=E+-L&gt;-ZP`]78YPR'`\'VAIU3Y!!!!!!!!%!!!!3A!!!!1!!!!!!!!!$!!"1E2)5!!!!!!!!Q!!!')!!!"S?*RD9'$)%Z"A_M&gt;1^Z?"3?!LE#(^FY&amp;:U)`R.Q-$JZ`!93$.+#!*&amp;*&lt;^S]!OK!U7VD[CS]%!";JMD"S3()=&amp;/=!S(#U;$0```_@Y?O1;8-52(TB4::9]BQ1!&amp;'):!!!!!!!!"!!!!!=!!!3H!!!!"Q!!!#&amp;@&lt;GF@4'&amp;T&gt;%NO&lt;X&gt;O4X&gt;O;7ZH4&amp;:$&lt;'&amp;T=U.M&gt;8.U:8)!!!$^&amp;Q#!!!!!!!%!#!!Q`````Q!"!!!!!!$B!!!!"1"%1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.162"-TAU-T1O&lt;(:D&lt;'&amp;T=Q!!%5V"6%%T/$1T.#ZM&gt;G.M98.T!!R!)1:$;'FQ)$%!!!R!)1:$;'FQ)$)!!'%!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T&amp;U.I;8!A5X2B&gt;(6T,5.M&gt;8.U:8)O9X2M!"J!5!!#!!%!!AJ535%A5X2B&gt;(6T!!!=1&amp;!!!A!!!!-25HAA4X6U=(6U,GRW9WRB=X-!!1!%!!!!!!!!!"J-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;597*0=G2F=A!!!$58!)!!!!!!!A!&amp;!!=!!!Q!1!!"`````Q!!!!%!!1!!!!1!!!!!!!!!!1!!!!)!!!!$!!!!!!!!!"N-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;5;7VF=X2B&lt;8!!!!!:&amp;Q#!!!!!!!%!"1!(!!!"!!$;HT#I!!!!!!!!!#:-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;-98.U18"Q&lt;'FF:&amp;2J&lt;76T&gt;'&amp;N=!!!!"E8!)!!!!!!!1!&amp;!!=!!!%!!.K@-+A!!!!!!!!!'ER71WRB=X.1=GFW982F2'&amp;U962Z='6%:8.D!!!!`2=!A!!!!!!"!!A!-0````]!!1!!!!!!Y1!!!!5!2%"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)245&amp;514-Y.$-U,GRW9WRB=X-!!"&amp;.162"-TAU-T1O&lt;(:D&lt;'&amp;T=Q!-1#%'1WBJ=#!R!!!-1#%'1WBJ=#!S!!"B!0%!!!!!!!!!!R2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;5?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=R&gt;$;'FQ)&amp;.U982V=SV$&lt;(6T&gt;'6S,G.U&lt;!!;1&amp;!!!A!"!!)+6%F")&amp;.U982V=Q!!(%"1!!)!!!!$%6*Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!%!"!!!!!!!!!!?4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B2':M&gt;%2B&gt;'&amp;4;8JF!!!!'2=!A!!!!!!"!!5!!Q!!!1!!!!!!"A!!!!!!!!!;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B2':M&gt;%2B&gt;'%!!!%D&amp;Q#!!!!!!!5!2%"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)245&amp;514-Y.$-U,GRW9WRB=X-!!"&amp;.162"-TAU-T1O&lt;(:D&lt;'&amp;T=Q!-1#%'1WBJ=#!R!!!-1#%'1WBJ=#!S!!"B!0%!!!!!!!!!!R2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;5?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=R&gt;$;'FQ)&amp;.U982V=SV$&lt;(6T&gt;'6S,G.U&lt;!!;1&amp;!!!A!"!!)+6%F")&amp;.U982V=Q!!(%"1!!)!!!!$%6*Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!%!"!!!!!%I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"6%%T/$1T.#ZM&gt;G.M98.T!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1!"Q!/!!!!"!!!!']!!!!I!!!!!A!!"!!!!!!,!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!8)!!!/&amp;?*SV5=N/QE!5076YPQ2&amp;5!1&gt;&gt;CS5+,"QW1:CQI*)%0@7UGK4*D2U3FD[*W\^*L^#`M$&lt;5M1IBJBI4T+&gt;_TLXXD-!4J#7;^(/IWHT#[TP45$&amp;!MO0&amp;1:+P]W\_MT5&gt;.\L.+S::&gt;\H2X._\1L&lt;&amp;72LFOIY*&lt;`U2KD#&gt;=Y[FOM)@&gt;L1B)7S0%#)C+45JQRU:2P(1(UT?V]:+;X,&gt;KO^9A=W_0ZDS/3IJ[RGB)H&amp;[`0ZS]].BF]&lt;:.9?D`&lt;5JW5)6Y.G@'*Q0Z(&lt;5X/G#JW06;%C1E!39RLA$6%S7'#CB4A3=AT-M"Y1FZ`9:#C7$HVC)%@]D$*43$.X&lt;+#*8[K+-'KY`4M&gt;M`!7XC&amp;E`&gt;7TS$%R(^/)HJ6"HGY;.1J"1H'DZP6!MM*7S;AW1G,2)9&amp;5OP/XO;*'(%75+/,BA#)L2!.]^[QDBT[7WEMI@YST\18$1,#E6V?A@Q1R*,#,0?TD#"65029[+Y2D&lt;W3SW$OP0](O!!!!!!#-!!%!!A!$!!1!!!")!!]!!!!!!!]!\1$D!!!!8A!0!!!!!!!0!/U!YQ!!!(1!$Q!!!!!!$Q$N!/-!!!#+A!#!!!0I!!]!^Q$J&amp;5VJ9X*P=W^G&gt;#"+;'6O:UBF;3"6326.;7.S&lt;X.P:H1A3GBF&lt;G&gt;):7EA65E647FD=G^T&lt;W:U)%JI:7ZH3'6J)&amp;6*!4"35V*$$1I!!UR71U.-1F:8!!!&lt;J!!!"()!!!!A!!!&lt;B!!!!!!!!!!!!!!!)!!!!$1!!!2E!!!!(%R*1EY!!!!!!!!"9%R75V)!!!!!!!!"&gt;&amp;*55U=!!!!!!!!"C%.$5V1!!!!!!!!"H%R*&gt;GE!!!!!!!!"M%.04F!!!!!!!!!"R&amp;2./$!!!!!"!!!"W%2'2&amp;-!!!!!!!!#!%R*:(-!!!!!!!!#&amp;&amp;:*1U1!!!!#!!!#+(:F=H-!!!!%!!!#:&amp;.$5V)!!!!!!!!#S%&gt;$5&amp;)!!!!!!!!#X%F$4UY!!!!!!!!#]'FD&lt;$1!!!!!!!!$"'FD&lt;$A!!!!!!!!$'%R*:H!!!!!!!!!$,%:13')!!!!!!!!$1%:15U5!!!!!!!!$6&amp;:12&amp;!!!!!!!!!$;%R*9G1!!!!!!!!$@%*%3')!!!!!!!!$E%*%5U5!!!!!!!!$J&amp;:*6&amp;-!!!!!!!!$O%253&amp;!!!!!!!!!$T%V6351!!!!!!!!$Y%B*5V1!!!!!!!!$^&amp;:$6&amp;!!!!!!!!!%#%:515)!!!!!!!!%(!!!!!$`````!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!-!!!!!!!!!!!`````Q!!!!!!!!$5!!!!!!!!!!$`````!!!!!!!!!/A!!!!!!!!!!0````]!!!!!!!!!]!!!!!!!!!!!`````Q!!!!!!!!)9!!!!!!!!!!$`````!!!!!!!!!C!!!!!!!!!!!P````]!!!!!!!!#4!!!!!!!!!!!`````Q!!!!!!!!*E!!!!!!!!!!$`````!!!!!!!!!L1!!!!!!!!!!0````]!!!!!!!!#R!!!!!!!!!!"`````Q!!!!!!!!35!!!!!!!!!!,`````!!!!!!!!"&lt;1!!!!!!!!!"0````]!!!!!!!!'A!!!!!!!!!!(`````Q!!!!!!!!;5!!!!!!!!!!D`````!!!!!!!!"K1!!!!!!!!!#@````]!!!!!!!!'O!!!!!!!!!!+`````Q!!!!!!!!&lt;)!!!!!!!!!!$`````!!!!!!!!"NQ!!!!!!!!!!0````]!!!!!!!!'^!!!!!!!!!!!`````Q!!!!!!!!=)!!!!!!!!!!$`````!!!!!!!!"YQ!!!!!!!!!!0````]!!!!!!!!*E!!!!!!!!!!!`````Q!!!!!!!!W5!!!!!!!!!!$`````!!!!!!!!$VQ!!!!!!!!!!0````]!!!!!!!!5"!!!!!!!!!!!`````Q!!!!!!!"1-!!!!!!!!!!$`````!!!!!!!!&amp;"1!!!!!!!!!!0````]!!!!!!!!5*!!!!!!!!!!!`````Q!!!!!!!"3-!!!!!!!!!!$`````!!!!!!!!&amp;*1!!!!!!!!!!0````]!!!!!!!!:1!!!!!!!!!!!`````Q!!!!!!!"F)!!!!!!!!!!$`````!!!!!!!!'6!!!!!!!!!!!0````]!!!!!!!!:@!!!!!!!!!#!`````Q!!!!!!!"LU!!!!!!V3?#"0&gt;82Q&gt;81O9X2M!!!!!!</Property>
	<Property Name="NI.LVClass.Geneology" Type="Xml"><String>

<Name></Name>

<Val>!!!!!B2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;3?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q"16%AQ!!!!!!!!!!!!!!!"!!%!!!!!!!%!!!!!"1"%1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;.162"-TAU-T1O&lt;(:D&lt;'&amp;T=Q!!%5V"6%%T/$1T.#ZM&gt;G.M98.T!!R!)1:$;'FQ)$%!!!R!)1:$;'FQ)$)!!'%!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T&amp;U.I;8!A5X2B&gt;(6T,5.M&gt;8.U:8)O9X2M!"J!5!!#!!%!!AJ535%A5X2B&gt;(6T!!"J!0(;HT#I!!!!!R2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;3?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=QV3?#"0&gt;82Q&gt;81O9X2M!#R!5!!#!!!!!RV$&lt;(6T&gt;'6S)'^G)'.M98.T)("S;8:B&gt;'5A:'&amp;U91!"!!1!!!!%`````Q!!!!%!!!!#!!!!!Q!!!!%I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"6%%T/$1T.#ZM&gt;G.M98.T!!!!!!!!!!!!!!!!!!!!!!!!!!!#&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!&amp;"53$!!!!!!!!!!!!!8!)!!!!!!!!!!!!!!!Q!!!#&gt;115UU)%.%5C"%:8:J9W5O&lt;(:M;7)[5HAA2'6W;7.F,GRW9WRB=X-!!!!H5%&amp;..#"$2&amp;)A2'6W;7.F,GRW&lt;'FC/F*Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!!,V&amp;42F!N2%1A5%&amp;..#"$2&amp;)A2'6W;7.F,GRW&lt;'FC/F*Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T</Val>

</String>

</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">false</Property>
	<Property Name="NI.LVClass.LowestCompatibleVersion" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LVClass.ParentClassLinkInfo" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"7!!!!!B2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;5?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q"16%AQ!!!!)A!"!!1!!!F5?#"0&gt;82Q&gt;8126(AA4X6U=(6U,GRW9WRB=X-!!!!!</Property>
	<Item Name="Rx Output.ctl" Type="Class Private Data" URL="Rx Output.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="Accessor" Type="Folder">
		<Item Name="MATA38434.lvclass" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">Rx Device:TIA:MATA38434.lvclass</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">MATA38434.lvclass</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
			<Item Name="Read MATA38434.lvclass.vi" Type="VI" URL="../Accessor/MATA38434.lvclass Property/Read MATA38434.lvclass.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'M!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!'*!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"6%%T/$1T.#ZM&gt;G.M98.T!!!P56.'5#V%2#"115UU)%.%5C"%:8:J9W5O&lt;(:M;7)[45&amp;514-Y.$-U,GRW9WRB=X-!1%"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)25HAA4X6U=(6U,GRW9WRB=X-!!!V3?#"0&gt;82Q&gt;81A&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!1%"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)25HAA4X6U=(6U,GRW9WRB=X-!!!R3?#"0&gt;82Q&gt;81A;7Y!!'%!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!A!!?!!!$1A!!!!!!!!*!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
			</Item>
			<Item Name="Write MATA38434.lvclass.vi" Type="VI" URL="../Accessor/MATA38434.lvclass Property/Write MATA38434.lvclass.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'M!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%6*Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.5HAA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!'*!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%5V"6%%T/$1T.#ZM&gt;G.M98.T!!!P56.'5#V%2#"115UU)%.%5C"%:8:J9W5O&lt;(:M;7)[45&amp;514-Y.$-U,GRW9WRB=X-!1%"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)25HAA4X6U=(6U,GRW9WRB=X-!!!R3?#"0&gt;82Q&gt;81A;7Y!!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!A!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!"!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
			</Item>
		</Item>
		<Item Name="TIA Status" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">TIA Status</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">TIA Status</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="Read TIA Status.vi" Type="VI" URL="../Accessor/TIA Status Property/Read TIA Status.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!($!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!R!)1:$;'FQ)$%!!!R!)1:$;'FQ)$)!!'%!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T&amp;U.I;8!A5X2B&gt;(6T,5.M&gt;8.U:8)O9X2M!"J!5!!#!!5!"AN$;'FQ)&amp;.U982V=Q"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;3?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q!!$6*Y)%^V&gt;("V&gt;#"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;3?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q!!$&amp;*Y)%^V&gt;("V&gt;#"J&lt;A!!91$Q!!Q!!Q!%!!=!#!!%!!1!"!!%!!E!"!!%!!I#!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!#Q!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">276832256</Property>
			</Item>
			<Item Name="Write TIA Status.vi" Type="VI" URL="../Accessor/TIA Status Property/Write TIA Status.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!($!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%6*Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.5HAA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!R!)1:$;'FQ)$%!!!R!)1:$;'FQ)$)!!'%!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%62Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T&amp;U.I;8!A5X2B&gt;(6T,5.M&gt;8.U:8)O9X2M!"J!5!!#!!=!#!N$;'FQ)&amp;.U982V=Q"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;3?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q!!$&amp;*Y)%^V&gt;("V&gt;#"J&lt;A!!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!*!!I#!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#Q!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">276832256</Property>
			</Item>
		</Item>
	</Item>
	<Item Name="Control" Type="Folder">
		<Item Name="Output Swing-Enum.ctl" Type="VI" URL="../Control/Output Swing-Enum.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!-!!!!!1!%!!!!!1!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">4194304</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074541056</Property>
		</Item>
		<Item Name="Tune Ref-Enum.ctl" Type="VI" URL="../Control/Tune Ref-Enum.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$;!!!!!1$3!0%!!!!!!!!!!R2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;3?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=R60&gt;82Q&gt;81A5X&gt;J&lt;G=N27ZV&lt;3ZD&gt;'Q!D5!7!"!)-CYQ.TB78T!'-CYR-4&amp;7"D)O-41T6A9S,D%X.F9'-CYS-$B7"D)O-D1R6A9S,D)X-V9'-CYT-$:7"T%O-4AU.V9'-3YY.TF7"D%O/4%S6A9R,DEU.69'-3YZ.TB7"D)O-$%R6A5S,D!U.!AS,D!X/&amp;:@2A!-4X6U=(6U)&amp;.X;7ZH!!!"!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">1048576</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074278912</Property>
		</Item>
	</Item>
	<Item Name="Override" Type="Folder">
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Item Name="CDR" Type="Folder">
			<Item Name="Get IC Channel.vi" Type="VI" URL="../Override/Get IC Channel.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;=!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"&amp;!"1!+35-A1WBB&lt;GZF&lt;!!!1%"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)25HAA4X6U=(6U,GRW9WRB=X-!!!V3?#"0&gt;82Q&gt;81A&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!$E!B#5F$)&amp;.F&lt;'6D&gt;!"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;3?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q!!$&amp;*Y)%^V&gt;("V&gt;#"J&lt;A!!6!$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!)!!E#!!"Y!!!.#!!!!!!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!#!!!!*!!!!!!!1!+!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1082139136</Property>
			</Item>
			<Item Name="Set Default Value.vi" Type="VI" URL="../Override/Set Default Value.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%^!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%6*Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.5HAA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%6*Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-5HAA4X6U=(6U)'FO!!"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!E!!!!!!"!!A!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1082139152</Property>
			</Item>
		</Item>
		<Item Name="TIA" Type="Folder">
			<Item Name="Set Rx Low Power Mode.vi" Type="VI" URL="../Override/Set Rx Low Power Mode.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;2!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%6*Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.5HAA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"2!)1Z&amp;&lt;G&amp;C&lt;'5P2'FT97*M:1!!1%"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)25HAA4X6U=(6U,GRW9WRB=X-!!!R3?#"0&gt;82Q&gt;81A;7Y!!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!Q!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!A!!!#3!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
			</Item>
			<Item Name="Get Bias Mode.vi" Type="VI" URL="../Override/Get Bias Mode.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;,!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!Z!)1B":'&amp;Q&gt;'FW:1!!1%"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)25HAA4X6U=(6U,GRW9WRB=X-!!!V3?#"0&gt;82Q&gt;81A&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!1%"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)25HAA4X6U=(6U,GRW9WRB=X-!!!R3?#"0&gt;82Q&gt;81A;7Y!!&amp;1!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!Q!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!#3!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
			</Item>
			<Item Name="Get DCD Mode.vi" Type="VI" URL="../Override/Get DCD Mode.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;,!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!Z!)1B":'&amp;Q&gt;'FW:1!!1%"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)25HAA4X6U=(6U,GRW9WRB=X-!!!V3?#"0&gt;82Q&gt;81A&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!1%"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)25HAA4X6U=(6U,GRW9WRB=X-!!!R3?#"0&gt;82Q&gt;81A;7Y!!&amp;1!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!Q!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!#3!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
			</Item>
			<Item Name="Get Adaptive AGC Value.vi" Type="VI" URL="../Override/Get Adaptive AGC Value.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;'!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!F!"A!$15&gt;$!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%6*Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.5HAA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%6*Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-5HAA4X6U=(6U)'FO!!"5!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!-!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!EA!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
			</Item>
			<Item Name="Get Adaptive Bias Value.vi" Type="VI" URL="../Override/Get Adaptive Bias Value.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;)!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!N!"A!%1GFB=Q!!1%"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)25HAA4X6U=(6U,GRW9WRB=X-!!!V3?#"0&gt;82Q&gt;81A&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!1%"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)25HAA4X6U=(6U,GRW9WRB=X-!!!R3?#"0&gt;82Q&gt;81A;7Y!!&amp;1!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!Q!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!#3!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
			</Item>
			<Item Name="Get Adaptive DCD Value.vi" Type="VI" URL="../Override/Get Adaptive DCD Value.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;'!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!F!"A!$2%.%!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%6*Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.5HAA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%6*Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-5HAA4X6U=(6U)'FO!!"5!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!-!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!EA!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
			</Item>
			<Item Name="Get Pre-Emphasis.vi" Type="VI" URL="../Override/Get Pre-Emphasis.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;1!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!".!#A!-5(*F,56N='BB=WFT!!"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;3?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q!!$6*Y)%^V&gt;("V&gt;#"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;3?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q!!$&amp;*Y)%^V&gt;("V&gt;#"J&lt;A!!6!$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A$!!"Y!!!.#!!!!!!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!*)!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
			</Item>
			<Item Name="Get Output Swing.vi" Type="VI" URL="../Override/Get Output Swing.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(F!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!+A!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%6*Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T&amp;5^V&gt;("V&gt;#"4&gt;WFO:SV&amp;&lt;H6N,G.U&lt;!"D1"9!%!-V.#5$.D%F!T9X*1-X.#5$/$!F!TAX*1-Z-S5%-4!Q*11R-$=F"$%R-S5%-4)Q*11R-D9F"$%T-S5%-4-Z*11R.$9F"$%V-35!!!R0&gt;82Q&gt;81A5X&gt;J&lt;G=!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%6*Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.5HAA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%6*Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-5HAA4X6U=(6U)'FO!!"5!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!-!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!EA!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710290</Property>
			</Item>
			<Item Name="Get Tune Ref.vi" Type="VI" URL="../Override/Get Tune Ref.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)(!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!-I!]1!!!!!!!!!$&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%6*Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T%62V&lt;G5A5G6G,56O&gt;7UO9X2M!)F!&amp;A!1#$)O-$=Y6F]Q"D)O-4%R6A9S,D%U-V9'-CYR.T:7"D)O-D!Y6A9S,D)U-69'-CYS.T.7"D)O-T!W6A=R,D%Y.$&gt;7"D%O/$=Z6A9R,DER-F9'-3YZ.$67"D%O/4=Y6A9S,D!R-69&amp;-CYQ.$1)-CYQ.TB78U9!#&amp;2V&lt;G5A5G6G!!"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;3?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q!!$6*Y)%^V&gt;("V&gt;#"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;3?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q!!$&amp;*Y)%^V&gt;("V&gt;#"J&lt;A!!6!$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A$!!"Y!!!.#!!!!!!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!*)!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
			</Item>
			<Item Name="Get Sel Ref Low.vi" Type="VI" URL="../Override/Get Sel Ref Low.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;.!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!""!)1N4:7QA5G6G)%RP&gt;Q"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;3?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q!!$6*Y)%^V&gt;("V&gt;#"P&gt;81!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;3?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q!!$&amp;*Y)%^V&gt;("V&gt;#"J&lt;A!!6!$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A$!!"Y!!!.#!!!!!!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!*)!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
			</Item>
			<Item Name="Set Bias Mode.vi" Type="VI" URL="../Override/Set Bias Mode.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;,!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%6*Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.5HAA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!Z!)1B":'&amp;Q&gt;'FW:1!!1%"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)25HAA4X6U=(6U,GRW9WRB=X-!!!R3?#"0&gt;82Q&gt;81A;7Y!!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!Q!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!A!!!#3!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
			</Item>
			<Item Name="Set DCD Mode.vi" Type="VI" URL="../Override/Set DCD Mode.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;,!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%6*Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.5HAA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!Z!)1B":'&amp;Q&gt;'FW:1!!1%"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)25HAA4X6U=(6U,GRW9WRB=X-!!!R3?#"0&gt;82Q&gt;81A;7Y!!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!Q!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!A!!!#3!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
			</Item>
			<Item Name="Set Manual Bias Value.vi" Type="VI" URL="../Override/Set Manual Bias Value.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;)!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%6*Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.5HAA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!N!"A!%1GFB=Q!!1%"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)25HAA4X6U=(6U,GRW9WRB=X-!!!R3?#"0&gt;82Q&gt;81A;7Y!!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!Q!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!A!!!#3!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
			</Item>
			<Item Name="Set Manual DCD Value.vi" Type="VI" URL="../Override/Set Manual DCD Value.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;'!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%6*Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.5HAA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!F!"A!$2%.%!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%6*Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-5HAA4X6U=(6U)'FO!!"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!-!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!)!!!!EA!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
			</Item>
			<Item Name="Set Pre-Emphasis.vi" Type="VI" URL="../Override/Set Pre-Emphasis.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;1!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%6*Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.5HAA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!".!#A!-5(*F,56N='BB=WFT!!"!1(!!(A!!+"2115UU)%2F&gt;GFD:3"*1SZM&gt;GRJ9B&amp;3?#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q!!$&amp;*Y)%^V&gt;("V&gt;#"J&lt;A!!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A$!!"Y!!!.#!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!#A!!!*)!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
			</Item>
		</Item>
		<Item Name="Page" Type="Folder">
			<Item Name="Set TIA Page.vi" Type="VI" URL="../Override/Set TIA Page.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%^!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%6*Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.5HAA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%6*Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-5HAA4X6U=(6U)'FO!!"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!E!!!!!!"!!A!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1082139152</Property>
			</Item>
			<Item Name="Set CDR Input Page.vi" Type="VI" URL="../Override/Set CDR Input Page.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%^!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%6*Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.5HAA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%6*Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-5HAA4X6U=(6U)'FO!!"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!E!!!!!!"!!A!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1082139152</Property>
			</Item>
			<Item Name="Get Device Pages.vi" Type="VI" URL="../Override/Get Device Pages.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;E!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!V!"1!(4H6N:8*J9Q!;1%!!!@````]!"1R%:8:J9W5A5'&amp;H:8-!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%6*Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.5HAA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%6*Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-5HAA4X6U=(6U)'FO!!"5!0!!$!!$!!1!"A!(!!1!"!!%!!1!#!!%!!1!#1)!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!E!!!!!!"!!I!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1082139136</Property>
			</Item>
			<Item Name="Set CDR Output Page.vi" Type="VI" URL="../Override/Set CDR Output Page.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%^!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%6*Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.5HAA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%6*Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-5HAA4X6U=(6U)'FO!!"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!E!!!!!!"!!A!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1082139152</Property>
			</Item>
		</Item>
		<Item Name="Tune" Type="Folder">
			<Item Name="Get Tune IC Channel.vi" Type="VI" URL="../Override/Get Tune IC Channel.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;/!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"&amp;!"1!+35-A1WBB&lt;GZF&lt;!!!1%"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)25HAA4X6U=(6U,GRW9WRB=X-!!!V3?#"0&gt;82Q&gt;81A&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!1%"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)25HAA4X6U=(6U,GRW9WRB=X-!!!R3?#"0&gt;82Q&gt;81A;7Y!!&amp;1!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!Q!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!#3!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
			</Item>
			<Item Name="Get Tune Polarity.vi" Type="VI" URL="../Override/Get Tune Polarity.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%^!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%6*Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.5HAA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%6*Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-5HAA4X6U=(6U)'FO!!"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!EA!!!!!"!!A!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
			</Item>
			<Item Name="Get CDR Pages.vi" Type="VI" URL="../Override/Get CDR Pages.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;A!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!$5!&amp;!!&gt;/&gt;7VF=GFD!":!1!!"`````Q!%#5.%5C"197&gt;F=Q!%!!!!1%"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)25HAA4X6U=(6U,GRW9WRB=X-!!!V3?#"0&gt;82Q&gt;81A&lt;X6U!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!1%"Q!"Y!!#A55%&amp;..#"%:8:J9W5A35-O&lt;(:M;7)25HAA4X6U=(6U,GRW9WRB=X-!!!R3?#"0&gt;82Q&gt;81A;7Y!!&amp;1!]!!-!!-!"1!'!!=!"A!'!!9!"A!)!!9!"A!*!A!!?!!!$1A!!!E!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!#1!!!!!!%!#A!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1082139136</Property>
			</Item>
		</Item>
		<Item Name="Get Chip Status.vi" Type="VI" URL="../Override/Get Chip Status.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%^!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%6*Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.5HAA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%6*Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-5HAA4X6U=(6U)'FO!!"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!EA!!!!!"!!A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1350574608</Property>
		</Item>
	</Item>
	<Item Name="Public" Type="Folder">
		<Item Name="Prepare Rx Device Class.vi" Type="VI" URL="../Public/Prepare Rx Device Class.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;0!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%6*Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.5HAA2'6W;7.F)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!&amp;*!=!!?!!!Q&amp;E^Q&gt;'FD97QA5(*P:(6D&gt;#ZM&gt;GRJ9H!84X"U;7.B&lt;#"1=G^E&gt;7.U,GRW9WRB=X-!!"&gt;0=(2J9W&amp;M)&amp;"S&lt;W2V9X1O&lt;(:D&lt;'&amp;T=Q"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!U)!!!!!!!!!!!!!!E!!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!#A!!!!!"!!A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082139152</Property>
		</Item>
		<Item Name="Set Rx Low Power  By CH.vi" Type="VI" URL="../Public/Set Rx Low Power  By CH.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;L!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%6*Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!.5HAA4X6U=(6U)'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"2!)1Z&amp;&lt;G&amp;C&lt;'5P2'FT97*M:1!!$5!$!!&gt;$;'&amp;O&lt;G6M!%"!=!!?!!!I&amp;&amp;""441A2'6W;7.F)%F$,GRW&lt;'FC%6*Y)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!-5HAA4X6U=(6U)'FO!!"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!(!!A!#1-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!A!!!!)!!!!#A!!$1!!!!Q!!!!!!!!!!!!!!1!+!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
		</Item>
	</Item>
</LVClass>
